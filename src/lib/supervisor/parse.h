// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PARSE_20180621_H
#define PARSE_20180621_H

#include <array>

#include <boost/algorithm/string.hpp>
#include <boost/units/systems/si/prefixes.hpp>

#include <goby/common/time3.h>

#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/supervisor/exception.h"
#include "cgsn-mooring/sensor/units.h"


namespace cgsn
{
    namespace supervisor
    {
        /// \brief calculates the MPIC checksum
        template<typename CharIterator>
            std::uint16_t calc_checksum(CharIterator begin, CharIterator end)
        {
            std::uint16_t cs = 0;
            for(auto it = begin; it != end; ++it)
                cs += *it;
            return cs;
        }

        template<typename CharIterator>
            std::string calc_checksum_str(CharIterator begin, CharIterator end)
        {
            auto cs = cgsn::supervisor::calc_checksum(begin, end);
            std::stringstream cs_ss;
            cs_ss << std::hex << cs;
            return cs_ss.str();
        }

        /// \brief Checks if the MPIC checksum is valid
        bool checksum_valid(const std::string& received)
        {

            auto last_space_pos = received.find_last_of(" ");
            if(last_space_pos == std::string::npos)
                return false;

            std::uint16_t expected = std::stoul(received.c_str()+last_space_pos, nullptr, 16);

            return calc_checksum(received.begin(),
                                 received.begin()+last_space_pos) == expected;
        }

        /// \brief Parses the "dcl: " string from the MPIC into the Protobuf equivalent (cgsn::protobuf::MPICStatus)
        inline cgsn::protobuf::MPICStatus parse_mpic_dcl_status(std::string status)
        {
            boost::trim(status);
            cgsn::protobuf::MPICStatus pb_status;

            // index into space-delimited status string
            enum { DCL = 0,
                   MAIN_VOLTAGE = 1,
                   MAIN_CURRENT = MAIN_VOLTAGE + 1,
                   ERRORS = MAIN_CURRENT + 1,
                   TEMPERATURE  = ERRORS + 1,
                   HUMIDITY  = TEMPERATURE + 6,
                   PRESSURE = HUMIDITY + 2,
                   GROUND_FAULT = PRESSURE + 2,
                   LEAK_DETECT = GROUND_FAULT + 5,
                   PORT1 = LEAK_DETECT + 4,
                   PORT2 = PORT1 + 5,
                   PORT3 = PORT2 + 5,
                   PORT4 = PORT3 + 5,
                   PORT5 = PORT4 + 5,
                   PORT6 = PORT5 + 5,
                   PORT7 = PORT6 + 5,
                   PORT8 = PORT7 + 5,
                   HB = PORT8 + 5,
                   WAKE = HB + 4,
                   WTC = WAKE + 2,
                   WPC = WTC + 2,
                   PWR = WPC + 2,
                   CHECKSUM = PWR + 10,
                   TOTAL_FIELDS = CHECKSUM + 1
            };

            std::vector<std::string> status_parts;
            boost::split(status_parts, status, boost::is_space());

            if(status_parts.size() != TOTAL_FIELDS)
                throw(Exception("\"dcl:\" message invalid. Expected " + std::to_string(TOTAL_FIELDS) + " fields, received: " + std::to_string(status_parts.size())));

            auto check_marker = [](std::string expected, std::string received)
                { if(expected != received) {
                        throw(Exception("Marker \"" + expected + "\" was expected, got \"" + received + "\" instead")); } };


            check_marker("dcl:", status_parts[DCL]);
            namespace si = boost::units::si;
            pb_status.set_main_voltage_with_units(std::stof(status_parts[MAIN_VOLTAGE])*si::volts);
            pb_status.set_main_current_with_units(std::stof(status_parts[MAIN_CURRENT])*si::milli*si::amperes);

            std::uint32_t error_mask = std::stoul(status_parts[ERRORS], nullptr, 16);

            for(int bit = 0; bit < 32; ++bit)
            {
                // bit 0 error
                auto mpic_dcl_error_offset = protobuf::Error::MPIC__DCL_ERR_VMAIN_RANGE;
                if(error_mask & (1 << bit))
                {
                    if (protobuf::Error::ErrorCode_IsValid(bit + mpic_dcl_error_offset))
                        pb_status.add_error(
                            static_cast<protobuf::Error::ErrorCode>(bit + mpic_dcl_error_offset));
                    else
                        pb_status.add_error(protobuf::Error::MPIC__DCL_UNKNOWN_ERROR);
                }
            }
            check_marker("t", status_parts[TEMPERATURE]);
            auto& pb_temp = *pb_status.mutable_temperature();

            auto degrees_C = boost::units::absolute<boost::units::celsius::temperature>();
            pb_temp.set_bmp085_with_units(std::stof(status_parts[TEMPERATURE+1])*degrees_C);
            pb_temp.set_sht_with_units(std::stof(status_parts[TEMPERATURE+2])*degrees_C);
            pb_temp.set_lp12v_with_units(std::stof(status_parts[TEMPERATURE+3])*degrees_C);
            pb_temp.set_lp24v_with_units(std::stof(status_parts[TEMPERATURE+4])*degrees_C);
            pb_temp.set_hp12v_with_units(std::stof(status_parts[TEMPERATURE+5])*degrees_C);

            check_marker("h", status_parts[HUMIDITY]);
            pb_status.set_sht_humidity(std::stof(status_parts[HUMIDITY+1]));

            check_marker("p", status_parts[PRESSURE]);
            auto psi = units::psi_base_unit::unit_type();
            pb_status.set_pmb085_pressure_with_units(std::stof(status_parts[PRESSURE + 1])*psi);

            check_marker("gf", status_parts[GROUND_FAULT]);
            pb_status.set_ground_leak_data_enabled(std::stoi(status_parts[GROUND_FAULT + 1]));
            pb_status.add_ground_leak_data_max_with_units(std::stof(status_parts[GROUND_FAULT + 2])*si::micro*si::amperes);
            pb_status.add_ground_leak_data_max_with_units(std::stof(status_parts[GROUND_FAULT + 3])*si::micro*si::amperes);
            pb_status.add_ground_leak_data_max_with_units(std::stof(status_parts[GROUND_FAULT + 4])*si::micro*si::amperes);

            check_marker("ld", status_parts[LEAK_DETECT]);
            for(int i = 0, n = 2; i < n; ++i)
            {
                auto& ld = *pb_status.add_leak_detect();
                ld.set_enabled(std::stoi(status_parts[LEAK_DETECT+1]) & (1 << i));
                ld.set_voltage(std::stof(status_parts[LEAK_DETECT+i+2]));
            }

            check_marker("p1", status_parts[PORT1]);
            check_marker("p2", status_parts[PORT2]);
            check_marker("p3", status_parts[PORT3]);
            check_marker("p4", status_parts[PORT4]);
            check_marker("p5", status_parts[PORT5]);
            check_marker("p6", status_parts[PORT6]);
            check_marker("p7", status_parts[PORT7]);
            check_marker("p8", status_parts[PORT8]);
            const int num_ports = 8;
            const auto delta_port = (PORT2-PORT1);
            for(int i = 0; i < num_ports; ++i)
            {
                auto& cpic = *pb_status.add_cpic();
                cpic.set_port(i+1);
                cpic.set_state(std::stoi(status_parts[PORT1 + 1 + i*delta_port]) ? cgsn::protobuf::Port::ON : cgsn::protobuf::Port::OFF);
                cpic.set_voltage_with_units(std::stof(status_parts[PORT1 + 2 + i*delta_port])*si::volts);
                cpic.set_current_with_units(std::stof(status_parts[PORT1 + 3 + i*delta_port])*si::milli*si::amperes);
                cpic.set_ierr(std::stoi(status_parts[PORT1 + 4 + i*delta_port]));
            }

            check_marker("hb", status_parts[HB]);
            check_marker("wake", status_parts[WAKE]);
            check_marker("wtc", status_parts[WTC]);
            check_marker("wpc", status_parts[WPC]);
            check_marker("pwr", status_parts[PWR]);
            auto& dpb = *pb_status.mutable_dpb();
            dpb.set_pwr_sel(std::stoi(status_parts[PWR+1]));
            dpb.set_mode(std::stoi(status_parts[PWR+2]));
            dpb.set_state(std::stoi(status_parts[PWR+3]));
            dpb.set_vmain_with_units(std::stof(status_parts[PWR+4])*si::volts);
            dpb.set_imain_with_units(std::stof(status_parts[PWR+5])*si::milli*si::amperes);
            dpb.set_v12_with_units(std::stof(status_parts[PWR+6])*si::volts);
            dpb.set_i12_with_units(std::stof(status_parts[PWR+7])*si::milli*si::amperes);
            dpb.set_v24_with_units(std::stof(status_parts[PWR+8])*si::volts);
            dpb.set_i24_with_units(std::stof(status_parts[PWR+9])*si::milli*si::amperes);

            return pb_status;
        }



        // TO MPIC
        // pon 1 FF
        // poff 1 FF
        // FROM MPIC
        // pon: 1 ok FF
        // pon: 0 error (invalid port) FF
        // pon: 1 error (no data / invalid response) FF
        // poff: 1 ok FF
        // poff: 0 error (invalid port) FF
        // poff: 1 error (no response) FF
        /// \brief Parses the "pon: " and "poff: " messages to/from the MPIC into the Protobuf equivalent (cgsn::protobuf::PortOnOff)
        inline cgsn::protobuf::PortOnOff parse_mpic_pon_poff(std::string msg, cgsn::protobuf::Port::Direction direction)
        {
            cgsn::protobuf::PortOnOff pb_msg;

            std::vector<std::string> parts;
            boost::split(parts, msg, boost::is_space());

            enum { PON_POFF = 0,
                   PORT = 1,
                   STATUS = 2 // if response, "ok" or "error"
            };
            
            const int min_size_to_mpic = 3,
                min_size_from_mpic = 4;

            auto min_size = (direction == cgsn::protobuf::Port::TO_MPIC) ? min_size_to_mpic : min_size_from_mpic;

            pb_msg.set_state((parts[PON_POFF] == "pon:" || parts[PON_POFF] == "pon") ? cgsn::protobuf::Port::ON : cgsn::protobuf::Port::OFF);

            if(parts.size() >= min_size)
            {
                pb_msg.set_port_id(std::stoi(parts[PORT]));
                if(direction == cgsn::protobuf::Port::FROM_MPIC && parts[STATUS] != "ok")
                {
                    if(msg.find("error (invalid port)") != std::string::npos)
                        pb_msg.set_error(cgsn::protobuf::PortOnOff::INVALID_PORT);
                    else if(msg.find("error (no response)") != std::string::npos)
                        pb_msg.set_error(cgsn::protobuf::PortOnOff::NO_RESPONSE);
                    else
                        pb_msg.set_error(cgsn::protobuf::PortOnOff::UNKNOWN_ERROR);
                }

            }
            else
            {
                pb_msg.set_error(cgsn::protobuf::PortOnOff::INCORRECTLY_FORMATTED);
            }
            return pb_msg;
        }


        // FROM MPIC
        // "get_pcfg: 3 0 0 0 0 0 55c\r\n"
        // "get_pcfg: 3 error no data / invalid response"
        // "get_pcfg: 0 error invalid port"
        // TO MPIC
        // "set_pcfg 3 0 0 0 0 0 ffff\r\n"
        // "get_pcfg 4 ffff\r\n"
        //
        /// \brief Parses the "get_pcfg: " response from the MPIC and "set_pcfg: " command to the MPIC into the Protobuf equivalent (cgsn::protobuf::PowerConfig)
        inline cgsn::protobuf::PowerConfig parse_mpic_pcfg(std::string pcfg,
                                                           cgsn::protobuf::Port::Direction direction,
                                                           cgsn::protobuf::Port::GetOrSet getorset)
        {
            boost::trim(pcfg);
            cgsn::protobuf::PowerConfig pb_pcfg;

            if(direction == cgsn::protobuf::Port::FROM_MPIC && getorset == cgsn::protobuf::Port::SET)
                throw(Exception("MPIC does not return set_pcfg message"));

            std::vector<std::string> pcfg_parts;
            boost::split(pcfg_parts, pcfg, boost::is_space());

            // index into space-delimited status string
            enum { GET_SET_PCFG = 0,
                   PORT_ID = 1,
                   VOLTAGE_SELECT = 2,
                   STATUS = 2,
                   CURRENT_LIMIT = 3,
                   PROTOCOL = 4,
                   SLOW_START = 5,
                   POWER_SELECT = 6,
                   CHECKSUM = 7,
                   TOTAL_FIELDS = CHECKSUM + 1
            };

            if(pcfg_parts.size() > PORT_ID)
                pb_pcfg.set_port_id(std::stoi(pcfg_parts[PORT_ID]));

            if(direction == cgsn::protobuf::Port::TO_MPIC && getorset == cgsn::protobuf::Port::GET)
                return pb_pcfg;


            if(pcfg_parts.size() > STATUS && pcfg_parts[STATUS] == "error")
            {
                if(pcfg.find("error no data / invalid response") != std::string::npos)
                    pb_pcfg.set_error(protobuf::PowerConfig::CPIC_NO_DATA_OR_INVALID_RESPONSE);
                else if(pcfg.find("error invalid port") != std::string::npos)
                    pb_pcfg.set_error(protobuf::PowerConfig::INVALID_PORT);
                else
                    pb_pcfg.set_error(protobuf::PowerConfig::UNKNOWN_ERROR);

                return pb_pcfg;
            }

            if(pcfg_parts.size() != TOTAL_FIELDS)
                throw(Exception("\"get_pcfg:\" message invalid. Expected " + std::to_string(TOTAL_FIELDS) + " fields, received: " + std::to_string(pcfg_parts.size())));

            pb_pcfg.set_port_id(std::stoi(pcfg_parts[PORT_ID]));

            auto voltage_select_value = std::stoi(pcfg_parts[VOLTAGE_SELECT]);
            if(protobuf::PowerConfig::VoltageSelection_IsValid(voltage_select_value))
                pb_pcfg.set_voltage(static_cast<protobuf::PowerConfig::VoltageSelection>(voltage_select_value));
            else
                pb_pcfg.set_voltage(protobuf::PowerConfig::VOLTAGE_UNKNOWN);

            namespace si = boost::units::si;
            pb_pcfg.set_current_limit_with_units(std::stoi(pcfg_parts[CURRENT_LIMIT])*si::milli*si::amperes);

            auto protocol_value = std::stoi(pcfg_parts[PROTOCOL]);
            if(protobuf::PowerConfig::Protocol_IsValid(protocol_value))
                pb_pcfg.set_protocol(static_cast<protobuf::PowerConfig::Protocol>(protocol_value));
            else
                pb_pcfg.set_protocol(protobuf::PowerConfig::PROTOCOL_UNKNOWN);

            pb_pcfg.set_soft_start(std::stoi(pcfg_parts[SLOW_START]));

            auto power_select_value = std::stoi(pcfg_parts[POWER_SELECT]);
            if(protobuf::PowerConfig::PowerSelection_IsValid(power_select_value))
                pb_pcfg.set_power(static_cast<protobuf::PowerConfig::PowerSelection>(power_select_value));
            else
                pb_pcfg.set_power(protobuf::PowerConfig::POWER_SELECTION_UNKNOWN);

            return pb_pcfg;
        }
    }
}
#endif
