// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SERIALIZE_20181110_H
#define SERIALIZE_20181110_H

#include "parse.h"
#include "cgsn-mooring/supervisor/exception.h"

namespace cgsn
{
    namespace supervisor
    {
        /// \brief Creates the MPIC "dcl: " string from the Protobuf equivalent (cgsn::protobuf::MPICStatus)
        inline std::string serialize_mpic_dcl_status(const cgsn::protobuf::MPICStatus& pb_status)
        {
            if(!pb_status.IsInitialized())
                throw(Exception("MPICStatus message cannot be serialized: missing fields"));

            std::stringstream dcl;
            dcl << "dcl: ";

            namespace si = boost::units::si;
            dcl << std::fixed << std::setprecision(1)
                << pb_status.main_voltage_with_units()/si::volts
                << " ";
            dcl << std::fixed << std::setprecision(1)
                << pb_status.main_current_with_units()/(si::milli*si::amperes) << " ";

            std::uint32_t error_mask = 0;
            auto mpic_dcl_error_offset = protobuf::Error::MPIC__DCL_ERR_VMAIN_RANGE;
            for(auto error : pb_status.error())
                error_mask |= 1 << static_cast<int>(error - mpic_dcl_error_offset);

            const int BYTES_IN_UINT32 = 4, HEX_CHARS_IN_BYTE = 2;
            dcl << std::hex << std::setw(BYTES_IN_UINT32*HEX_CHARS_IN_BYTE) << std::setfill('0') << error_mask;

            dcl << " t ";

            const auto& pb_temp = pb_status.temperature();
            using quantity_degreesC = boost::units::quantity<boost::units::absolute<boost::units::celsius::temperature>>;
            dcl << std::fixed << std::setprecision(1)
                << quantity_degreesC(pb_temp.bmp085_with_units()).value()
                << " ";
            dcl << std::fixed << std::setprecision(1)
                << quantity_degreesC(pb_temp.sht_with_units()).value()
                << " ";
            dcl << std::fixed << std::setprecision(1)
                << quantity_degreesC(pb_temp.lp12v_with_units()).value()
                << " ";
            dcl << std::fixed << std::setprecision(1)
                << quantity_degreesC(pb_temp.lp24v_with_units()).value()
                << " ";
            dcl << std::fixed << std::setprecision(1)
                << quantity_degreesC(pb_temp.hp12v_with_units()).value()
                << " ";

            dcl << "h ";
            dcl << std::fixed << std::setprecision(1)
                << pb_status.sht_humidity() << " ";

            dcl << "p ";
            using quantity_psi = boost::units::quantity<units::psi_base_unit::unit_type>;
            dcl << std::fixed << std::setprecision(1) <<
                quantity_psi(pb_status.pmb085_pressure_with_units()).value() << " ";

            dcl << "gf ";
            dcl << pb_status.ground_leak_data_enabled() << " ";

            for(int i = 0, n = pb_status.ground_leak_data_max_size(); i < n; ++i)
                dcl << pb_status.ground_leak_data_max_with_units(i)/(si::micro*si::amperes) << " ";

            dcl << "ld ";
            unsigned ld_enabled = 0;
            for(int i = 0, n = pb_status.leak_detect_size(); i < n; ++i)
                ld_enabled |= (static_cast<int>(pb_status.leak_detect(i).enabled()) << i);
            dcl << ld_enabled << " ";

            for(const auto& leak_detect : pb_status.leak_detect())
                dcl << std::fixed << std::setprecision(0) << leak_detect.voltage() << " ";

            for(const auto& cpic : pb_status.cpic())
            {
                dcl << "p" << cpic.port() << " ";
                dcl << cpic.state() << " ";
                dcl << std::fixed << std::setprecision(1)
                    << cpic.voltage_with_units()/si::volts << " ";
                dcl << std::fixed << std::setprecision(1)
                    << cpic.current_with_units()/(si::milli*si::amperes) << " ";
                dcl << cpic.ierr() << " ";
            }

            dcl << "hb 0 0 0 ";
            dcl << "wake 0 ";
            dcl << "wtc 0 ";
            dcl << "wpc 0 ";
            dcl << "pwr ";
            const auto& dpb = pb_status.dpb();
            dcl << dpb.pwr_sel() << " ";
            dcl << dpb.mode() << " ";
            dcl << dpb.state() << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.vmain_with_units()/si::volts << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.imain_with_units()/(si::milli*si::amperes) << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.v12_with_units()/(si::volts) << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.i12_with_units()/(si::milli*si::amperes) << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.v24_with_units()/(si::volts) << " ";
            dcl << std::fixed << std::setprecision(1)
                << dpb.i24_with_units()/(si::milli*si::amperes);

            std::string dcl_str = dcl.str();
            dcl << " " << calc_checksum_str(dcl_str.begin(), dcl_str.end());
            return dcl.str();
        }

        inline std::string serialize_mpic_pon_poff(cgsn::protobuf::PortOnOff pb_msg,
                                                   cgsn::protobuf::Port::Direction direction)
        {
            std::stringstream pon_poff;

            switch(pb_msg.state())
            {
                case cgsn::protobuf::Port::OFF:
                    pon_poff << "poff";
                    break;
                case cgsn::protobuf::Port::ON:
                    pon_poff << "pon";
                    break;
            }

            switch(direction)
            {
                case cgsn::protobuf::Port::TO_MPIC:
                    pon_poff << " ";
                    break;
                case cgsn::protobuf::Port::FROM_MPIC:
                    pon_poff << ": ";
                    break;
            }

            pon_poff << pb_msg.port_id();

            if(direction == cgsn::protobuf::Port::FROM_MPIC)
            {
                if(!pb_msg.has_error())
                {
                    pon_poff << " ok";
                }
                else
                {
                    switch(pb_msg.error())
                    {
                        default:
                            pon_poff << " error";
                            break;
                        case cgsn::protobuf::PortOnOff::INVALID_PORT:
                            pon_poff << " error (invalid port)";
                            break;
                        case cgsn::protobuf::PortOnOff::NO_RESPONSE:
                            pon_poff << " error (no response)";
                            break;
                    }
                }
            }

            std::string pon_poff_str = pon_poff.str();
            pon_poff << " " << calc_checksum_str(pon_poff_str.begin(), pon_poff_str.end());
            return pon_poff.str();
        }


        inline std::string serialize_mpic_pcfg(cgsn::protobuf::PowerConfig pb_msg,
                                               cgsn::protobuf::Port::Direction direction,
                                               cgsn::protobuf::Port::GetOrSet getorset)
        {
            std::stringstream pcfg;

            switch(getorset)
            {
                case cgsn::protobuf::Port::SET:
                    pcfg << "set_pcfg";
                    break;
                case cgsn::protobuf::Port::GET:
                    pcfg << "get_pcfg";
                    break;
            }

            switch(direction)
            {
                case cgsn::protobuf::Port::TO_MPIC:
                    pcfg << " ";
                    break;
                case cgsn::protobuf::Port::FROM_MPIC:
                    pcfg << ": ";
                    break;
            }

            pcfg << pb_msg.port_id();

            if(!(direction == cgsn::protobuf::Port::TO_MPIC && getorset == cgsn::protobuf::Port::GET))
            {
                if(pb_msg.has_error())
                {
                    switch(pb_msg.error())
                    {
                        case cgsn::protobuf::PowerConfig::CPIC_NO_DATA_OR_INVALID_RESPONSE:
                            pcfg << " error no data / invalid response";
                            break;
                        case cgsn::protobuf::PowerConfig::INVALID_PORT:
                            pcfg << " error invalid port";
                            break;
                        default:
                            pcfg << " error";
                            break;
                    }
                }
                else
                {
                    namespace si = boost::units::si;

                    pcfg << " " << pb_msg.voltage() << " "
                         << pb_msg.current_limit_with_units()/(si::milli*si::amperes) << " "
                         << pb_msg.protocol() << " "
                         << pb_msg.soft_start() << " "
                         << pb_msg.power();
                }
            }
            std::string pcfg_str = pcfg.str();
            pcfg << " " << calc_checksum_str(pcfg_str.begin(), pcfg_str.end());
            return pcfg.str();
        }
    }
}



#endif
