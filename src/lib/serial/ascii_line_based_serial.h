// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef ASCII_LINE_BASED_SERIAL_20171109_H
#define ASCII_LINE_BASED_SERIAL_20171109_H

#include <regex>

#include "cgsn-mooring/serial/serial.h"

namespace cgsn
{
namespace io
{
/// \brief Provides a matching function object for the boost::asio::async_read_until based on a std::regex
class match_regex
{
  public:
    explicit match_regex(std::string eol) : eol_regex_(eol) {}

    template <typename Iterator>
    std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const
    {
        std::match_results<Iterator> result;
        if (std::regex_search(begin, end, result, eol_regex_))
            return std::make_pair(begin + result.position() + result.length(), true);
        else
            return std::make_pair(begin, false);
    }

  private:
    std::regex eol_regex_;
};

/// \brief Reads/Writes strings from/to serial port using a line-based (typically ASCII) protocol with a defined end-of-line regex.
/// \tparam line_in_group goby::Group to publish to after receiving data from the serial port
/// \tparam line_out_group goby::Group to subcribe to for data to send to the serial port
template <const goby::Group& line_in_group, const goby::Group& line_out_group>
class AsciiLineSerialThread : public SerialThread<line_in_group, line_out_group>
{
    using Base = SerialThread<line_in_group, line_out_group>;

  public:
    /// \brief Constructs the thread.
    /// \param config A reference to the Protocol Buffers config read by the main application at launch
    AsciiLineSerialThread(const cgsn::protobuf::SerialConfig& config)
        : Base(config), eol_matcher_(this->cfg().end_of_line())
    {
    }

    ~AsciiLineSerialThread() {}

  private:
    /// \brief Starts an asynchronous read on the serial port until the end-of-line string is reached. When the read completes, a lambda is called that publishes the received line.
    void async_read() override;

  private:
    match_regex eol_matcher_;
    boost::asio::streambuf buffer_;
};
} // namespace io
} // namespace cgsn

namespace boost
{
namespace asio
{
template <> struct is_match_condition<cgsn::io::match_regex> : public boost::true_type
{
};
} // namespace asio
} // namespace boost

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::AsciiLineSerialThread<line_in_group, line_out_group>::async_read()
{
    boost::asio::async_read_until(
        this->mutable_serial_port(), buffer_, eol_matcher_,
        [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
            if (!ec && bytes_transferred > 0)
            {
                std::string bytes(bytes_transferred, 0);
                std::istream is(&buffer_);
                is.read(&bytes[0], bytes_transferred);
                this->handle_read_success(bytes_transferred, bytes);
            }
            else
            {
                this->handle_read_error(ec);
            }
        });
}

#endif
