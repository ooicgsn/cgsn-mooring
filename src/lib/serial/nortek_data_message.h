// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef NORTEK_DATA_MESSAGE_20181210H
#define NORTEK_DATA_MESSAGE_20181210H

#include <cstdint>
#include <stdexcept>

namespace goby
{
class Group;
}

namespace cgsn
{
namespace io
{
/// \brief Struct representing a generic Nortek data message
/// \details Data structure (little-endian):
/// [Sync = 0xA5, 1 byte][Id, 1 byte][Entire size (including header) in words (=N/2), 2 bytes][N bytes][Checksum, 2 bytes = 0xb58C + sum of all words]

class NortekDataMessage
{
  public:
    using byte_t = std::uint8_t;
    using word_t = std::uint16_t;

    // as defined in Nortek AS "SYSTEM INTEGRATOR MANUAL" dated Apr 2014
    constexpr static byte_t nortek_sync{0xA5};
    constexpr static int bits_in_byte{8};
    constexpr static int bytes_in_word{std::numeric_limits<word_t>::digits / bits_in_byte};

    constexpr static int sync_field_size{1};
    constexpr static int id_field_size{1};
    constexpr static int size_field_size{2};
    constexpr static int checksum_field_size{2};

    constexpr static int minimum_size{sync_field_size + id_field_size + size_field_size +
                                      checksum_field_size};
    constexpr static int maximum_size{((1 << (size_field_size * bits_in_byte)) - 1) *
                                      bytes_in_word};

    constexpr static int sync_field_offset{0};
    constexpr static int id_field_offset{sync_field_offset + sync_field_size};
    constexpr static int size_field_offset{id_field_offset + id_field_size};
    constexpr static int body_offset{size_field_offset + size_field_size};

    NortekDataMessage() {}

    template <typename CharIterator>
    NortekDataMessage(CharIterator begin, CharIterator end) : message_(begin, end)
    {
        if (message_.size() < minimum_size)
            throw(std::out_of_range("Message size must be at least " +
                                    std::to_string(minimum_size) + " bytes"));
        else if (message_.size() > maximum_size)
            throw(std::out_of_range("Message size must be no more than " +
                                    std::to_string(maximum_size) + " bytes"));
        else if (message_.size() % bytes_in_word != 0)
            throw(std::out_of_range("Message must be an even number of words"));

        std::copy(begin + sync_field_offset, begin + sync_field_offset + sync_field_size,
                  sync_.begin());
        std::copy(begin + id_field_offset, begin + id_field_offset + id_field_size, id_.begin());
        std::copy(begin + size_field_offset, begin + size_field_offset + size_field_size,
                  size_.begin());

        std::copy(end - checksum_field_size, end, checksum_.begin());
    }

    NortekDataMessage(const std::string& bytes) : NortekDataMessage(bytes.begin(), bytes.end()) {}
    NortekDataMessage(const std::vector<byte_t>& bytes)
        : NortekDataMessage(bytes.begin(), bytes.end())
    {
    }

    byte_t sync() const { return sync_[0]; }
    byte_t id() const { return id_[0]; }
    word_t size_words() const { return to_word_t(size_); }
    std::vector<byte_t> body() const
    {
        return std::vector<byte_t>(message_.begin() + body_offset,
                                   message_.end() - checksum_field_size);
    }

    int size_bytes() const { return size_words() * bytes_in_word; }
    word_t reported_checksum() const { return to_word_t(checksum_); }
    word_t calculated_checksum() const
    {
        const word_t checksum_magic = 0xb58c;
        auto cs = checksum_magic;
        for (std::vector<byte_t>::size_type i = 0, n = message_.size() - checksum_field_size; i < n;
             i += bytes_in_word)
        {
            std::array<byte_t, bytes_in_word> word_arr({{message_[i], message_[i + 1]}});
            cs += to_word_t(word_arr);
        }
        return cs;
    }
    bool is_valid() const { return reported_checksum() == calculated_checksum(); }

  private:
    word_t to_word_t(const std::array<byte_t, bytes_in_word>& arr) const
    {
        word_t val(0);
        val |= (arr[0] & 0xFF);
        val |= (arr[1] & 0xFF) << bits_in_byte;
        return val;
    }

  private:
    // Allow the serial thread to build the message up piecewise
    template <const goby::Group& line_in_group, const goby::Group& line_out_group>
    friend class NortekDataBinarySerialThread;

    // entire message (including sync, id, size, etc.)
    std::vector<byte_t> message_;

    // sync field, 0xA5
    std::array<byte_t, sync_field_size> sync_;
    // Message ID field
    std::array<byte_t, id_field_size> id_;
    // Size field
    std::array<byte_t, size_field_size> size_;
    // checksum field
    std::array<byte_t, checksum_field_size> checksum_;
};

} // namespace io
} // namespace cgsn

#endif
