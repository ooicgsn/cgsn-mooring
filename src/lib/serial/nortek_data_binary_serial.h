// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef NORTEK_DATA_BINARY_SERIAL_20181207H
#define NORTEK_DATA_BINARY_SERIAL_20181207H

#include <cstdint>

#include "cgsn-mooring/serial/nortek_data_message.h"
#include "cgsn-mooring/serial/serial.h"

namespace cgsn
{
namespace io
{
/// \brief Reads/Writes data for the Nortek AS binary data format
/// \details The Nortek instruments use a binary protocol for data. See cgsn::io::NortekDataMessage for more details
/// \tparam line_in_group goby::Group to publish to after receiving data from the serial port
/// \tparam line_out_group goby::Group to subcribe to for data to send to the serial port
template <const goby::Group& line_in_group, const goby::Group& line_out_group>
class NortekDataBinarySerialThread : public SerialThread<line_in_group, line_out_group>
{
    using Base = SerialThread<line_in_group, line_out_group>;

  public:
    /// \brief Constructs the thread.
    /// \param config A reference to the Protocol Buffers config read by the main application at launch
    NortekDataBinarySerialThread(const cgsn::protobuf::SerialConfig& config) : Base(config) {}

    ~NortekDataBinarySerialThread() {}

  private:
    /// \brief Starts an asynchronous read on the serial port until the Sync is found and message is read
    void async_read() override;

    /// \brief Reads ID byte
    void async_read_id();

    /// \brief Reads Size word
    void async_read_size();

    /// \brief Reads the body (except the checksum)
    void async_read_body(int total_size_bytes);

    /// \brief Reads Checksum
    void async_read_checksum();

    /// \brief Called by other async_read_* functions to do the actual read.
    void async_read_core(NortekDataMessage::byte_t* buffer, std::size_t bytes_to_transfer,
                         std::function<void()> on_success)
    {
        boost::asio::async_read(
            this->mutable_serial_port(), boost::asio::buffer(buffer, bytes_to_transfer),
            boost::asio::transfer_exactly(bytes_to_transfer),
            [=](const boost::system::error_code& ec, std::size_t bytes_transferred) {
                if (!ec && (bytes_transferred == bytes_to_transfer))
                    on_success();
                else
                    this->handle_read_error(ec);
            });
    }

    /// \brief Overload of async_read_core for fixed size buffer where we want to read the entire buffer
    template <typename ByteArray>
    void async_read_core(ByteArray& buffer, std::function<void()> on_success)
    {
        async_read_core(&buffer[0], buffer.size(), on_success);
    }

  private:
    NortekDataMessage buffer_;
};
} // namespace io
} // namespace cgsn

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::NortekDataBinarySerialThread<line_in_group, line_out_group>::async_read()
{
    async_read_core(buffer_.sync_, [this]() {
        goby::glog.is_debug2() && goby::glog << group("i/o") << "Nortek Sync: 0x" << std::hex
                                             << static_cast<int>(buffer_.sync_[0]) << std::endl;

        if (buffer_.sync_[0] == NortekDataMessage::nortek_sync) // sync buffer matches
            async_read_id();
        else // keep looking for the sync byte
            async_read();
    });
}

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::NortekDataBinarySerialThread<line_in_group, line_out_group>::async_read_id()
{
    async_read_core(buffer_.id_, [this]() {
        goby::glog.is_debug2() && goby::glog << group("i/o") << "Nortek ID: 0x" << std::hex
                                             << static_cast<int>(buffer_.id_[0]) << std::endl;
        async_read_size();
    });
}

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::NortekDataBinarySerialThread<line_in_group, line_out_group>::async_read_size()
{
    async_read_core(buffer_.size_, [this]() {
        auto size_bytes = buffer_.size_bytes();
        goby::glog.is_debug2() && goby::glog << group("i/o") << "Size (words): 0x" << std::hex
                                             << buffer_.size_words() << ", bytes: " << std::dec
                                             << size_bytes << std::endl;
        async_read_body(size_bytes);
    });
}

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::NortekDataBinarySerialThread<line_in_group, line_out_group>::async_read_body(
    int total_size_bytes)
{
    auto body_size_bytes =
        total_size_bytes - NortekDataMessage::body_offset - NortekDataMessage::checksum_field_size;

    if (body_size_bytes < 0)
    {
        goby::glog.is_warn() && goby::glog << group("i/o") << "Invalid size" << std::endl;
        async_read();
        return;
    }

    goby::glog.is_debug2() && goby::glog << group("i/o") << "body_size_bytes: " << std::dec
                                         << body_size_bytes << std::endl;

    std::copy(buffer_.sync_.begin(), buffer_.sync_.end(), std::back_inserter(buffer_.message_));
    std::copy(buffer_.id_.begin(), buffer_.id_.end(), std::back_inserter(buffer_.message_));
    std::copy(buffer_.size_.begin(), buffer_.size_.end(), std::back_inserter(buffer_.message_));
    buffer_.message_.resize(total_size_bytes - NortekDataMessage::checksum_field_size);

    async_read_core(&buffer_.message_[NortekDataMessage::body_offset], body_size_bytes, [this]() {
        goby::glog.is_debug2() && goby::glog << group("i/o") << "Read message ("
                                             << buffer_.message_.size() << "B)" << std::endl;
        async_read_checksum();
    });
}

template <const goby::Group& line_in_group, const goby::Group& line_out_group>
void cgsn::io::NortekDataBinarySerialThread<line_in_group, line_out_group>::async_read_checksum()
{
    async_read_core(buffer_.checksum_, [this]() {
        goby::glog.is_debug2() && goby::glog << group("i/o") << "Rx checksum: 0x" << std::hex
                                             << buffer_.reported_checksum() << std::endl;

        std::copy(buffer_.checksum_.begin(), buffer_.checksum_.end(),
                  std::back_inserter(buffer_.message_));

        std::string bytes(buffer_.message_.begin(), buffer_.message_.end());
        this->handle_read_success(bytes.size(), bytes);
    });
}

#endif
