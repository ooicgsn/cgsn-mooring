// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef OOI_GROUPS20171128_H
#define OOI_GROUPS20171128_H

#include <goby/middleware/group.h>

#ifndef __clang__
#error "cgsn-mooring requires clang++ to compile"
#endif

namespace cgsn
{
namespace groups
{
constexpr goby::Group health{"cgsn::health"};

namespace io
{
constexpr goby::Group status{"cgsn::io::status"};
}

namespace computer_hardware
{
constexpr goby::Group status{"cgsn::computer_hardware::status"};
}

namespace time
{
constexpr goby::Group status{"cgsn::time::status"};
}

namespace sensor
{
constexpr goby::Group command_status{"cgsn::sensor::command_status"};
constexpr goby::Group validator_status{"cgsn::sensor::validator_status"};
constexpr goby::Group driver_status{"cgsn::sensor::driver_status"};
} // namespace sensor

namespace gps
{
constexpr goby::Group raw_in{"cgsn::gps::raw::in"};
constexpr goby::Group raw_out{"cgsn::gps::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::gps::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::gps::parsed"};
constexpr goby::Group control{"cgsn::gps::control"};
} // namespace gps

namespace ctd_sbe
{
constexpr goby::Group raw_in{"cgsn::ctd_sbe::raw::in"};
constexpr goby::Group raw_out{"cgsn::ctd_sbe::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::ctd_sbe::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::ctd_sbe::parsed"};
} // namespace ctd_sbe

namespace driver_pattern
{
constexpr goby::Group raw_in{"cgsn::driver_pattern::raw::in"};
constexpr goby::Group raw_out{"cgsn::driver_pattern::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::driver_pattern::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::driver_pattern::parsed"};
} // namespace driver_pattern

namespace phsen_pco2w_sunburst
{
constexpr goby::Group raw_in{"cgsn::phsen_pco2w_sunburst::raw::in"};
constexpr goby::Group raw_out{"cgsn::phsen_pco2w_sunburst::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::phsen_pco2w_sunburst::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::phsen_pco2w_sunburst::parsed"};
} // namespace phsen_pco2w_sunburst

namespace velpt_nortek
{
constexpr goby::Group raw_in{"cgsn::velpt_nortek::raw::in"};
constexpr goby::Group raw_out{"cgsn::velpt_nortek::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::velpt_nortek::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::velpt_nortek::parsed"};
} // namespace velpt_nortek

namespace dosta_optode
{
constexpr goby::Group raw_in{"cgsn::dosta_optode::raw::in"};
constexpr goby::Group raw_out{"cgsn::dosta_optode::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::dosta_optode::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::dosta_optode::parsed"};
} // namespace dosta_optode

namespace presf_sbe
{
constexpr goby::Group raw_in{"cgsn::presf_sbe::raw::in"};
constexpr goby::Group raw_out{"cgsn::presf_sbe::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::presf_sbe::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::presf_sbe::parsed"};
} // namespace presf_sbe

namespace adcp_rdi
{
constexpr goby::Group raw_in{"cgsn::adcp_rdi::raw::in"};
constexpr goby::Group raw_out{"cgsn::adcp_rdi::raw::out"};
constexpr goby::Group raw_in_validated{"cgsn::adcp_rdi::raw::in_validated"};
constexpr goby::Group parsed{"cgsn::adcp_rdi::parsed"};
} // namespace adcp_rdi

namespace scheduler
{
constexpr goby::Group command{"cgsn::scheduler::command"};
constexpr goby::Group status{"cgsn::scheduler::status"};
} // namespace scheduler

namespace supervisor
{
constexpr goby::Group mpic_raw_in{"cgsn::supervisor::mpic::raw::in"};
constexpr goby::Group mpic_raw_out{"cgsn::supervisor::mpic::raw::out"};
constexpr goby::Group mpic_command{"cgsn::supervisor::mpic::command"};
constexpr goby::Group status{"cgsn::supervisor::status"};

} // namespace supervisor

namespace command
{
constexpr goby::Group logging{"cgsn::command::logging"};
}

namespace sim
{
constexpr goby::Group raw_in{"cgsn::sim::raw::in"};
constexpr goby::Group raw_out{"cgsn::sim:raw::out"};
} // namespace sim

constexpr goby::Group ready{"cgsn::ready"};

} // namespace groups
} // namespace cgsn

#endif
