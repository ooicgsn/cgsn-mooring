// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef COMMON_20180810_H
#define COMMON_20180810_H

#include <string>

namespace cgsn
{
namespace protobuf
{
class Header;
}

namespace system
{
/// \brief Returns the current mooring name, e.g. "cp01cnsm"
///
/// Given the hostname of "cp01cnsm-dcl37" returns "cp01cnsm"
/// \return mooring name, e.g. "cp01cnsm"
std::string mooring_name();

/// \brief Returns the current CPU name, e.g. "dcl37"
///
/// Given the hostname of "cp01cnsm-dcl37" returns "dcl37"
/// \return CPU name, e.g. "dcl37"
std::string cpu_name();

/// \brief Set and/or get the instrument key
///
/// The first call to this function will set the instrument key. All future calls will
/// return this previously set key.
inline std::string instrument_key(const std::string& instr_key = std::string())
{
    static const auto key = instr_key;
    return key;
}

} // namespace system

void populate_header(cgsn::protobuf::Header& header, std::string thread_name = "main",
                     bool set_time = true);

template <typename ProtobufMessage>
ProtobufMessage make_with_header(std::string thread_name = "main", bool set_time = true)
{
    ProtobufMessage msg;
    populate_header(*msg.mutable_header(), thread_name, set_time);
    return msg;
}
} // namespace cgsn

#endif
