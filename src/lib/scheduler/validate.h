// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SCHEDULER_VALIDATE_20180717_H
#define SCHEDULER_VALIDATE_20180717_H

#include <deque>
#include <numeric>

#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/scheduler/exception.h"
#include <boost/units/base_units/metric/hour.hpp>

namespace cgsn
{
namespace scheduler
{
// calculate the minimum duration between any two configured hours
boost::units::quantity<boost::units::si::second_base_unit::unit_type,
                       int> inline minimum_time_between_slots(const protobuf::PortSchedule&
                                                                  schedule)
{
    if (schedule.hour_size() == 0)
        throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_HOURLY_NO_HOURS));

    std::vector<int> hours(schedule.hour().begin(), schedule.hour().end());

    // make sure we account for the wrapping hour
    const int hours_in_day = 24;
    hours.push_back(schedule.hour(0) + hours_in_day);
    decltype(hours) hour_diffs(hours.size(), 0);
    std::adjacent_difference(hours.begin(), hours.end(), hour_diffs.begin());

    // first element of adjacent_difference return value is not a difference, so ignore it
    int min_hour_diff = *std::min_element(hour_diffs.begin() + 1, hour_diffs.end());

    using QuantitySeconds =
        boost::units::quantity<boost::units::si::second_base_unit::unit_type, int>;

    auto hours_unit = boost::units::metric::hour_base_unit::unit_type();
    return QuantitySeconds(min_hour_diff * hours_unit);
};

inline void validate(const protobuf::PortSchedule& schedule)
{
    if (!schedule.IsInitialized())
        throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_NOT_INITIALIZED));

    switch (schedule.schedule_type())
    {
        case protobuf::PortSchedule::ALWAYS_ON:
            if (schedule.has_duration() || schedule.has_offset() || schedule.hour_size() ||
                schedule.has_interval())
            {
                throw(
                    InvalidScheduleException(protobuf::SCHEDULE_ERROR_ALWAYS_ON_GIVEN_PARAMETERS));
            }
            break;
        case protobuf::PortSchedule::ALWAYS_OFF:
            if (schedule.has_duration() || schedule.has_offset() || schedule.hour_size() ||
                schedule.has_interval())
            {
                throw(
                    InvalidScheduleException(protobuf::SCHEDULE_ERROR_ALWAYS_OFF_GIVEN_PARAMETERS));
            }
            break;
        case protobuf::PortSchedule::HOURLY:
        {
            // check this first so that we can assume hour_size() >= 1 for later checks
            if (schedule.hour_size() == 0)
                throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_HOURLY_NO_HOURS));

            // make sure all hours fall in [0, 24) range
            auto min_max_hours =
                std::minmax_element(schedule.hour().begin(), schedule.hour().end());
            int min_hour = *min_max_hours.first, max_hour = *min_max_hours.second;
            bool hours_in_range = min_hour >= 0 && max_hour < 24;

            auto hours_unit = boost::units::metric::hour_base_unit::unit_type();

            auto schedule_offset = schedule.offset_with_units();
            decltype(schedule_offset) offset_one_hour(1 * hours_unit);

            auto schedule_duration = schedule.duration_with_units();
            decltype(schedule_duration) duration_one_hour(1 * hours_unit);

            using QuantitySeconds =
                boost::units::quantity<boost::units::si::second_base_unit::unit_type, int>;

            QuantitySeconds offset_seconds(schedule.offset_with_units()),
                duration_seconds(schedule.duration_with_units()),
                buffer_seconds(schedule.slot_buffer_time_with_units()),
                force_stop_seconds(schedule.force_stop_delay_with_units()),
                one_hour_seconds(1 * hours_unit);

            if (schedule.has_interval())
                throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_HOURLY_GIVEN_INTERVAL));
            else if (!hours_in_range)
                throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_HOURLY_INVALID_HOUR));
            else if (schedule_offset > offset_one_hour)
                throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_HOURLY_INVALID_OFFSET));
            else if ((offset_seconds + duration_seconds + buffer_seconds) >
                     (minimum_time_between_slots(schedule) + offset_seconds))
                throw(InvalidScheduleException(
                    protobuf::SCHEDULE_ERROR_HOURLY_OFFSET_PLUS_DURATION_OVERFLOW));
            else if (buffer_seconds < force_stop_seconds)
                throw(InvalidScheduleException(
                    protobuf::SCHEDULE_ERROR_HOURLY_FORCE_STOP_DELAY_MUST_BE_LESS_THAN_BUFFER));
        }
        break;

        case protobuf::PortSchedule::INTERVAL:
            using QuantitySeconds =
                boost::units::quantity<boost::units::si::second_base_unit::unit_type, int>;

            QuantitySeconds offset_seconds(schedule.offset_with_units()),
                duration_seconds(schedule.duration_with_units()),
                buffer_seconds(schedule.slot_buffer_time_with_units()),
                force_stop_seconds(schedule.force_stop_delay_with_units()),
                interval_seconds(schedule.interval_with_units());

            if (schedule.hour_size())
                throw(InvalidScheduleException(protobuf::SCHEDULE_ERROR_INTERVAL_GIVEN_HOURS));
            else if (!schedule.has_interval())
                throw(InvalidScheduleException(protobuf::SCHEDULE_NO_INTERVAL_GIVEN));
            else if ((offset_seconds + duration_seconds + buffer_seconds) >
                     (interval_seconds + offset_seconds))
                throw(InvalidScheduleException(
                    protobuf::SCHEDULE_ERROR_INTERVAL_OFFSET_PLUS_DURATION_OVERFLOW));
            else if (buffer_seconds < force_stop_seconds)
                throw(InvalidScheduleException(
                    protobuf::SCHEDULE_ERROR_INTERVAL_FORCE_STOP_DELAY_MUST_BE_LESS_THAN_BUFFER));

            break;
    }
}

} // namespace scheduler
} // namespace cgsn

#endif
