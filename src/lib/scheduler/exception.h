// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SCHEDULER_EXCEPTION_20180717_H
#define SCHEDULER_EXCEPTION_20180717_H

#include <exception>

#include "cgsn-mooring/config/scheduler_config.pb.h"

namespace cgsn
{
    namespace scheduler
    {
        class InvalidScheduleException : public std::runtime_error
        {
        public:
            InvalidScheduleException(cgsn::protobuf::ScheduleError error)
                : std::runtime_error(cgsn::protobuf::ScheduleError_Name(error)),
                error_(error)
            {}

            cgsn::protobuf::ScheduleError error() const { return error_; }

        private:
            cgsn::protobuf::ScheduleError error_;

        };

        class LegacyScheduleException : public std::runtime_error
        {
        public:
            LegacyScheduleException(const std::string& what) : std::runtime_error(what) { }
        };
    }
}

#endif
