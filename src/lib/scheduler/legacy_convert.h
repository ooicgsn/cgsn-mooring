// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SCHEDULER_LEGACY_CONVERT_20180717_H
#define SCHEDULER_LEGACY_CONVERT_20180717_H

#include <regex>

#include <boost/algorithm/string.hpp>

#include "cgsn-mooring/scheduler/exception.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"


namespace cgsn
{
    namespace scheduler
    {
        // e.g. "0:0-23:0:30" # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
        protobuf::PortSchedule convert_legacy_hour_sched(const protobuf::SchedulerConfig::LegacyHourlySchedule& legacy)
        {
            std::string regex_str = "^[01]:(([0-9]+|[0-9]+-[0-9]+),)*([0-9]+|[0-9]+-[0-9]+):[0-9]+:[0-9]+$";
            std::regex sched_regex(regex_str);
            if(!regex_match(legacy.sched(), sched_regex))
                throw(LegacyScheduleException("sched is the wrong format (does not match regex: " + regex_str + ")"));

            protobuf::PortSchedule schedule;
            schedule.set_port_id(legacy.port_id());
            schedule.set_sensor_type(legacy.sensor_type());

            enum
            {
                ENABLED = 0,
                HOURS = 1,
                OFFSET = 2,
                DURATION = 3,
                NUM_FIELDS = DURATION + 1
            };


            std::vector<std::string> sched_parts;
            boost::split(sched_parts, legacy.sched(), boost::is_any_of(":"));

            if(!std::stoi(sched_parts[ENABLED]))
            {
                schedule.set_schedule_type(protobuf::PortSchedule::ALWAYS_OFF);
                return schedule;
            }
            else
            {
                schedule.set_schedule_type(protobuf::PortSchedule::HOURLY);
            }

            auto minutes { boost::units::metric::minute_base_unit::unit_type()};

            std::vector<std::string> hour_parts;
            boost::split(hour_parts, sched_parts[HOURS], boost::is_any_of(","));

            std::set<int> hours;
            for(auto hour : hour_parts)
            {
                auto hyphen_pos = hour.find('-');

                if(hyphen_pos == std::string::npos)
                {
                    // just a number
                    hours.insert(std::stoi(hour));
                }
                else
                {
                    // range of hours
                    int begin_hr = std::stoi(hour.substr(0, hyphen_pos));
                    int end_hr = std::stoi(hour.substr(hyphen_pos+1));

                    if(begin_hr > end_hr)
                        throw(LegacyScheduleException("sched contains hour range 'A-B' where A > B"));

                    for(int h = begin_hr; h <= end_hr; ++h)
                        hours.insert(h);
                }
            }

            for(int hour : hours)
                schedule.add_hour(hour);

            schedule.set_duration_with_units(std::stoi(sched_parts[DURATION])*minutes);
            schedule.set_offset_with_units(std::stoi(sched_parts[OFFSET])*minutes);



            return schedule;
        }

        // e.g. "1:15:00:04" # 0-disable, 1-enable : interval(min) : offset(min) : duration (min)
        protobuf::PortSchedule convert_legacy_interval_sched(const protobuf::SchedulerConfig::LegacyIntervalSchedule& legacy)
        {
            std::string regex_str = "^[01]:[0-9]+:[0-9]+:[0-9]+$";
            std::regex isched_regex(regex_str);
            if(!regex_match(legacy.isched(), isched_regex))
                throw(LegacyScheduleException("isched is the wrong format (does not match regex: " + regex_str + ")"));

            protobuf::PortSchedule schedule;
            schedule.set_port_id(legacy.port_id());
            schedule.set_sensor_type(legacy.sensor_type());

            enum
            {
                ENABLED = 0,
                INTERVAL = 1,
                OFFSET = 2,
                DURATION = 3,
                NUM_FIELDS = DURATION + 1
            };

            std::vector<std::string> isched_parts;
            boost::split(isched_parts, legacy.isched(), boost::is_any_of(":"));

            if(!std::stoi(isched_parts[ENABLED]))
            {
                schedule.set_schedule_type(protobuf::PortSchedule::ALWAYS_OFF);
                return schedule;
            }
            else
            {
                schedule.set_schedule_type(protobuf::PortSchedule::INTERVAL);
            }

            auto minutes { boost::units::metric::minute_base_unit::unit_type()};
            schedule.set_interval_with_units(std::stoi(isched_parts[INTERVAL])*minutes);
            schedule.set_duration_with_units(std::stoi(isched_parts[DURATION])*minutes);
            schedule.set_offset_with_units(std::stoi(isched_parts[OFFSET])*minutes);

            return schedule;
        }

    }
}


#endif


