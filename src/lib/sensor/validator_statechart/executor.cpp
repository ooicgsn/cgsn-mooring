// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/sensor/validator_statechart/executor.h"
#include "cgsn-mooring/sensor/validator_statechart/interface.h"

#include <goby/common/logger.h>

cgsn::validator::ValidatorStateChartExecutor::ValidatorStateChartExecutor(
    const cgsn::protobuf::ValidatorConfig& cfg, goby::InterThreadTransporter& interthread)
    : cfg_(cfg)
{
    state_machine_.reset(new statechart::Machine(interthread));
    state_machine_->initiate();
}

cgsn::validator::ValidatorStateChartExecutor::~ValidatorStateChartExecutor()
{
    state_machine_->terminate();
    state_machine_.reset();
}

void cgsn::validator::ValidatorStateChartExecutor::check_timeouts()
{
    using namespace goby::common::logger;
    using goby::glog;

    auto now = goby::time::now();

    if (now > (state_machine_->last_message_time(statechart::Machine::TimeoutComponent::BYTES) +
               goby::time::MicroTime(cfg_.bytes_timeout_with_units())))
        state_machine_->process_event(statechart::EvBytesTimeout());
    if (now > (state_machine_->last_message_time(statechart::Machine::TimeoutComponent::VALID_RAW) +
               goby::time::MicroTime(cfg_.valid_raw_data_timeout_with_units())))
        state_machine_->process_event(statechart::EvValidRawDataTimeout());

    if (now >
        (state_machine_->last_message_time(statechart::Machine::TimeoutComponent::VALID_PARSED) +
         goby::time::MicroTime(cfg_.valid_parsed_data_timeout_with_units())))
        state_machine_->process_event(statechart::EvValidParsedDataTimeout());
}

void cgsn::validator::ValidatorStateChartExecutor::notify_receiving_bytes(
    goby::time::MicroTime mooring_time)
{
    state_machine_->set_last_message_time(statechart::Machine::TimeoutComponent::BYTES,
                                          mooring_time);
    state_machine_->process_event(statechart::EvReceivingBytes());
}

void cgsn::validator::ValidatorStateChartExecutor::notify_valid_raw_data(
    goby::time::MicroTime mooring_time)
{
    if (state_machine_->state() != cgsn::protobuf::sensor::NOTLOGGING__STANDBY)
    {
        state_machine_->set_last_message_time(statechart::Machine::TimeoutComponent::VALID_RAW,
                                              mooring_time);
        state_machine_->process_event(statechart::EvReceivingValidRawData());
    }
    else
    {
        goby::glog.is_warn() && goby::glog
                                    << group("validator")
                                    << "Raw message is valid, but we are not supposed to be logging"
                                    << std::endl;
    }
}

void cgsn::validator::ValidatorStateChartExecutor::notify_invalid_raw_data(
    goby::time::MicroTime mooring_time)
{
    // only put out a debug message if we're supposed be logging and we get an invalid message
    if (state_machine_->state() != cgsn::protobuf::sensor::NOTLOGGING__STANDBY)
        goby::glog.is_warn() && goby::glog << group("validator") << "Raw message is invalid"
                                           << std::endl;
}

void cgsn::validator::ValidatorStateChartExecutor::notify_valid_parsed_data(
    goby::time::MicroTime mooring_time)
{
    state_machine_->set_last_message_time(statechart::Machine::TimeoutComponent::VALID_PARSED,
                                          mooring_time);
    state_machine_->process_event(statechart::EvReceivingValidParsedData());
}


void cgsn::validator::ValidatorStateChartExecutor::handle_command(bool cmd)
{
    if (cmd)
        state_machine_->process_event(statechart::EvCommanderLoggingStarted());
    else
        state_machine_->process_event(statechart::EvCommanderLoggingStopped());
}
