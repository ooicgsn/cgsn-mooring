// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VALIDATOR_STATECHART_20181315_H
#define VALIDATOR_STATECHART_20181315_H

#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>

#include <goby/common/logger.h>
#include <goby/common/time.h>
#include <goby/middleware/transport-interthread.h>

#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/sensor_status.pb.h"

namespace cgsn
{
namespace validator
{
namespace statechart
{
// events
struct EvReceivingBytes : boost::statechart::event<EvReceivingBytes>
{
};
struct EvReceivingValidRawData : boost::statechart::event<EvReceivingValidRawData>
{
};
struct EvReceivingValidParsedData : boost::statechart::event<EvReceivingValidParsedData>
{
};

struct EvBytesTimeout : boost::statechart::event<EvBytesTimeout>
{
};
struct EvValidRawDataTimeout : boost::statechart::event<EvValidRawDataTimeout>
{
};
struct EvValidParsedDataTimeout : boost::statechart::event<EvValidParsedDataTimeout>
{
};

struct EvCommanderLoggingStopped : boost::statechart::event<EvCommanderLoggingStopped>
{
};
struct EvCommanderLoggingStarted : boost::statechart::event<EvCommanderLoggingStarted>
{
};

// pre-declare

// state machine
struct Machine;
// 1st tier states
struct NotLogging;
struct Logging;

namespace logging
{
struct Initializing;
struct CriticalFailure;
struct Degraded;
struct Functioning;

namespace functioning
{
struct RawDataValid;
struct ParsedDataValid;
} // namespace functioning
} // namespace logging

namespace notlogging
{
struct Standby;
}

struct Machine : boost::statechart::state_machine<Machine, NotLogging>
{
  public:
    Machine(goby::InterThreadTransporter& interthread) : interthread_(interthread)
    {
        goby::glog.add_group("validator::statechart", goby::common::Colors::yellow);
    }

    protobuf::sensor::ValidatorState state() { return state_; }
    protobuf::sensor::ValidatorState previous_state() { return previous_state_; }

    void set_error(protobuf::Error error)
    {
        error_ = error;
        publish_health();
    }
    protobuf::Error error() { return error_; }
    bool has_error() { return error_.has_code(); }
    void clear_error()
    {
        error_.Clear();
        publish_health();
    }

    enum class TimeoutComponent
    {
        BYTES,
        VALID_RAW,
        VALID_PARSED
    };
    void set_last_message_time(TimeoutComponent component, goby::time::MicroTime time)
    {
        last_message_time_[component] = time;
    }

    goby::time::MicroTime last_message_time(TimeoutComponent component)
    {
        return last_message_time_[component];
    }

    // to allow calling set_state
    template <typename Derived, protobuf::sensor::ValidatorState state> friend struct Notify;

  private:
    void set_state(protobuf::sensor::ValidatorState state)
    {
        state_ = state;
        this->publish_health();
    }
    void unset_state(protobuf::sensor::ValidatorState state)
    {
        previous_state_ = state;
        state_ = protobuf::sensor::VALIDATOR_STATE_UNKNOWN;
    }

    void publish_health()
    {
        protobuf::sensor::ValidatorStatus health;
        health.set_state(state_);
        if (this->has_error())
            *health.mutable_error() = this->error();

        this->interthread_.template publish<cgsn::groups::sensor::validator_status>(health);
    }

  private:
    protobuf::sensor::ValidatorState state_{protobuf::sensor::VALIDATOR_STATE_UNKNOWN};
    protobuf::sensor::ValidatorState previous_state_{protobuf::sensor::VALIDATOR_STATE_UNKNOWN};

    protobuf::Error error_;

    std::map<TimeoutComponent, goby::time::MicroTime> last_message_time_{
        {TimeoutComponent::BYTES, goby::time::MicroTime(0 * boost::units::si::seconds)},
        {TimeoutComponent::VALID_RAW, goby::time::MicroTime(0 * boost::units::si::seconds)},
        {TimeoutComponent::VALID_PARSED, goby::time::MicroTime(0 * boost::units::si::seconds)}};

    goby::InterThreadTransporter& interthread_;
};

// RAII tool for setting enumerations for reporting based on current state
template <typename Derived, protobuf::sensor::ValidatorState state> struct Notify
{
    Notify()
    {
        goby::glog.is_debug1() && goby::glog << group("validator::statechart")
                                             << protobuf::sensor::ValidatorState_Name(state)
                                             << std::endl;
        machine().set_state(state);
    }
    ~Notify()
    {
        machine().unset_state(state);
        goby::glog.is_debug1() && goby::glog << group("validator::statechart") << "~"
                                             << protobuf::sensor::ValidatorState_Name(state)
                                             << std::endl;
    }

  private:
    Machine& machine() { return static_cast<Derived*>(this)->outermost_context(); }
};

struct NotLogging : boost::statechart::state<NotLogging, Machine, notlogging::Standby>
{
    NotLogging(my_context c) : my_base(c) {}

    typedef boost::mpl::list<boost::statechart::transition<EvCommanderLoggingStarted, Logging>>
        reactions;
};

struct Logging : boost::statechart::state<Logging, Machine, logging::Initializing>
{
    Logging(my_context c) : my_base(c) {}

    typedef boost::mpl::list<
        boost::statechart::transition<EvCommanderLoggingStopped, NotLogging>,
        boost::statechart::transition<EvBytesTimeout, logging::CriticalFailure>>
        reactions;
};

namespace logging
{
struct Initializing : boost::statechart::state<Initializing, Logging>,
                      Notify<Initializing, protobuf::sensor::LOGGING__INITIALIZING>
{
    Initializing(my_context c) : my_base(c)
    {
        // update VALID_RAW last message time so we don't immediately transition to Degraded or Critical Failure
        // but rather wait for the timeout seconds in Initializing
        Machine& machine = outermost_context();
        auto now = goby::time::now();
        machine.set_last_message_time(statechart::Machine::TimeoutComponent::BYTES, now);
        machine.set_last_message_time(statechart::Machine::TimeoutComponent::VALID_RAW, now);
    }

    typedef boost::mpl::list<boost::statechart::transition<EvReceivingValidRawData, Functioning>,
                             boost::statechart::transition<EvValidRawDataTimeout, Degraded>>
        reactions;
};

struct CriticalFailure : boost::statechart::state<CriticalFailure, Logging>,
                         Notify<CriticalFailure, protobuf::sensor::LOGGING__CRITICAL_FAILURE>
{
    CriticalFailure(my_context c) : my_base(c)
    {
        if (dynamic_cast<const EvBytesTimeout*>(triggering_event()))
        {
            protobuf::Error error;
            error.set_code(cgsn::protobuf::Error::IO__SERIAL_DATA_TIMEOUT);
            Machine& machine = outermost_context();
            machine.set_error(error);
        }
    }

    ~CriticalFailure()
    {
        Machine& machine = outermost_context();
        machine.clear_error();
    }

    boost::statechart::result react(const EvBytesTimeout&) { return discard_event(); }

    typedef boost::mpl::list<boost::statechart::transition<EvReceivingBytes, Degraded>,
                             boost::statechart::custom_reaction<EvBytesTimeout>>
        reactions;
};

struct Degraded : boost::statechart::state<Degraded, Logging>,
                  Notify<Degraded, protobuf::sensor::LOGGING__DEGRADED>
{
    Degraded(my_context c) : my_base(c) {}
    typedef boost::mpl::list<boost::statechart::transition<EvReceivingValidRawData, Functioning>>
        reactions;
};

struct Functioning : boost::statechart::state<Functioning, Logging, functioning::RawDataValid>
{
    Functioning(my_context c) : my_base(c) {}

    typedef boost::mpl::list<boost::statechart::transition<EvValidRawDataTimeout, Degraded>>
        reactions;
};

namespace functioning
{
struct RawDataValid : boost::statechart::state<RawDataValid, Functioning>,
                      Notify<RawDataValid, protobuf::sensor::LOGGING__FUNCTIONING__RAW_DATA_VALID>
{
    RawDataValid(my_context c) : my_base(c) {}

    typedef boost::mpl::list<
        boost::statechart::transition<EvReceivingValidParsedData, ParsedDataValid>>
        reactions;
};

struct ParsedDataValid
    : boost::statechart::state<ParsedDataValid, Functioning>,
      Notify<ParsedDataValid, protobuf::sensor::LOGGING__FUNCTIONING__PARSED_DATA_VALID>
{
    ParsedDataValid(my_context c) : my_base(c) {}

    typedef boost::mpl::list<boost::statechart::transition<EvValidParsedDataTimeout, RawDataValid>>
        reactions;
};
} // namespace functioning
} // namespace logging

namespace notlogging
{
struct Standby : boost::statechart::state<Standby, NotLogging>,
                 Notify<Standby, protobuf::sensor::NOTLOGGING__STANDBY>
{
    Standby(my_context c) : my_base(c) {}
};
} // namespace notlogging

} // namespace statechart
} // namespace validator
} // namespace cgsn

#endif
