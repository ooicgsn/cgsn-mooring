// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VALIDATOR_STATECHART_EXECUTOR_20181210_H
#define VALIDATOR_STATECHART_EXECUTOR_20181210_H

#include <goby/common/time3.h>
#include <goby/middleware/transport-interthread.h>

#include "cgsn-mooring/config/sensors_config.pb.h"
#include "cgsn-mooring/messages/error.pb.h"

namespace cgsn
{
namespace validator
{
namespace statechart
{
struct Machine;
}

/// \brief Handles the interfacing between the ValidatorThread and the validator::statechart::Machine
/// \details The primary purpose of this class is to provide a non-template interface to the validator::statechart::Machine and thus allow this template-heavy Boost Statechart code to be isolated to a single translation unit in the cgsn_sensor library.
class ValidatorStateChartExecutor
{
  protected:
    ValidatorStateChartExecutor(const cgsn::protobuf::ValidatorConfig& cfg,
                                goby::InterThreadTransporter& interthread);

    virtual ~ValidatorStateChartExecutor();

    void check_timeouts();

    void notify_receiving_bytes(goby::time::MicroTime mooring_time);
    void notify_valid_raw_data(goby::time::MicroTime mooring_time);
    void notify_invalid_raw_data(goby::time::MicroTime mooring_time);
    void notify_valid_parsed_data(goby::time::MicroTime mooring_time);

    void handle_command(bool cmd);

  private:
    const cgsn::protobuf::ValidatorConfig& cfg_;
    std::unique_ptr<statechart::Machine> state_machine_;
};

} // namespace validator
} // namespace cgsn

#endif
