// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "presf_sbe_validator.h"

using namespace goby::common::logger;
using goby::glog;

cgsn::validator::PRESF_SBE_ValidatorThread::PRESF_SBE_ValidatorThread(
    const protobuf::ValidatorConfig& cfg)
    : ValidatorBaseThread<cgsn::groups::presf_sbe::raw_in,
                          cgsn::groups::presf_sbe::raw_in_validated, cgsn::protobuf::PRESF_SBE_Data,
                          cgsn::groups::presf_sbe::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
    glog.add_group("presf_sbe::validator", goby::common::Colors::yellow);
}

bool cgsn::validator::PRESF_SBE_ValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    std::regex presf_data_regex("^tide: start time = (([0-9])|([0-2][0-9])|([3][0-1])) "
                                "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [0-9]{4} "
                                "[0-9]{2}:[0-5][0-9]:[0-5][0-9], "
                                "((p|pt|t|c|s) = -?[0-9]*\\.?[0-9]+(, )?)+\r\n$");

    return std::regex_match(raw.raw_data(), presf_data_regex);
}

bool cgsn::validator::PRESF_SBE_ValidatorThread::validate_parsed(
    const protobuf::PRESF_SBE_Data& pos)
{
    return false;
}
