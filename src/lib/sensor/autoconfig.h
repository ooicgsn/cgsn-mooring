// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef AUTOCONFIG_20181120_H
#define AUTOCONFIG_20181120_H

#include <map>
#include <string>

#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "exception.h"

namespace cgsn
{
namespace sensor_command
{
namespace autoconfig
{
/// \brief Returns the bool formatted as a string based on this sensor's policy
inline std::string bool_to_string(bool value, const google::protobuf::Descriptor* sensor_desc)
{
    switch (sensor_desc->options().GetExtension(cgsn::msg).sensor_cfg().bool_format())
    {
        case cgsn::MessageOptions::SensorConfig::BOOL_Y_N: return (value ? "Y" : "N");
        case cgsn::MessageOptions::SensorConfig::BOOL_1_0: return (value ? "1" : "0");
        case cgsn::MessageOptions::SensorConfig::BOOL_Yes_No: return (value ? "Yes" : "No");
    }
}

/// \brief Return microseconds between sending commands
inline int delay_usec(const google::protobuf::Descriptor* sensor_desc)
{
    boost::units::quantity<decltype(boost::units::si::micro * boost::units::si::seconds), float>
        usec(sensor_desc->options()
                 .GetExtension(cgsn::msg)
                 .sensor_cfg()
                 .parameter_delay_sec_with_units());
    return usec.value();
}

/// \brief Gather the parameter command strings for the fixed parameters (sensor_cfg { fixed_param {} })
///
/// \param sensor_desc Descriptor for the given SensorConfig Protobuf message
/// \param initialize_state If true, returns the parameters required for Initialize::Configure, if false, return those used for On::Configure (a subset)
/// \return Vector of parameter commands to the corresponding FixedParam protobuf object
inline std::deque<std::pair<std::string, cgsn::MessageOptions::SensorConfig::FixedParam>>
fixed_params(const google::protobuf::Descriptor* sensor_desc, bool initialize_state = true)
{
    const auto& fixed_params =
        sensor_desc->options().GetExtension(cgsn::msg).sensor_cfg().fixed_param();
    std::deque<std::pair<std::string, cgsn::MessageOptions::SensorConfig::FixedParam>> cmds;

    for (const auto& fixed_param : fixed_params)
    {
        if (!initialize_state && !fixed_param.send_before_logging())
            continue;

        switch (fixed_param.type())
        {
            case cgsn::MessageOptions::SensorConfig::FixedParam::NORMAL:
                cmds.push_back(std::make_pair(fixed_param.value(), fixed_param));
                break;
            case cgsn::MessageOptions::SensorConfig::FixedParam::DATE_TIME:
                std::stringstream stream;
                auto now_ptime = goby::time::to_ptime(goby::time::now());
                // Use a facet to display time in a custom format
                boost::posix_time::time_facet* facet = new boost::posix_time::time_facet();
                facet->format(fixed_param.date_time_format().c_str());
                stream.imbue(std::locale(std::locale::classic(), facet));
                stream << now_ptime;
                boost::format f(fixed_param.value());
                f % stream.str();
                cmds.push_back(std::make_pair(f.str(), fixed_param));
                break;
        }
    }
    return cmds;
}

/// \brief Gather the parameter command strings for the dynamic run-time parameters defined in the *_SensorConfig (e.g. cgsn::protobuf::PRESF_SBE_SensorConfig) Protobuf message
///
/// \param sensor_msg The given SensorConfig Protobuf message  (e.g. cgsn::protobuf::PRESF_SBE_SensorConfig)
/// \return Map of parameter commands to the corresponding cgsn::FieldOptions::SensorConfig protobuf object
inline std::deque<std::pair<std::string, cgsn::FieldOptions::SensorConfig>>
dynamic_params(const google::protobuf::Message& sensor_msg)
{
    const auto* desc = sensor_msg.GetDescriptor();
    const auto* refl = sensor_msg.GetReflection();

    // batch strings, i.e. ones where multiple Protobuf fields are used to create one command
    struct BatchData
    {
        BatchData(const cgsn::MessageOptions::SensorConfig::BatchConfig& b) : batch(b)
        {
            for (auto fmt : batch.format())
            {
                cgsn::FieldOptions::SensorConfig cfg;
                cfg.set_key(batch.key());
                boost::format formatter(fmt);
                formatter.exceptions(boost::io::all_error_bits ^
                                     (boost::io::too_many_args_bit | boost::io::too_few_args_bit));
                formatter % batch.key();
                fmts.push_back(std::make_pair(formatter, cfg));
            }
            fmts_it = fmts.begin();
        }

        void add_sensor_cfg(const cgsn::FieldOptions::SensorConfig& sensor_cfg)
        {
            fmts_it->second = sensor_cfg;
        }

        boost::format& next_format()
        {
            if (fmts_it != fmts.end())
                return (fmts_it++)->first;
            else
                throw(InvalidSensorConfiguration("Too few format strings given for batch: " +
                                                 batch.key()));
        }

        const cgsn::MessageOptions::SensorConfig::BatchConfig& batch;
        std::vector<std::pair<boost::format, cgsn::FieldOptions::SensorConfig>> fmts;
        typename decltype(fmts)::iterator fmts_it;
    };

    std::map<std::string, BatchData> batch_formats;
    for (const auto& batch : desc->options().GetExtension(cgsn::msg).sensor_cfg().batch())
    {
        batch_formats.insert(std::make_pair(batch.key(), BatchData(batch)));
    }

    std::deque<std::pair<std::string, cgsn::FieldOptions::SensorConfig>> cmds;

    for (int i = 0, n = desc->field_count(); i < n; ++i)
    {
        bool has_sensor_cfg = desc->options().GetExtension(cgsn::msg).has_sensor_cfg();
        if (!has_sensor_cfg)
            continue;

        boost::format single_fmt(desc->options().GetExtension(cgsn::msg).sensor_cfg().format());
        const auto* field_desc = desc->field(i);
        const auto& field_cfg = field_desc->options().GetExtension(cgsn::field);
        const auto& field_sensor_cfg = field_cfg.sensor_cfg();

        single_fmt % field_sensor_cfg.key();
        bool is_in_batch = batch_formats.count(field_sensor_cfg.key());
        if (is_in_batch)
            batch_formats.at(field_sensor_cfg.key()).add_sensor_cfg(field_sensor_cfg);
        auto& fmt =
            is_in_batch ? batch_formats.at(field_sensor_cfg.key()).next_format() : single_fmt;

        switch (field_desc->cpp_type())
        {
            case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedInt32(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetInt32(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedInt64(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetInt64(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedUInt32(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetUInt32(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedUInt64(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetUInt64(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedDouble(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetDouble(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedFloat(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetFloat(sensor_msg, field_desc);
                }
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt %
                            bool_to_string(refl->GetRepeatedBool(sensor_msg, field_desc, i), desc);
                }
                else
                {
                    fmt % bool_to_string(refl->GetBool(sensor_msg, field_desc), desc);
                }
                break;

            case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedEnum(sensor_msg, field_desc, i)->number();
                }
                else
                {
                    fmt % refl->GetEnum(sensor_msg, field_desc)->number();
                }
                break;

            case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
                if (field_desc->is_repeated())
                {
                    for (int i = 0, n = refl->FieldSize(sensor_msg, field_desc); i < n; ++i)
                        fmt % refl->GetRepeatedString(sensor_msg, field_desc, i);
                }
                else
                {
                    fmt % refl->GetString(sensor_msg, field_desc);
                }
                break;

            case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
            {
                if (field_desc->is_repeated())
                    throw(UnsupportedSensorConfiguration("Repeated embedded message fields are not "
                                                         "supported by cgsn-mooring autoconfig"));

                auto child_cmds = dynamic_params(refl->GetMessage(sensor_msg, field_desc));
                cmds.insert(cmds.end(), child_cmds.begin(), child_cmds.end());
                break;
            }
        }
        if (!is_in_batch &&
            field_desc->cpp_type() != google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
            cmds.push_back(std::make_pair(fmt.str(), field_sensor_cfg));
    }

    for (const auto& p : batch_formats)
    {
        const BatchData& batch_data = p.second;
        boost::format key_formatter(batch_data.batch.key_format());
        key_formatter % batch_data.batch.key();
        cgsn::FieldOptions::SensorConfig key_cfg;
        key_cfg.set_key(batch_data.batch.key());
        // first send the key (formatted as needed)
        cmds.push_back(std::make_pair(key_formatter.str(), key_cfg));
        // next send all the batch values
        for (const auto& fmt_p : p.second.fmts)
            cmds.push_back(std::make_pair(fmt_p.first.str(), fmt_p.second));
    }

    return cmds;
}
} // namespace autoconfig
} // namespace sensor_command
}

#endif
