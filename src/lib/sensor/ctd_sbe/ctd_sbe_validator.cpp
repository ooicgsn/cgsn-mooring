// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "ctd_sbe_validator.h"

#include <boost/units/io.hpp>
#include <regex>

using namespace goby::common::logger;
using goby::glog;


// Validator
cgsn::validator::CTD_SBE_ValidatorThread::CTD_SBE_ValidatorThread(const protobuf::ValidatorConfig& cfg)
	: ValidatorBaseThread<cgsn::groups::ctd_sbe::raw_in,
                         cgsn::groups::ctd_sbe::raw_in_validated,
                         cgsn::protobuf::CTD_SBE_Sample, cgsn::groups::ctd_sbe::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
    glog.add_group("validator::ctd_sbe", goby::common::Colors::lt_green);
}

bool cgsn::validator::CTD_SBE_ValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    // Use regex to validate the sample begins with a '#' and it contains
    // 4 comma delimited samples (including date & time)
    // or that it is simply a blank line
    std::regex sample_regex_4_samples("^#{1}(.*,){3}.*\\r\\n$|^\\r\\n$");
    return regex_match(raw.raw_data(), sample_regex_4_samples);
}

bool cgsn::validator::CTD_SBE_ValidatorThread::validate_parsed(const protobuf::CTD_SBE_Sample& sample)
{
    glog.is(VERBOSE) && glog << group("validator::ctd_sbe") << "Received sample: " << sample.raw() << std::endl;

	// TODO: For now, return false but eventually need to utilize cgsn-parser code
    return false;
}
