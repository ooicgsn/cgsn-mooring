// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef CTD_SBE_SENSOR_COMMAND_STATECHART_H
#define CTD_SBE_SENSOR_COMMAND_STATECHART_H

#include <goby/common/time.h>

#include "cgsn-mooring/config/ctd_sbe_config.pb.h"
#include "cgsn-mooring/sensor/command_statechart/autoconfig.h"
#include "cgsn-mooring/sensor/command_statechart/interface.h"

namespace cgsn
{
    namespace sensor_command
    {
        namespace statechart
        {
        template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
        struct CTD_SBE_WaitForPrompt : boost::statechart::state<Derived, Parent>,
                                       Notify<Derived, state>,
                                       TimeAccess
        {
            using StateBase = boost::statechart::state<Derived, Parent>;
            CTD_SBE_WaitForPrompt(typename StateBase::my_context c) : StateBase(c)
            {
                switch (this->cfg().command().op_mode())
                {
                    case protobuf::SensorCommandConfig::NORMAL: this->send_enter(); break;

                    case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                        // passthrough
                        this->post_event(EvPromptReceived());
                        break;
                }
            }
            ~CTD_SBE_WaitForPrompt() {}

            typedef boost::mpl::list<boost::statechart::custom_reaction<EvPromptTimeoutExceeded>>
                local_reactions;

            boost::statechart::result react(const EvPromptTimeoutExceeded& ev)
            {
                ++get_prompt_retries_;
                goby::glog.is(goby::common::logger::DEBUG2) &&
                    goby::glog << group("sensor_command::statechart")
                               << "CTD-SBE did not show prompt, but we sent ENTER" << std::endl;
                if (get_prompt_retries_ >= max_get_prompt_retries_)
                {
                    return this->template transit<on::CriticalFailure>();
                }
                else
                {
                    goby::glog.is(goby::common::logger::DEBUG1) &&
                        goby::glog << group("sensor_command::statechart") << "Retry "
                                   << get_prompt_retries_ << std::endl;
                    this->send_enter();
                    return this->discard_event();
                }
            }

          private:
            void send_enter()
            {
                const char* enter_string = "\r\n";
                // CTD requires user to receive "S>" prompt prior to any commands being sent
                // Attempt to get prompt (S>) by sending /r/n
                auto send_enter = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                send_enter.set_raw_data(enter_string);

                this->interprocess().template publish<cgsn::groups::ctd_sbe::raw_out>(send_enter);
                last_enter_time_ = goby::time::now();
            }

            goby::time::MicroTime last_time() const override { return last_enter_time_; }

          private:
            const int max_get_prompt_retries_{10};
            int get_prompt_retries_{0};
            goby::time::MicroTime last_enter_time_;
            };

            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            using CTD_SBE_Configure =
                AutoConfigure<Derived, Parent, state, protobuf::CTD_SBE_SensorConfig,
                              cgsn::groups::ctd_sbe::raw_out>;

            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct CTD_SBE_StartLogging : boost::statechart::state<Derived, Parent>,
                                          Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;
                CTD_SBE_StartLogging(typename StateBase::my_context c) : StateBase(c)
                {

                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                            send_start();
                            break;

                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            // passthrough
                            this->post_event(EvLoggingStarted());
                            break;
                    }
                }
                ~CTD_SBE_StartLogging() { }

                typedef boost::mpl::list<> local_reactions;

            private:
                void send_start()
                {
                    // send STARTNOW
                    auto send_start_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                    send_start_cmd.set_raw_data(start_logging_string);
                    this->interprocess().template publish<cgsn::groups::ctd_sbe::raw_out>(send_start_cmd);
                }

            private:
                const char* start_logging_string = "STARTNOW\r\n";
            };

            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct CTD_SBE_StopLogging : boost::statechart::state<Derived, Parent>,
                                         Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                CTD_SBE_StopLogging(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                            send_stop();
                            break;

                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            // passthrough
                            this->post_event(EvLoggingStopped());
                            break;
                    }
                }
                ~CTD_SBE_StopLogging(){}

                typedef boost::mpl::list<> local_reactions;

            private:
                void send_stop()
                {
                    // send STOP
                    auto send_stop_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                    send_stop_cmd.set_raw_data(stop_logging_string);

                    this->interprocess().template publish<cgsn::groups::ctd_sbe::raw_out>(send_stop_cmd);
                }

            private:
                const char* stop_logging_string = "STOP\r\n";

            };

            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct CTD_SBE_SensorSleep : boost::statechart::state<Derived, Parent>,
                                         Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                CTD_SBE_SensorSleep(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                            send_sleep();
                            break;

                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            break;
                    }
                }
                ~CTD_SBE_SensorSleep() { }

                typedef boost::mpl::list<> local_reactions;

            private:
                void send_sleep()
                {
                    //send QS to put instrument to sleep
                    const char* sleep_string = "QS\r\n";
                    auto send_sleep_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                    send_sleep_cmd.set_raw_data(sleep_string);

                    this->interprocess().template publish<cgsn::groups::ctd_sbe::raw_out>(send_sleep_cmd);
                }
            };

            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct CTD_SBE_SelfTest : boost::statechart::state<Derived, Parent>,
                                      Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                CTD_SBE_SelfTest(typename StateBase::my_context c) : StateBase(c)
                {
                    // no self test
                    this->post_event(EvSelfTestCompleted());
                }
                ~CTD_SBE_SelfTest() { }

                typedef boost::mpl::list<> local_reactions;
            };
        }
    }
}

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(CTD_SBE_)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
