// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VALIDATOR_BASE_20180315_H
#define VALIDATOR_BASE_20180315_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/config/sensors_config.pb.h"

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"
#include "cgsn-mooring/messages/sensor_status.pb.h"

#include "cgsn-mooring/sensor/validator_statechart/executor.h"

namespace cgsn
{
namespace validator
{
/// \brief Validates the raw data coming from the sensor and the parsed data from the parser
/// \details
/// Publishes:
/// Group                                 | Layer          | Type                                       | Value
/// --------------------------------------|----------------|--------------------------------------------|-------------------------
/// template raw_in_validated_group       | interprocess   | cgsn::protobuf::SensorRaw                  | Raw data that passes an initial validity check and sent to the parser
/// cgsn::groups::sensor::validator_state | interthread    | protobuf::sensor::ValidatorState     | State of the Validator thread
///
/// ----------
/// Subscribes:
/// Group                              | Layer          | Type                         | Value
/// -----------------------------------|----------------|------------------------------|-------------------------
/// template raw_data_in_group         | interthread    | cgsn::protobuf::SensorRaw    | Raw data from sensor
/// template parsed_data_group         | interprocess   | ParsedDataProtobuf           | Parsed GPRMC message
/// cgsn::groups::command::logging     | interthread    | bool                         | Enable/disable logging from the sensor driver commander (main thread)
/// \tparam raw_data_in_group The group to subscribe for raw incoming messages
/// \tparam raw_data_in_validated_group The group to publish for validated raw messages (incoming from the sensor)
/// \tparam ParsedDataProtobuf The protobuf message for the parsed output data
/// \tparam parsed_data_group The group to publish the parsed message to
template <const goby::Group& raw_data_in_group, const goby::Group& raw_data_in_validated_group,
          typename ParsedDataProtobuf, const goby::Group& parsed_data_group>
class ValidatorBaseThread : public goby::SimpleThread<cgsn::protobuf::ValidatorConfig>,
                            ValidatorStateChartExecutor
{
    using Base = goby::SimpleThread<cgsn::protobuf::ValidatorConfig>;

  public:
    ValidatorBaseThread(const cgsn::protobuf::ValidatorConfig& config)
        : Base(config, // loop at 10x faster than the shortest timeout
               10.0 /
                   std::min(std::min(config.bytes_timeout(), config.valid_raw_data_timeout()),
                            config.valid_parsed_data_timeout()) *
                   boost::units::si::hertz),
          ValidatorStateChartExecutor(config, this->interthread())
    {
        this->interthread().template subscribe<raw_data_in_group, protobuf::SensorRaw>(
            [this](const protobuf::SensorRaw& msg) { base_validate_raw(msg); });
        this->interprocess().template subscribe<parsed_data_group, ParsedDataProtobuf>(
            [this](const ParsedDataProtobuf& parsed) { base_validate_parsed(parsed); });
        this->interthread().template subscribe<cgsn::groups::command::logging, bool>(
            [this](const bool& cmd) { this->handle_command(cmd); });

        goby::glog.add_group("validator", goby::common::Colors::lt_blue);
    }

    virtual ~ValidatorBaseThread() {}

  protected:
    /// \brief Validates a raw message
    /// \return true if the message is valid and should be sent to the parser, false otherwise
    virtual bool validate_raw(const cgsn::protobuf::SensorRaw& msg) = 0;
    /// \brief Validates a parsed message
    /// \return true if the parsed message is valid, false otherwise
    virtual bool validate_parsed(const ParsedDataProtobuf& parsed) = 0;

    template <typename ProtobufMessage>
    void validator_populate_header(ProtobufMessage* msg, bool set_time = true)
    {
        cgsn::populate_header(*msg->mutable_header(), "validator", set_time);
    }

  private:
    void loop() override;
    void base_validate_raw(const cgsn::protobuf::SensorRaw& msg);
    void base_validate_parsed(const ParsedDataProtobuf& parsed);
};
} // namespace validator
} // namespace cgsn

template <const goby::Group& raw_data_in_group, const goby::Group& raw_data_in_validated_group,
          typename ParsedDataProtobuf, const goby::Group& parsed_data_group>
void cgsn::validator::ValidatorBaseThread<raw_data_in_group, raw_data_in_validated_group,
                                          ParsedDataProtobuf, parsed_data_group>::loop()
{
    this->check_timeouts();
}

template <const goby::Group& raw_data_in_group, const goby::Group& raw_data_in_validated_group,
          typename ParsedDataProtobuf, const goby::Group& parsed_data_group>
void cgsn::validator::ValidatorBaseThread<
    raw_data_in_group, raw_data_in_validated_group, ParsedDataProtobuf,
    parsed_data_group>::base_validate_raw(const protobuf::SensorRaw& raw)
{
    using namespace goby::common::logger;
    using goby::glog;

    auto mooring_time = raw.header().mooring_time_with_units();
    this->notify_receiving_bytes(mooring_time);

    bool message_valid = validate_raw(raw);
    if (message_valid)
    {
        this->notify_valid_raw_data(mooring_time);

        auto valid_raw = raw;
        // don't overwrite the original mooring time
        validator_populate_header(&valid_raw, false);
        this->interprocess().template publish<raw_data_in_validated_group>(valid_raw);
    }
    else
    {
        this->notify_invalid_raw_data(mooring_time);
    }
}

template <const goby::Group& raw_data_in_group, const goby::Group& raw_data_in_validated_group,
          typename ParsedDataProtobuf, const goby::Group& parsed_data_group>
void cgsn::validator::ValidatorBaseThread<
    raw_data_in_group, raw_data_in_validated_group, ParsedDataProtobuf,
    parsed_data_group>::base_validate_parsed(const ParsedDataProtobuf& parsed)
{
    if (validate_parsed(parsed))
    {
        this->notify_valid_parsed_data(parsed.header().mooring_time_with_units());
    }
}



#endif
