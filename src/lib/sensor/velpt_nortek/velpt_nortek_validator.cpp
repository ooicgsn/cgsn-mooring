// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "velpt_nortek_validator.h"
#include "cgsn-mooring/serial/nortek_data_message.h"

using namespace goby::common::logger;
using goby::glog;

cgsn::validator::VELPT_NORTEK_ValidatorThread::VELPT_NORTEK_ValidatorThread(const protobuf::ValidatorConfig& cfg)
    : ValidatorBaseThread<cgsn::groups::velpt_nortek::raw_in,
                         cgsn::groups::velpt_nortek::raw_in_validated,
                         cgsn::protobuf::VELPT_NORTEK_Data, cgsn::groups::velpt_nortek::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
}

bool cgsn::validator::VELPT_NORTEK_ValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    try
    {
        cgsn::io::NortekDataMessage msg(raw.raw_data());
        return msg.is_valid();
    }
    catch (std::out_of_range& e)
    {
        return false;
    }
}

bool cgsn::validator::VELPT_NORTEK_ValidatorThread::validate_parsed(const protobuf::VELPT_NORTEK_Data& pos)
{
    return false;
}


