// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VELPT_NORTEK_SENSOR_COMMAND_STATECHART_H
#define VELPT_NORTEK_SENSOR_COMMAND_STATECHART_H

#include "cgsn-mooring/sensor/command_statechart/interface.h"
#include "cgsn-mooring/serial/nortek_data_binary_serial.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
struct EvNortekAckAck : boost::statechart::event<EvNortekAckAck>
{
};
struct EvNortekNackNack : boost::statechart::event<EvNortekNackNack>
{
};
struct EvNortekBreakReceived : boost::statechart::event<EvNortekBreakReceived>
{
};

// creates little endian Nortek command
inline std::string nortek_cmd(char a, char b) { return std::string(1, b) + std::string(1, a); }

namespace wait_for_prompt
{
template <typename Parent> struct InterrogateForPrompt;
template <typename Parent> struct ConfirmCommandMode;
} // namespace wait_for_prompt

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_WaitForPrompt
    : boost::statechart::state<Derived, Parent,
                               boost::mpl::list<wait_for_prompt::InterrogateForPrompt<Derived>>>,
      Notify<Derived, state>
{
    using StateBase =
        boost::statechart::state<Derived, Parent,
                                 boost::mpl::list<wait_for_prompt::InterrogateForPrompt<Derived>>>;
    VELPT_NORTEK_WaitForPrompt(typename StateBase::my_context c) : StateBase(c) {}
    ~VELPT_NORTEK_WaitForPrompt()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart") << "~VELPT_NORTEK_WaitForPrompt()"
                       << std::endl;
    }

    typedef boost::mpl::list<> local_reactions;
};

namespace wait_for_prompt
{
template <typename Parent>
struct InterrogateForPrompt : boost::statechart::state<InterrogateForPrompt<Parent>, Parent>,
                              DriverMethodsAccess<InterrogateForPrompt<Parent>>,
                              TimeAccess
{
    using StateBase = boost::statechart::state<InterrogateForPrompt<Parent>, Parent>;
    InterrogateForPrompt(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "*__WAIT_FOR_PROMPT__INTERROGATE_FOR_PROMPT" << std::endl;
        switch (this->cfg().command().op_mode())
        {
                // TODO: Uncomment this when we finalize the op_mode: NORMAL functionality
                // case protobuf::SensorCommandConfig::NORMAL: this->send_break(); break;

            case protobuf::SensorCommandConfig::NORMAL:
                goby::glog.is_die() && goby::glog << "op_mode: NORMAL is not yet implemented."
                                                  << std::endl;
                break;

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvPromptReceived());
                break;
        }
    }

    ~InterrogateForPrompt()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "~*__WAIT_FOR_PROMPT__INTERROGATE_FOR_PROMPT" << std::endl;
    }

    typedef boost::mpl::list<
        boost::statechart::custom_reaction<EvPromptTimeoutExceeded>,
        boost::statechart::transition<EvNortekBreakReceived, ConfirmCommandMode<Parent>>>
        reactions;

    boost::statechart::result react(const EvPromptTimeoutExceeded& ev)
    {
        ++get_prompt_retries_;
        if (get_prompt_retries_ >= max_get_prompt_retries_)
        {
            return this->template transit<on::CriticalFailure>();
        }
        else
        {
            goby::glog.is(goby::common::logger::DEBUG1) &&
                goby::glog << group("sensor_command::statechart") << "Retry " << get_prompt_retries_
                           << std::endl;
            this->send_break();
            return this->discard_event();
        }
    }

  private:
    void send_break()
    {
        auto send_break = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        send_break.set_raw_data("@@@@@@");
        this->interprocess().template publish<cgsn::groups::velpt_nortek::raw_out>(send_break);
        // delay 100ms
        usleep(100 * 1000);

        send_break.set_raw_data("K1W%!Q");
        this->interprocess().template publish<cgsn::groups::velpt_nortek::raw_out>(send_break);
        last_tx_time_ = goby::time::now();
    }

    goby::time::MicroTime last_time() const override { return last_tx_time_; }

  private:
    const int max_get_prompt_retries_{5};
    int get_prompt_retries_{0};
    goby::time::MicroTime last_tx_time_;
};

template <typename Parent>
struct ConfirmCommandMode : boost::statechart::state<ConfirmCommandMode<Parent>, Parent>,
                            DriverMethodsAccess<ConfirmCommandMode<Parent>>,
                            TimeAccess
{
    using StateBase = boost::statechart::state<ConfirmCommandMode<Parent>, Parent>;
    ConfirmCommandMode(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "*__WAIT_FOR_PROMPT__CONFIRM_COMMAND_MODE" << std::endl;

        this->send_command_mode();
    }

    ~ConfirmCommandMode()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "~*__WAIT_FOR_PROMPT__CONFIRM_COMMAND_MODE" << std::endl;
    }

    // WaitForPrompt handles EvPromptReceived
    typedef boost::mpl::list<boost::statechart::custom_reaction<EvPromptTimeoutExceeded>,
                             boost::statechart::custom_reaction<EvNortekAckAck>>
        reactions;

    boost::statechart::result react(const EvPromptTimeoutExceeded& ev)
    {
        ++get_prompt_retries_;
        if (get_prompt_retries_ >= max_get_prompt_retries_)
        {
            return this->template transit<on::CriticalFailure>();
        }
        else
        {
            goby::glog.is(goby::common::logger::DEBUG1) &&
                goby::glog << group("sensor_command::statechart") << "Retry " << get_prompt_retries_
                           << std::endl;
            this->send_command_mode();
            return this->discard_event();
        }
    }

    boost::statechart::result react(const EvNortekAckAck& ev)
    {
        this->post_event(EvPromptReceived());
        return this->discard_event();
    }

  private:
    void send_command_mode()
    {
        auto command_mode = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        command_mode.set_raw_data(nortek_cmd('M', 'C'));
        this->interprocess().template publish<cgsn::groups::velpt_nortek::raw_out>(command_mode);
        last_tx_time_ = goby::time::now();
    }

    goby::time::MicroTime last_time() const override { return last_tx_time_; }

  private:
    const int max_get_prompt_retries_{5};
    int get_prompt_retries_{0};
    goby::time::MicroTime last_tx_time_;
};

} // namespace wait_for_prompt

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_Configure : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    VELPT_NORTEK_Configure(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvConfigured());
    }
    ~VELPT_NORTEK_Configure() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_StartLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    VELPT_NORTEK_StartLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStarted());
    }
    ~VELPT_NORTEK_StartLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_StopLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    VELPT_NORTEK_StopLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStopped());
    }
    ~VELPT_NORTEK_StopLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_SensorSleep : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    VELPT_NORTEK_SensorSleep(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvSensorSleeping());
    }
    ~VELPT_NORTEK_SensorSleep() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct VELPT_NORTEK_SelfTest : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    VELPT_NORTEK_SelfTest(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvSelfTestCompleted());
    }
    ~VELPT_NORTEK_SelfTest() {}

    typedef boost::mpl::list<> local_reactions;
};

} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(VELPT_NORTEK_)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
