// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DOSTA_OPTODE_SENSOR_COMMAND_STATECHART_H
#define DOSTA_OPTODE_SENSOR_COMMAND_STATECHART_H
#include <goby/common/time.h>

#include "cgsn-mooring/config/dosta_optode_config.pb.h"
#include "cgsn-mooring/sensor/command_statechart/autoconfig.h"
#include "cgsn-mooring/sensor/command_statechart/interface.h"
#include "cgsn-mooring/sensor/dosta_optode/dosta_optode_common.h"

namespace cgsn
{
    namespace sensor_command
    {
        namespace statechart
        {
            // '#' from sensor
            struct EvValidCommandResponse : boost::statechart::event<EvValidCommandResponse> { };



            //
            // WAIT FOR PROMPT
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct DOSTA_OPTODE_WaitForPrompt : boost::statechart::state<Derived, Parent>,
                                                Notify<Derived, state>,
                                                TimeAccess
            {
                using StateBase = boost::statechart::state<Derived, Parent>;
                DOSTA_OPTODE_WaitForPrompt(typename StateBase::my_context c) : StateBase(c)
                {
                     switch(this->cfg().command().op_mode())
                     {
                         case protobuf::SensorCommandConfig::NORMAL:
                             this->send_wakeup_string();
                             break;

                         case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                             // passthrough
                             this->post_event(EvPromptReceived());
                             break;
                     }
                }
                ~DOSTA_OPTODE_WaitForPrompt() { }

                typedef boost::mpl::list<
                    boost::statechart::custom_reaction < EvPromptTimeoutExceeded >
                    > local_reactions;

                boost::statechart::result react(const EvPromptTimeoutExceeded& ev)
                {
                    ++get_prompt_retries_;
                    goby::glog.is(goby::common::logger::DEBUG2) && goby::glog << group("sensor_command::statechart") << "DOSTA did not show prompt" << std::endl;
                    if(get_prompt_retries_ >= max_get_prompt_retries_)
                    {
                        return this->template transit<on::CriticalFailure>();
                    }
                    else
                    {
                        goby::glog.is(goby::common::logger::DEBUG1) && goby::glog << group("sensor_command::statechart") << "Retry " << get_prompt_retries_ << std::endl;
                        this->send_wakeup_string();
                        return this->discard_event();
                    }
                }

            private:
                 void send_wakeup_string()
                 {
                     const char* wakeup_string = "Get Product name\r\n";
                     auto send_wakeup = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                     send_wakeup.set_raw_data(wakeup_string);

                     this->interprocess().template publish<cgsn::groups::dosta_optode::raw_out>(send_wakeup);
                     last_wakeup_time_ = goby::time::now();
                 }

                 goby::time::MicroTime last_time() const override
                 {
                     return last_wakeup_time_;
                 }

            private:
                const int max_get_prompt_retries_ {10};
                int get_prompt_retries_ {0};
                goby::time::MicroTime last_wakeup_time_;

            };

            //
            // CONFIGURE
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            using DOSTA_OPTODE_Configure =
                AutoConfigure<Derived, Parent, state, protobuf::DOSTA_OPTODE_SensorConfig,
                              cgsn::groups::dosta_optode::raw_out>;

            //
            // START LOGGING
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct DOSTA_OPTODE_StartLogging : boost::statechart::state<Derived, Parent>,
                                               Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;
                DOSTA_OPTODE_StartLogging(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                        {
                            // publish a message to start a measurement sequence
                            auto enable_start = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                            enable_start.set_raw_data("Start\r\n");
                            this->interprocess().template publish<cgsn::groups::dosta_optode::raw_out>(enable_start);
                            break;
                        }
                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            this->post_event(EvLoggingStarted());
                            break;
                    }
                }
                ~DOSTA_OPTODE_StartLogging() { }

                typedef boost::mpl::list<
                    boost::statechart::custom_reaction<EvValidCommandResponse>
                    > local_reactions;

                boost::statechart::result react(const EvValidCommandResponse& ev)
                {
                    this->post_event(EvLoggingStarted());
                    return this->discard_event();
                }
            };

            //
            // STOP LOGGING
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct DOSTA_OPTODE_StopLogging : boost::statechart::state<Derived, Parent>,
                                              Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                DOSTA_OPTODE_StopLogging(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                        {
                            // publish a message to stop a measurement sequence
                            auto enable_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                            enable_raw.set_raw_data("Stop\r\n");
                            this->interprocess().template publish<cgsn::groups::dosta_optode::raw_out>(enable_raw);
                            break;
                        }
                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            this->post_event(EvLoggingStopped());
                            break;
                    }
                }
                ~DOSTA_OPTODE_StopLogging() { }

                typedef boost::mpl::list<
                    boost::statechart::custom_reaction<EvValidCommandResponse>
                    > local_reactions;

                boost::statechart::result react(const EvValidCommandResponse& ev)
                {
                    this->post_event(EvLoggingStopped());
                    return this->discard_event();
                }
            };


            //
            // SENSOR SLEEP
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct DOSTA_OPTODE_SensorSleep : boost::statechart::state<Derived, Parent>,
                                              Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                DOSTA_OPTODE_SensorSleep(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:
                        {
                            // there is not a command to send to the DOSTA to put it to
                            // sleep; it sleeps automatically based on the Comm Timeout value
                            // (unless it's set to "Always On"
                            // the DOSTA responds with an "%" prior to entering sleep mode, which is caught in the driver
                            // and triggers the EvSensorSleeping event
                            break;
                        }
                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            this->post_event(EvSensorSleeping());
                            break;
                    }
                }
                ~DOSTA_OPTODE_SensorSleep() { }

                typedef boost::mpl::list<> local_reactions;
            };
            

            //
            // SELF TEST
            //
            template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
            struct DOSTA_OPTODE_SelfTest : boost::statechart::state<Derived, Parent>,
                                           Notify<Derived, state>
            {
                using StateBase = boost::statechart::state<Derived, Parent>;

                DOSTA_OPTODE_SelfTest(typename StateBase::my_context c) : StateBase(c)
                {
                    switch(this->cfg().command().op_mode())
                    {
                        case protobuf::SensorCommandConfig::NORMAL:

                            if(this->cfg().command().run_self_test())
                            {
                                auto enable_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                                enable_raw.set_raw_data("Do Test\r\n");
                                this->interprocess().template publish<cgsn::groups::dosta_optode::raw_out>(enable_raw);
                            }
                            else
                            {
                                this->post_event(EvSelfTestCompleted());
                            }
                            break;

                        case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                            this->post_event(EvSelfTestCompleted());
                            break;
                    }
                }
                ~DOSTA_OPTODE_SelfTest() { }

                typedef boost::mpl::list<> local_reactions;
            };

        }
    }
}

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(DOSTA_OPTODE_)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
