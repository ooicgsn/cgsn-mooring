// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VALIDATOR_TEST_FIXTURE_20180406_H
#define VALIDATOR_TEST_FIXTURE_20180406_H

namespace cgsn
{
    /// Objects/functions used for unit testing.
    namespace test
    {
        /// \brief Creates a test fixture class suitable for running with BOOST_FIXTURE_TEST_CASE for the ValidatorThreads
        /// \tparam ValidatorThread The cgsn::validator::ValidatorBaseThread derived class to be tested using this fixture.
        /// \tparam ValidatorConfig The Protobuf config message used by ValidatorThread.
        template<typename ValidatorThread, typename ValidatorConfig>
            struct ValidatorTestFixture
            {
                ValidatorTestFixture() : validator_(cfg_) { }
                ~ValidatorTestFixture() { }

                /// \brief Checks the raw string against ValidatorThread::validate_raw and sees if the result is the same as expected.
                /// \param raw raw data to check
                /// \param expected expected return result of validate_raw() for this raw data string.
                void check_raw(std::string raw, bool expected);

                /// \brief Checks the parsed object against ValidatorThread::validate_parsed and sees if the result is the same as expected.
                /// \param parsed Parsed data to check
                /// \param expected expected return result of validate_parsed().
                /// \tparam ParsedDataProtobuf The Protobuf Message type used by ValidatorThread::validate_parsed()
                template<typename ParsedDataProtobuf>
                void check_parsed(ParsedDataProtobuf parsed, bool expected);

                ValidatorConfig cfg_;
                ValidatorThread validator_;
            };
    }
}

/// macro shorthand for declaring this test fixture as a friend, which is required so it can call validate_raw() and validate_parsed() private methods
#define CGSN_VALIDATOR_TEST_FIXTURE_FRIEND template<typename ValidatorThread, typename ValidatorConfig> friend struct cgsn::test::ValidatorTestFixture;

#endif
