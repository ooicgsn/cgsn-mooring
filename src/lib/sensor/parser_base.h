// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PARSER_BASE_20181315_H
#define PARSER_BASE_20181315_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/config/sensors_config.pb.h"

#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"

namespace cgsn
{
    namespace parser
    {
        /// \brief Base class for parser threads.
        /// \details Handles populating the `raw` (and optionally `time`) of the cgsn::protobuf::SensorCommon message (the field must be named `common` in the Protobuf passed as a template parameter)
        /// Publishes:
        /// Group                         | Layer           | Type                 | Value
        /// ------------------------------|-----------------|----------------------|-------------------------
        /// template parsed_data_group    | interprocess    | ParsedDataProtobuf   | Parsed data
        /// ----------
        /// Subscribes:
        /// Group                         | Layer           | Type                        | Value
        /// ------------------------------|-----------------|-----------------------------|-------------------------
        /// template raw_data_in_group    | interprocess    | cgsn::protobuf::SensorRaw   | input ASCII line
        /// \tparam raw_data_in_group The group to subscribe for raw incoming messages
        /// \tparam ParsedDataProtobuf The protobuf message for the parsed output data
        /// \tparam parsed_data_group The group to publish the parsed message to
        template<const goby::Group& raw_data_in_group, typename ParsedDataProtobuf, const goby::Group& parsed_data_group>
        class LineParserBaseThread : public goby::SimpleThread<cgsn::protobuf::ParserConfig>
        {
            using Base = goby::SimpleThread<cgsn::protobuf::ParserConfig>;
        public:
        LineParserBaseThread(const cgsn::protobuf::ParserConfig& config) :
            Base(config)
            {
                Base::interprocess().template subscribe<raw_data_in_group, cgsn::protobuf::SensorRaw>(
                    [this](const cgsn::protobuf::SensorRaw& raw)
                    {
                        ParsedDataProtobuf parsed;
                        parser_populate_header(&parsed);
                        auto* parsed_header = parsed.mutable_parsed_header();
                        parsed_header->set_raw_data_mooring_time_with_units(raw.header().mooring_time_with_units());
                        parse(raw, &parsed);
                        if(parsed.IsInitialized())
                            Base::interprocess().template publish<parsed_data_group>(parsed);
                    }
                    );
            }
            virtual ~LineParserBaseThread() {}

        protected:
            /// \brief Overridden by the derived parser class to implement parsing this type
            /// \param raw Raw data from the sensor
            /// \param parsed Pointer to the Protobuf object to insert the parsed data into
            virtual void parse(const cgsn::protobuf::SensorRaw& raw, ParsedDataProtobuf* parsed) = 0;

            template<typename ProtobufMessage>
                void parser_populate_header(ProtobufMessage* msg)
            {
                cgsn::populate_header(*msg->mutable_header(), "parser");
            }

        };

    }
}

#endif
