// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PHSEN_PCO2W_SUNBURST_SENSOR_COMMAND_STATECHART_H
#define PHSEN_PCO2W_SUNBURST_SENSOR_COMMAND_STATECHART_H

#include "cgsn-mooring/sensor/command_statechart/interface.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
/// Status message ":XXXX" was received by the Driver
struct EvSAMIStatusMessageReceived : boost::statechart::event<EvSAMIStatusMessageReceived>
{
};

/// No status message received within a certain time by the driver
struct EvSAMIStatusMessageTimeout : boost::statechart::event<EvSAMIStatusMessageTimeout>
{
};

namespace wait_for_prompt
{
template <typename Parent> struct SettingRTSHigh;
template <typename Parent> struct DisablingSAMIStatus;
}; // namespace wait_for_prompt

namespace sensor_sleep
{
template <typename Parent> struct EnablingSAMIStatus;
template <typename Parent> struct SettingRTSLow;
}; // namespace sensor_sleep

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_WaitForPrompt
    : boost::statechart::state<Derived, Parent,
                               boost::mpl::list<wait_for_prompt::SettingRTSHigh<Derived>>>,
      Notify<Derived, state>
{
    using StateBase =
        boost::statechart::state<Derived, Parent,
                                 boost::mpl::list<wait_for_prompt::SettingRTSHigh<Derived>>>;
    PHSEN_PCO2W_SUNBURST_WaitForPrompt(typename StateBase::my_context c) : StateBase(c) {}
    ~PHSEN_PCO2W_SUNBURST_WaitForPrompt() {}

    typedef boost::mpl::list<> local_reactions;
};

namespace wait_for_prompt
{
template <typename Parent>
struct SettingRTSHigh : boost::statechart::state<SettingRTSHigh<Parent>, Parent>,
                        DriverMethodsAccess<SettingRTSHigh<Parent>>,
                        TimeAccess
{
    using StateBase = boost::statechart::state<SettingRTSHigh<Parent>, Parent>;
    SettingRTSHigh(typename StateBase::my_context c) : StateBase(c)
    {
        // We have already set the RTS High and disabled the status in On::WaitForPrompt
        // so don't try to do it again here
        if (std::is_same<Parent, off::WaitForPrompt>::value)
            this->post_event(EvPromptReceived());
        else
            set_rts_high();

        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*__WAIT_FOR_PROMPT__SETTING_RTS_HIGH" << std::endl;
    }
    ~SettingRTSHigh()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "~*__WAIT_FOR_PROMPT__SETTING_RTS_HIGH"
                                             << std::endl;
    }

    boost::statechart::result react(const EvSAMIStatusMessageTimeout& ev)
    {
        // if we haven't gotten a status message, we've haven't woken it up
        if (rts_attempts_ < max_rts_attempts_)
        {
            set_rts_high();
            ++rts_attempts_;
        }
        else
        {
            goby::glog.is_warn() &&
                goby::glog << "Failed to get SAMI Status message after " << rts_attempts_
                           << " attempts to set RTS high. Continuing on anyway." << std::endl;
            this->post_event(EvSAMIStatusMessageReceived());
        }
        return this->discard_event();
    }

    typedef boost::mpl::list<
        boost::statechart::transition<EvSAMIStatusMessageReceived, DisablingSAMIStatus<Parent>>,
        boost::statechart::custom_reaction<EvSAMIStatusMessageTimeout>>
        reactions;

    goby::time::MicroTime last_time() const override { return last_time_; }

  private:
    void set_rts_high()
    {
        goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                             << "Commanding RTS HIGH" << std::endl;
        protobuf::SerialCommand cmd;
        cmd.set_command(protobuf::SerialCommand::RTS_HIGH);
        this->interthread().template publish<cgsn::groups::phsen_pco2w_sunburst::raw_out>(cmd);
        last_time_ = goby::time::now();
    }
    goby::time::MicroTime last_time_;

    const int max_rts_attempts_{3};
    int rts_attempts_{0};
};

template <typename Parent>
struct DisablingSAMIStatus : boost::statechart::state<DisablingSAMIStatus<Parent>, Parent>,
                             DriverMethodsAccess<DisablingSAMIStatus<Parent>>,
                             TimeAccess
{
    using StateBase = boost::statechart::state<DisablingSAMIStatus<Parent>, Parent>;
    DisablingSAMIStatus(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*__WAIT_FOR_PROMPT__DISABLING_STATUS" << std::endl;

        disable_status();
    }
    ~DisablingSAMIStatus()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "~*__WAIT_FOR_PROMPT__DISABLING_STATUS"
                                             << std::endl;
    }
    boost::statechart::result react(const EvSAMIStatusMessageReceived& ev)
    {
        // if we got a status message, it's not disabled yet...
        disable_status();
        return this->discard_event();
    }

    boost::statechart::result react(const EvSAMIStatusMessageTimeout& ev)
    {
        // if we haven't gotten a status message, we've successfully disabled it
        this->post_event(EvPromptReceived());
        return this->discard_event();
    }

    typedef boost::mpl::list<boost::statechart::custom_reaction<EvSAMIStatusMessageReceived>,
                             boost::statechart::custom_reaction<EvSAMIStatusMessageTimeout>>
        reactions;

    goby::time::MicroTime last_time() const override { return last_time_; }

  private:
    void disable_status()
    {
        auto raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        raw.set_raw_data("F5A\r");
        this->interprocess().template publish<cgsn::groups::phsen_pco2w_sunburst::raw_out>(raw);

        last_time_ = goby::time::now();
    }
    goby::time::MicroTime last_time_;
};

} // namespace wait_for_prompt

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_Configure : boost::statechart::state<Derived, Parent>,
                                        Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    PHSEN_PCO2W_SUNBURST_Configure(typename StateBase::my_context c) : StateBase(c)
    {
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
                goby::glog.is_die() && goby::glog << "op_mode: NORMAL is not yet implemented."
                                                  << std::endl;
                break;

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                this->post_event(EvConfigured());
                break;
        }
    }
    ~PHSEN_PCO2W_SUNBURST_Configure() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_StartLogging : boost::statechart::state<Derived, Parent>,
                                           Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    PHSEN_PCO2W_SUNBURST_StartLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStarted());
    }
    ~PHSEN_PCO2W_SUNBURST_StartLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_StopLogging : boost::statechart::state<Derived, Parent>,
                                          Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    PHSEN_PCO2W_SUNBURST_StopLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStopped());
    }
    ~PHSEN_PCO2W_SUNBURST_StopLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_SensorSleep
    : boost::statechart::state<Derived, Parent,
                               boost::mpl::list<sensor_sleep::EnablingSAMIStatus<Derived>>>,
      Notify<Derived, state>
{
    using StateBase =
        boost::statechart::state<Derived, Parent,
                                 boost::mpl::list<sensor_sleep::EnablingSAMIStatus<Derived>>>;

    PHSEN_PCO2W_SUNBURST_SensorSleep(typename StateBase::my_context c) : StateBase(c) {}
    ~PHSEN_PCO2W_SUNBURST_SensorSleep() {}

    typedef boost::mpl::list<> local_reactions;
};

namespace sensor_sleep
{
template <typename Parent>
struct EnablingSAMIStatus : boost::statechart::state<EnablingSAMIStatus<Parent>, Parent>,
                            DriverMethodsAccess<EnablingSAMIStatus<Parent>>,
                            TimeAccess
{
    using StateBase = boost::statechart::state<EnablingSAMIStatus<Parent>, Parent>;
    EnablingSAMIStatus(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*__WAIT_FOR_PROMPT__ENABLING_STATUS" << std::endl;

        enable_status();
    }
    ~EnablingSAMIStatus()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "~*__WAIT_FOR_PROMPT__ENABLING_STATUS" << std::endl;
    }

    boost::statechart::result react(const EvSAMIStatusMessageTimeout& ev)
    {
        // if we haven't gotten a status message, we haven't enabled it...
        enable_status();
        return this->discard_event();
    }

    typedef boost::mpl::list<
        boost::statechart::custom_reaction<EvSAMIStatusMessageTimeout>,
        boost::statechart::transition<EvSAMIStatusMessageReceived, SettingRTSLow<Parent>>>
        reactions;

    goby::time::MicroTime last_time() const override { return last_time_; }

  private:
    void enable_status()
    {
        auto raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        raw.set_raw_data("F01\r");
        this->interprocess().template publish<cgsn::groups::phsen_pco2w_sunburst::raw_out>(raw);
        last_time_ = goby::time::now();
    }

    goby::time::MicroTime last_time_;

}; // namespace sensor_sleep

template <typename Parent>
struct SettingRTSLow : boost::statechart::state<SettingRTSLow<Parent>, Parent>,
                       DriverMethodsAccess<SettingRTSLow<Parent>>,
                       TimeAccess
{
    using StateBase = boost::statechart::state<SettingRTSLow<Parent>, Parent>;
    SettingRTSLow(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*__WAIT_FOR_PROMPT__SETTING_RTS_LOW" << std::endl;

        set_rts_low();
    }
    ~SettingRTSLow()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "~*__WAIT_FOR_PROMPT__SETTING_RTS_LOW" << std::endl;
    }

    boost::statechart::result react(const EvSAMIStatusMessageTimeout& ev)
    {
        // if we haven't gotten a status message, we've successfully put it to sleep
        this->post_event(EvSensorSleeping());
        return this->discard_event();
    }

    boost::statechart::result react(const EvSAMIStatusMessageReceived& ev)
    {
        if (rts_attempts_ < max_rts_attempts_)
        {
            // if we got a status message, it's not asleep yet
            set_rts_low();
            ++rts_attempts_;
        }
        else
        {
            goby::glog.is_warn() &&
                goby::glog << "Failed to stop SAMI Status message after " << rts_attempts_
                           << " attempts to set RTS low. Continuing on anyway." << std::endl;
            this->post_event(EvSAMIStatusMessageTimeout());
        }

        return this->discard_event();
    }

    typedef boost::mpl::list<boost::statechart::custom_reaction<EvSAMIStatusMessageReceived>,
                             boost::statechart::custom_reaction<EvSAMIStatusMessageTimeout>>
        reactions;

    goby::time::MicroTime last_time() const override { return last_time_; }

  private:
    void set_rts_low()
    {
        goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                             << "Commanding RTS LOW" << std::endl;
        protobuf::SerialCommand cmd;
        cmd.set_command(protobuf::SerialCommand::RTS_LOW);
        this->interthread().template publish<cgsn::groups::phsen_pco2w_sunburst::raw_out>(cmd);

        last_time_ = goby::time::now();
    }
    goby::time::MicroTime last_time_;

    const int max_rts_attempts_{3};
    int rts_attempts_{0};
};

} // namespace sensor_sleep

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct PHSEN_PCO2W_SUNBURST_SelfTest : boost::statechart::state<Derived, Parent>,
                                       Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    PHSEN_PCO2W_SUNBURST_SelfTest(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvSelfTestCompleted());
    }
    ~PHSEN_PCO2W_SUNBURST_SelfTest() {}

    typedef boost::mpl::list<> local_reactions;
};

} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(PHSEN_PCO2W_SUNBURST_)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
