// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SENSORS_UNITS_20180621_H
#define SENSORS_UNITS_20180621_H

#include <boost/units/unit.hpp>
#include <boost/units/systems/si/pressure.hpp>

namespace cgsn
{
  namespace units
  {
    struct psi_base_unit : boost::units::base_unit<psi_base_unit, boost::units::pressure_dimension, 1> { };
  }
}

BOOST_UNITS_DEFINE_CONVERSION_FACTOR(cgsn::units::psi_base_unit, boost::units::si::pressure, double, 6894.7572931783);
BOOST_UNITS_DEFAULT_CONVERSION(cgsn::units::psi_base_unit, si::pressure);

namespace boost
{
  namespace units
  {

    template<> struct base_unit_info<cgsn::units::psi_base_unit>
      {
	static std::string name()   { return "pound-force per square inch"; }
	static std::string symbol() { return "lbf/in^2"; }
      };
  }
}


#endif

