// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VALIDATOR_TEST_FIXTURE_IMPL_20180406_H
#define VALIDATOR_TEST_FIXTURE_IMPL_20180406_H

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"

template<typename ValidatorThread, typename ValidatorConfig>
    void cgsn::test::ValidatorTestFixture<ValidatorThread, ValidatorConfig>::check_raw(std::string raw, bool expected)
{
    cgsn::protobuf::SensorRaw raw_msg;
    raw_msg.set_raw_data(raw);
    populate_header(*raw_msg.mutable_header());
    BOOST_CHECK_MESSAGE(validator_.validate_raw(raw_msg) == expected,
                        "check_raw (expected: " << std::boolalpha <<
                        expected << ") for \"" << raw << "\"");
}

template<typename ValidatorThread, typename ValidatorConfig>
    template<typename ParsedDataProtobuf>
    void cgsn::test::ValidatorTestFixture<ValidatorThread, ValidatorConfig>::check_parsed(ParsedDataProtobuf parsed, bool expected)
{
    populate_header(*parsed.mutable_header());

    BOOST_CHECK_MESSAGE(validator_.validate_parsed(parsed) == expected,
                        "check_parsed (expected: " <<
                        std::boolalpha << expected << ") for " <<
                        parsed.ShortDebugString());
}

#endif
