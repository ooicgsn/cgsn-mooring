// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SIMULATOR_BASE_H
#define SIMULATOR_BASE_H

#include <goby/middleware/log.h>
#include <goby/common/time.h>
#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/serial/ascii_line_based_serial.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"

namespace cgsn
{
    /// \brief Base class for sensor simulators
    /// \details
    /// Publishes (see also publish/subscribe for base class):
    /// Group                              | Layer           | Type                         | Value
    /// -----------------------------------|-----------------|------------------------------|-------------------------
    /// cgsn::groups::sim::raw_out         | interthread     | cgsn::protobuf::SensorRaw    | Raw data from log
    /// ----------
    /// Subscribes:
    /// \tparam Config The driver configuration (Protobuf) type
    template<typename Config, const goby::Group& raw_data_in_validated_group>
        class SimulatorBase : public goby::MultiThreadStandaloneApplication<Config>
        {
        public:
        SimulatorBase() :
            goby::MultiThreadStandaloneApplication<Config>(50*boost::units::si::hertz),
                log_(this->cfg().log().c_str()),
                start_wall_time_(goby::time::now()),
                start_log_time_(goby::time::MicroTime::from_value(-1))
                {
                    using goby::glog;
                    using namespace goby::common::logger;
                    if(!log_.is_open())
                        glog.is(DIE) && glog << "Failed to open log: " << this->cfg().log() << std::endl;

                    this->template launch_thread<cgsn::io::AsciiLineSerialThread<cgsn::groups::sim::raw_in, cgsn::groups::sim::raw_out>>(this->cfg().serial());
                }

        protected:
            virtual void simulator_loop() = 0;
            void set_is_logging(bool logging) { logging_ = logging; }

        private:
            void loop() override
            {
                using goby::glog;
                using namespace goby::common::logger;

                simulator_loop();

                auto now = goby::time::now();
                auto wall_time_delta = (now - start_wall_time_);

                if(next_raw_.IsInitialized())
                {
                    if(wall_time_delta >= next_log_time_delta_)
                        publish_next();
                    else
                        return;
                }

                // find a new raw entry
                try
                {
                    while(1)
                    {
                        goby::LogEntry log_entry;
                        log_entry.parse(&log_);

                        if((log_entry.scheme() == goby::MarshallingScheme::DCCL || log_entry.scheme() == goby::MarshallingScheme::PROTOBUF) &&
                           log_entry.type() == protobuf::SensorRaw::descriptor()->full_name() &&
                           log_entry.group() == raw_data_in_validated_group)
                        {
                            next_raw_.ParseFromArray(&log_entry.data()[0], log_entry.data().size());

                            if(start_log_time_ < 0*typename decltype(start_log_time_)::unit_type())
                                start_log_time_ = next_raw_.header().mooring_time_with_units();

                            next_log_time_delta_ = (next_raw_.header().mooring_time_with_units() - start_log_time_);
                            if(wall_time_delta >= next_log_time_delta_)
                                publish_next();
                            else
                                break;
                        }
                    }

                }
                catch(std::exception& e)
                {
                    if(!log_.eof())
                        glog.is(WARN) && glog << "Error while reading log file." << std::endl;
                    this->quit();
                }
            }

            void publish_next()
            {
                using goby::glog;
                using namespace goby::common::logger;

                if(logging_)
                {
                    glog.is(DEBUG1) && glog << next_raw_.raw_data() << std::endl;
                    this->interthread().template publish<cgsn::groups::sim::raw_out>(next_raw_);
                }
                next_raw_.Clear();
            }


        private:
            std::ifstream log_;
            goby::time::MicroTime start_wall_time_;
            goby::time::MicroTime start_log_time_;

            goby::time::MicroTime next_log_time_delta_{0};
            protobuf::SensorRaw next_raw_;
            bool logging_{false};

        };

}


#endif
