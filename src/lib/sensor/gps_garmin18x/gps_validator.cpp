// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "gps_validator.h"

#include <goby/util/linebasedcomms/nmea_sentence.h>

#include <boost/units/io.hpp>

using namespace goby::common::logger;
using goby::glog;

// Validator
cgsn::validator::GPSValidatorThread::GPSValidatorThread(const protobuf::ValidatorConfig& cfg)
    : ValidatorBaseThread<cgsn::groups::gps::raw_in,
                         cgsn::groups::gps::raw_in_validated,
                         cgsn::protobuf::GPSPosition, cgsn::groups::gps::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
}

bool cgsn::validator::GPSValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    // for this simple example, valid data is a RMC sentence and passes the various goby::util::NMEASentence constructor checks
    // (requires valid checksum, etc.)
    try
    {
        goby::util::NMEASentence nmea(raw.raw_data(), goby::util::NMEASentence::REQUIRE);

        if(nmea.sentence_id() == "RMC")
            return true;
    }
    catch(goby::util::bad_nmea_sentence& e)
    {
        glog.is(WARN) && glog << group("validator::gps") << "Invalid NMEA sentence: " << e.what() << std::endl;
    }
    return false;
}

bool cgsn::validator::GPSValidatorThread::validate_parsed(const protobuf::GPSPosition& pos)
{
    glog.is(VERBOSE) && glog << group("validator::gps") << "Received position: sensor time: "
                             << std::setprecision(std::numeric_limits<dccl::int64>::digits10)
                             << pos.parsed_header().sensor_time_with_units()
                             << " (= "
                             << std::setprecision(std::numeric_limits<double>::digits10)
                             << boost::units::quantity<boost::units::si::time>(pos.parsed_header().sensor_time_with_units())
                             << ")"
                             << std::setprecision(std::numeric_limits<double>::digits10)
                             << ", latitude: " << pos.latitude_with_units()
                             << ", longitude: " << pos.longitude_with_units() << std::endl;


    bool parsed_data_valid = (pos.fix_valid() && !std::isnan(pos.latitude()) && !std::isnan(pos.longitude()));
    return parsed_data_valid;
}


