// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef GPS_SENSOR_COMMAND_STATECHART_20181316_H
#define GPS_SENSOR_COMMAND_STATECHART_20181316_H

#include <goby/common/time.h>
#include <goby/util/linebasedcomms/nmea_sentence.h>

#include "cgsn-mooring/sensor/command_statechart/interface.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
// indicates the GPS is not configured as desired ($PGRMC), and includes the desired config
struct EvNotConfiguredPhase1 : boost::statechart::event<EvNotConfiguredPhase1>
{
    EvNotConfiguredPhase1(goby::util::NMEASentence n) : nmea(n) {}
    goby::util::NMEASentence nmea; // $PGRMC
};

// GPS is not configured as desired ($PGRMC1)
struct EvNotConfiguredPhase2 : boost::statechart::event<EvNotConfiguredPhase2>
{
    EvNotConfiguredPhase2(goby::util::NMEASentence n) : nmea(n) {}
    goby::util::NMEASentence nmea; // $PGRMC1
};

struct EvConfiguredPhase1 : boost::statechart::event<EvConfiguredPhase1>
{
};
using EvConfiguredPhase2 = EvConfigured;

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSWaitForPrompt : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    GPSWaitForPrompt(typename StateBase::my_context c) : StateBase(c)
    {
        // use Sensor Initialization Information query as proxy for querying prompt
        // this isn't necessary for the GPS, but it gives us a message to demonstrate with
        goby::util::NMEASentence query_nmea("$PGRMIE");
        auto query_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        query_raw.set_raw_data(query_nmea.message_cr_nl());

        this->interprocess().template publish<cgsn::groups::gps::raw_out>(query_raw);
    }
    ~GPSWaitForPrompt() {}

    typedef boost::mpl::list<> local_reactions;
};

namespace configure
{
// demonstrate having child states just for a particular driver
template <typename Parent> struct GPSPhase1;
template <typename Parent> struct GPSPhase2;
}; // namespace configure

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSConfigure
    : boost::statechart::state<Derived, Parent, boost::mpl::list<configure::GPSPhase1<Derived>>>,
      Notify<Derived, state>
{
    using StateBase =
        boost::statechart::state<Derived, Parent, boost::mpl::list<configure::GPSPhase1<Derived>>>;
    GPSConfigure(typename StateBase::my_context c) : StateBase(c) {}
    ~GPSConfigure() {}

    typedef boost::mpl::list<> local_reactions;
};

namespace configure
{
template <typename Derived, typename Parent, int phase>
struct GPSBase : boost::statechart::state<Derived, Parent>, DriverMethodsAccess<Derived>
{
    static_assert(phase == 1 || phase == 2, "GPS configure has only 2 phases");

    using StateBase = boost::statechart::state<Derived, Parent>;

    GPSBase(typename StateBase::my_context c)
        : StateBase(c), max_configuration_attempts_(this->cfg().command().max_cfg_attempts())
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart") << "*__CONFIGURE__GPSPHASE" << phase
                       << std::endl;

        // query configuration first
        std::string query_talker(phase == 1 ? "$PGRMCE" : "$PGRMC1E");
        goby::util::NMEASentence query_nmea(query_talker);

        auto query_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        query_raw.set_raw_data(query_nmea.message_cr_nl());

        this->interprocess().template publish<cgsn::groups::gps::raw_out>(query_raw);
    }
    ~GPSBase()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart") << "~*__CONFIGURE__GPSPHASE" << phase
                       << std::endl;
    }

    template <typename EvNotConfiguredType, typename EvConfiguredType>
    boost::statechart::result base_react(const EvNotConfiguredType& ev)
    {
        using goby::glog;
        using namespace goby::common::logger;
        if (configuration_attempts_ < max_configuration_attempts_)
        {
            auto cfg_set_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
            cfg_set_raw.set_raw_data(ev.nmea.message_cr_nl());

            glog.is(DEBUG2) && glog << group("gps::sensor_command")
                                    << "Setting configuration to: " << ev.nmea.message()
                                    << std::endl;
            this->interprocess().template publish<cgsn::groups::gps::raw_out>(cfg_set_raw);
            ++configuration_attempts_;

            // we have handled this event, so discard it to avoid further processing by outer states
            return this->discard_event();
        }
        else
        {
            glog.is(WARN) &&
                glog << group("gps::sensor_command") << "Failed to set configuration after "
                     << configuration_attempts_ << " attempts. "
                     << "Will continue on in hopes of still getting valid data." << std::endl;
            this->post_event(EvConfiguredType());
            return this->discard_event();
        }
    }

  private:
    int configuration_attempts_{0};
    const int max_configuration_attempts_;
};

// handle $PGRMC
template <typename Parent> struct GPSPhase1 : GPSBase<GPSPhase1<Parent>, Parent, 1>
{
    using Base = GPSBase<GPSPhase1<Parent>, Parent, 1>;
    GPSPhase1(typename Base::my_context c) : Base(c) {}
    ~GPSPhase1() {}

    typedef boost::mpl::list<
        boost::statechart::custom_reaction<EvNotConfiguredPhase1>,
        boost::statechart::transition<EvConfiguredPhase1, configure::GPSPhase2<Parent>>>
        reactions;

    boost::statechart::result react(const EvNotConfiguredPhase1& ev)
    {
        return this->template base_react<EvNotConfiguredPhase1, EvConfiguredPhase1>(ev);
    }
};

// handle $PGRMC1
template <typename Parent> struct GPSPhase2 : GPSBase<GPSPhase2<Parent>, Parent, 2>
{
    using Base = GPSBase<GPSPhase2<Parent>, Parent, 2>;
    GPSPhase2(typename Base::my_context c) : Base(c) {}
    ~GPSPhase2() {}

    typedef boost::mpl::list<boost::statechart::custom_reaction<EvNotConfiguredPhase2>> reactions;

    boost::statechart::result react(const EvNotConfiguredPhase2& ev)
    {
        return this->template base_react<EvNotConfiguredPhase2, EvConfiguredPhase2>(ev);
    }
};

} // namespace configure

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSStartLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    GPSStartLogging(typename StateBase::my_context c) : StateBase(c)
    {
        // publish a message to enable GPRMC sentences
        goby::util::NMEASentence enable_nmea("$PGRMO");
        enable_nmea.push_back("GPRMC");
        enable_nmea.push_back(nmea::PGRMO::ENABLE_SPECIFIED_SENTENCE);

        auto enable_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        enable_raw.set_raw_data(enable_nmea.message_cr_nl());

        this->interprocess().template publish<cgsn::groups::gps::raw_out>(enable_raw);
    }
    ~GPSStartLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSStopLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    GPSStopLogging(typename StateBase::my_context c) : StateBase(c)
    {
        // publish a message to disable all sentences
        goby::util::NMEASentence disable_nmea("$PGRMO");
        disable_nmea.push_back("");
        disable_nmea.push_back(nmea::PGRMO::DISABLE_ALL);
        auto disable_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        disable_raw.set_raw_data(disable_nmea.message_cr_nl());

        this->interprocess().template publish<cgsn::groups::gps::raw_out>(disable_raw);
    }
    ~GPSStopLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSSensorSleep : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    GPSSensorSleep(typename StateBase::my_context c) : StateBase(c)
    {
        // no battery, so no power save mode
        this->post_event(EvSensorSleeping());
    }
    ~GPSSensorSleep() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct GPSSelfTest : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    GPSSelfTest(typename StateBase::my_context c) : StateBase(c)
    {
        // no self test
        this->post_event(EvSelfTestCompleted());
    }
    ~GPSSelfTest() {}

    typedef boost::mpl::list<> local_reactions;
};

} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(GPS)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
