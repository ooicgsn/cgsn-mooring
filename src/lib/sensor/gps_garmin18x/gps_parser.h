// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef GPS_PARSER_20171109_H
#define GPS_PARSER_20171109_H

#include <boost/units/systems/si/prefixes.hpp>

#include <goby/util/primitive_types.h>

#include "cgsn-mooring/sensor/parser_base.h"
#include "gps_common.h"

namespace cgsn
{
    namespace parser
    {
        /// \brief Parses GPS strings into Protobuf equivalent
        /// \details
        /// Publishes (see also cgsn::parser::LineParserBaseThread):
        /// Group                        | Layer              | Type                         | Value
        /// -----------------------------|--------------------|------------------------------|-------------------------
        /// cgsn::groups::ready          | interthread        | cgsn::DriverModule           | Thread has subscribed and is ready for data
        ///
        /// ----------
        /// Subscribes (see also cgsn::parser::LineParserBaseThread)
        /// (none other than base class)
        class GPSParserThread : public LineParserBaseThread<cgsn::groups::gps::raw_in_validated, cgsn::protobuf::GPSPosition, cgsn::groups::gps::parsed>
        {
            using Base = LineParserBaseThread<cgsn::groups::gps::raw_in_validated, cgsn::protobuf::GPSPosition, cgsn::groups::gps::parsed>;

        public:
            GPSParserThread(const cgsn::protobuf::ParserConfig& config);
        private:
            /// \brief Parses line into GPSPosition (`common.raw` is populated by the base class)
            void parse(const cgsn::protobuf::SensorRaw& raw, cgsn::protobuf::GPSPosition* gps) override;

            // given a time and date in "NMEA form", returns the value as microseconds since the start of the epoch (1970-01-01 00:00:00Z)
            // NMEA form for time is HHMMSS where "H" is hours, "M" is minutes, "S" is seconds or fractional seconds
            // NMEA form for date is DDMMYY where "D" is day, "M" is month, "Y" is year

            using Quantity_of_Microseconds = boost::units::quantity<decltype(boost::units::si::micro*boost::units::si::seconds), std::int64_t>;
            Quantity_of_Microseconds nmea_time_to_microseconds(int nmea_time, int nmea_date);

            // given a latitude or longitude in "NMEA form" and the hemisphere character ('N', 'S', 'E' or 'W'), returns the value as decimal degrees
            // NMEA form is DDDMM.MMMM or DDMM.MMMM where "D" is degrees, and "M" is minutes
            using Quantity_of_Degrees = boost::units::quantity<boost::units::degree::plane_angle>;
            Quantity_of_Degrees nmea_geo_to_degrees(double nmea_geo, char hemi);

        };
    }
}



#endif
