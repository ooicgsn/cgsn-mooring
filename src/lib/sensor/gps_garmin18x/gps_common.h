// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef GPS_COMMON20171118_H
#define GPS_COMMON20171118_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/gps.pb.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"

#include "cgsn-mooring/config/gps_garmin18x_config.pb.h"

#include "cgsn-mooring/sensor/driver_modules.h"



namespace cgsn
{
    namespace nmea
    {
        namespace PGRMO
        {
            enum Fields
            {
                TARGET_SENTENCE_DESCRIPTION = 1,
                TARGET_SENTENCE_MODE = 2
            };

            enum Mode
            {
                DISABLE_SPECIFIED_SENTENCE = 0,
                ENABLE_SPECIFIED_SENTENCE = 1,
                DISABLE_ALL = 2,
                ENABLE_ALL = 3,
                RESTORE_DEFAULTS = 4
            };
        }
        namespace PGRMC
        {
            enum Fields
            {
                FIX_MODE = 1,
                ALTITUDE_RELATIVE_MEAN_SEA_LEVEL = 2,
                EARTH_DATUM_INDEX = 3,
                EARTH_DATUM_SEMI_MAJOR_AXIS = 4,
                EARTH_DATUM_INVERSE_FLATTENING_FACTOR = 5,
                EARTH_DATUM_DELTA_X_EARTH_CENTERED_COORDINATE = 6,
                EARTH_DATUM_DELTA_Y_EARTH_CENTERED_COORDINATE = 7,
                EARTH_DATUM_DELTA_Z_EARTH_CENTERED_COORDINATE = 8,
                DIFFERENTIAL_MODE = 9,
                NMEA_0183_BAUD_RATE = 10,
                FIELD11_UNUSED = 11,
                MEASUREMENT_PULSE_OUTPUT = 12,
                MEASUREMENT_PULSE_LENGTH = 13,
                DEAD_RECKONING = 14,
                SIZE = 15
            };

            enum MeasurementPulseOutput
            {
                MEASUREMENT_PULSE_DISABLED = 1,
                MEASUREMENT_PULSE_ENABLED = 2,
            };
        }

        namespace PGRMC1
        {
            enum Fields
            {
                NMEA_0183_OUTPUT_TIME = 1,
                BINARY_OUTPUT_DATA = 2,
                FIELD3_UNUSED = 3,
                DGPS_BEACON_FREQUENCY = 4,
                DGPS_BEACON_BIT_RATE = 5,
                DGPS_BEACON_SCANNING = 6,
                NMEA_0183_VERSION_2_30_MODE_INDICATOR = 7,
                DGPS_WAAS_MODE = 8,
                POWER_SAVE_MODE = 9,
                FIELD10_UNUSED = 10,
                FIELD11_UNUSED = 11,
                FIELD12_UNUSED = 12,
                MEASUREMENT_PULSE_OUTPUT_AUTO_OFF_MODE = 13,
                FIELD14_UNUSED = 14,
                LOW_VELOCITY_THRESHOLD = 15,
                SIZE = 16
            };

        }

    }

}



#endif
