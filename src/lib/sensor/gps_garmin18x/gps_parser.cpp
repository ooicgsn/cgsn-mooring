// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <limits>

#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/angle/degrees.hpp>

#include <goby/util/linebasedcomms/nmea_sentence.h>

#include "gps_parser.h"

using namespace goby::common::logger;
using goby::glog;

// Parser
cgsn::parser::GPSParserThread::GPSParserThread(const protobuf::ParserConfig& config)
    : Base(config)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::PARSER);
}

void cgsn::parser::GPSParserThread::parse(const protobuf::SensorRaw& raw, cgsn::protobuf::GPSPosition* gps)
{
    try
    {
        goby::util::NMEASentence nmea(raw.raw_data());

        if(nmea.sentence_id() == "RMC")
        {
            glog.is(VERBOSE) && glog << "[Parser]: Parsing RMC: " << raw.raw_data() << std::flush;

            enum RMCFields {
                TALKER = 0,
                UTC = 1,
                VALIDITY = 2,
                LAT = 3,
                LAT_HEMI = 4,
                LON = 5,
                LON_HEMI = 6,
                SOG_KNOTS = 7,
                TRACK_DEGREES = 8,
                UT_DATE = 9,
                MAG_VAR_DEG = 10,
                MAG_VAR_HEMI = 11,
                RMC_SIZE = 12};

            if(nmea.size() < RMC_SIZE)
                throw(goby::util::bad_nmea_sentence("Message too short"));

            gps->mutable_parsed_header()->set_sensor_time_with_units(nmea_time_to_microseconds(nmea.as<float>(UTC), nmea.as<int>(UT_DATE)));
            gps->set_fix_valid(nmea.as<char>(VALIDITY) == 'A');

            if(gps->fix_valid())
            {
                gps->set_latitude_with_units(nmea_geo_to_degrees(nmea.as<double>(LAT), nmea.as<char>(LAT_HEMI)));
                gps->set_longitude_with_units(nmea_geo_to_degrees(nmea.as<double>(LON), nmea.as<char>(LON_HEMI)));
            }

            glog.is(DEBUG1) && glog << "[Parser]: Parsed message is: " << gps->ShortDebugString() << std::endl;
        }
        else
        {
            glog.is(VERBOSE) && glog << "[Parser]: Ignoring sentence: " << raw.raw_data() << std::flush;
        }
    }
    catch(goby::util::bad_nmea_sentence& e)
    {
        glog.is(WARN) && glog << "[Parser]: Invalid NMEA sentence: " << e.what() << std::endl;
    }
}

cgsn::parser::GPSParserThread::Quantity_of_Microseconds cgsn::parser::GPSParserThread::nmea_time_to_microseconds(int nmea_time, int nmea_date)
{
    namespace si = boost::units::si;
    static const auto microseconds = si::micro*si::seconds;

    using namespace boost::posix_time;
    using namespace boost::gregorian;
    ptime unix_epoch(date(1970,1,1),time_duration(0,0,0));

    int hours = nmea_time / 10000;
    nmea_time -= hours*10000;
    int minutes = nmea_time / 100;
    nmea_time -= minutes*100;
    int seconds = nmea_time;

    int day = nmea_date / 10000;
    nmea_date -= day*10000;
    int month = nmea_date / 100;
    nmea_date -= month*100;
    int year = nmea_date;

    try
    {
        ptime given_time(date((year < 70) ? (year + 2000) : (year + 1900), month, day),
                         time_duration(hours, minutes, seconds));

        if (given_time == not_a_date_time)
        {
            return -1*microseconds;
        }
        else
        {
            const int microsec_in_sec = 1000000;

            date_duration date_diff = given_time.date() - date(1970,1,1);
            time_duration time_diff = given_time.time_of_day();

            return
                (static_cast<std::int64_t>(date_diff.days())*24*3600*microsec_in_sec +
                static_cast<std::int64_t>(time_diff.total_seconds())*microsec_in_sec +
                static_cast<std::int64_t>(time_diff.fractional_seconds()) /
                 (time_duration::ticks_per_second() / microsec_in_sec)) *microseconds;
        }
    }
    catch(std::exception& e)
    {
        return -1*microseconds;
    }
}


cgsn::parser::GPSParserThread::Quantity_of_Degrees cgsn::parser::GPSParserThread::nmea_geo_to_degrees(double nmea_geo, char hemi)
{
    // DDMM.MMMM
    double deg_int = std::floor(nmea_geo / 1e2);
    double deg_frac = (nmea_geo - (deg_int * 1e2)) / 60;

    double deg = std::numeric_limits<double>::quiet_NaN();

    if(hemi == 'N' || hemi == 'E')
        deg = (deg_int + deg_frac);
    else if(hemi == 'S' || hemi == 'W')
        deg = -(deg_int + deg_frac);

    return deg*boost::units::degree::degrees;
}
