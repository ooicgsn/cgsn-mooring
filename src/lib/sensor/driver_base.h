// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DRIVER_BASE_H
#define DRIVER_BASE_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/health.h"

#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/module_health.pb.h"
#include "cgsn-mooring/messages/scheduler.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"
#include "cgsn-mooring/sensor/command_statechart/interface.h"

#define CGSN_SENSOR_DRIVER_MAIN(Prefix)                                                \
    int main(int argc, char* argv[])                                                   \
    {                                                                                  \
        return goby::run<cgsn::Prefix##Driver>(                                        \
            cgsn::DriverProtobufConfigurator<cgsn::Prefix##Configurator>(argc, argv)); \
    }

namespace cgsn
{
/// \brief Helper function for creating a Protobuf message with the Header already correctly filled out for the Driver to publish
template <typename ProtobufMessage> ProtobufMessage driver_make_with_header()
{
    return cgsn::make_with_header<ProtobufMessage>("main/control");
}

/// \brief Performs post-launch final configuration for drivers. Not implemented as a direct subclass of DriverProtobufConfigurator so that we can call virtual methods via finalize_configuration()
/// \details
/// Performs any runtime configuration modifications specific to this driver (in addition or in-place of those in the YAML->PB configuration. Sets the validator timeouts based on the sampling interval
template <typename Config> class DriverConfigurator
{
  public:
    DriverConfigurator() {}

    using ConfigType = Config;

    void finalize_configuration(Config& cfg)
    {
        finalize_validator_cfg(*cfg.mutable_validator(), cfg);
        finalize_serial_cfg(*cfg.mutable_serial(), cfg);
        finalize_command_cfg(*cfg.mutable_command(), cfg);
    }

  private:
    /// \brief Provide the configured sampling interval for the sensor
    virtual goby::time::MicroTime sampling_interval(const Config& cfg) const = 0;

    /// \brief Finalize the validator configuration. The default implementation sets the validator timeouts to multiples of the sampling_interval()
    virtual void finalize_validator_cfg(protobuf::ValidatorConfig& validator_cfg, const Config& cfg)
    {
        // Declare raw data timeout after this multiple of the expected sampling interval
        const float timeout_padding = 1.5;

        const boost::units::quantity<cgsn::protobuf::ValidatorConfig::valid_raw_data_timeout_unit,
                                     float>
            expected_interval(sampling_interval(cfg));

        validator_cfg.set_bytes_timeout_with_units(timeout_padding * expected_interval);
        validator_cfg.set_valid_raw_data_timeout_with_units(
            validator_cfg.bytes_timeout_with_units());

        // Declare parsed data timeout after this multiple of the raw data timeout
        const float parsed_data_allowed_multiple = 2;
        auto raw_data_timeout =
            validator_cfg.template valid_raw_data_timeout_with_units<boost::units::quantity<
                cgsn::protobuf::ValidatorConfig::valid_raw_data_timeout_unit, float>>();
        validator_cfg.set_valid_parsed_data_timeout_with_units(parsed_data_allowed_multiple *
                                                               raw_data_timeout);
    }

    /// \brief Implement to modify the serial configuration at run time
    /// \param serial_cfg Mutable serial configuration object
    /// \param cfg Const complete configuration object for reference
    virtual void finalize_serial_cfg(protobuf::SerialConfig& serial_cfg, const Config& cfg) {}

    /// \brief Implement to modify the command configuration at run time
    /// \param command_cfg Mutable command configuration object
    /// \param cfg Const complete configuration object for reference
    virtual void finalize_command_cfg(protobuf::SensorCommandConfig& command_cfg, const Config& cfg)
    {
    }
};

/// \brief Implements goby::common::ProtobufConfigurator to call DriverConfigurator
template <typename DriverConfigurator>
class DriverProtobufConfigurator
    : public goby::common::ProtobufConfigurator<typename DriverConfigurator::ConfigType>
{
  public:
    DriverProtobufConfigurator(int argc, char* argv[])
        : goby::common::ProtobufConfigurator<typename DriverConfigurator::ConfigType>(argc, argv)
    {
        DriverConfigurator driver_cfgtor;
        driver_cfgtor.finalize_configuration(this->mutable_cfg());

        // set the instrument key for all future publishers from any thread
        cgsn::system::instrument_key(this->cfg().instrument_key());
    }
};

/// \brief Base class for sensor drivers
/// \details
/// Publishes (see also publish/subscribe for base class):
/// Group                                  | Layer           | Type                                       | Value
/// ---------------------------------------|-----------------|--------------------------------------------|-------------------------
/// cgsn::groups::command::logging         | interthread     | bool                                       | Enable/disable logging
/// cgsn::groups::sensor::command_status   | interthread     | protobuf::sensor::CommandStatus | Status of the SensorCommand (main) thread
/// cgsn::groups::supervisor::mpic_command | interprocess    | cgsn::protobuf::MPICCommand                | Turn port on (with desired power config) or turn port off
/// cgsn::groups::sensor::driver_status                   | interprocess    | cgsn::protobuf::sensor::DriverStatus               | Aggregated status of the driver
/// ----------
/// Subscribes:
/// Group                                 | Layer          | Type                                       | Value
/// --------------------------------------|----------------|--------------------------------------------|-------------------------
/// cgsn::groups::ready                   | interthread    | cgsn::DriverModule                         | Module is ready
/// cgsn::groups::scheduler::command      | interprocesss  | cgsn::protobuf::SchedulerCommand           | External control of the driver
/// cgsn::groups::supervisor::status | interprocess   | cgsn::protobuf::SupervisorStatus                | Supervisor and MPIC status message (used to determine if port is powered on or off, according to the MPIC)
/// cgsn::groups::sensor::command_status   | interthread    | protobuf::sensor::CommandStatus | Status of the SensorCommand (main) thread
/// cgsn::groups::sensor::validator_status | interthread    | protobuf::sensor::ValidatorStatus     | Status of the Validator thread
/// \tparam Config The driver configuration (Protobuf) type
/// \tparam ValidatorThread The driver's validator thread type
/// \tparam raw_data_in_group The group to subscribe for raw incoming messages
/// \tparam sensor_type The sensor type enumeration
template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
class DriverBase : public goby::MultiThreadApplication<Config>
{
  public:
    DriverBase();
    ~DriverBase();

  protected:
    virtual void driver_loop() = 0;
    virtual void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) = 0;
    sensor_command::statechart::Machine& state_machine() { return state_machine_; }

  private:
    //    void initialize() override {}
    void loop() override;
    //  void finalize() override {}

    void incoming_command(const protobuf::SchedulerCommand& cmd);
    void incoming_mpic_status(const protobuf::MPICStatus& status);
    void publish_health();

  private:
    sensor_command::statechart::Machine state_machine_;

    protobuf::sensor::CommandStatus sensor_command_status_;
    protobuf::sensor::ValidatorStatus validator_status_;
    protobuf::IOStatus io_status_;

    goby::time::MicroTime last_status_report_time_{0 * boost::units::si::seconds};
};

} // namespace cgsn

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group, sensor_type>::DriverBase()
    : goby::MultiThreadApplication<Config>(this->app_cfg().command().loop_frequency_with_units()),
      state_machine_(
          this->app_cfg(), this->interprocess(),
          [this](const protobuf::SerialConfig& s) { this->template launch_thread<IOThread>(s); },
          [this]() { this->template join_thread<IOThread>(); })
{
    // initialize these required fields until we get a report from the statecharts
    sensor_command_status_.set_state(protobuf::sensor::COMMAND_STATE_UNKNOWN);
    validator_status_.set_state(protobuf::sensor::VALIDATOR_STATE_UNKNOWN);
    io_status_.set_state(protobuf::IO__STATE_UNKNOWN);

    // registor our glog group(s)
    goby::glog.add_group("sensor_command", goby::common::Colors::green);
    goby::glog.add_group("driver_status", goby::common::Colors::lt_green);

    // subscribe to commands from the scheduler
    this->interprocess()
        .template subscribe<cgsn::groups::scheduler::command, protobuf::SchedulerCommand>(
            [this](const protobuf::SchedulerCommand& cmd) { this->incoming_command(cmd); });

    // subscribe to data from the I/O thread (e.g. serial port)
    this->interthread().template subscribe<raw_data_in_group, protobuf::SensorRaw>(
        [this](const protobuf::SensorRaw& raw) { this->incoming_raw(raw); });

    // subscribe to ready signal from the other threads
    this->interthread().template subscribe<cgsn::groups::ready, cgsn::DriverModule>(
        [this](const cgsn::DriverModule& module) {
            if (module == cgsn::DriverModule::IO)
            {
                goby::glog.is_debug2() && goby::glog << "Received IO module ready " << std::endl;
                this->state_machine().process_event(sensor_command::statechart::EvIOThreadReady());
            }
        });

    // subscribe to the MPIC status message from the supervisor (to verify if ports are powered on or off)
    this->interprocess()
        .template subscribe<cgsn::groups::supervisor::status, protobuf::SupervisorStatus>(
            [this](const protobuf::SupervisorStatus& status) {
                if (status.has_mpic())
                    this->incoming_mpic_status(status.mpic());
            });

    // subscribe to the sensor command status (from the sensor command statechart, running in this thread)
    this->interthread()
        .template subscribe<cgsn::groups::sensor::command_status, protobuf::sensor::CommandStatus>(
            [this](const protobuf::sensor::CommandStatus& status) {
                sensor_command_status_ = status;
                publish_health();
            });

    // subscribe to the validator status message
    this->interthread()
        .template subscribe<cgsn::groups::sensor::validator_status,
                            protobuf::sensor::ValidatorStatus>(
            [this](const protobuf::sensor::ValidatorStatus& status) {
                validator_status_ = status;
                publish_health();
            });

    // subscribe to the I/O thread status message
    // don't always publish a status message based on this value here
    this->interthread().template subscribe<cgsn::groups::io::status, protobuf::IOStatus>(
        [this](const protobuf::IOStatus& status) {
            io_status_ = status;
            if (status.has_error())
                publish_health();
        });

    goby::glog.is_debug2() &&
        goby::glog << "ValidatorConfig: " << this->cfg().validator().DebugString() << std::endl;

    this->template launch_thread<ValidatorThread>(this->cfg().validator());

    state_machine_.initiate();
}

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group, sensor_type>::~DriverBase()
{
    state_machine_.terminate();
    this->template join_thread<ValidatorThread>();
}

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
void cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group, sensor_type>::loop()
{
    // handle timeouts for WaitForPrompt
    if (this->state_machine().state() == protobuf::sensor::INITIALIZE__WAIT_FOR_PROMPT ||
        this->state_machine().state() == protobuf::sensor::ON__WAIT_FOR_PROMPT ||
        this->state_machine().state() == protobuf::sensor::OFF__WAIT_FOR_PROMPT)
    {
        const auto* time_access =
            this->state_machine()
                .template state_cast<const sensor_command::statechart::TimeAccess*>();
        if (time_access)
        {
            goby::time::SITime last_time(time_access->last_time()), now(goby::time::now()),
                timeout_duration(this->cfg().command().prompt_timeout_with_units());

            if (now >= last_time + timeout_duration)
                this->state_machine().process_event(
                    sensor_command::statechart::EvPromptTimeoutExceeded());
        }
    }
    // handle timeouts for Configure
    else if (this->state_machine().state() == protobuf::sensor::INITIALIZE__CONFIGURE ||
             this->state_machine().state() == protobuf::sensor::ON__CONFIGURE)
    {
        const auto* time_access =
            this->state_machine()
                .template state_cast<const sensor_command::statechart::TimeAccess*>();
        if (time_access)
        {
            goby::time::SITime last_time(time_access->last_time()), now(goby::time::now()),
                timeout_duration(this->cfg().command().config_value_timeout_with_units());

            if (now >= last_time + timeout_duration)
                this->state_machine().process_event(
                    sensor_command::statechart::EvConfigureValueTimeout());
        }
    }

    // call the specific driver loop
    driver_loop();

    // publish status at least every `report_interval` for the MODULE__SENSOR_DRIVER__PORT_N module
    decltype(last_status_report_time_) now(goby::time::now()),
        status_interval(cgsn::health::health_metadata(protobuf::MODULE__SENSOR_DRIVER__PORT_N)
                            .report_interval_with_units());
    if (now > last_status_report_time_ + status_interval)
        publish_health();
}

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
void cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group,
                      sensor_type>::incoming_command(const protobuf::SchedulerCommand& cmd)
{
    goby::glog.is_verbose() && goby::glog << group("sensor_command")
                                          << "Incoming command: " << cmd.ShortDebugString()
                                          << std::endl;

    if (cmd.type() == sensor_type)
    {
        if (cmd.type() == sensor_type && cmd.port_id() == this->cfg().power().port_id())
        {
            if (cmd.enable())
                this->state_machine().process_event(
                    sensor_command::statechart::EvPortLoggerTurnOn());
            else
                this->state_machine().process_event(
                    sensor_command::statechart::EvPortLoggerTurnOff());
        }
    }
}

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
void cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group,
                      sensor_type>::incoming_mpic_status(const protobuf::MPICStatus& status)
{
    if (this->cfg().has_power())
    {
        // MPIC indexes at 1
        int port_index = this->cfg().power().port_id() - 1;
        if (status.cpic_size() > port_index)
        {
            if (status.cpic(port_index).state())
                this->state_machine().process_event(
                    sensor_command::statechart::EvMPICReportsPortPowerOn());
            else
                this->state_machine().process_event(
                    sensor_command::statechart::EvMPICReportsPortPowerOff());
        }
    }
}

template <typename Config, typename IOThread, typename ValidatorThread,
          const goby::Group& raw_data_in_group, cgsn::protobuf::sensor::Type sensor_type>
void cgsn::DriverBase<Config, IOThread, ValidatorThread, raw_data_in_group,
                      sensor_type>::publish_health()
{
    auto status = driver_make_with_header<cgsn::protobuf::sensor::DriverStatus>();

    status.set_type(sensor_type);
    *status.mutable_command() = sensor_command_status_;
    *status.mutable_validator() = validator_status_;
    *status.mutable_io() = io_status_;
    status.set_port_id(this->cfg().power().port_id());

    goby::glog.is_verbose() && goby::glog << group("driver_status")
                                          << "Status: " << status.ShortDebugString() << std::endl;
    this->interprocess().template publish<cgsn::groups::sensor::driver_status>(status);

    last_status_report_time_ = goby::time::now();
}

#endif
