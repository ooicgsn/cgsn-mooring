// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DRIVER_PATTERN_SENSOR_COMMAND_STATECHART_H
#define DRIVER_PATTERN_SENSOR_COMMAND_STATECHART_H

#include "cgsn-mooring/sensor/command_statechart/interface.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
//using DriverPatternIOThread = cgsn::io::AsciiLineSerialThread<groups::driver_pattern::raw_in,
//                                                              groups::driver_pattern::raw_out>;

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternWaitForPrompt : boost::statechart::state<Derived, Parent>,
                                    Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    DriverPatternWaitForPrompt(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvPromptReceived());
    }
    ~DriverPatternWaitForPrompt() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternConfigure : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    DriverPatternConfigure(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvConfigured());
    }
    ~DriverPatternConfigure() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternStartLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    DriverPatternStartLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStarted());
    }
    ~DriverPatternStartLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternStopLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    DriverPatternStopLogging(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvLoggingStopped());
    }
    ~DriverPatternStopLogging() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternSensorSleep : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    DriverPatternSensorSleep(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvSensorSleeping());
    }
    ~DriverPatternSensorSleep() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct DriverPatternSelfTest : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    DriverPatternSelfTest(typename StateBase::my_context c) : StateBase(c)
    {
        this->post_event(EvSelfTestCompleted());
    }
    ~DriverPatternSelfTest() {}

    typedef boost::mpl::list<> local_reactions;
};

} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(DriverPattern)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
