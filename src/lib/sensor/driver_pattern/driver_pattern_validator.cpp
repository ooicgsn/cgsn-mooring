// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "driver_pattern_validator.h"

using namespace goby::common::logger;
using goby::glog;

cgsn::validator::DriverPatternValidatorThread::DriverPatternValidatorThread(const protobuf::ValidatorConfig& cfg)
    : ValidatorBaseThread<cgsn::groups::driver_pattern::raw_in,
                         cgsn::groups::driver_pattern::raw_in_validated,
                         cgsn::protobuf::DriverPatternData, cgsn::groups::driver_pattern::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
}

bool cgsn::validator::DriverPatternValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    return false;
}

bool cgsn::validator::DriverPatternValidatorThread::validate_parsed(const protobuf::DriverPatternData& pos)
{
    return false;
}


