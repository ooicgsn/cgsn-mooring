// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef COMMON_IMPL_20181208_H
#define COMMON_IMPL_20181208_H

// intended to be included after local (i.e. the implementation for each driver) states
// to finalize the actual states
namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
//
// actual states
//

using InitializeStateBase =
    boost::statechart::state<Initialize, Machine, initialize::PortPoweringOn>;
struct Initialize : InitializeStateBase
{
    Initialize(typename InitializeStateBase::my_context c) : InitializeStateBase(c) {}
    ~Initialize() {}

    typedef boost::mpl::list<> reactions;
};

using OnStateBase = boost::statechart::state<On, Machine, on::PortPoweringOn>;
struct On : OnStateBase
{
    On(typename OnStateBase::my_context c) : OnStateBase(c) {}
    ~On() {}

    typedef boost::mpl::list<boost::statechart::transition<EvPortLoggerTurnOff, Off>> reactions;
};

using OffStateBase = boost::statechart::state<Off, Machine, off::WaitForPrompt>;
struct Off : OffStateBase
{
    Off(typename OffStateBase::my_context c) : OffStateBase(c) {}
    ~Off() {}

    typedef boost::mpl::list<> reactions;
};

namespace initialize
{
using PortPoweringOnStateBase =
    boost::statechart::state<PortPoweringOn, Initialize, port_powering_on::MPICPortOn>;
struct PortPoweringOn : PortPoweringOnStateBase
{
    PortPoweringOn(typename PortPoweringOnStateBase::my_context c) : PortPoweringOnStateBase(c) {}
    ~PortPoweringOn() {}

    typedef boost::mpl::list<boost::statechart::transition<EvIOThreadReady, WaitForPrompt>>
        reactions;
};

namespace port_powering_on
{
using MPICPortOnBaseType =
    MPICPortOnBase<MPICPortOn, PortPoweringOn,
                   protobuf::sensor::INITIALIZE__PORT_POWERING_ON__MPIC_POWER_ON,
                   LaunchingIOThread>;
struct MPICPortOn : MPICPortOnBaseType
{
    MPICPortOn(typename MPICPortOnBaseType::my_context c) : MPICPortOnBaseType(c) {}
    ~MPICPortOn() {}
};

using LaunchingIOThreadBaseType =
    LaunchingIOThreadBase<LaunchingIOThread, PortPoweringOn,
                          protobuf::sensor::INITIALIZE__PORT_POWERING_ON__LAUNCHING_IO_THREAD>;
struct LaunchingIOThread : LaunchingIOThreadBaseType
{
    LaunchingIOThread(typename LaunchingIOThreadBaseType::my_context c)
        : LaunchingIOThreadBaseType(c)
    {
    }
    ~LaunchingIOThread() {}
};
} // namespace port_powering_on

using PortPoweringOffStateBase =
    boost::statechart::state<PortPoweringOff, Initialize, port_powering_off::JoiningIOThread>;
struct PortPoweringOff : PortPoweringOffStateBase
{
    PortPoweringOff(typename PortPoweringOffStateBase::my_context c) : PortPoweringOffStateBase(c)
    {
    }
    ~PortPoweringOff() {}

    typedef boost::mpl::list<boost::statechart::transition<EvMPICReportsPortPowerOff, off::Standby>>
        reactions;
};

namespace port_powering_off
{
using JoiningIOThreadBaseType =
    JoiningIOThreadBase<JoiningIOThread, PortPoweringOff,
                        protobuf::sensor::INITIALIZE__PORT_POWERING_OFF__JOINING_IO_THREAD,
                        MPICPortOff>;
struct JoiningIOThread : JoiningIOThreadBaseType
{
    JoiningIOThread(typename JoiningIOThreadBaseType::my_context c) : JoiningIOThreadBaseType(c) {}
    ~JoiningIOThread() {}
};

using MPICPortOffBaseType =
    MPICPortOffBase<MPICPortOff, PortPoweringOff,
                    protobuf::sensor::INITIALIZE__PORT_POWERING_OFF__MPIC_POWER_OFF>;
struct MPICPortOff : MPICPortOffBaseType
{
    MPICPortOff(typename MPICPortOffBaseType::my_context c) : MPICPortOffBaseType(c) {}
    ~MPICPortOff() {}
};
} // namespace port_powering_off
} // namespace initialize

namespace on
{
using PortPoweringOnStateBase =
    boost::statechart::state<PortPoweringOn, On, port_powering_on::MPICPortOn>;
struct PortPoweringOn : PortPoweringOnStateBase
{
    PortPoweringOn(typename PortPoweringOnStateBase::my_context c) : PortPoweringOnStateBase(c) {}
    ~PortPoweringOn() {}

    typedef boost::mpl::list<boost::statechart::transition<EvIOThreadReady, WaitForPrompt>>
        reactions;
};

namespace port_powering_on
{
using MPICPortOnBaseType =
    MPICPortOnBase<MPICPortOn, PortPoweringOn,
                   protobuf::sensor::ON__PORT_POWERING_ON__MPIC_POWER_ON, LaunchingIOThread>;
struct MPICPortOn : MPICPortOnBaseType
{
    MPICPortOn(typename MPICPortOnBaseType::my_context c) : MPICPortOnBaseType(c) {}
    ~MPICPortOn() {}
};

using LaunchingIOThreadBaseType =
    LaunchingIOThreadBase<LaunchingIOThread, PortPoweringOn,
                          protobuf::sensor::ON__PORT_POWERING_ON__LAUNCHING_IO_THREAD>;
struct LaunchingIOThread : LaunchingIOThreadBaseType
{
    LaunchingIOThread(typename LaunchingIOThreadBaseType::my_context c)
        : LaunchingIOThreadBaseType(c)
    {
    }
    ~LaunchingIOThread() {}
};
} // namespace port_powering_on

using CriticalFailureStateBase = boost::statechart::state<CriticalFailure, On>;

struct CriticalFailure : CriticalFailureStateBase,
                         Notify<CriticalFailure, protobuf::sensor::ON__CRITICAL_FAILURE>
{
    CriticalFailure(typename CriticalFailureStateBase::my_context c) : CriticalFailureStateBase(c)
    {
    }

    typedef boost::mpl::list<> reactions;
};

using LoggingStateBase = boost::statechart::state<Logging, On>;

struct Logging : LoggingStateBase, Notify<Logging, protobuf::sensor::ON__LOGGING>
{
    using StateBase = boost::statechart::state<Logging, On>;
    Logging(typename StateBase::my_context c) : StateBase(c) {}

    typedef boost::mpl::list<> reactions;
};

} // namespace on

namespace off
{
using CriticalFailureStateBase = boost::statechart::state<CriticalFailure, Off>;

struct CriticalFailure : CriticalFailureStateBase,
                         Notify<CriticalFailure, protobuf::sensor::OFF__CRITICAL_FAILURE>
{
    CriticalFailure(typename CriticalFailureStateBase::my_context c) : CriticalFailureStateBase(c)
    {
    }

    typedef boost::mpl::list<> reactions;
};

using StandbyStateBase = boost::statechart::state<Standby, Off>;

struct Standby : StandbyStateBase, Notify<Standby, protobuf::sensor::OFF__STANDBY>
{
    Standby(typename StandbyStateBase::my_context c) : StandbyStateBase(c) {}

    typedef boost::mpl::list<boost::statechart::transition<EvPortLoggerTurnOn, On>> reactions;
};

using PortPoweringOffStateBase =
    boost::statechart::state<PortPoweringOff, Off, port_powering_off::JoiningIOThread>;
struct PortPoweringOff : PortPoweringOffStateBase
{
    PortPoweringOff(typename PortPoweringOffStateBase::my_context c) : PortPoweringOffStateBase(c)
    {
    }
    ~PortPoweringOff() {}

    typedef boost::mpl::list<boost::statechart::transition<EvMPICReportsPortPowerOff, off::Standby>>
        reactions;
};

namespace port_powering_off
{
using JoiningIOThreadBaseType =
    JoiningIOThreadBase<JoiningIOThread, PortPoweringOff,
                        protobuf::sensor::OFF__PORT_POWERING_OFF__JOINING_IO_THREAD, MPICPortOff>;

struct JoiningIOThread : JoiningIOThreadBaseType
{
    JoiningIOThread(typename JoiningIOThreadBaseType::my_context c) : JoiningIOThreadBaseType(c) {}
    ~JoiningIOThread() {}
};

using MPICPortOffBaseType =
    MPICPortOffBase<MPICPortOff, PortPoweringOff,
                    protobuf::sensor::OFF__PORT_POWERING_OFF__MPIC_POWER_OFF>;
struct MPICPortOff : MPICPortOffBaseType
{
    MPICPortOff(typename MPICPortOffBaseType::my_context c) : MPICPortOffBaseType(c) {}
    ~MPICPortOff() {}
};
} // namespace port_powering_off
} // namespace off
} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

#endif
