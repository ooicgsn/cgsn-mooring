// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SENSOR_COMMAND_STATECHART_20180316_H
#define SENSOR_COMMAND_STATECHART_20180316_H

#include <goby/common/time.h>

#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/config/sensors_config.pb.h"
#include "cgsn-mooring/config/serial_config.pb.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/sensor_status.pb.h"

// generates the correct base states from the local implementations (e.g. GPSConfigure -> on::ConfigureLocal)
#define CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(Prefix)                           \
    namespace cgsn                                                                     \
    {                                                                                  \
    namespace sensor_command                                                           \
    {                                                                                  \
    namespace statechart                                                               \
    {                                                                                  \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using WaitForPromptLocal = Prefix##WaitForPrompt<Derived, Parent, state>;          \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using ConfigureLocal = Prefix##Configure<Derived, Parent, state>;                  \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using SelfTestLocal = Prefix##SelfTest<Derived, Parent, state>;                    \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using SensorSleepLocal = Prefix##SensorSleep<Derived, Parent, state>;              \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using StartLoggingLocal = Prefix##StartLogging<Derived, Parent, state>;            \
    template <typename Derived, typename Parent, protobuf::sensor::CommandState state> \
    using StopLoggingLocal = Prefix##StopLogging<Derived, Parent, state>;              \
    }                                                                                  \
    }                                                                                  \
    }

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
// events
struct EvSelfTestCompleted : boost::statechart::event<EvSelfTestCompleted>
{
};

struct EvPortLoggerTurnOn : boost::statechart::event<EvPortLoggerTurnOn>
{
};
struct EvPortLoggerTurnOff : boost::statechart::event<EvPortLoggerTurnOff>
{
};

struct EvMPICReportsPortPowerOn : boost::statechart::event<EvMPICReportsPortPowerOn>
{
};
struct EvMPICReportsPortPowerOff : boost::statechart::event<EvMPICReportsPortPowerOff>
{
};

struct EvIOThreadReady : boost::statechart::event<EvIOThreadReady>
{
};
struct EvIOThreadJoined : boost::statechart::event<EvIOThreadJoined>
{
};

struct EvPromptReceived : boost::statechart::event<EvPromptReceived>
{
};
struct EvPromptTimeoutExceeded : boost::statechart::event<EvPromptTimeoutExceeded>
{
};

struct EvConfigured : boost::statechart::event<EvConfigured>
{
};

struct EvSensorSleeping : boost::statechart::event<EvSensorSleeping>
{
};

struct EvLoggingStarted : boost::statechart::event<EvLoggingStarted>
{
};
struct EvLoggingStopped : boost::statechart::event<EvLoggingStopped>
{
};

// Autoconfigure events
// posted by driver after timeout without ack from last configuration send
struct EvConfigureValueTimeout : boost::statechart::event<EvConfigureValueTimeout>
{
};
// posted by driver when sensor acknowledges last configuration value
struct EvConfigureValueAck : boost::statechart::event<EvConfigureValueAck>
{
};
// posted by driver when sensor requests that the last configuration value be resent
struct EvConfigureRequiresRepeat : boost::statechart::event<EvConfigureRequiresRepeat>
{
};

// pre-declare

// state machine
struct Machine;
// 1st tier states
struct Initialize;
struct On;
struct Off;

namespace initialize
{
struct PortPoweringOn;

namespace port_powering_on
{
struct MPICPortOn;
struct LaunchingIOThread;
} // namespace port_powering_on
struct WaitForPrompt;
struct StopLogging;
struct Configure;
struct SelfTest;
struct SensorSleep;
struct PortPoweringOff;

namespace port_powering_off
{
struct JoiningIOThread;
struct MPICPortOff;
} // namespace port_powering_off
} // namespace initialize

namespace on
{
struct PortPoweringOn;

namespace port_powering_on
{
struct MPICPortOn;
struct LaunchingIOThread;
} // namespace port_powering_on

struct CriticalFailure;
struct Logging;
struct WaitForPrompt;
struct Configure;
struct StartLogging;
} // namespace on

namespace off
{
struct WaitForPrompt;
struct StopLogging;
struct SensorSleep;
struct CriticalFailure;
struct PortPoweringOff;

namespace port_powering_off
{
struct JoiningIOThread;
struct MPICPortOff;
} // namespace port_powering_off

struct Standby;
} // namespace off

// for why we need a boost::mpl::list<> wrapping Off<>, see https://lists.boost.org/Archives/boost/2006/03/101866.php
struct Machine : boost::statechart::state_machine<Machine, Initialize>
{
  public:
    template <typename Config>
    Machine(const Config& cfg, goby::InterProcessPortal<goby::InterThreadTransporter>& interprocess,
            std::function<void(const protobuf::SerialConfig&)> launch_thread_func,
            std::function<void()> join_thread_func)
        : cfg_(cfg.serial(), cfg.power(), cfg.command() /*, cfg.sensor()*/),
          interprocess_(interprocess), launch_thread_func_(launch_thread_func),
          join_thread_func_(join_thread_func)

    {
        goby::glog.add_group("sensor_command::statechart", goby::common::Colors::magenta);
    }

    protobuf::sensor::CommandState state() { return state_; }
    protobuf::sensor::CommandState previous_state() { return previous_state_; }

    void set_error(protobuf::Error error);
    protobuf::Error error();
    bool has_error();
    void clear_error();

    class CommonConfig
    {
      public:
        CommonConfig(const protobuf::SerialConfig& serial, const protobuf::PowerConfig& power,
                     const protobuf::SensorCommandConfig& command)
            : serial_(serial), power_(power), command_(command)
        {
        }

        const protobuf::SerialConfig& serial() const { return serial_; }
        const protobuf::PowerConfig& power() const { return power_; }
        const protobuf::SensorCommandConfig& command() const { return command_; }

      private:
        const protobuf::SerialConfig& serial_;
        const protobuf::PowerConfig& power_;
        const protobuf::SensorCommandConfig& command_;
    };

    const CommonConfig& cfg() const { return cfg_; }
    goby::InterProcessPortal<goby::InterThreadTransporter>& interprocess() { return interprocess_; }
    goby::InterThreadTransporter& interthread() { return interprocess_.inner(); }

    void launch_thread(const protobuf::SerialConfig& serial) { launch_thread_func_(serial); }
    void join_thread() { join_thread_func_(); }

  private:
    template <typename Derived, protobuf::sensor::CommandState state> friend struct Notify;

    void set_state(protobuf::sensor::CommandState state);
    void unset_state(protobuf::sensor::CommandState state);

    void publish_health();

  private:
    CommonConfig cfg_;
    goby::InterProcessPortal<goby::InterThreadTransporter>& interprocess_;
    std::function<void(const protobuf::SerialConfig&)> launch_thread_func_;
    std::function<void()> join_thread_func_;

    protobuf::sensor::CommandState state_{protobuf::sensor::COMMAND_STATE_UNKNOWN};
    protobuf::sensor::CommandState previous_state_{protobuf::sensor::COMMAND_STATE_UNKNOWN};

    protobuf::Error error_;
};

// gives shorthand access to commonly used methods in the Machine
template <typename Derived> class DriverMethodsAccess
{
  protected:
    const typename Machine::CommonConfig& cfg() const { return machine().cfg(); }

    goby::InterProcessPortal<goby::InterThreadTransporter>& interprocess()
    {
        return machine().interprocess();
    }

    goby::InterThreadTransporter& interthread() { return machine().interthread(); }

    template <typename ProtobufMessage> ProtobufMessage driver_make_with_header()
    {
        return cgsn::make_with_header<ProtobufMessage>("main/sensor_command_statechart");
    }

    const Machine& machine() const
    {
        return static_cast<const Derived*>(this)->outermost_context();
    }
    Machine& machine() { return static_cast<Derived*>(this)->outermost_context(); }
};

// RAII tool for setting enumerations for reporting based on current state
template <typename Derived, protobuf::sensor::CommandState state>
struct Notify : public DriverMethodsAccess<Derived>
{
    Notify()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << protobuf::sensor::CommandState_Name(state) << std::endl;
        this->machine().set_state(state);
    }
    ~Notify()
    {
        this->machine().unset_state(state);
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart") << "~"
                       << protobuf::sensor::CommandState_Name(state) << std::endl;
    }
};

// Allows access to the last time the state performed some action (transmitted data, etc.)
struct TimeAccess
{
    virtual goby::time::MicroTime last_time() const = 0;
    virtual std::string last_command() const { return std::string(); }
};
} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

#endif
