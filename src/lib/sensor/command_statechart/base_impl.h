// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef BASE_IMPL_20181208_H
#define BASE_IMPL_20181208_H

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
//
// reused states that are common for all drivers
//
template <typename Derived, typename Parent, protobuf::sensor::CommandState state_enum,
          typename NextState>
struct MPICPortOnBase : boost::statechart::state<Derived, Parent>, Notify<Derived, state_enum>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    MPICPortOnBase(typename StateBase::my_context c) : StateBase(c) { send_mpic_power_on(); }

    ~MPICPortOnBase() {}

    typedef boost::mpl::list<boost::statechart::transition<EvMPICReportsPortPowerOn, NextState>,
                             boost::statechart::custom_reaction<EvMPICReportsPortPowerOff>>
        reactions;

    boost::statechart::result react(const EvMPICReportsPortPowerOff& ev)
    {
        if (goby::time::now() > (retry_wait_ + last_retry_time_))
        {
            ++mpic_power_retries_;
            goby::glog.is(goby::common::logger::WARN) &&
                goby::glog << group("sensor_command::statechart")
                           << "MPIC reports power OFF, but we commanded power ON" << std::endl;
            if (mpic_power_retries_ >= max_mpic_power_retries_)
            {
                return this->template transit<on::CriticalFailure>();
            }
            else
            {
                send_mpic_power_on();
                goby::glog.is(goby::common::logger::DEBUG1) &&
                    goby::glog << group("sensor_command::statechart") << "Retry "
                               << mpic_power_retries_ << std::endl;
                return this->discard_event();
            }
        }
        return this->discard_event();
    }

  private:
    void send_mpic_power_on()
    {
        auto cmd = driver_make_with_header<cgsn::protobuf::MPICCommand>();
        cmd.set_command(cgsn::protobuf::Port::ON);
        cmd.set_port_id(this->cfg().power().port_id());
        *cmd.mutable_port_cfg() = this->cfg().power();
        this->interprocess().template publish<groups::supervisor::mpic_command>(cmd);
        last_retry_time_ = goby::time::now();
    }

  private:
    const int max_mpic_power_retries_{3};
    goby::time::MicroTime last_retry_time_;
    goby::time::MicroTime retry_wait_{
        5 *
        boost::units::si::seconds}; // wait between retries to let supervisor/MPIC command process
    int mpic_power_retries_{0};
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state_enum>
struct LaunchingIOThreadBase : boost::statechart::state<Derived, Parent>,
                               Notify<Derived, state_enum>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    LaunchingIOThreadBase(typename StateBase::my_context c) : StateBase(c)
    {
        try
        {
            this->machine().launch_thread(this->cfg().serial());
        }
        catch (goby::Exception& e)
        {
            goby::glog.is(goby::common::logger::DEBUG1) &&
                goby::glog << group("sensor_command::statechart")
                           << "Could not launch thread: " << e.what() << std::endl;
        }
    }

    ~LaunchingIOThreadBase() {}

    typedef boost::mpl::list<> reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state_enum,
          typename NextState>
struct JoiningIOThreadBase : boost::statechart::state<Derived, Parent>, Notify<Derived, state_enum>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    JoiningIOThreadBase(typename StateBase::my_context c) : StateBase(c)
    {
        try
        {
            this->machine().join_thread();
        }
        catch (goby::Exception& e)
        {
            goby::glog.is(goby::common::logger::DEBUG1) &&
                goby::glog << group("sensor_command::statechart")
                           << "Could not join thread: " << e.what() << std::endl;
        }
        this->post_event(EvIOThreadJoined());
    }
    ~JoiningIOThreadBase() {}

    typedef boost::mpl::list<boost::statechart::transition<EvIOThreadJoined, NextState>> reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state_enum>
struct MPICPortOffBase : boost::statechart::state<Derived, Parent>, Notify<Derived, state_enum>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    MPICPortOffBase(typename StateBase::my_context c) : StateBase(c) { send_mpic_power_off(); }
    ~MPICPortOffBase() {}

    typedef boost::mpl::list<boost::statechart::custom_reaction<EvMPICReportsPortPowerOn>>
        reactions;

    boost::statechart::result react(const EvMPICReportsPortPowerOn& ev)
    {
        if (goby::time::now() > (retry_wait_ + last_retry_time_))
        {
            ++mpic_power_retries_;
            goby::glog.is(goby::common::logger::WARN) &&
                goby::glog << group("sensor_command::statechart")
                           << "MPIC reports power ON, but we commanded power OFF" << std::endl;
            if (mpic_power_retries_ >= max_mpic_power_retries_)
            {
                return this->template transit<on::CriticalFailure>();
            }
            else
            {
                send_mpic_power_off();
                goby::glog.is(goby::common::logger::DEBUG1) &&
                    goby::glog << group("sensor_command::statechart") << "Retry "
                               << mpic_power_retries_ << std::endl;
                return this->discard_event();
            }
        }
        return this->discard_event();
    }

  private:
    void send_mpic_power_off()
    {
        auto cmd = driver_make_with_header<cgsn::protobuf::MPICCommand>();
        cmd.set_command(cgsn::protobuf::Port::OFF);
        cmd.set_port_id(this->cfg().power().port_id());
        this->interprocess().template publish<groups::supervisor::mpic_command>(cmd);
        last_retry_time_ = goby::time::now();
    }

  private:
    const int max_mpic_power_retries_{3};
    goby::time::MicroTime last_retry_time_;
    goby::time::MicroTime retry_wait_{3 * boost::units::si::seconds};
    int mpic_power_retries_{0};
};
} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

#endif
