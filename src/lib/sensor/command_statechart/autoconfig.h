// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SENSOR_COMMAND_STATECHART_AUTOCONFIG_20181120_H
#define SENSOR_COMMAND_STATECHART_AUTOCONFIG_20181120_H

#include <boost/algorithm/string.hpp>

#include "cgsn-mooring/sensor/autoconfig.h"
#include "cgsn-mooring/sensor/command_statechart/interface.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
// posted by configure::AutoFixed after all fixed parameters have been sent
struct EvConfigured_Fixed : boost::statechart::event<EvConfigured_Fixed>
{
};

// posted by driver if dynamic configuration doesn't match desired
template <typename SensorConfig>
struct EvNotConfigured : boost::statechart::event<EvNotConfigured<SensorConfig>>
{
    EvNotConfigured(SensorConfig cfg) : proposed_sensor_cfg(cfg) {}
    SensorConfig proposed_sensor_cfg; // proposed dynamic configuration
};

namespace configure
{
// Fixed configuration values
template <typename Parent, typename SensorConfig, const goby::Group& raw_data_out_group>
struct AutoFixed;
// Configurable (dynamic values) defined in pb.cfg (via YAML)
template <typename Parent, typename SensorConfig, const goby::Group& raw_data_out_group>
struct AutoDynamic;
} // namespace configure

// To use, typedef AutoConfigure -> DriverNameConfigure
// for example
// template<typename Derived, typename Parent, protobuf::sensor::CommandState state>
//    using CTD_SBE_Configure = AutoConfigure<Derived, Parent, state, protobuf::CTD_SBE_SensorConfig, cgsn::groups::ctd_sbe::raw_out>;
template <typename Derived, typename Parent, protobuf::sensor::CommandState state,
          typename SensorConfig, const goby::Group& raw_data_out_group>
struct AutoConfigure
    : boost::statechart::state<
          Derived, Parent,
          boost::mpl::list<configure::AutoFixed<Derived, SensorConfig, raw_data_out_group>>>,
      Notify<Derived, state>
{
    using Base = boost::statechart::state<
        Derived, Parent,
        boost::mpl::list<configure::AutoFixed<Derived, SensorConfig, raw_data_out_group>>>;

    AutoConfigure(typename Base::my_context c) : Base(c) {}
    ~AutoConfigure() {}
    typedef boost::mpl::list<> local_reactions;
};

namespace configure
{
// Shared functions for AutoFixed and AutoDynamic states
template <typename Derived, typename ParamProtobuf, const goby::Group& raw_data_out_group>
struct AutoBase : DriverMethodsAccess<Derived>, TimeAccess
{
    // send the next parameter; if none, send the store settings command (if any)
    void send_next_param()
    {
        if (is_complete())
            return;

        auto raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        if (params_.empty())
        {
            goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                                 << "Sending store command" << std::endl;
            raw.set_raw_data(store_cfg_cmd_.front());
        }
        else
        {
            const auto& p = params_.front();
            goby::glog.is_debug2() && goby::glog
                                          << group("sensor_command::statechart")
                                          << "Setting config value: " << boost::trim_copy(p.first)
                                          << ", Purpose: " << p.second.purpose() << std::endl;
            raw.set_raw_data(p.first);
        }
        static_cast<Derived*>(this)->interprocess().template publish<raw_data_out_group>(raw);
        last_time_ = goby::time::now();
        last_command_ = raw.raw_data();
    }

    // check if all fixed parameters and store settings command (if any) have been sent
    bool is_complete() { return static_cast<Derived*>(this)->is_complete(); }
    // the last time a command was sent
    goby::time::MicroTime last_time() const override { return last_time_; }
    // the last command that was sent
    std::string last_command() const override { return last_command_; }
    // common reactions
    typedef boost::mpl::list<boost::statechart::custom_reaction<EvConfigureValueAck>,
                             boost::statechart::custom_reaction<EvConfigureRequiresRepeat>,
                             boost::statechart::custom_reaction<EvConfigureValueTimeout>>
        common_reactions;

    boost::statechart::result common_react(const EvConfigureValueAck& ev)
    {
        if (is_complete())
            return static_cast<Derived*>(this)->discard_event();

        if (this->params_.empty())
        {
            goby::glog.is_debug2() &&
                goby::glog << group("sensor_command::statechart")
                           << "Ack for command: " << boost::trim_copy(store_cfg_cmd_.front())
                           << std::endl;
            store_cfg_cmd_.pop_front();
        }
        else
        {
            goby::glog.is_debug2() &&
                goby::glog << group("sensor_command::statechart")
                           << "Ack for command: " << boost::trim_copy(params_.front().first)
                           << std::endl;
            params_.pop_front();
        }

        send_next_param();
        return static_cast<Derived*>(this)->discard_event();
    }

    boost::statechart::result common_react(const EvConfigureValueTimeout& ev)
    {
        goby::glog.is_warn() && goby::glog << group("sensor_command::statechart")
                                           << "Timeout waiting for Ack on last command; resending"
                                           << std::endl;
        send_next_param();
        return static_cast<Derived*>(this)->discard_event();
    }

    boost::statechart::result common_react(const EvConfigureRequiresRepeat& ev)
    {
        goby::glog.is_warn() && goby::glog << group("sensor_command::statechart")
                                           << "Sensor requested repeat, resending" << std::endl;
        send_next_param();
        return static_cast<Derived*>(this)->discard_event();
    }

    // data
    std::deque<std::pair<std::string, ParamProtobuf>> params_;
    std::deque<std::string> store_cfg_cmd_;
    goby::time::MicroTime last_time_{goby::time::now()};
    std::string last_command_;
};

// Send any fixed configuration that is required for this driver to work correctly with the sensor (only once at Initialize)
template <typename Parent, typename SensorConfig, const goby::Group& raw_data_out_group>
struct AutoFixed
    : boost::statechart::state<AutoFixed<Parent, SensorConfig, raw_data_out_group>, Parent>,
      AutoBase<AutoFixed<Parent, SensorConfig, raw_data_out_group>,
               MessageOptions::SensorConfig::FixedParam, raw_data_out_group>
{
    using Base = AutoBase<AutoFixed<Parent, SensorConfig, raw_data_out_group>,
                          MessageOptions::SensorConfig::FixedParam, raw_data_out_group>;
    using StateBase =
        boost::statechart::state<AutoFixed<Parent, SensorConfig, raw_data_out_group>, Parent>;

    // ENTER
    AutoFixed(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*::configure::AutoFixed()" << std::endl;

        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL: this->send_fixed_settings(); break;
            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvConfigured_Fixed());
                break;
        }
    }

    // EXIT
    ~AutoFixed()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*::configure::~AutoFixed()" << std::endl;
    }

    // REACTIONS
    typedef boost::mpl::list<boost::statechart::transition<
        EvConfigured_Fixed, configure::AutoDynamic<Parent, SensorConfig, raw_data_out_group>>>
        local_reactions;

    typedef typename boost::mpl::copy<typename Base::common_reactions,
                                      boost::mpl::front_inserter<local_reactions>>::type reactions;

    boost::statechart::result react(const EvConfigureValueAck& ev)
    {
        return this->common_react(ev);
    }
    boost::statechart::result react(const EvConfigureValueTimeout& ev)
    {
        return this->common_react(ev);
    }
    boost::statechart::result react(const EvConfigureRequiresRepeat& ev)
    {
        return this->common_react(ev);
    }

    bool is_complete()
    {
        if (this->params_.empty() && this->store_cfg_cmd_.empty())
        {
            this->post_event(EvConfigured_Fixed());
            return true;
        }
        return false;
    }

  private:
    // queue up fixed_param settings to send, followed by store settings command (if applicable)
    void send_fixed_settings()
    {
        const auto* desc = SensorConfig::descriptor();

        bool is_initialize_state = std::is_same<Parent, initialize::Configure>::value;
        this->params_ = autoconfig::fixed_params(desc, is_initialize_state);
        bool requires_store_command = false;
        for (const auto& p : this->params_)
        {
            if (p.second.requires_store_command())
                requires_store_command = true;
        }

        if (this->params_.empty())
        {
            goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                                 << "No fixed_param(s), continuing on" << std::endl;

            this->post_event(EvConfigured_Fixed());
            return;
        }

        const auto& sensor_cfg = desc->options().GetExtension(cgsn::msg).sensor_cfg();
        if (requires_store_command && sensor_cfg.has_store_cfg_command())
            this->store_cfg_cmd_.push_back(sensor_cfg.store_cfg_command());

        this->send_next_param();
    }
};

// Check dynamic (protobuf defined) configuration; if correct continue on; if incorrect, set it, then check again. Repeat until
// correct or until cfg().command().max_cfg_attempts() attempts
template <typename Parent, typename SensorConfig, const goby::Group& raw_data_out_group>
struct AutoDynamic
    : boost::statechart::state<AutoDynamic<Parent, SensorConfig, raw_data_out_group>, Parent>,
      AutoBase<AutoDynamic<Parent, SensorConfig, raw_data_out_group>, FieldOptions::SensorConfig,
               raw_data_out_group>
{
    using StateBase =
        boost::statechart::state<AutoDynamic<Parent, SensorConfig, raw_data_out_group>, Parent>;
    using Base = AutoBase<AutoDynamic<Parent, SensorConfig, raw_data_out_group>,
                          FieldOptions::SensorConfig, raw_data_out_group>;
    // ENTER
    AutoDynamic(typename StateBase::my_context c)
        : StateBase(c), max_configuration_attempts_(this->cfg().command().max_cfg_attempts())
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*::configure::AutoDynamic()" << std::endl;

        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL: send_get_configuration_command(); break;
            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvConfigured());
                break;
        }
    }

    // EXIT
    ~AutoDynamic()
    {
        goby::glog.is_debug1() && goby::glog << group("sensor_command::statechart")
                                             << "*::configure::~AutoDynamic()" << std::endl;
    }

    // REACTIONS
    // EvConfigured handled by Parent (*::Configure) state
    typedef boost::mpl::list<boost::statechart::custom_reaction<EvNotConfigured<SensorConfig>>>
        local_reactions;

    boost::statechart::result react(const EvNotConfigured<SensorConfig>& ev)
    {
        // send the actual configuration based on the values in the Protobuf config file
        if (configuration_attempts_ < max_configuration_attempts_)
        {
            std::string proposed_config_string;
            goby::glog.is_debug2() &&
                goby::glog << group("sensor_command::statechart") << "Proposed configuration: "
                           << ev.proposed_sensor_cfg.ShortDebugString() << std::endl;

            const auto* desc = SensorConfig::descriptor();
            this->params_ = autoconfig::dynamic_params(ev.proposed_sensor_cfg);
            cfg_was_set_ = true;

            if (this->params_.empty())
            {
                goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                                     << "Empty configuration message, continuing on"
                                                     << std::endl;

                this->post_event(EvConfigured());
                return this->discard_event();
            }

            const auto& sensor_cfg = desc->options().GetExtension(cgsn::msg).sensor_cfg();
            if (sensor_cfg.has_store_cfg_command())
                this->store_cfg_cmd_.push_back(sensor_cfg.store_cfg_command());

            this->send_next_param();
        }
        else
        {
            goby::glog.is_warn() &&
                goby::glog << group("sensor_command::statechart")
                           << "Failed to set configuration after " << configuration_attempts_
                           << " attempts. "
                           << "Will continue on in hopes of still getting valid data." << std::endl;
            this->post_event(EvConfigured());
        }
        // we have handled this event, so discard it to avoid further processing by outer states
        return this->discard_event();
    }

    typedef typename boost::mpl::copy<typename Base::common_reactions,
                                      boost::mpl::front_inserter<local_reactions>>::type reactions;

    boost::statechart::result react(const EvConfigureValueAck& ev)
    {
        return this->common_react(ev);
    }
    boost::statechart::result react(const EvConfigureValueTimeout& ev)
    {
        return this->common_react(ev);
    }
    boost::statechart::result react(const EvConfigureRequiresRepeat& ev)
    {
        return this->common_react(ev);
    }

    bool is_complete()
    {
        if (this->params_.empty() && this->store_cfg_cmd_.empty())
        {
            if (cfg_was_set_)
            {
                // query the configuration again to see if they match now
                send_get_configuration_command();
                ++configuration_attempts_;
                cfg_was_set_ = false;
            }
            return true;
        }

        return false;
    }

  private:
    void send_get_configuration_command()
    {
        const auto* desc = SensorConfig::descriptor();
        const auto& sensor_cfg = desc->options().GetExtension(cgsn::msg).sensor_cfg();

        if (sensor_cfg.get_cfg_command_size() == 0)
        {
            goby::glog.is_debug2() && goby::glog << group("sensor_command::statechart")
                                                 << "No get_cfg_command defined, continuing on"
                                                 << std::endl;

            this->post_event(EvConfigured());
            return;
        }

        for (const std::string& cfg_command : sensor_cfg.get_cfg_command())
        {
            FieldOptions::SensorConfig cfg;
            cfg.set_key(boost::trim_copy(cfg_command));
            cfg.set_purpose("(get config command)");
            this->params_.push_back(std::make_pair(cfg_command, cfg));
        }
        this->send_next_param();
    }

  private:
    int configuration_attempts_{0};
    const int max_configuration_attempts_;
    bool cfg_was_set_{false};
};
} // namespace configure
} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

#endif
