// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/sensor/command_statechart/interface.h"

void cgsn::sensor_command::statechart::Machine::set_error(protobuf::Error error)
{
    error_ = error;
    publish_health();
}
cgsn::protobuf::Error cgsn::sensor_command::statechart::Machine::error() { return error_; }
bool cgsn::sensor_command::statechart::Machine::has_error() { return error_.has_code(); }
void cgsn::sensor_command::statechart::Machine::clear_error()
{
    error_.Clear();
    publish_health();
}

void cgsn::sensor_command::statechart::Machine::set_state(protobuf::sensor::CommandState state)
{
    state_ = state;
    this->publish_health();
}

void cgsn::sensor_command::statechart::Machine::unset_state(protobuf::sensor::CommandState state)
{
    previous_state_ = state;
    state_ = protobuf::sensor::COMMAND_STATE_UNKNOWN;
}

void cgsn::sensor_command::statechart::Machine::publish_health()
{
    // echo these back so that the DriverBase (running in the same thread) receives these messages
    goby::protobuf::TransporterConfig tcfg;
    tcfg.set_echo(true);
    protobuf::sensor::CommandStatus health;
    health.set_state(state_);
    if (this->has_error())
        *health.mutable_error() = this->error();
    this->interthread().template publish<cgsn::groups::sensor::command_status>(health, tcfg);
}
