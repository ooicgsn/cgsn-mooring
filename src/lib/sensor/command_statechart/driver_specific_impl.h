// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DRIVER_SPECIFIC_IMPL_20181208_H
#define DRIVER_SPECIFIC_IMPL_20181208_H

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
//
// reused states that derive from a specific base implementation for each driver
// the {StateName}Local base class must be implemented completely by each sensor driver
//
namespace initialize
{
using WaitForPromptLocalBase =
    WaitForPromptLocal<WaitForPrompt, Initialize, protobuf::sensor::INITIALIZE__WAIT_FOR_PROMPT>;
struct WaitForPrompt : WaitForPromptLocalBase
{
    WaitForPrompt(typename WaitForPromptLocalBase::my_context c) : WaitForPromptLocalBase(c) {}
    ~WaitForPrompt() {}

    typedef boost::mpl::list<boost::statechart::transition<EvPromptReceived, StopLogging>>
        common_reactions;

    typedef typename boost::mpl::copy<typename WaitForPromptLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using StopLoggingLocalBase =
    StopLoggingLocal<StopLogging, Off, protobuf::sensor::INITIALIZE__STOP_LOGGING>;
struct StopLogging : StopLoggingLocalBase
{
    StopLogging(typename StopLoggingLocalBase::my_context c) : StopLoggingLocalBase(c) {}
    ~StopLogging() {}

    typedef boost::mpl::list<boost::statechart::transition<EvLoggingStopped, Configure>>
        common_reactions;

    typedef typename boost::mpl::copy<typename StopLoggingLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using ConfigureLocalBase =
    ConfigureLocal<Configure, Initialize, protobuf::sensor::INITIALIZE__CONFIGURE>;
struct Configure : ConfigureLocalBase
{
    Configure(typename ConfigureLocalBase::my_context c) : ConfigureLocalBase(c) {}
    ~Configure() {}

    typedef boost::mpl::list<boost::statechart::transition<EvConfigured, SelfTest>>
        common_reactions;

    typedef typename boost::mpl::copy<typename ConfigureLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using SelfTestLocalBase =
    SelfTestLocal<SelfTest, Initialize, protobuf::sensor::INITIALIZE__SELF_TEST>;

struct SelfTest : SelfTestLocalBase
{
    SelfTest(typename SelfTestLocalBase::my_context c) : SelfTestLocalBase(c) {}
    ~SelfTest() {}

    typedef boost::mpl::list<boost::statechart::transition<EvSelfTestCompleted, SensorSleep>>
        common_reactions;

    typedef typename boost::mpl::copy<typename SelfTestLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using SensorSleepLocalBase =
    SensorSleepLocal<SensorSleep, Initialize, protobuf::sensor::INITIALIZE__SENSOR_SLEEP>;
struct SensorSleep : SensorSleepLocalBase
{
    SensorSleep(typename SensorSleepLocalBase::my_context c) : SensorSleepLocalBase(c) {}
    ~SensorSleep() {}

    typedef boost::mpl::list<boost::statechart::transition<EvSensorSleeping, PortPoweringOff>>
        common_reactions;

    typedef typename boost::mpl::copy<typename SensorSleepLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

} // namespace initialize

namespace on
{
using WaitForPromptLocalBase =
    WaitForPromptLocal<WaitForPrompt, On, protobuf::sensor::ON__WAIT_FOR_PROMPT>;
struct WaitForPrompt : WaitForPromptLocalBase
{
    WaitForPrompt(typename WaitForPromptLocalBase::my_context c) : WaitForPromptLocalBase(c) {}
    ~WaitForPrompt() {}

    typedef boost::mpl::list<boost::statechart::transition<EvPromptReceived, Configure>>
        common_reactions;

    typedef typename boost::mpl::copy<typename WaitForPromptLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using ConfigureLocalBase = ConfigureLocal<Configure, On, protobuf::sensor::ON__CONFIGURE>;
struct Configure : ConfigureLocalBase
{
    Configure(typename ConfigureLocalBase::my_context c) : ConfigureLocalBase(c) {}
    ~Configure() {}

    typedef boost::mpl::list<boost::statechart::transition<EvConfigured, StartLogging>>
        common_reactions;

    typedef typename boost::mpl::copy<typename ConfigureLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using StartLoggingLocalBase =
    StartLoggingLocal<StartLogging, On, protobuf::sensor::ON__START_LOGGING>;
struct StartLogging : StartLoggingLocalBase
{
    StartLogging(typename StartLoggingLocalBase::my_context c) : StartLoggingLocalBase(c) {}
    ~StartLogging()
    {
        bool enable_logging = true;
        this->interthread().template publish<cgsn::groups::command::logging>(enable_logging);
    }

    typedef boost::mpl::list<boost::statechart::transition<EvLoggingStarted, Logging>>
        common_reactions;

    typedef typename boost::mpl::copy<typename StartLoggingLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

} // namespace on

namespace off
{
using WaitForPromptLocalBase =
    WaitForPromptLocal<WaitForPrompt, Off, protobuf::sensor::OFF__WAIT_FOR_PROMPT>;
struct WaitForPrompt : WaitForPromptLocalBase
{
    WaitForPrompt(typename WaitForPromptLocalBase::my_context c) : WaitForPromptLocalBase(c) {}
    ~WaitForPrompt() {}

    typedef boost::mpl::list<boost::statechart::transition<EvPromptReceived, StopLogging>>
        common_reactions;

    typedef typename boost::mpl::copy<typename WaitForPromptLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using StopLoggingLocalBase =
    StopLoggingLocal<StopLogging, Off, protobuf::sensor::OFF__STOP_LOGGING>;
struct StopLogging : StopLoggingLocalBase
{
    StopLogging(typename StopLoggingLocalBase::my_context c) : StopLoggingLocalBase(c)
    {
        // stop the validator from logging
        bool enable_logging = false;
        this->interthread().template publish<cgsn::groups::command::logging>(enable_logging);
    }
    ~StopLogging() {}

    typedef boost::mpl::list<boost::statechart::transition<EvLoggingStopped, SensorSleep>>
        common_reactions;

    typedef typename boost::mpl::copy<typename StopLoggingLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};

using SensorSleepLocalBase =
    SensorSleepLocal<SensorSleep, Off, protobuf::sensor::OFF__SENSOR_SLEEP>;
struct SensorSleep : SensorSleepLocalBase
{
    SensorSleep(typename SensorSleepLocalBase::my_context c) : SensorSleepLocalBase(c) {}
    ~SensorSleep() {}

    typedef boost::mpl::list<boost::statechart::transition<EvSensorSleeping, PortPoweringOff>>
        common_reactions;

    typedef typename boost::mpl::copy<typename SensorSleepLocalBase::local_reactions,
                                      boost::mpl::front_inserter<common_reactions>>::type reactions;
};
} // namespace off
} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

#endif
