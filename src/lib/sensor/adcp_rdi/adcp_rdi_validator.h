// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef ADCP_RDI_VALIDATOR_H
#define ADCP_RDI_VALIDATOR_H

#include "cgsn-mooring/messages/sensor_status.pb.h"
#include "cgsn-mooring/sensor/validator_base.h"
#include "cgsn-mooring/sensor/validator_test_fixture.h"

#include "adcp_rdi_common.h"

namespace cgsn
{

    namespace validator
    {
        /// \brief Validates the data coming from the sensor
        /// \details
        /// Publishes (see also cgsn::validator::ValidatorBaseThread):
        /// Group                        | Layer           | Type                         | Value
        /// -----------------------------|-----------------|------------------------------|-------------------------
        /// cgsn::groups::ready          | interthread     | cgsn::DriverModule           | Thread has subscribed and is ready for data
        ///
        /// ----------
        /// Subscribes (see also cgsn::validator::ValidatorBaseThread)
        /// (none other than base class)
        class ADCP_RDI_ValidatorThread :
            public ValidatorBaseThread<cgsn::groups::adcp_rdi::raw_in,
            cgsn::groups::adcp_rdi::raw_in_validated,
            cgsn::protobuf::ADCP_RDI_Data, cgsn::groups::adcp_rdi::parsed>
        {
        public:
            ADCP_RDI_ValidatorThread(const cgsn::protobuf::ValidatorConfig& cfg);
        private:
            CGSN_VALIDATOR_TEST_FIXTURE_FRIEND;
            bool validate_raw(const cgsn::protobuf::SensorRaw& raw_data) override;
            bool validate_parsed(const cgsn::protobuf::ADCP_RDI_Data& parsed_data) override;
        };
    }
}



#endif
