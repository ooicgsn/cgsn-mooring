// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef ADCP_RDI_PARSE_H
#define ADCP_RDI_PARSE_H

#include <boost/units/base_units/metric/hour.hpp>
#include <boost/units/base_units/metric/minute.hpp>

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_common.h"

#include "cgsn-mooring/sensor/exception.h"

namespace cgsn
{
    namespace sensor_internal
    {
        inline boost::units::quantity<boost::units::si::time> time_per_ensemble(const std::string& time_per_ensemble_str)
        {

            std::string regex_str = "^[0-9]{2}:[0-5][0-9]:[0-5][0-9]\\.[0-9]{2}$";
            std::regex time_per_ensemble_regex(regex_str);
            if(!regex_match(time_per_ensemble_str, time_per_ensemble_regex))
                throw(InvalidSensorConfiguration("Invalid time_per_ensemble string: " + time_per_ensemble_str));

            std::vector<std::string> time_parts;
            boost::split(time_parts, time_per_ensemble_str, boost::is_any_of(":"));

            using Time = boost::units::quantity<boost::units::si::time>;

            auto hours = boost::units::metric::hour_base_unit::unit_type();
            auto minutes = boost::units::metric::minute_base_unit::unit_type();
            auto seconds = boost::units::si::seconds;

            return Time(std::stod(time_parts.at(0))*hours) +
                Time(std::stod(time_parts.at(1))*minutes) +
                Time(std::stod(time_parts.at(2))*seconds);

        }

        inline boost::units::quantity<boost::units::si::time> time_between_pings(const std::string& time_between_pings_str)
        {

            std::string regex_str = "^[0-5][0-9]:[0-5][0-9]\\.[0-9]{2}$";
            std::regex time_between_pings_regex(regex_str);
            if(!regex_match(time_between_pings_str, time_between_pings_regex))
                throw(InvalidSensorConfiguration("Invalid time_between_pings string: " + time_between_pings_str));

            std::vector<std::string> time_parts;
            boost::split(time_parts, time_between_pings_str, boost::is_any_of(":"));

            using Time = boost::units::quantity<boost::units::si::time>;

            auto minutes = boost::units::metric::minute_base_unit::unit_type();
            auto seconds = boost::units::si::seconds;

            return Time(std::stod(time_parts.at(0))*minutes) +
                Time(std::stod(time_parts.at(1))*seconds);

        }

    }
}


#endif
