// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef ADCP_RDI_SENSOR_COMMAND_STATECHART_H
#define ADCP_RDI_SENSOR_COMMAND_STATECHART_H

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include "cgsn-mooring/sensor/command_statechart/autoconfig.h"
#include "cgsn-mooring/sensor/command_statechart/interface.h"

#include "cgsn-mooring/messages/serial.pb.h"

#include "cgsn-mooring/messages/adcp_rdi.pb.h"

namespace cgsn
{
namespace sensor_command
{
namespace statechart
{
struct EvBreakWakeup : boost::statechart::event<EvBreakWakeup>
{
};

namespace wait_for_prompt
{
template <typename Parent> struct SendBreakForWakeUp;
template <typename Parent> struct InterrogateForPrompt;
} // namespace wait_for_prompt

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct ADCP_RDI_WaitForPrompt
    : boost::statechart::state<Derived, Parent,
                               boost::mpl::list<wait_for_prompt::SendBreakForWakeUp<Derived>>>,
      Notify<Derived, state>
{
    using StateBase =
        boost::statechart::state<Derived, Parent,
                                 boost::mpl::list<wait_for_prompt::SendBreakForWakeUp<Derived>>>;

    ADCP_RDI_WaitForPrompt(typename StateBase::my_context c) : StateBase(c) {}
    ~ADCP_RDI_WaitForPrompt() {}

    typedef boost::mpl::list<> local_reactions;
};

namespace wait_for_prompt
{
template <typename Parent>
struct SendBreakForWakeUp : boost::statechart::state<SendBreakForWakeUp<Parent>, Parent>,
                            DriverMethodsAccess<SendBreakForWakeUp<Parent>>
{
    using StateBase = boost::statechart::state<SendBreakForWakeUp<Parent>, Parent>;
    SendBreakForWakeUp(typename StateBase::my_context c) : StateBase(c)
    {
        using goby::glog;
        glog.is_debug1() && glog << group("sensor_command::statechart")
                                 << "*__WAIT_FOR_PROMPT__SEND_BREAK_FOR_WAKEUP" << std::endl;

        glog.is_debug1() && glog << group("sensor_command::statechart")
                                 << "Current operation mode = " << this->cfg().command().op_mode()
                                 << std::endl;
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
            {
                protobuf::SerialCommand cmd;
                cmd.set_command(protobuf::SerialCommand::SEND_BREAK);

                this->interthread().template publish<cgsn::groups::adcp_rdi::raw_out>(cmd);
                glog.is_debug1() && glog << group("sensor_command::statechart")
                                         << "Published cmd: " << cmd.ShortDebugString()
                                         << std::endl;
                break;
            }
            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvBreakWakeup());
                break;
        }
    }

    ~SendBreakForWakeUp()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "~*__WAIT_FOR_PROMPT__SEND_BREAK_FOR_WAKEUP" << std::endl;
    }

    typedef boost::mpl::list<
        boost::statechart::transition<EvBreakWakeup, InterrogateForPrompt<Parent>>>
        reactions;
};

template <typename Parent>
struct InterrogateForPrompt : boost::statechart::state<InterrogateForPrompt<Parent>, Parent>,
                              DriverMethodsAccess<InterrogateForPrompt<Parent>>,
                              TimeAccess
{
    using StateBase = boost::statechart::state<InterrogateForPrompt<Parent>, Parent>;
    InterrogateForPrompt(typename StateBase::my_context c) : StateBase(c)
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "*__WAIT_FOR_PROMPT__INTERROGATE_FOR_PROMPT" << std::endl;

        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL: this->send_enter(); break;

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvPromptReceived());
                break;
        }
    }

    ~InterrogateForPrompt()
    {
        goby::glog.is(goby::common::logger::DEBUG1) &&
            goby::glog << group("sensor_command::statechart")
                       << "~*__WAIT_FOR_PROMPT__INTERROGATE_FOR_PROMPT" << std::endl;
    }

    // WaitForPrompt handles EvPromptReceived
    typedef boost::mpl::list<boost::statechart::custom_reaction<EvPromptTimeoutExceeded>> reactions;

    boost::statechart::result react(const EvPromptTimeoutExceeded& ev)
    {
        ++get_prompt_retries_;
        if (get_prompt_retries_ >= max_get_prompt_retries_)
        {
            return this->template transit<on::CriticalFailure>();
        }
        else
        {
            goby::glog.is(goby::common::logger::DEBUG1) &&
                goby::glog << group("sensor_command::statechart") << "Retry " << get_prompt_retries_
                           << std::endl;
            this->send_enter();
            return this->discard_event();
        }
    }

  private:
    void send_enter()
    {
        auto interrogate_raw = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        interrogate_raw.set_raw_data("\r\n");
        this->interprocess().template publish<cgsn::groups::adcp_rdi::raw_out>(interrogate_raw);
        last_enter_time_ = goby::time::now();
    }

    goby::time::MicroTime last_time() const override { return last_enter_time_; }

  private:
    const int max_get_prompt_retries_{5};
    int get_prompt_retries_{0};
    goby::time::MicroTime last_enter_time_;
};

} // namespace wait_for_prompt

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
using ADCP_RDI_Configure = AutoConfigure<Derived, Parent, state, protobuf::ADCP_RDI_SensorConfig,
                                         cgsn::groups::adcp_rdi::raw_out>;

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct ADCP_RDI_StartLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;
    ADCP_RDI_StartLogging(typename StateBase::my_context c) : StateBase(c)
    {
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
                send_start_pinging_command();
                this->post_event(EvLoggingStarted());
                break;

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvLoggingStarted());
                break;
        }
    }
    ~ADCP_RDI_StartLogging() {}

    typedef boost::mpl::list<> local_reactions;

  private:
    void send_start_pinging_command()
    {
        auto start_pinging_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
        start_pinging_cmd.set_raw_data("CS\r\n");
        this->interprocess().template publish<cgsn::groups::adcp_rdi::raw_out>(start_pinging_cmd);
    }
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct ADCP_RDI_StopLogging : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    ADCP_RDI_StopLogging(typename StateBase::my_context c) : StateBase(c)
    {
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
            {
                protobuf::SerialCommand cmd;
                cmd.set_command(protobuf::SerialCommand::SEND_BREAK);

                this->interthread().template publish<cgsn::groups::adcp_rdi::raw_out>(cmd);
                goby::glog.is(goby::common::logger::DEBUG1) &&
                    goby::glog << group("sensor_command::statechart")
                               << "Published cmd: " << cmd.ShortDebugString() << std::endl;
                break;
            }

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvLoggingStopped());
                break;
        }
    }
    ~ADCP_RDI_StopLogging() {}

    typedef boost::mpl::list<boost::statechart::custom_reaction<EvBreakWakeup>> local_reactions;

    boost::statechart::result react(const EvBreakWakeup& ev)
    {
        // sending a break inherently stops logging
        this->post_event(EvLoggingStopped());
        // no further use for the EvBreakWakeup
        return this->discard_event();
    }
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct ADCP_RDI_SensorSleep : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    ADCP_RDI_SensorSleep(typename StateBase::my_context c) : StateBase(c)
    {
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
            {
                auto sleep_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                sleep_cmd.set_raw_data("CZ\r\n");
                this->interprocess().template publish<cgsn::groups::adcp_rdi::raw_out>(sleep_cmd);
                break;
            }

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvSensorSleeping());
                break;
        }
    }
    ~ADCP_RDI_SensorSleep() {}

    typedef boost::mpl::list<> local_reactions;
};

template <typename Derived, typename Parent, protobuf::sensor::CommandState state>
struct ADCP_RDI_SelfTest : boost::statechart::state<Derived, Parent>, Notify<Derived, state>
{
    using StateBase = boost::statechart::state<Derived, Parent>;

    ADCP_RDI_SelfTest(typename StateBase::my_context c) : StateBase(c)
    {
        switch (this->cfg().command().op_mode())
        {
            case protobuf::SensorCommandConfig::NORMAL:
            {
                if (this->cfg().command().run_self_test())
                {
                    auto self_test_cmd = driver_make_with_header<cgsn::protobuf::SensorRaw>();
                    self_test_cmd.set_raw_data("PA\r\n");
                    this->interprocess().template publish<cgsn::groups::adcp_rdi::raw_out>(
                        self_test_cmd);
                }
                else
                {
                    this->post_event(EvSelfTestCompleted());
                }

                break;
            }

            case protobuf::SensorCommandConfig::SENSOR_INTERNAL_SCHEDULE:
                // passthrough
                this->post_event(EvSelfTestCompleted());
                break;
        }
    }

    ~ADCP_RDI_SelfTest() {}

    typedef boost::mpl::list<> local_reactions;
};

} // namespace statechart
} // namespace sensor_command
} // namespace cgsn

CGSN_SENSOR_COMMAND_CREATE_LOCAL_BASE_STATES(ADCP_RDI_)

#include "cgsn-mooring/sensor/command_statechart/implementation.h"

#endif
