// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "adcp_rdi_validator.h"

using namespace goby::common::logger;
using goby::glog;

cgsn::validator::ADCP_RDI_ValidatorThread::ADCP_RDI_ValidatorThread(const protobuf::ValidatorConfig& cfg)
    : ValidatorBaseThread<cgsn::groups::adcp_rdi::raw_in,
                         cgsn::groups::adcp_rdi::raw_in_validated,
                         cgsn::protobuf::ADCP_RDI_Data, cgsn::groups::adcp_rdi::parsed>(cfg)
{
    interthread().publish<cgsn::groups::ready, cgsn::DriverModule>(DriverModule::VALIDATOR);
    glog.add_group("adcp_rdi::validator", goby::common::Colors::magenta);
}

bool cgsn::validator::ADCP_RDI_ValidatorThread::validate_raw(const protobuf::SensorRaw& raw)
{
    // "2018/06/19 15:05:12.22 00003\r\n" or
    // "Hdg: ..."
    // "Temp:  ..."
    // "Bin ..."
    // "  1 ..."
    // "  2 ..."
    // ...
    // " 10 ..."
    std::regex adcp_regex("^[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{2} [0-9]{5}\r\n$|"
                          "^Hdg: .*\r\n$|^Temp: .*\r\n$|"
                          "^Bin .*\r\n$|"
                          "^[ 0-9]{3}[ 0-9.-]*\r\n$");

    return std::regex_match(raw.raw_data(), adcp_regex);
}

bool cgsn::validator::ADCP_RDI_ValidatorThread::validate_parsed(const protobuf::ADCP_RDI_Data& pos)
{
    return false;
}


