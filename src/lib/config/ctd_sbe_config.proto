import "goby/common/protobuf/app3.proto";
import "goby/middleware/protobuf/interprocess_config.proto";
import "cgsn-mooring/config/serial_config.proto";
import "cgsn-mooring/config/sensors_config.proto";
import "dccl/option_extensions.proto";
import "cgsn-mooring/messages/mpic.proto";
import "cgsn-mooring/messages/option_extensions.proto";


package cgsn.protobuf;


message CTD_SBE_SensorConfig
{
    option (dccl.msg) = { unit_system: "si" };
    option (cgsn.msg) = { sensor_cfg { format: "%1%=%2%\r\n"  // %1% = parameter, %2% = value
                                       bool_format: BOOL_Y_N // true: "Y", false: "N"
                                       parameter_delay_sec: 0
                                       fixed_param { value: "OUTPUTEXECUTEDTAG=N\r\n" purpose: "Do not write </Executed> tag after each command" }
                                       fixed_param { value: "ECHO=Y\r\n" purpose: "Echo serial commands back" }
                                       get_cfg_command: "GetCD\r\n"
        }
    };


    // Runtime configurable configuration settings for CTD

    required bool output_salinity = 1 [default = true,
                                       (cgsn.field) = { sensor_cfg { key: "OUTPUTSAL" purpose: "Sets output of salinity data" } }];

    required bool output_sound_velocity = 2 [default = true,
                                             (cgsn.field) = { sensor_cfg { key: "OUTPUTSV" purpose: "Sets output of sound velocity data" } }];

    required bool output_sigmat_v_i = 3 [default = true,
                                         (cgsn.field) = { sensor_cfg { key: "OUTPUTUCSD" purpose: "Sets output of sigma-t, voltage, and current data" } }];

    required int32 measurements_per_sample = 4 [default = 4,
                                                (cgsn.field) = { sensor_cfg { key: "NCYCLES" purpose: "Sets the number of samples per measurement" min: 1 max: 100 } }];

    enum PumpMode
    {
        NO_PUMP = 0;
        RUN_PUMP_BEFORE_SAMPLE = 1;
        RUN_PUMP_DURING_SAMPLE = 2;
    }
    required PumpMode pump_mode = 5 [default = RUN_PUMP_BEFORE_SAMPLE,
                                     (cgsn.field) = { sensor_cfg { key: "PUMPMODE" purpose: "Sets pump operational mode." } }];
    required int32 sample_interval = 10 [default = 10,
                                         (dccl.field) = { units { base_dimensions: "T" }},
                                         (cgsn.field) = { sensor_cfg { key: "SAMPLEINTERVAL" purpose: "Sets the time between samples in seconds" min: 10 max: 14400 } }];

    enum OutputFormat
    {
        OUTPUT_RAW_HEXADECIMAL = 0;
        OUTPUT_CONVERTED_DATA_HEXADECIMAL = 1;
        OUTPUT_RAW_DECIMAL = 2;
        OUTPUT_CONVERTED_DATA_DECIMAL = 3;
        INVALID_OUTPUT_FORMAT = 4;
        OUTPUT_CONVERTED_DATA_DECIMAL_XML = 5;
    }
    required OutputFormat output_format = 11 [default = OUTPUT_CONVERTED_DATA_DECIMAL,
                                              (cgsn.field) = { sensor_cfg { key: "OUTPUTFORMAT" purpose: "Sets the format of the outputted data" } }];

    required bool transmit_real_time = 12 [default = true,
                                           (cgsn.field) = { sensor_cfg { key: "TXREALTIME" purpose: "If true, data are transmitted in real time, otherwise they are only stored on the flash memory." } }];
    required bool optode = 15 [default = false,
                               (cgsn.field) = { sensor_cfg { key: "OPTODE" purpose: "Set true to enable interface with an Aanderaa Optode" } } ];
}

message CTD_SBE_DriverConfig
{
    // required parameters for ApplicationBase3 class
    optional goby.protobuf.App3Config app = 1;
    // required parameters for connecting to 'gobyd'
    optional goby.protobuf.InterProcessPortalConfig interprocess = 2;

    required string instrument_key = 10;
    required cgsn.protobuf.SerialConfig serial = 11;
    optional cgsn.protobuf.ValidatorConfig validator = 12;
    optional cgsn.protobuf.PowerConfig power = 13;
    optional cgsn.protobuf.SensorCommandConfig command = 14;

    required cgsn.protobuf.CTD_SBE_SensorConfig sensor = 20;
}


message CTD_SBE_ParserConfig
{
    optional goby.protobuf.App3Config app = 1;
    optional goby.protobuf.InterProcessPortalConfig interprocess = 2;

    optional cgsn.protobuf.ParserConfig parser = 3;
}


message CTD_SBE_SimulatorConfig
{
    optional goby.protobuf.App3Config app = 1;
    optional goby.protobuf.InterProcessPortalConfig interprocess = 2;

    required cgsn.protobuf.SerialConfig serial = 10;
    required string log = 11;
}


message CTD_SBE_TestConfig
{
    optional goby.protobuf.App3Config app = 1;
    optional cgsn.protobuf.ParserConfig parser = 2;
    optional cgsn.protobuf.ValidatorConfig validator = 4;
}
