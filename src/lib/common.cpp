// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <thread>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <goby/common/time3.h>

#include "cgsn-mooring/messages/header.pb.h"

#include "common.h"

std::string cgsn::system::mooring_name()
{
    // read from /etc/hostname as this is static and won't be affected by networking
    std::ifstream hs("/etc/hostname");
    if (hs.is_open())
    {
        std::string hostname((std::istreambuf_iterator<char>(hs)),
                             std::istreambuf_iterator<char>());
        boost::trim(hostname);
        return hostname.substr(0, hostname.find('-'));
    }

    return "{unknown}";
};

std::string cgsn::system::cpu_name()
{
    std::ifstream hs("/etc/hostname");
    if (hs.is_open())
    {
        std::string hostname((std::istreambuf_iterator<char>(hs)),
                             std::istreambuf_iterator<char>());
        boost::trim(hostname);
        auto delim_pos = hostname.find('-');
        if (delim_pos != std::string::npos && delim_pos + 1 < hostname.size())
            return hostname.substr(delim_pos + 1);
    }

    return "{unknown}";
};

void cgsn::populate_header(cgsn::protobuf::Header& header, std::string thread_name, bool set_time)
{
    // read argv from /proc/self/cmdline
    // look for --app_name (or -a) and use that, or else use the binary name (argv[0])
    static const auto read_process_name = []() -> std::string {
        std::ifstream cmdline("/proc/self/cmdline");
        if (cmdline.is_open())
        {
            std::vector<std::string> argv;
            int app_name_index = 0;
            while (cmdline)
            {
                std::string line;
                std::getline(cmdline, line, '\0');
                if (line == "--app_name" || line == "-a")
                    app_name_index = argv.size() + 1;
                argv.push_back(line);
            }

            if (app_name_index != 0 && app_name_index < argv.size())
                return argv[app_name_index] + ":" + std::to_string(getpid());
            else
            {
                boost::filesystem::path process_path(argv[0]);
                return process_path.filename().native() + ":" + std::to_string(getpid());
            }
        }
        else
        {
            return std::to_string(getpid());
        }
    };

    static const std::string process_name = read_process_name();
    static const std::string mooring_name = cgsn::system::mooring_name();

    if (set_time)
        header.set_mooring_time_with_units(goby::time::now());

    auto* publisher = header.mutable_publisher();

    std::stringstream thread_ss;
    if (!thread_name.empty())
        thread_ss << thread_name << ":";

    thread_ss << std::this_thread::get_id();
    publisher->set_thread(thread_ss.str());
    publisher->set_process(process_name);
    publisher->set_mooring(mooring_name);

    auto key = cgsn::system::instrument_key();
    if (!key.empty())
        publisher->set_instrument_key(key);
}
