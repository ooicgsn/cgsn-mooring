// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <fstream>

#include <goby/common/hdf5_plugin.h>
#include <goby/common/logger.h>
#include <goby/middleware/log.h>
#include <goby/middleware/serialize_parse.h>

using namespace goby::common::logger;
using goby::glog;

namespace cgsn
{

    class HDF5Plugin : public goby::common::HDF5Plugin
    {
    public:
        HDF5Plugin(goby::common::protobuf::HDF5Config* cfg);


    private:
        bool provide_entry(goby::common::HDF5ProtobufEntry* entry) override;
    private:
        std::deque<std::pair<std::string, std::unique_ptr<std::ifstream>>> logs_;
    };

}

extern "C"
{
    goby::common::HDF5Plugin* goby_hdf5_load(goby::common::protobuf::HDF5Config* cfg)
    {
        return new cgsn::HDF5Plugin(cfg);
    }

}


cgsn::HDF5Plugin::HDF5Plugin(goby::common::protobuf::HDF5Config* cfg)
    : goby::common::HDF5Plugin(cfg)
{
    for(auto file : cfg->input_file())
    {
        std::unique_ptr<std::ifstream> log(new std::ifstream(file.c_str()));

        if(!log->is_open())
            glog.is(WARN) && glog << "Could not open " << file << " for reading" << std::endl;
        else
            logs_.push_back(std::make_pair(file, std::move(log)));
    }

    glog.is(VERBOSE) && glog << "Processing log: " << logs_.front().first << std::endl;
}

bool cgsn::HDF5Plugin::provide_entry(goby::common::HDF5ProtobufEntry* entry)
{
    while(!logs_.empty())
    {
        auto& current_log = *logs_.front().second;
        try
        {
            goby::LogEntry log_entry;
            log_entry.parse(&current_log);

            if(log_entry.scheme() == goby::MarshallingScheme::DCCL || log_entry.scheme() == goby::MarshallingScheme::PROTOBUF)
            {
                auto msg = dccl::DynamicProtobufManager::new_protobuf_message<std::unique_ptr<google::protobuf::Message>>(log_entry.type());
                msg->ParseFromArray(&log_entry.data()[0], log_entry.data().size());
                if(msg)
                {
                    entry->channel = std::string(log_entry.group());
                    std::cout << log_entry.type() << " " << log_entry.group() << " " << entry->channel << std::endl;

                    auto desc = msg->GetDescriptor();
                    auto refl = msg->GetReflection();
                    // TODO use common header or some better way to get the time
                    auto time_field_desc = desc->FindFieldByName("logger_time");
                    if(time_field_desc)
                    {
                        switch(time_field_desc->cpp_type())
                        {
                            case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
                                entry->time = refl->GetUInt64(*msg, time_field_desc);
                                break;
                            case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
                                entry->time = refl->GetInt64(*msg, time_field_desc);
                                break;
                            default:
                                entry->time = 0;
                                break;
                       }
                    }
                    else
                    {
                        entry->time = 0;
                    }

                    entry->msg = boost::shared_ptr<google::protobuf::Message>(std::move(msg));
                    return true;
                }
                else
                {
                    glog.is(WARN) && glog << "Protobuf message type " << log_entry.type() << " is not loaded. Skipping." << std::endl;
                }
            }
            else
            {
                glog.is(DEBUG2) && glog << "Skipping scheme: " << log_entry.scheme() << ", group: " << log_entry.group() << ", type: " << log_entry.type() << std::endl;
            }
        }
        catch(std::exception& e)
        {
            if(!current_log.eof())
                glog.is(WARN) && glog << "Error processing log [" << logs_.front().first << "]: " << e.what() << std::endl;

            goby::LogEntry::reset();
            logs_.pop_front();
            if(!logs_.empty())
                glog.is(VERBOSE) && glog << "Processing log: " << logs_.front().first << std::endl;
        }
    }
    return false;

}

