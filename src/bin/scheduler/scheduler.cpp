// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <boost/units/base_units/metric/day.hpp>
#include <boost/units/io.hpp>

#include <google/protobuf/text_format.h>

#include <goby/middleware/single-thread-application.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/scheduler.pb.h"
#include "cgsn-mooring/messages/sensor_status.pb.h"

#include "cgsn-mooring/messages/mpic.pb.h"

#include "cgsn-mooring/scheduler/legacy_convert.h"
#include "cgsn-mooring/scheduler/validate.h"

#include "scheduler.h"

using goby::glog;
using namespace goby::common::logger;

// convert legacy messages to new config format
cgsn::SchedulerConfigurator::SchedulerConfigurator(int argc, char* argv[])
    : goby::common::ProtobufConfigurator<protobuf::SchedulerConfig>(argc, argv)
{
    protobuf::SchedulerConfig& cfg = mutable_cfg();
    for (const auto& legacy_sched : cfg.legacy_hour_sched())
    {
        try
        {
            *cfg.add_schedule() = scheduler::convert_legacy_hour_sched(legacy_sched);
        }
        catch (const scheduler::LegacyScheduleException& e)
        {
            glog.is(DIE) && glog << "Invalid legacy schedule: " << legacy_sched.ShortDebugString()
                                 << ", " << e.what() << std::endl;
        }
    }

    for (const auto& legacy_isched : cfg.legacy_interval_sched())
    {
        try
        {
            *cfg.add_schedule() = scheduler::convert_legacy_interval_sched(legacy_isched);
        }
        catch (const scheduler::LegacyScheduleException& e)
        {
            glog.is(DIE) && glog << "Invalid legacy schedule: " << legacy_isched.ShortDebugString()
                                 << ", " << e.what() << std::endl;
        }
    }
}

cgsn::Scheduler::Scheduler()
    : goby::SingleThreadApplication<protobuf::SchedulerConfig>(10.0 * boost::units::si::hertz)
{
    for (const auto& schedule : cfg().schedule())
    {
        if (sched_data_.count(schedule.port_id()))
        {
            glog.is(DIE) && glog << "Multiple schedules defined for port: " << schedule.port_id()
                                 << std::endl;
        }

        try
        {
            scheduler::validate(schedule);
        }
        catch (const scheduler::InvalidScheduleException& e)
        {
            glog.is(DIE) && glog << "Invalid schedule: " << schedule.ShortDebugString() << ", "
                                 << e.what() << std::endl;
        }

        sched_data_.emplace(
            schedule.port_id(),
            std::unique_ptr<ScheduleData>(new ScheduleData(
                schedule, [this](const protobuf::PortSchedule& ps) { start_timer_expired(ps); },
                [this](const protobuf::PortSchedule& ps) { stop_timer_expired(ps); },
                [this](const protobuf::PortSchedule& ps) { force_stop_timer_expired(ps); })));

        if (schedule.schedule_type() == protobuf::PortSchedule::ALWAYS_ON)
            always_on_pending_.insert(std::make_pair(schedule.port_id(), schedule));
    }

    // subscribe to the MPIC status to see if we need to invoke the force stop on a driver
    interprocess().subscribe<cgsn::groups::supervisor::status, protobuf::SupervisorStatus>(
        [this](const protobuf::SupervisorStatus& status) {
            if (status.has_mpic())
                last_mpic_status_ = status.mpic();
        });

    // subscribe to health message to defer sending ALWAYS_ON schedules until drivers are in
    // in OFF__STANDBY
    if (!always_on_pending_.empty())
    {
        interprocess()
            .subscribe<cgsn::groups::sensor::driver_status, cgsn::protobuf::sensor::DriverStatus>(
                [this](const protobuf::sensor::DriverStatus& health) {
                    auto it = always_on_pending_.find(health.port_id());
                    if (it == always_on_pending_.end())
                        return;

                    // set start logging
                    if (health.command().state() == protobuf::sensor::OFF__STANDBY)
                    {
                        send_scheduler_command(it->second, true);
                    }
                    // successful - clear out ALWAYS_ON schedule pending
                    else if (health.command().state() == protobuf::sensor::ON__LOGGING)
                    {
                        glog.is_debug1() && glog << "Successfully turned on ALWAYS_ON port: "
                                                 << it->first << std::endl;
                        always_on_pending_.erase(it);
                        if (always_on_pending_.empty())
                        {
                            glog.is_debug1() && glog << "All ALWAYS_ON ports are ON__LOGGING"
                                                     << std::endl;
                            interprocess()
                                .unsubscribe<cgsn::groups::sensor::driver_status,
                                             cgsn::protobuf::sensor::DriverStatus>();
                        }
                    }
                });
    }
}

cgsn::Scheduler::~Scheduler()
{
    // turn off all
    for (const auto& schedule : cfg().schedule()) send_scheduler_command(schedule, false);
}

void cgsn::Scheduler::loop()
{
    for (auto& s : sched_data_) s.second->poll();
}

std::string cgsn::Scheduler::compact_schedule_str(const protobuf::PortSchedule& ps)
{
    std::string s;
    google::protobuf::TextFormat::Printer printer;
    printer.SetUseShortRepeatedPrimitives(true);
    printer.SetSingleLineMode(true);
    printer.PrintToString(ps, &s);
    return s;
}

void cgsn::Scheduler::start_timer_expired(const protobuf::PortSchedule& ps)
{
    glog.is(DEBUG2) && glog << "Start: " << compact_schedule_str(ps) << std::endl;
    send_scheduler_command(ps, true);
}

void cgsn::Scheduler::stop_timer_expired(const protobuf::PortSchedule& ps)
{
    glog.is(DEBUG2) && glog << "Stop: " << compact_schedule_str(ps) << std::endl;
    send_scheduler_command(ps, false);
}

void cgsn::Scheduler::force_stop_timer_expired(const protobuf::PortSchedule& ps)
{
    glog.is(DEBUG2) && glog << "Check force stop: " << compact_schedule_str(ps) << std::endl;

    // port ids start at 1
    auto port_id_index = ps.port_id() - 1;
    if (port_id_index >= 0 && port_id_index < last_mpic_status_.cpic_size())
    {
        auto desired_state = cgsn::protobuf::Port::OFF,
             reported_state = last_mpic_status_.cpic(port_id_index).state();

        if (reported_state != desired_state)
        {
            glog.is(WARN) && glog << "Port is still on after force stop timeout. Turning off."
                                  << std::endl;

            auto cmd = make_with_header<cgsn::protobuf::MPICCommand>();
            cmd.set_command(cgsn::protobuf::Port::OFF);
            cmd.set_port_id(ps.port_id());
            interprocess().publish<groups::supervisor::mpic_command>(cmd);

            // TODO restart sensor driver?
        }
        else
        {
            glog.is(DEBUG2) && glog << "Port state OK." << std::endl;
        }
    }
    else
    {
        glog.is(WARN) && glog << "Cannot read port status for port id: " << ps.port_id()
                              << std::endl;
    }
}

void cgsn::Scheduler::send_scheduler_command(const cgsn::protobuf::PortSchedule& ps, bool enabled)
{
    auto cmd = make_with_header<protobuf::SchedulerCommand>();
    cmd.set_type(ps.sensor_type());
    cmd.set_port_id(ps.port_id());
    cmd.set_enable(enabled);
    interprocess().publish<cgsn::groups::scheduler::command>(cmd);
}

cgsn::Scheduler::ScheduleData::ScheduleData(const cgsn::protobuf::PortSchedule& c,
                                            CallbackFunc start_cb, CallbackFunc stop_cb,
                                            CallbackFunc force_stop_cb)
    : cfg_(c), start_cb_(start_cb), stop_cb_(stop_cb), force_stop_cb_(force_stop_cb)
{
    // set initial timers
    set_next_timers();
}

void cgsn::Scheduler::ScheduleData::set_next_timers()
{
    goby::time::MicroTime now = goby::time::now(), interval, reference,
                          offset(cfg_.offset_with_units()), duration(cfg_.duration_with_units()),
                          force_stop_delay(cfg_.force_stop_delay_with_units());

    auto minute = boost::units::metric::minute_base_unit::unit_type(), minutes = minute;
    auto hour = boost::units::metric::hour_base_unit::unit_type(), hours = hour;
    auto day = boost::units::metric::day_base_unit::unit_type(), days = day;

    goby::time::MicroTime one_day(1 * day), one_hour(1 * hour);

    switch (cfg_.schedule_type())
    {
        case cgsn::protobuf::PortSchedule::HOURLY:
            interval = one_hour;
            // start of the current day UTC
            reference = (now / one_day) * one_day;
            reference += offset;

            // if the reference is in the future, rewind it one day
            if (reference > now)
                reference -= one_day;
            break;

        case cgsn::protobuf::PortSchedule::INTERVAL:
            interval = goby::time::MicroTime(cfg_.interval_with_units());
            reference = cfg_.reference_time_with_units();
            reference += offset;
            break;

        default: return;
    }

    goby::time::MicroTime::value_type slots_since_reference = (now - reference) / interval,
                                      next_slot;

    switch (cfg_.schedule_type())
    {
        case cgsn::protobuf::PortSchedule::HOURLY:
        {
            bool next_slot_is_today = false;
            for (auto active_hour : cfg_.hour())
            {
                if (active_hour > slots_since_reference)
                {
                    next_slot_is_today = true;
                    next_slot = active_hour;
                    break;
                }
            }
            if (!next_slot_is_today)
            {
                reference += one_day;
                next_slot = cfg_.hour(0);
                slots_since_reference -= one_day / one_hour;
            }
        }
        break;

        case cgsn::protobuf::PortSchedule::INTERVAL: next_slot = slots_since_reference + 1; break;

        default: break;
    }

    goby::time::MicroTime next_start_time = (reference + next_slot * interval),
                          next_stop_time = next_start_time + duration,
                          next_force_stop_time = next_stop_time + force_stop_delay;

    start_timer.expires_at(goby::time::to_ptime(next_start_time));
    stop_timer.expires_at(goby::time::to_ptime(next_stop_time));
    force_stop_timer.expires_at(goby::time::to_ptime(next_force_stop_time));

    glog.is(DEBUG1) &&
        glog << "Updated timers for port: " << cfg_.port_id()
             << ", reference: " << goby::time::to_ptime(reference) << ", interval: "
             << boost::units::quantity<boost::units::metric::minute_base_unit::unit_type, int>(
                    interval)
             << ", slots since reference: " << slots_since_reference << ", next_slot: " << next_slot
             << ", next_start: " << start_timer.expires_at()
             << ", next_stop: " << stop_timer.expires_at()
             << ", next_force_stop: " << force_stop_timer.expires_at() << std::endl;

    start_timer.async_wait([this](const boost::system::error_code& error) {
        if (error == boost::asio::error::operation_aborted)
            glog.is(WARN) && glog << "Start timer operation aborted:" << cfg_.DebugString()
                                  << std::endl;
        else
            start_cb_(cfg_);
    });
    stop_timer.async_wait([this](const boost::system::error_code& error) {
        if (error == boost::asio::error::operation_aborted)
            glog.is(WARN) && glog << "Stop timer operation aborted" << cfg_.DebugString()
                                  << std::endl;
        else
            stop_cb_(cfg_);
    });
    force_stop_timer.async_wait([this](const boost::system::error_code& error) {
        if (error == boost::asio::error::operation_aborted)
            glog.is(WARN) && glog << "Force stop timer operation aborted" << cfg_.DebugString()
                                  << std::endl;
        else
            force_stop_cb_(cfg_);

        set_next_timers();
    });
}

int main(int argc, char* argv[])
{
    return goby::run<cgsn::Scheduler>(cgsn::SchedulerConfigurator(argc, argv));
}
