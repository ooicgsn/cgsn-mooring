// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SCHEDULER_20180618_H
#define SCHEDULER_20180618_H

#include <boost/asio/deadline_timer.hpp>

#include "cgsn-mooring/config/scheduler_config.pb.h"

namespace cgsn
{
// dummy struct for use with boost::asio::time_traits
struct Goby3Time
{
};
} // namespace cgsn

namespace boost
{
namespace asio
{
/// Time traits specialised for GobyTime so we
/// can use simulation time warp
/// \todo Move this into Goby3
template <> struct time_traits<cgsn::Goby3Time>
{
    /// The time type.
    typedef boost::posix_time::ptime time_type;

    /// The duration type.
    typedef boost::posix_time::time_duration duration_type;

    /// Get the current time.
    static time_type now() { return goby::time::to_ptime(goby::time::now()); }

    /// Add a duration to a time.
    static time_type add(const time_type& t, const duration_type& d) { return t + d; }

    /// Subtract one time from another.
    static duration_type subtract(const time_type& t1, const time_type& t2) { return t1 - t2; }

    /// Test whether one time is less than another.
    static bool less_than(const time_type& t1, const time_type& t2) { return t1 < t2; }

    /// Convert to POSIX duration type.
    static boost::posix_time::time_duration to_posix_duration(const duration_type& d)
    {
        if (goby::time::SimulatorSettings::using_sim_time)
            return d / goby::time::SimulatorSettings::warp_factor;
        else
            return d;
    }
};
} // namespace asio
} // namespace boost

namespace cgsn
{
class SchedulerConfigurator : public goby::common::ProtobufConfigurator<protobuf::SchedulerConfig>
{
  public:
    SchedulerConfigurator(int argc, char* argv[]);
};

class Scheduler : public goby::SingleThreadApplication<protobuf::SchedulerConfig>
{
  public:
    Scheduler();
    ~Scheduler();

    static std::atomic<bool> do_quit;

  private:
    void loop() override;

    void start_timer_expired(const cgsn::protobuf::PortSchedule& ps);
    void stop_timer_expired(const cgsn::protobuf::PortSchedule& ps);

    /// \todo Add to Systemd d-bus to restart driver after force top
    void force_stop_timer_expired(const cgsn::protobuf::PortSchedule& ps);

    void send_scheduler_command(const cgsn::protobuf::PortSchedule& ps, bool enabled);

    std::string compact_schedule_str(const protobuf::PortSchedule& ps);

  private:
    class ScheduleData
    {
      public:
        using CallbackFunc = std::function<void(const cgsn::protobuf::PortSchedule&)>;
        ScheduleData(const cgsn::protobuf::PortSchedule& c, CallbackFunc start_cb,
                     CallbackFunc stop_cb, CallbackFunc force_stop_cb);

        void set_next_timers();
        void poll() { io_.poll(); }

      private:
        const cgsn::protobuf::PortSchedule& cfg_;

        boost::asio::io_service io_;

        // command start sampling
        boost::asio::basic_deadline_timer<Goby3Time> start_timer{io_};
        // command stop sampling
        boost::asio::basic_deadline_timer<Goby3Time> stop_timer{io_};
        // if sensor command not in Off::Standby at this time,
        // command the port off and reset the sensor driver
        boost::asio::basic_deadline_timer<Goby3Time> force_stop_timer{io_};

        CallbackFunc start_cb_;
        CallbackFunc stop_cb_;
        CallbackFunc force_stop_cb_;
    };

    // port id to ScheduleData mapping
    std::map<int, std::unique_ptr<ScheduleData>> sched_data_;

    // port id to ALWAYS_ON schedule until we receive a DriverStatus message with ON__LOGGING
    std::map<int, cgsn::protobuf::PortSchedule> always_on_pending_;

    protobuf::MPICStatus last_mpic_status_;
};
} // namespace cgsn

#endif
