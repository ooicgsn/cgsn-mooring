// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>

#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/units/io.hpp>
#include <boost/units/base_units/metric/day.hpp>

#include <goby/common/logger.h>
#include <goby/common/time3.h>
#include <goby/middleware/log.h>
#include <goby/middleware/single-thread-application.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/config/logger_config.pb.h"
#include "cgsn-mooring/messages/legacy_log.pb.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"

#include "formatters/log_formatter.h"
#include "formatters/shared.h"

using namespace goby::common::logger;
using goby::glog;


namespace cgsn
{
    namespace logger
    {

        class LegacyLogService: public goby::SingleThreadApplication<protobuf::LegacyLogServiceConfig>
        {
        public:
            LegacyLogService();
            ~LegacyLogService() { };

        private:
            std::string getLogPath() const;
            void log(const cgsn::protobuf::SensorRaw& raw, const goby::Group& group);

        private:
            std::shared_ptr<cgsn::logger::LogFormatter> formatter;
            std::string last_path;
            std::ofstream logfile;
        };
    }
}

cgsn::logger::LegacyLogService::LegacyLogService() :
    goby::SingleThreadApplication<protobuf::LegacyLogServiceConfig>()
{
    // Get the log formatter to use
    std::map<cgsn::protobuf::LogOutputFormat, std::shared_ptr<cgsn::logger::LogFormatter>> formatters;
    auto add_formatter = [&formatters](std::shared_ptr<cgsn::logger::LogFormatter> formatter)
    {
        formatters[formatter->config()] = formatter;
    };
    add_formatter(std::make_shared<cgsn::logger::LegacyRawLogFormatter>());
    add_formatter(std::make_shared<cgsn::logger::LegacyRawTimestampedLogFormatter>());
    
    auto search = formatters.find(app_cfg().settings().format());
    if(search == formatters.end())
        glog.is(DIE) && glog << "No known LogFormatter for format " << app_cfg().settings().format() << std::endl;
    formatter = search->second;
    
    // Subscribe to all validated messages coming from sensor drivers
    interprocess().subscribe_regex(
        [this](const std::vector<unsigned char>& data, int scheme, const std::string& type, const goby::Group& group) {
            // Attempt to parse the data as a SensorRaw message
            try
            {
                auto pb = dccl::DynamicProtobufManager::new_protobuf_message<
                    std::shared_ptr<google::protobuf::Message>>(type);
                pb->ParseFromArray(&data[0], data.size());
                auto raw = std::dynamic_pointer_cast<cgsn::protobuf::SensorRaw>(pb);
                this->log(*raw, group);
            }
            catch (const std::exception& e)
            {
                glog.is(WARN) && glog << "Cannot decode message: "
                                      << e.what() << std::endl;
            }
        },
        {goby::MarshallingScheme::DCCL, goby::MarshallingScheme::PROTOBUF}, /* schemes */
        "cgsn\\.protobuf\\.SensorRaw", /* raw sensor messages */
        "cgsn::.*::raw::in_validated" /* any validated raw input group */
    );
}

void cgsn::logger::LegacyLogService::log(const cgsn::protobuf::SensorRaw& raw, const goby::Group& group)
{
    // Filter out messages that aren't related to our instrument_key 
    auto publisher = raw.header().publisher();
    if(!publisher.has_instrument_key())
    {
        glog.is(WARN) && glog << "Message does not have an instrument_key!"
                              << std::endl;
        return;
    }
    if(publisher.instrument_key() != app_cfg().instrument_key())
        return;

    // Create a dummy log entry
    goby::LogEntry log_entry(std::vector<unsigned char>{}, 0, "", group);

    // Ask the formatter to process this message
    auto result = formatter->process_message(log_entry, raw);
    if(!result.second)
        return;  // ignore this log entry

    // Open the log file
    std::string logpath = getLogPath();
    if(logfile.bad() || logpath != last_path)
    {
        // Create the parent directory if it doesn't exist
        try
        {
            boost::filesystem::path parent = boost::filesystem::path(logpath).parent_path();
            boost::filesystem::create_directories(parent);
        }
        catch(std::exception& e) {
            glog.is(WARN) && glog << "Could not create output directory: "
                                  << e.what() << std::endl;
        }

        if(logfile.is_open())
            logfile.close();
        logfile.open(logpath, std::ofstream::out | std::ofstream::app);
        last_path = logpath;

        glog.is(DEBUG2) && glog << "Using log file " << logpath << std::endl;
    }

    if(logfile.bad())
    {
        glog.is(WARN) && glog << "Cannot write to log file" << std::endl;
        return;
    }

    // Write out the formatted log to the file
    logfile << result.first << formatter->delimiter() << std::flush;
}

std::string cgsn::logger::LegacyLogService::getLogPath() const
{
    std::string date_format;
    switch(app_cfg().settings().timestamp())
    {
        case cgsn::protobuf::LegacyLogSettings_TimestampFormat_YYYYMMDD:
            date_format = "%Y%m%d";
            break;
        case cgsn::protobuf::LegacyLogSettings_TimestampFormat_YYYYMMDD_HHMMSS:
            date_format = "%Y%m%d_%H%M%S";
            break;
    }
    
    const std::string instrument = (app_cfg().has_legacy_name() ? app_cfg().legacy_name() : app_cfg().instrument_key());
    const std::string date = cgsn::logger::shared::format_datetime(date_format,
        boost::posix_time::second_clock::universal_time());
    return "/media/data/legacy/cg_data/" + instrument + "/" + date + "." + instrument + ".log";
}

int main(int argc, char* argv[])
{
    int ret = goby::run<cgsn::logger::LegacyLogService>(argc, argv);
    dccl::DynamicProtobufManager::protobuf_shutdown();
    return ret;
}
