// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include "log_formatter.h"


bool cgsn::logger::DebugTextLogFormatter::test(std::ostream *out) const
{
    return true;
}

std::pair<const std::string, bool> cgsn::logger::DebugTextLogFormatter::process_message(const goby::LogEntry &log_entry, const google::protobuf::Message &message) const
{
    std::stringstream stream;
    stream << log_entry.group() << " | "
           << log_entry.type() << " | "
           << message.ShortDebugString();
    return std::make_pair<const std::string, bool>(stream.str(), true);
}
