// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <string>

#include <goby/common/time3.h>
#include <goby/middleware/log.h>

#include "cgsn-mooring/messages/sensor_raw.pb.h"

#include "log_formatter.h"
#include "shared.h"


static bool test_emit_data(const cgsn::logger::LegacyRawTimestampedLogFormatter &formatter, std::ostream *out)
{
    /*
     Test case timestamps converted to microseconds since January 1, 1970 with Python:
        from datetime import datetime
        timestamp = datetime.strptime('2018/10/28 16:00:08.496', '%Y/%m/%d %H:%M:%S.%f')
        epoch = datetime(1970, 1, 1)
        print('%d' % ((timestamp - epoch).total_seconds() * 1E6))
    */
    const unsigned long long mooring_time = 1540742408496000ULL;
    const std::string time_string = "2018/10/28 16:00:08.496";

    // Piece together the expected log output
    const std::string testcase = "testing 123";
    const std::string expected = time_string + " " + testcase;

    // Build up the log message and log entry
    cgsn::protobuf::SensorRaw message;
    message.mutable_header()->set_mooring_time(mooring_time);
    message.set_raw_data(testcase + "\n");

    goby::LogEntry log_entry(std::vector<unsigned char>{}, 0, "", goby::Group("asdf::asdf"));

    // Run the formatter and check the output
    auto output = formatter.process_message(log_entry, message);
    if(out)
    {
        *out << "Expected: " << expected << std::endl;
        *out << "Actual:   " << output.first << std::endl;
    }
    return output.second && output.first == expected;
}


bool cgsn::logger::LegacyRawTimestampedLogFormatter::test(std::ostream *out) const
{
    return test_emit_data(*this, out);
}


std::pair<const std::string, bool> cgsn::logger::LegacyRawTimestampedLogFormatter::process_message(const goby::LogEntry &log_entry, const google::protobuf::Message &_message) const
{
    try
    {
        const cgsn::protobuf::SensorRaw &message = dynamic_cast<const cgsn::protobuf::SensorRaw &>(_message);

        // Strip up to one newline from the end of the data
        std::string data(message.raw_data());
        if(data.back() == '\n')
            data.pop_back();

        std::stringstream stream;
        stream << cgsn::logger::shared::format_datetime("%Y/%m/%d %H:%M:%S%.3f", goby::time::to_ptime(message.header().mooring_time_with_units()))
               << " "
               << data;
        return std::make_pair<const std::string, bool>(stream.str(), true);
    }
    catch(const std::bad_cast& e)
    {
        return std::make_pair<const std::string, bool>("", false);
    }
}
