// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <locale>
#include <sstream>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/date_time.hpp>

#include "shared.h"


std::string cgsn::logger::shared::format_datetime(const std::string &format, const boost::posix_time::ptime &time)
{
    std::stringstream stream;
    stream.imbue(std::locale(std::locale::classic(), new boost::posix_time::time_facet(format.c_str())));
    stream << time;
    std::string output = stream.str();

    // Add support for %.3f because boost::date_time doesn't do it
    if(format.find("%.3f") != std::string::npos)
    {
      stream.str("");
      stream << std::fixed << std::setprecision(3);
      auto td = time.time_of_day();
      stream << (td.fractional_seconds() / ::pow(10, td.num_fractional_digits()));
      boost::algorithm::replace_all(output, "%.3f", stream.str().substr(1));
    }

    return output;
}
