// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <string>

#include <goby/common/time3.h>
#include <goby/middleware/log.h>

#include "cgsn-mooring/messages/sensor_raw.pb.h"

#include "log_formatter.h"
#include "shared.h"


static bool test_emit_data(const cgsn::logger::LegacyRawLogFormatter &formatter, std::ostream *out)
{
    const std::string expected = "testing 123";

    // Build up the log message and log entry
    cgsn::protobuf::SensorRaw message;
    message.set_raw_data(expected);

    goby::LogEntry log_entry(std::vector<unsigned char>{}, 0, "", goby::Group("asdf::asdf"));

    // Run the formatter and check the output
    auto output = formatter.process_message(log_entry, message);
    if(out)
    {
        *out << "Expected: " << expected << std::endl;
        *out << "Actual:   " << output.first << std::endl;
    }
    return output.second && output.first == expected;
}


bool cgsn::logger::LegacyRawLogFormatter::test(std::ostream *out) const
{
    return test_emit_data(*this, out);
}


std::pair<const std::string, bool> cgsn::logger::LegacyRawLogFormatter::process_message(const goby::LogEntry &log_entry, const google::protobuf::Message &_message) const
{
    try
    {
        const cgsn::protobuf::SensorRaw &message = dynamic_cast<const cgsn::protobuf::SensorRaw &>(_message);
        return std::make_pair<const std::string, bool>(std::string(message.raw_data()), true);
    }
    catch(const std::bad_cast& e)
    {
        return std::make_pair<const std::string, bool>("", false);
    }
}
