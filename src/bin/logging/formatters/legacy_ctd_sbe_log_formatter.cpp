// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <string>

#include <goby/common/time3.h>
#include <goby/middleware/log.h>

#include "cgsn-mooring/messages/ctd_sbe.pb.h"

#include "log_formatter.h"
#include "shared.h"


/*
Example log entries from CPM03ISSM/D00009/cg_data/dcl37/ctdbp2/20181028.ctdbp2.log:

2018/10/28 15:58:06.722 [ctdbp2:DLOGP3]:Logger started: Idle state, without initialize
2018/10/28 15:58:14.725 [ctdbp2:DLOGP3]:Battery on - port power on
2018/10/28 16:00:04.342 [ctdbp2:DLOGP3]:time out
2018/10/28 16:00:08.496 # 14.4499,  0.00010,   -0.095, 28 Oct 2018 16:00:06<CR>
2018/10/28 16:03:00.610 [ctdbp2:DLOGP3]:Battery on - port power off
2018/10/28 16:14:01.097 [ctdbp2:DLOGP3]:Battery on - stop

Test case timestamps converted to microseconds since January 1, 1970 with Python:
    from datetime import datetime
    timestamp = datetime.strptime('2018/10/28 16:00:08.496', '%Y/%m/%d %H:%M:%S.%f')
    # or timestamp = datetime.strptime('28 Oct 2018 16:00:06', '%d %b %Y %H:%M:%S')
    epoch = datetime(1970, 1, 1)
    print('%d' % ((timestamp - epoch).total_seconds() * 1E6))
*/

bool cgsn::logger::LegacyCTDSBELogFormatter::test(std::ostream *out) const
{
    std::string expected = "2018/10/28 16:00:08.496 # 14.4499,  0.00010,   -0.095, 28 Oct 2018 16:00:06\r";

    cgsn::protobuf::CTD_SBE_Sample message;
    message.mutable_header()->set_mooring_time(1540742408496000ULL);
    message.mutable_parsed_header()->set_sensor_time(1540742406000000ULL);
    message.set_temperature(14.4499);
    message.set_conductivity(0.00010);
    message.set_pressure(-0.095);

    goby::LogEntry log_entry;
    auto output = this->process_message(log_entry, message);
    if(out)
    {
        *out << "Expected: " << expected << std::endl;
        *out << "Actual:   " << output.first << std::endl;
    }
    return output.second && output.first == expected;
}

std::pair<const std::string, bool> cgsn::logger::LegacyCTDSBELogFormatter::process_message(const goby::LogEntry &log_entry, const google::protobuf::Message &_message) const
{
    try
    {
        const cgsn::protobuf::CTD_SBE_Sample &message = dynamic_cast<const cgsn::protobuf::CTD_SBE_Sample &>(_message);

        std::stringstream stream;
        stream << cgsn::logger::shared::format_datetime("%Y/%m/%d %H:%M:%S%.3f", goby::time::to_ptime(message.header().mooring_time_with_units()))
               << " # "
               << std::setw(7) << std::fixed << std::setprecision(4) << message.temperature() << ", "
               << std::setw(8) << std::fixed << std::setprecision(5) << message.conductivity() << ", "
               << std::setw(8) << std::fixed << std::setprecision(3) << message.pressure() << ", "
               << cgsn::logger::shared::format_datetime("%d %b %Y %H:%M:%S", goby::time::to_ptime(message.parsed_header().sensor_time_with_units()))
               << "\r";
        return std::make_pair<const std::string, bool>(stream.str(), true);
    }
    catch(const std::bad_cast &e)
    {
        return std::make_pair<const std::string, bool>("", false);
    }
}
