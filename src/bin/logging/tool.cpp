// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <memory>
#include <map>

#include <goby/common/application_base3.h>
#include <goby/common/logger.h>
#include <goby/middleware/log.h>
#include <goby/middleware/serialize_parse.h>

#include "cgsn-mooring/config/logger_config.pb.h"

#include "formatters/log_formatter.h"

using namespace goby::common::logger;
using goby::glog;


namespace cgsn
{
    namespace logger
    {
        class Tool : public goby::common::ApplicationBase3<cgsn::protobuf::LogToolConfig>
        {
        public:
            Tool();
            ~Tool() { }


        private:
            // never gets called
            void run() override { }

            std::ifstream f_in_;
            std::ofstream f_out_;
        };
    }
}

int main(int argc, char* argv[])
{ return goby::run<cgsn::logger::Tool>(argc, argv); }

cgsn::logger::Tool::Tool() :
    f_in_(app_cfg().input_file().c_str()),
    f_out_(app_cfg().output_file().c_str())
{
    if(!f_in_.is_open())
        glog.is(DIE) && glog << "Failed to open input file for reading: " << app_cfg().input_file() << std::endl;

    if(!f_out_.is_open())
        glog.is(DIE) && glog << "Failed to open output file for reading: " << app_cfg().output_file() << std::endl;

    // Construct a mapping from --format options to LogFormatter instances
    std::map<cgsn::protobuf::LogOutputFormat, std::shared_ptr<cgsn::logger::LogFormatter>> formatters;
    auto add_formatter = [&formatters](std::shared_ptr<cgsn::logger::LogFormatter> formatter)
    {
        formatters[formatter->config()] = formatter;
    };
    add_formatter(std::make_shared<cgsn::logger::DebugTextLogFormatter>());
    add_formatter(std::make_shared<cgsn::logger::LegacyRawLogFormatter>());
    add_formatter(std::make_shared<cgsn::logger::LegacyRawTimestampedLogFormatter>());
    add_formatter(std::make_shared<cgsn::logger::LegacyCTDSBELogFormatter>());

    // Look up the LogFormatter we want to use
    auto search = formatters.find(app_cfg().format());
    if(search == formatters.end())
        glog.is(DIE) && glog << "No known LogFormatter for format " << app_cfg().format() << std::endl;
    auto formatter = search->second;

    while(true)
    {
        try
        {
            goby::LogEntry log_entry;
            log_entry.parse(&f_in_);

            if(log_entry.scheme() == goby::MarshallingScheme::DCCL || log_entry.scheme() == goby::MarshallingScheme::PROTOBUF)
            {
                auto msg = dccl::DynamicProtobufManager::new_protobuf_message<std::unique_ptr<google::protobuf::Message>>(log_entry.type());
                msg->ParseFromArray(&log_entry.data()[0], log_entry.data().size());
                if(msg)
                {
                    auto converted = formatter->process_message(log_entry, *msg);
                    if(converted.second)
                        f_out_ << converted.first << formatter->delimiter() << std::flush;
                }
                else
                {
                    glog.is(WARN) && glog << "Protobuf message type " << log_entry.type() << " is not loaded. Skipping." << std::endl;
                }
            }
            else
            {
                glog.is(DEBUG2) && glog << "Skipping scheme: " << log_entry.scheme() << ", group: " << log_entry.group() << ", type: " << log_entry.type() << std::endl;
            }
        }
        catch(std::exception& e)
        {
            if(!f_in_.eof())
                glog.is(WARN) && glog << "Error processing input log: " << e.what() << std::endl;

            break;
        }
    }
    quit();
}
