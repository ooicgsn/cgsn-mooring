// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>

#include "cgsn-mooring/config/logger_config.pb.h"

#include "formatters/log_formatter.h"


static
bool test_formatter(const cgsn::logger::LogFormatter &formatter)
{
    std::cout << "Testing " << formatter.name() << std::flush;
    std::ostringstream stream;
    bool passed = formatter.test(&stream);
    if(passed)
    {
        std::cout << " (PASSED)" << std::endl;
    }
    else
    {
        std::cout << " (FAILED!)" << std::endl;
        std::cout << stream.str();
    }
    return passed;
}

int main(int argc, const char *argv[])
{
    bool passed = true;
    passed &= test_formatter(cgsn::logger::DebugTextLogFormatter());
    passed &= test_formatter(cgsn::logger::LegacyRawLogFormatter());
    passed &= test_formatter(cgsn::logger::LegacyRawTimestampedLogFormatter());
    passed &= test_formatter(cgsn::logger::LegacyCTDSBELogFormatter());

    google::protobuf::ShutdownProtobufLibrary();
    return passed ? 0 : 1;
}
