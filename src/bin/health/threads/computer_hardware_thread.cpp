// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

extern "C"
{
#include <sys/sysinfo.h>
}

#include <boost/filesystem.hpp>
#include <boost/units/systems/information/byte.hpp>

#include "system_health_threads.h"

using goby::glog;

cgsn::health::ComputerHardwareThread::ComputerHardwareThread(
    const protobuf::ComputerHardwareConfig& cfg)
    : HealthMonitorThread(cfg, "computer_hardware")
{
}

void cgsn::health::ComputerHardwareThread::issue_status_summary()
{
    status_ = make_with_header<protobuf::ComputerHardwareStatus>(thread_name());
    sysinfo_successful_ = read_sysinfo();
    meminfo_successful_ = read_meminfo();
    disk_check_successful_ = read_disk_usage();

    glog.is_debug2() && glog << group(thread_name()) << "Status: " << status_.DebugString()
                             << std::endl;

    interprocess().publish<cgsn::groups::computer_hardware::status>(status_);
}

bool cgsn::health::ComputerHardwareThread::read_sysinfo()
{
    struct sysinfo sys;
    if (sysinfo(&sys) != 0)
        return false;

    status_.set_uptime_with_units(sys.uptime * boost::units::si::seconds);

    auto& proc = *status_.mutable_processor();

    auto& loads = *proc.mutable_loads();
    loads.set_one_min(sys.loads[0] / (float)(1 << SI_LOAD_SHIFT));
    loads.set_five_min(sys.loads[1] / (float)(1 << SI_LOAD_SHIFT));
    loads.set_fifteen_min(sys.loads[2] / (float)(1 << SI_LOAD_SHIFT));

    proc.set_num_processes(sys.procs);
    proc.set_num_processors(get_nprocs());

    return true;
}

bool cgsn::health::ComputerHardwareThread::read_meminfo()
{
    std::ifstream meminfo_ifs("/proc/meminfo");

    if (!meminfo_ifs.is_open())
        return false;

    auto& mem = *status_.mutable_memory();

    std::string line;
    while (std::getline(meminfo_ifs, line))
    {
        std::vector<std::string> parts;
        boost::split(parts, line, boost::is_any_of(" "), boost::token_compress_on);

        const int expected_parts = 3;
        enum
        {
            KEY = 0,
            VALUE = 1,
            UNIT = 2
        };
        if (parts.size() != expected_parts)
            continue;

        using boost::units::information::bytes;
        using boost::units::si::kilo;

        unsigned long long value = 0;

        try
        {
            value = std::stoull(parts[VALUE]);
        }
        catch (std::invalid_argument& e)
        {
            continue;
        }

        if (parts[KEY] == "MemTotal:")
            mem.mutable_ram()->set_total_with_units(value * kilo * bytes);
        else if (parts[KEY] == "MemAvailable:")
            mem.mutable_ram()->set_available_with_units(value * kilo * bytes);
        else if (parts[KEY] == "SwapTotal:")
            mem.mutable_swap()->set_total_with_units(value * kilo * bytes);
        else if (parts[KEY] == "SwapFree:")
            mem.mutable_swap()->set_available_with_units(value * kilo * bytes);
    }
    set_use_fraction(*mem.mutable_ram());
    set_use_fraction(*mem.mutable_swap());

    // we didn't get all the values for some reason
    if (!mem.IsInitialized())
    {
        status_.clear_memory();
        return false;
    }

    return true;
}

void cgsn::health::ComputerHardwareThread::set_use_fraction(
    protobuf::ComputerHardwareStatus::Information& info)
{
    using quant_float_bytes =
        boost::units::quantity<protobuf::ComputerHardwareStatus::Information::available_unit,
                               float>;
    auto total = info.total_with_units<quant_float_bytes>();
    auto available = info.available_with_units<quant_float_bytes>();
    info.set_use_percent(100.0f * (total - available) / total);
}

bool cgsn::health::ComputerHardwareThread::read_disk_usage()
{
    using boost::units::information::bytes;
    auto& disk = *status_.mutable_disk();

    bool ok = true;
    try
    {
        auto rootfs_spaceinfo = boost::filesystem::space("/");
        disk.mutable_rootfs()->set_total_with_units(rootfs_spaceinfo.capacity * bytes);
        disk.mutable_rootfs()->set_available_with_units(rootfs_spaceinfo.available * bytes);
        set_use_fraction(*disk.mutable_rootfs());
    }
    catch (boost::filesystem::filesystem_error& e)
    {
        glog.is_warn() && glog << group(thread_name())
                               << "Failed to read rootfs filesystem information: " << e.what();
        ok = false;
    }

    try
    {
        auto data_spaceinfo = boost::filesystem::space(cfg().data_disk_mountpoint());
        disk.mutable_data()->set_total_with_units(data_spaceinfo.capacity * bytes);
        disk.mutable_data()->set_available_with_units(data_spaceinfo.available * bytes);
        set_use_fraction(*disk.mutable_data());
    }
    catch (boost::filesystem::filesystem_error& e)
    {
        glog.is_warn() && glog << group(thread_name())
                               << "Failed to read data filesystem information (from "
                               << cfg().data_disk_mountpoint() << "): " << e.what();
        ok = false;
    }
    return ok;
}

void cgsn::health::ComputerHardwareThread::populate_health_report(protobuf::ModuleHealth& health)
{
    auto health_state = protobuf::HEALTH__OK;

    {
        auto& proc_health = *health.add_child();
        proc_health.set_module(protobuf::MODULE__COMPUTER_HARDWARE__PROCESSOR);
        auto proc_health_state = protobuf::HEALTH__OK;
        auto nproc = status_.processor().num_processors();
        const auto& loads = status_.processor().loads();
        if (!sysinfo_successful_)
        {
            proc_health_state = protobuf::HEALTH__FAILED;
            proc_health.add_error()->set_code(
                protobuf::Error::COMPUTER_HARDWARE__CANNOT_READ_SYSINFO);
        }
        else
        {
            auto set_load_factor_error = [&proc_health, &loads](protobuf::Error::ErrorCode code,
                                                                float threshold) {
                auto& err = *proc_health.add_error();
                err.set_code(code);
                std::stringstream ss;
                ss << "load average: " << loads.one_min() << ", " << loads.five_min() << ", "
                   << loads.fifteen_min() << " exceed threshold of " << threshold
                   << "*num_processors";
                err.set_text(ss.str());
            };

            if (loads.one_min() / nproc > cfg().critical_load_factor() ||
                loads.five_min() / nproc > cfg().critical_load_factor() ||
                loads.fifteen_min() / nproc > cfg().critical_load_factor())
            {
                proc_health_state = protobuf::HEALTH__FAILED;
                set_load_factor_error(protobuf::Error::COMPUTER_HARDWARE__LOAD_FACTOR_CRITICAL,
                                      cfg().critical_load_factor());
            }
            else if (loads.one_min() / nproc > cfg().high_load_factor() ||
                     loads.five_min() / nproc > cfg().high_load_factor() ||
                     loads.fifteen_min() / nproc > cfg().high_load_factor())
            {
                proc_health_state = protobuf::HEALTH__DEGRADED;
                set_load_factor_error(protobuf::Error::COMPUTER_HARDWARE__LOAD_FACTOR_HIGH,
                                      cfg().high_load_factor());
            }
        }

        proc_health.set_state(proc_health_state);
        demote_health(health_state, proc_health.state());
    }

    {
        auto& mem_health = *health.add_child();
        mem_health.set_module(protobuf::MODULE__COMPUTER_HARDWARE__MEMORY);
        auto mem_health_state = protobuf::HEALTH__OK;

        if (!meminfo_successful_)
        {
            mem_health_state = protobuf::HEALTH__FAILED;
            mem_health.add_error()->set_code(
                protobuf::Error::COMPUTER_HARDWARE__CANNOT_READ_MEMINFO);
        }
        else
        {
            using boost::units::quantity;
            using boost::units::information::bytes;
            using boost::units::si::mega;

            auto set_mem_error = [&mem_health, this](protobuf::Error::ErrorCode code) {
                auto& err = *mem_health.add_error();
                err.set_code(code);
                std::stringstream ss;
                std::uint64_t mb_available(
                    status_.memory()
                        .ram()
                        .available_with_units<quantity<decltype(mega * bytes), std::uint64_t>>()
                        .value());
                ss << "Available RAM: " << mb_available << " MB";
                err.set_text(ss.str());
            };

            if ((100 - status_.memory().ram().use_percent()) <
                cfg().ram_critical_available_percentage())
            {
                mem_health_state = protobuf::HEALTH__FAILED;
                set_mem_error(protobuf::Error::COMPUTER_HARDWARE__RAM_SPACE_CRITICAL);
            }
            else if ((100 - status_.memory().ram().use_percent()) <
                     cfg().ram_low_available_percentage())
            {
                mem_health_state = protobuf::HEALTH__DEGRADED;
                set_mem_error(protobuf::Error::COMPUTER_HARDWARE__RAM_SPACE_LOW);
            }
        }

        mem_health.set_state(mem_health_state);
        demote_health(health_state, mem_health.state());
    }

    {
        auto& disk_health = *health.add_child();
        disk_health.set_module(protobuf::MODULE__COMPUTER_HARDWARE__DISK);
        auto disk_health_state = protobuf::HEALTH__OK;
        if (!disk_check_successful_)
        {
            disk_health_state = protobuf::HEALTH__FAILED;
            disk_health.add_error()->set_code(
                protobuf::Error::COMPUTER_HARDWARE__CANNOT_READ_DISK_USAGE);
        }
        else
        {
            auto set_disk_error = [&disk_health_state, &disk_health](
                                      protobuf::ModuleHealthState state,
                                      protobuf::Error::ErrorCode code, const std::string& mount,
                                      const protobuf::ComputerHardwareStatus::Information& space) {
                if (disk_health_state < state)
                    disk_health_state = state;
                auto& err = *disk_health.add_error();
                err.set_code(code);
                std::stringstream ss;
                std::uint64_t mb_available(
                    space
                        .available_with_units<boost::units::quantity<
                            decltype(boost::units::si::mega * boost::units::information::bytes),
                            std::uint64_t>>()
                        .value());
                ss << "Available space on '" << mount << "': " << mb_available << " MB";
                err.set_text(ss.str());
            };

            if ((100 - status_.disk().rootfs().use_percent()) <
                cfg().disk_critical_available_percentage())
            {
                set_disk_error(protobuf::HEALTH__FAILED,
                               protobuf::Error::COMPUTER_HARDWARE__ROOTFS_DISK_SPACE_CRITICAL, "/",
                               status_.disk().rootfs());
            }
            else if ((100 - status_.disk().rootfs().use_percent()) <
                     cfg().disk_low_available_percentage())
            {
                set_disk_error(protobuf::HEALTH__DEGRADED,
                               protobuf::Error::COMPUTER_HARDWARE__ROOTFS_DISK_SPACE_LOW, "/",
                               status_.disk().rootfs());
            }

            if ((100 - status_.disk().data().use_percent()) <
                cfg().disk_critical_available_percentage())
            {
                set_disk_error(protobuf::HEALTH__FAILED,
                               protobuf::Error::COMPUTER_HARDWARE__DATA_DISK_SPACE_CRITICAL,
                               cfg().data_disk_mountpoint(), status_.disk().data());
            }
            else if ((100 - status_.disk().data().use_percent()) <
                     cfg().disk_low_available_percentage())
            {
                set_disk_error(protobuf::HEALTH__DEGRADED,
                               protobuf::Error::COMPUTER_HARDWARE__DATA_DISK_SPACE_LOW,
                               cfg().data_disk_mountpoint(), status_.disk().data());
            }
        }

        disk_health.set_state(disk_health_state);
        demote_health(health_state, disk_health.state());
    }

    health.set_state(health_state);
}
