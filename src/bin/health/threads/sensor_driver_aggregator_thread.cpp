// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "system_health_threads.h"

using goby::glog;

cgsn::health::SensorDriverAggregatorThread::SensorDriverAggregatorThread(
    const protobuf::SensorDriverAggregatorConfig& cfg)
    : HealthMonitorThread(cfg, "sensor_driver_aggregator"), driver_aggregator_(child_modules())
{
    interprocess().subscribe<cgsn::groups::sensor::driver_status, protobuf::sensor::DriverStatus>(
        [this](const protobuf::sensor::DriverStatus& status) { handle_status(status); });
}

void cgsn::health::SensorDriverAggregatorThread::populate_health_report(
    protobuf::ModuleHealth& health)
{
    driver_aggregator_.expire_reports();
    driver_aggregator_.populate_report(health, EnumValueOptions::HealthData::LEVEL__CPU__TIER2);

    glog.is_debug3() && glog << group(thread_name()) << "Health: " << health.DebugString()
                             << std::endl;
}

void cgsn::health::SensorDriverAggregatorThread::issue_status_summary()
{
    auto summary = make_with_header<protobuf::sensor::DriverSummary>(thread_name());
    for (const auto& status_pair : status_messages_)
    {
        const auto& status = status_pair.second;
        auto& wrapper = *summary.add_driver();
        goby::time::MicroTime now(summary.header().mooring_time_with_units()),
            status_time(status.header().mooring_time_with_units());
        wrapper.set_report_age_with_units(now - status_time);
        *wrapper.mutable_status() = status;
        wrapper.mutable_status()->clear_header();
    }

    glog.is_debug3() && glog << group(thread_name()) << "Summary: " << summary.DebugString()
                             << std::endl;

    interprocess().publish<cgsn::groups::sensor::driver_status>(summary);
}

std::multimap<cgsn::protobuf::Module, int>
cgsn::health::SensorDriverAggregatorThread::child_modules()
{
    std::multimap<cgsn::protobuf::Module, int> modules_map;

    for (auto port_id : cfg().configured_port())
        modules_map.insert(std::make_pair(protobuf::MODULE__SENSOR_DRIVER__PORT_N, port_id));

    return modules_map;
}

void cgsn::health::SensorDriverAggregatorThread::handle_status(
    const cgsn::protobuf::sensor::DriverStatus& status)
{
    status_messages_[status.port_id()] = status;

    auto health = make_with_header<cgsn::protobuf::ModuleHealth>(thread_name());
    health.set_module(protobuf::MODULE__SENSOR_DRIVER__PORT_N);
    health.set_id(status.port_id());
    health.set_name(status.header().publisher().instrument_key());

    protobuf::ModuleHealthState health_state = protobuf::HEALTH__OK;

    switch (status.command().state())
    {
        case protobuf::sensor::ON__CRITICAL_FAILURE:
        case protobuf::sensor::OFF__CRITICAL_FAILURE:
            demote_health(health_state, protobuf::HEALTH__FAILED);
            break;
        default: break;
    }
    switch (status.validator().state())
    {
        case protobuf::sensor::LOGGING__CRITICAL_FAILURE:
            demote_health(health_state, protobuf::HEALTH__FAILED);
            break;
        case protobuf::sensor::LOGGING__DEGRADED:
            demote_health(health_state, protobuf::HEALTH__DEGRADED);
            break;
        default: break;
    }
    switch (status.io().state())
    {
        case protobuf::IO__CRITICAL_FAILURE:
            demote_health(health_state, protobuf::HEALTH__FAILED);
            break;

        default: break;
    }

    if (status.command().has_error())
        *health.add_error() = status.command().error();
    if (status.validator().has_error())
        *health.add_error() = status.validator().error();
    if (status.io().has_error())
        *health.add_error() = status.io().error();

    health.set_state(health_state);
    driver_aggregator_.file_report(health);

    // force update
    issue_health_report();
}
