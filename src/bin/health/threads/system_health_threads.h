// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SYSTEM_HEALTH_THREADS_20190212_H
#define SYSTEM_HEALTH_THREADS_20190212_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/config/system_health_config.pb.h"
#include "cgsn-mooring/health.h"
#include "cgsn-mooring/messages/computer_hardware.pb.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/module_health.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/ntp_status.pb.h"
#include "cgsn-mooring/messages/sensor_status.pb.h"

#include "../report_aggregator.h"

namespace cgsn
{
namespace health
{
template <typename Config, protobuf::Module module>
class HealthMonitorThread : public goby::SimpleThread<Config>
{
  public:
    HealthMonitorThread(const Config& cfg, std::string thread_name)
        : goby::SimpleThread<Config>(cfg, module_report_freq(module)), thread_name_(thread_name)
    {
    }
    virtual ~HealthMonitorThread() {}

    const std::string& thread_name() { return thread_name_; }

  protected:
    void issue_health_report()
    {
        auto health = cgsn::make_with_header<protobuf::ModuleHealth>(thread_name());
        health.set_module(module);
        populate_health_report(health);
        if (!health.has_state())
            health.set_state(protobuf::HEALTH__FAILED);

        this->interthread().template publish<cgsn::groups::health>(health);
    }

    // sets current_state to check_state if check_state is worse (e.g. OK -> FAILED)
    // does not update current_state if check_state is better (e.g. FAILED -/-> OK)
    void demote_health(protobuf::ModuleHealthState& current_state,
                       protobuf::ModuleHealthState check_state)
    {
        if (current_state < check_state)
            current_state = check_state;
    }

  private:
    void loop() override
    {
        issue_status_summary();
        issue_health_report();
    }

    virtual void populate_health_report(protobuf::ModuleHealth& health) = 0;
    virtual void issue_status_summary() {}

  private:
    std::string thread_name_;
};

class ComputerHardwareThread : public HealthMonitorThread<protobuf::ComputerHardwareConfig,
                                                          protobuf::MODULE__COMPUTER_HARDWARE>
{
  public:
    ComputerHardwareThread(const protobuf::ComputerHardwareConfig& cfg);
    ~ComputerHardwareThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;
    void issue_status_summary() override;

    bool read_sysinfo();
    bool read_meminfo();
    bool read_disk_usage();
    void set_use_fraction(protobuf::ComputerHardwareStatus::Information& info);

  private:
    protobuf::ComputerHardwareStatus status_;

    bool sysinfo_successful_{true};
    bool meminfo_successful_{true};
    bool disk_check_successful_{true};
};

class NTPStatusThread
    : public HealthMonitorThread<protobuf::NTPStatusConfig, protobuf::MODULE__TIME>
{
  public:
    NTPStatusThread(const protobuf::NTPStatusConfig& cfg);
    ~NTPStatusThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;
    void issue_status_summary() override;

    bool read_ntpq_system_status();
    bool read_ntpq_peers();

  private:
    protobuf::NTPStatus status_;
    bool ntpq_system_status_successful_{true};
    bool ntpq_peers_successful_{true};
};

class SensorDriverAggregatorThread
    : public HealthMonitorThread<protobuf::SensorDriverAggregatorConfig,
                                 protobuf::MODULE__SENSOR_DRIVER>
{
  public:
    SensorDriverAggregatorThread(const protobuf::SensorDriverAggregatorConfig& cfg);
    ~SensorDriverAggregatorThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;
    void issue_status_summary() override;

    void handle_status(const cgsn::protobuf::sensor::DriverStatus& status);

    std::multimap<cgsn::protobuf::Module, int> child_modules();

  private:
    ReportAggregator driver_aggregator_;

    // port id to latest status message
    std::map<int, protobuf::sensor::DriverStatus> status_messages_;
};

class SupervisorThread
    : public HealthMonitorThread<protobuf::SupervisorHealthConfig, protobuf::MODULE__SUPERVISOR>
{
  public:
    SupervisorThread(const protobuf::SupervisorHealthConfig& cfg);
    ~SupervisorThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;

  private:
    cgsn::protobuf::SupervisorStatus status_;
};

class SchedulerThread
    : public HealthMonitorThread<protobuf::SchedulerHealthConfig, protobuf::MODULE__SCHEDULER>
{
  public:
    SchedulerThread(const protobuf::SchedulerHealthConfig& cfg);
    ~SchedulerThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;

  private:
};

class LoggerThread
    : public HealthMonitorThread<protobuf::LoggerHealthConfig, protobuf::MODULE__LOGGER>
{
  public:
    LoggerThread(const protobuf::LoggerHealthConfig& cfg);
    ~LoggerThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;

  private:
};

class GPSThread : public HealthMonitorThread<protobuf::GPSHealthConfig, protobuf::MODULE__GPS>
{
  public:
    GPSThread(const protobuf::GPSHealthConfig& cfg);
    ~GPSThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;

  private:
};

class LegacyThread
    : public HealthMonitorThread<protobuf::LegacyHealthConfig, protobuf::MODULE__LEGACY>
{
  public:
    LegacyThread(const protobuf::LegacyHealthConfig& cfg);
    ~LegacyThread() {}

  private:
    void populate_health_report(protobuf::ModuleHealth& health) override;

  private:
};

} // namespace health
} // namespace cgsn

#endif
