// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "system_health_threads.h"

cgsn::health::SupervisorThread::SupervisorThread(const protobuf::SupervisorHealthConfig& cfg)
    : HealthMonitorThread(cfg, "supervisor")
{
    interprocess().subscribe<cgsn::groups::supervisor::status, cgsn::protobuf::SupervisorStatus>(
        [this](const cgsn::protobuf::SupervisorStatus& status) {
            status_ = status;
            issue_health_report();
        });
}

void cgsn::health::SupervisorThread::populate_health_report(protobuf::ModuleHealth& health)
{
    auto health_state = protobuf::HEALTH__OK;

    if (status_.IsInitialized())
    {
        // this health report is based on the last state we received
        health.mutable_header()->set_mooring_time(status_.header().mooring_time());
        switch (status_.io().state())
        {
            case protobuf::IO__CRITICAL_FAILURE:
                demote_health(health_state, protobuf::HEALTH__FAILED);
                break;

            default: break;
        }

        if (status_.io().has_error())
            *health.add_error() = status_.io().error();

        if (status_.has_mpic())
        {
            auto& mpic_health = *health.add_child();
            mpic_health.set_module(protobuf::MODULE__MPIC);
            auto mpic_health_state = protobuf::HEALTH__OK;
            {
                auto& cpic_health = *mpic_health.add_child();
                cpic_health.set_module(protobuf::MODULE__CPIC);
                auto cpic_health_state = protobuf::HEALTH__OK;

                for (int i = 0, n = status_.mpic().error_size(); i < n; ++i)
                {
                    auto err = status_.mpic().error(i);
                    switch (err)
                    {
                        // CPIC related errors
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_OVERCURRENT:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_1_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_2_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_3_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_4_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_5_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_6_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_7_RX:
                        case protobuf::Error::MPIC__DCL_ERR_CPIC_8_RX:
                            demote_health(cpic_health_state, protobuf::HEALTH__DEGRADED);
                            cpic_health.add_error()->set_code(err);
                            break;

                        // consider the rest to be MPIC related errors
                        default:
                            demote_health(mpic_health_state, protobuf::HEALTH__DEGRADED);
                            mpic_health.add_error()->set_code(err);
                            break;
                    }
                }

                cpic_health.set_state(cpic_health_state);
                demote_health(mpic_health_state, cpic_health.state());
            }

            mpic_health.set_state(mpic_health_state);
            demote_health(health_state, mpic_health.state());
        }
        else
        {
            demote_health(health_state, protobuf::HEALTH__FAILED);
            health.add_error()->set_code(protobuf::Error::SUPERVISOR__MPIC_NOT_REPORTING);
        }
    }
    else
    {
        health_state = protobuf::HEALTH__INITIALIZING;
    }

    health.set_state(health_state);
}
