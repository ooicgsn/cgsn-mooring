// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef COMMON_20190213_H
#define COMMON_20190213_H

namespace cgsn
{
namespace health
{
inline const cgsn::EnumValueOptions::HealthData& health_metadata(cgsn::protobuf::Module module)
{
    const auto* module_desc = cgsn::protobuf::Module_descriptor();
    const auto* val_desc = module_desc->FindValueByNumber(module);
    return val_desc->options().GetExtension(cgsn::ev).health();
}

inline boost::units::quantity<boost::units::si::frequency>
module_report_freq(cgsn::protobuf::Module module)
{
    return 1.0 / health_metadata(module).report_interval_with_units();
}

inline cgsn::EnumValueOptions::HealthData::HealthLevel module_level(cgsn::protobuf::Module module)
{
    return health_metadata(module).level();
}

} // namespace health
} // namespace cgsn

#endif
