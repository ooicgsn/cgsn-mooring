// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef REPORT_AGGREGATOR_20190213_H
#define REPORT_AGGREGATOR_20190213_H

#include <atomic>
#include <set>

#include <goby/common/time3.h>

#include "cgsn-mooring/common.h"
#include "cgsn-mooring/health.h"

namespace cgsn
{
namespace health
{
class ReportAggregator
{
  public:
    // construct with a set of modules to track
    ReportAggregator(const std::set<cgsn::protobuf::Module>& modules);
    // construct with a map of modules and ids to track
    ReportAggregator(const std::multimap<cgsn::protobuf::Module, int>& modules_map);
    ~ReportAggregator() {}

    void file_report(const protobuf::ModuleHealth& health);
    void expire_reports();
    void populate_report(protobuf::ModuleHealth& health,
                         cgsn::EnumValueOptions::HealthData::HealthLevel level);

    static void set_overdue_factor(double factor) { overdue_factor_ = factor; }

    const int default_id{-1};

    // placeholder for reports that haven't been published at all yet
    const std::string initializing_publisher_{"REPORT_AGGREGATOR_INITIALIZING"};

  private:
    void initialize(const std::multimap<cgsn::protobuf::Module, int>& modules_map);

    void update_children_report_age(goby::time::MicroTime offset, protobuf::ModuleHealth& health);

  private:
    // map level -> module -> id -> health
    std::map<cgsn::EnumValueOptions::HealthData::HealthLevel,
             std::map<cgsn::protobuf::Module, std::map<int, cgsn::protobuf::ModuleHealth>>>
        health_reports_;

    static std::atomic<double> overdue_factor_;
};

} // namespace health
} // namespace cgsn

#endif
