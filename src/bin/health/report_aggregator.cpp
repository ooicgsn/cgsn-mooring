// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/common/logger.h>
#include <goby/common/time3.h>

#include "report_aggregator.h"

using goby::glog;

std::atomic<double> cgsn::health::ReportAggregator::overdue_factor_;

cgsn::health::ReportAggregator::ReportAggregator(const std::set<cgsn::protobuf::Module>& modules)
{
    std::multimap<cgsn::protobuf::Module, int> modules_map;
    for (auto module : modules) modules_map.insert(std::make_pair(module, default_id));

    initialize(modules_map);
}

cgsn::health::ReportAggregator::ReportAggregator(
    const std::multimap<cgsn::protobuf::Module, int>& modules_map)
{
    initialize(modules_map);
}

void cgsn::health::ReportAggregator::initialize(
    const std::multimap<cgsn::protobuf::Module, int>& modules_map)
{
    // initialize map
    for (auto module_pair : modules_map)
    {
        auto module = module_pair.first;
        auto id = module_pair.second;
        auto level = module_level(module);

        auto health_stub = cgsn::make_with_header<protobuf::ModuleHealth>();
        // insert a placeholder so we know this isn't a real report
        health_stub.mutable_header()->mutable_publisher()->set_process(initializing_publisher_);
        health_stub.set_module(module);
        if (id != default_id)
            health_stub.set_id(id);
        health_stub.set_state(protobuf::HEALTH__INITIALIZING);
        health_reports_[level][module][id] = health_stub;
    }
}

void cgsn::health::ReportAggregator::file_report(const protobuf::ModuleHealth& health)
{
    auto module = health.module();
    auto level = module_level(module);
    int id = health.id();

    // only file if we're tracking these reports
    if (health_reports_.count(level))
    {
        auto& level_reports = health_reports_.at(level);
        if (level_reports.count(module))
        {
            auto& id_reports = level_reports.at(module);
            if (id_reports.count(id))
                id_reports[id] = health;
        }
    }
}

void cgsn::health::ReportAggregator::expire_reports()
{
    auto now = goby::time::now();
    for (auto& level_reports : health_reports_)
    {
        for (auto& module_pair : level_reports.second)
        {
            for (auto& id_health_pair : module_pair.second)
            {
                auto id = id_health_pair.first;
                auto& report = id_health_pair.second;
                auto module = report.module();
                decltype(now) report_time(report.header().mooring_time_with_units()),
                    allowed_staleness(health_metadata(module).report_interval_with_units() *
                                      static_cast<double>(overdue_factor_));

                if (report.state() != protobuf::HEALTH__FAILED &&
                    now > report_time + allowed_staleness)
                {
                    bool no_report_ever = (report.state() == protobuf::HEALTH__INITIALIZING);
                    glog.is_warn() && glog
                                          << "Missing report from: "
                                          << ((id != default_id)
                                                  ? (std::string("id: ") + std::to_string(id) + " ")
                                                  : std::string())
                                          << cgsn::protobuf::Module_Name(module) << std::endl;

                    report.set_state(protobuf::HEALTH__FAILED);
                    report.clear_error();
                    auto* error = report.add_error();
                    if (no_report_ever)
                        error->set_code(
                            protobuf::Error::SYSTEM_HEALTH__MODULE_NEVER_REPORTED_HEALTH);
                    else
                        error->set_code(
                            protobuf::Error::SYSTEM_HEALTH__MODULE_STOPPED_REPORTING_HEALTH);
                }
            }
        }
    }
}

void cgsn::health::ReportAggregator::populate_report(
    cgsn::protobuf::ModuleHealth& health, cgsn::EnumValueOptions::HealthData::HealthLevel level)
{
    auto state = protobuf::HEALTH__INITIALIZING;

    bool have_initializing_children = false;

    for (const auto& module_pair : health_reports_.at(level))
    {
        for (auto& id_health_pair : module_pair.second)
        {
            const auto& report = id_health_pair.second;
            if (report.state() > state)
                state = report.state();

            if (report.state() == protobuf::HEALTH__INITIALIZING)
                have_initializing_children = true;

            auto& child = *health.add_child();
            child = report;

            // update the report time, but only for real reports received from elsewhere
            if (child.header().publisher().process() != initializing_publisher_)
            {
                // write the children times as an offset (report_age) from the main (root) message header.mooring_time
                goby::time::MicroTime now(health.header().mooring_time_with_units()),
                    child_time(child.header().mooring_time_with_units());
                // this involves updating the report_age recursively
                update_children_report_age(now - child_time, child);
            }
            child.clear_header();
        }
    }

    // stay in in INITIALIZING until all children report or time out
    if (have_initializing_children)
        state = protobuf::HEALTH__INITIALIZING;

    health.set_state(state);
}

// update "report_age" to be relative to the root message
void cgsn::health::ReportAggregator::update_children_report_age(goby::time::MicroTime offset,
                                                                protobuf::ModuleHealth& health)
{
    for (int i = 0, n = health.child_size(); i < n; ++i)
        update_children_report_age(offset, *health.mutable_child(i));

    goby::time::MicroTime old_report_age(health.report_age_with_units());
    health.set_report_age_with_units(old_report_age + offset);
}
