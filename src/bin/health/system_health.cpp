// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "report_aggregator.h"
#include "threads/system_health_threads.h"

namespace cgsn
{
namespace health
{
class SystemHealth : public goby::MultiThreadApplication<protobuf::SystemHealthConfig>
{
  public:
    SystemHealth();
    ~SystemHealth(){};

  private:
    void loop() override;

    std::set<cgsn::protobuf::Module> tier1_modules()
    {
        std::set<cgsn::protobuf::Module> modules;
        const auto* module_enum_desc = cgsn::protobuf::Module_descriptor();
        for (int i = 0, n = module_enum_desc->value_count(); i < n; ++i)
        {
            auto module = static_cast<cgsn::protobuf::Module>(module_enum_desc->value(i)->number());
            if (module_level(module) == EnumValueOptions::HealthData::LEVEL__CPU__TIER1)
                modules.insert(module);
        }

        return modules;
    }

  private:
    ReportAggregator tier1_aggregator_;
};

} // namespace health

} // namespace cgsn

using goby::glog;

int main(int argc, char* argv[]) { return goby::run<cgsn::health::SystemHealth>(argc, argv); }

cgsn::health::SystemHealth::SystemHealth()
    : goby::MultiThreadApplication<protobuf::SystemHealthConfig>(
          module_report_freq(app_cfg().this_cpu())),
      tier1_aggregator_(tier1_modules())
{
    cgsn::health::ReportAggregator::set_overdue_factor(cfg().report_overdue_factor());

    auto file_report_func = [this](const protobuf::ModuleHealth& health) {
        if (module_level(health.module()) == cgsn::EnumValueOptions::HealthData::LEVEL__CPU__TIER1)
            tier1_aggregator_.file_report(health);
    };

    interthread().subscribe<cgsn::groups::health, protobuf::ModuleHealth>(file_report_func);
    interprocess().subscribe<cgsn::groups::health, protobuf::ModuleHealth>(file_report_func);

    launch_thread<ComputerHardwareThread>(cfg().sys_info());
    launch_thread<NTPStatusThread>(cfg().ntp_status());
    launch_thread<SensorDriverAggregatorThread>(cfg().sensor_driver());
    launch_thread<SupervisorThread>(cfg().supervisor());
    launch_thread<SchedulerThread>(cfg().scheduler());
    launch_thread<LoggerThread>(cfg().logger());
    launch_thread<GPSThread>(cfg().gps());
    launch_thread<LegacyThread>(cfg().legacy());
}

void cgsn::health::SystemHealth::loop()
{
    tier1_aggregator_.expire_reports();
    auto report = cgsn::make_with_header<protobuf::ModuleHealth>();
    tier1_aggregator_.populate_report(report, EnumValueOptions::HealthData::LEVEL__CPU__TIER1);
    report.set_module(cfg().this_cpu());

    glog.is_debug1() && glog << "CPU health report: " << report.DebugString() << std::endl;

    interprocess().publish<cgsn::groups::health>(report);
}
