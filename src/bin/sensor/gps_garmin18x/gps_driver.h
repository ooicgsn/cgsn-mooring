// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef GPS_DRIVER_20171109_H
#define GPS_DRIVER_20171109_H

#include "cgsn-mooring/sensor/gps_garmin18x/gps_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/gps_garmin18x/gps_sensor_command_statechart.h"

namespace cgsn
{
class GPSConfigurator : public DriverConfigurator<protobuf::GPSDriverConfig>
{
  private:
    goby::time::MicroTime sampling_interval(const protobuf::GPSDriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().output_interval_with_units());
    }
};

class GPSDriver;

using GPSDriverBase =
    DriverBase<cgsn::protobuf::GPSDriverConfig,
               cgsn::io::AsciiLineSerialThread<groups::gps::raw_in, groups::gps::raw_out>,
               validator::GPSValidatorThread, cgsn::groups::gps::raw_in,
               cgsn::protobuf::sensor::GPS_GARMIN18X>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes:
/// Group                              | Layer           | Type                         | Value
/// -----------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::command::logging     | interthread     | bool                         | Enable/disable logging
/// cgsn::groups::sensor_command_state | interthread     | cgsn::protobuf::sensor::DriverStatus | State of the SensorCommand (main) thread
/// cgsn::groups::gps::raw_out         | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands to the GPS, e.g. $PGRMO
/// ----------
/// Subscribes:
/// Group                              | Layer          | Type                        | Value
/// -----------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::gps::control         | interprocesss  | cgsn::protobuf::GPSCommand  | External control of the driver
/// cgsn::groups::gps::raw_in          | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the GPS (for command acknowledgments)
class GPSDriver : public GPSDriverBase
{
  public:
    GPSDriver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw) override;
};
} // namespace cgsn

#endif
