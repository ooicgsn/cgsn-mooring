// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/gps_garmin18x/gps_validator.h"

#include "gps_driver.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(GPS)

cgsn::GPSDriver::GPSDriver()
{
    // some of the PGRM* talkers are 6 characters
    goby::util::NMEASentence::enforce_talker_length = false;
}

void cgsn::GPSDriver::driver_loop() {}

void cgsn::GPSDriver::incoming_raw(const cgsn::protobuf::SensorRaw& raw)
{
    try
    {
        goby::util::NMEASentence nmea(raw.raw_data(), goby::util::NMEASentence::REQUIRE);

        if (nmea.at(0) == "$PGRMO")
        {
            if (nmea.as<int>(nmea::PGRMO::TARGET_SENTENCE_MODE) == nmea::PGRMO::DISABLE_ALL)
                state_machine().process_event(sensor_command::statechart::EvLoggingStopped());
            else if (nmea.as<int>(nmea::PGRMO::TARGET_SENTENCE_MODE) ==
                         nmea::PGRMO::ENABLE_SPECIFIED_SENTENCE &&
                     nmea.at(nmea::PGRMO::TARGET_SENTENCE_DESCRIPTION) == "GPRMC")
                state_machine().process_event(sensor_command::statechart::EvLoggingStarted());
        }
        else if (nmea.at(0) == "$PGRMI")
        {
            // use the "Sensor Initialization Information" as a proxy for prompt received
            // (even though the GPSDriver doesn't need this, we
            // need to query for a prompt on other instruments, so we'll wait for this here
            // for demonstration purposes)
            state_machine().process_event(sensor_command::statechart::EvPromptReceived());
        }
        else if (nmea.at(0) == "$PGRMC")
        {
            // read/parse configuration
            protobuf::GPSSensorConfig reported_cfg;

            switch (nmea.as<char>(nmea::PGRMC::FIX_MODE))
            {
                case protobuf::GPSSensorConfig::FIX_MODE_AUTOMATIC:
                    reported_cfg.set_fix_mode(protobuf::GPSSensorConfig::FIX_MODE_AUTOMATIC);
                    break;
                case protobuf::GPSSensorConfig::FIX_MODE_3D:
                    reported_cfg.set_fix_mode(protobuf::GPSSensorConfig::FIX_MODE_3D);
                    break;
                default:
                    glog.is(WARN) && glog << group("gps::sensor_command") << "Unknown FixMode: "
                                          << nmea.as<char>(nmea::PGRMC::FIX_MODE) << std::endl;
                    break;
            }

            reported_cfg.set_measurement_pulse_enabled(
                nmea.as<int>(nmea::PGRMC::MEASUREMENT_PULSE_OUTPUT) ==
                nmea::PGRMC::MEASUREMENT_PULSE_ENABLED);

            glog.is(DEBUG2) && glog << group("gps::sensor_command")
                                    << "Reported configuration (phase 1): "
                                    << reported_cfg.ShortDebugString() << std::endl;

            // if the two configurations are the same
            if (reported_cfg.fix_mode() == cfg().sensor().fix_mode() &&
                reported_cfg.measurement_pulse_enabled() ==
                    cfg().sensor().measurement_pulse_enabled())
            {
                state_machine().process_event(sensor_command::statechart::EvConfiguredPhase1());
            }
            else
            {
                // not configured, post this, with desired configuration
                goby::util::NMEASentence cfg_set_nmea("$PGRMC");
                cfg_set_nmea.resize(nmea::PGRMC::Fields::SIZE);
                cfg_set_nmea[nmea::PGRMC::MEASUREMENT_PULSE_OUTPUT] =
                    cfg().sensor().measurement_pulse_enabled()
                        ? std::to_string(nmea::PGRMC::MEASUREMENT_PULSE_ENABLED)
                        : std::to_string(nmea::PGRMC::MEASUREMENT_PULSE_DISABLED);

                cfg_set_nmea[nmea::PGRMC::FIX_MODE] =
                    std::string(1, static_cast<char>(cfg().sensor().fix_mode()));

                state_machine().process_event(
                    sensor_command::statechart::EvNotConfiguredPhase1(cfg_set_nmea));
            }
        }
        else if (nmea.at(0) == "$PGRMC1")
        {
            protobuf::GPSSensorConfig reported_cfg;

            reported_cfg.set_output_interval_with_units(
                nmea.as<int>(nmea::PGRMC1::NMEA_0183_OUTPUT_TIME) * boost::units::si::seconds);

            glog.is(DEBUG2) && glog << group("gps::sensor_command")
                                    << "Reported configuration (phase 2): "
                                    << reported_cfg.ShortDebugString() << std::endl;

            if (reported_cfg.output_interval() == cfg().sensor().output_interval())
            {
                state_machine().process_event(sensor_command::statechart::EvConfiguredPhase2());
            }
            else
            {
                goby::util::NMEASentence cfg_set_nmea("$PGRMC1");
                cfg_set_nmea.resize(nmea::PGRMC1::Fields::SIZE);
                cfg_set_nmea[nmea::PGRMC1::NMEA_0183_OUTPUT_TIME] = std::to_string(
                    cfg().sensor().output_interval_with_units() / boost::units::si::seconds);

                state_machine().process_event(
                    sensor_command::statechart::EvNotConfiguredPhase2(cfg_set_nmea));
            }
        }
    }
    catch (goby::util::bad_nmea_sentence& e)
    {
        glog.is(WARN) && glog << group("gps::sensor_command")
                              << "Invalid NMEA sentence: " << e.what() << std::endl;
    }
}
