// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/sensor/gps_garmin18x/gps_parser.h"

namespace cgsn
{
    class GPSParser : public goby::MultiThreadApplication<cgsn::protobuf::GPSParserConfig>
    {
    public:
        GPSParser()
        { launch_thread<cgsn::parser::GPSParserThread>(cfg().parser()); }
    };
}

int main(int argc, char* argv[])
{ return goby::run<cgsn::GPSParser>(argc, argv); }

