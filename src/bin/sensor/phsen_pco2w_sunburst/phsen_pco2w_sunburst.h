// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PHSEN_PCO2W_SUNBURST_H
#define PHSEN_PCO2W_SUNBURST_H

#include "cgsn-mooring/sensor/phsen_pco2w_sunburst/phsen_pco2w_sunburst_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/phsen_pco2w_sunburst/phsen_pco2w_sunburst_sensor_command_statechart.h"

namespace cgsn
{
/// \brief Performs any runtime configuration modifications specific to this driver (in addition or in-place of those in the YAML->PB configuration
class PHSEN_PCO2W_SUNBURST_Configurator
    : public DriverConfigurator<protobuf::PHSEN_PCO2W_SUNBURST_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::PHSEN_PCO2W_SUNBURST_DriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().sampling_interval_with_units());
    }
    void finalize_serial_cfg(protobuf::SerialConfig& serial_cfg,
                             const protobuf::PHSEN_PCO2W_SUNBURST_DriverConfig& cfg) override
    {
        serial_cfg.set_end_of_line("\r");
    }
};

class PHSEN_PCO2W_SUNBURST_Driver;

using PHSEN_PCO2W_SUNBURST_DriverBase =
    DriverBase<cgsn::protobuf::PHSEN_PCO2W_SUNBURST_DriverConfig,
               cgsn::io::AsciiLineSerialThread<groups::phsen_pco2w_sunburst::raw_in,
                                               groups::phsen_pco2w_sunburst::raw_out>,
               validator::PHSEN_PCO2W_SUNBURST_ValidatorThread,
               cgsn::groups::phsen_pco2w_sunburst::raw_in,
               cgsn::protobuf::sensor::PHSEN_PCO2W_SUNBURST>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                                 | Layer           | Type                         | Value
/// --------------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::phsen_pco2w_sunburst::raw_out | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                                | Layer          | Type                        | Value
/// -------------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::phsen_pco2w_sunburst::raw_in | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class PHSEN_PCO2W_SUNBURST_Driver : public PHSEN_PCO2W_SUNBURST_DriverBase
{
  public:
    PHSEN_PCO2W_SUNBURST_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;

  private:
    // Should be 1Hz, so if we want 2.5 seconds, we haven't gotten one
    const goby::time::SITime status_timeout{2.5 * boost::units::si::seconds};
};
} // namespace cgsn

#endif
