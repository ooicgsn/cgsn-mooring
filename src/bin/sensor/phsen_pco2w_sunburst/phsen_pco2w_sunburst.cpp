// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/phsen_pco2w_sunburst/phsen_pco2w_sunburst_validator.h"

#include "phsen_pco2w_sunburst.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(PHSEN_PCO2W_SUNBURST_)

cgsn::PHSEN_PCO2W_SUNBURST_Driver::PHSEN_PCO2W_SUNBURST_Driver() {}

void cgsn::PHSEN_PCO2W_SUNBURST_Driver::driver_loop()
{
    // handle timeouts for SAMI Status message
    if (this->state_machine().state() == protobuf::sensor::INITIALIZE__WAIT_FOR_PROMPT ||
        this->state_machine().state() == protobuf::sensor::ON__WAIT_FOR_PROMPT ||
        this->state_machine().state() == protobuf::sensor::OFF__WAIT_FOR_PROMPT ||
        this->state_machine().state() == protobuf::sensor::OFF__SENSOR_SLEEP ||
        this->state_machine().state() == protobuf::sensor::INITIALIZE__SENSOR_SLEEP)
    {
        const auto* time_access =
            this->state_machine()
                .template state_cast<const sensor_command::statechart::TimeAccess*>();
        if (time_access)
        {
            goby::time::SITime last_time(time_access->last_time()), now(goby::time::now());

            if (now >= last_time + status_timeout)
                this->state_machine().process_event(
                    sensor_command::statechart::EvSAMIStatusMessageTimeout());
        }
    }
}

void cgsn::PHSEN_PCO2W_SUNBURST_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    const auto& line = raw_data.raw_data();

    if (line[0] == ':')
        state_machine().process_event(sensor_command::statechart::EvSAMIStatusMessageReceived());
}
