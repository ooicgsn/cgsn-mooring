// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <boost/algorithm/string/predicate.hpp>

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/ctd_sbe/ctd_sbe_validator.h"

#include "ctd_sbe_driver.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(CTD_SBE_)

cgsn::CTD_SBE_Driver::CTD_SBE_Driver()
{
    glog.add_group("ctd_sbe::driver", goby::common::Colors::magenta);
}

void cgsn::CTD_SBE_Driver::driver_loop() {}

void cgsn::CTD_SBE_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw)
{
    glog.is(DEBUG3) && glog << group("ctd_sbe::driver") << "Incoming raw: " << raw.raw_data()
                            << std::endl;

    if (raw.raw_data().find(ctd_prompt_) != std::string::npos)
    //||
    //(raw.raw_data().find(executed_prompt_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver")
                                << "Received CTD prompt (executed or S>)" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvPromptReceived());

        if (state_machine().state() == protobuf::sensor::INITIALIZE__CONFIGURE ||
            state_machine().state() == protobuf::sensor::ON__CONFIGURE)
        {
            // only ack "S>" if it was sent after the last message. This should ignore spurious "S>" "acks"
            const auto* time_access =
                state_machine().state_cast<const sensor_command::statechart::TimeAccess*>();
            if (time_access)
            {
                if (raw.header().mooring_time_with_units() > time_access->last_time())
                    state_machine().process_event(
                        sensor_command::statechart::EvConfigureValueAck());
            }
        }
    }

    if (raw.raw_data().find(repeat_command_) != std::string::npos)
    {
        state_machine().process_event(sensor_command::statechart::EvConfigureRequiresRepeat());
    }
    else if (raw.raw_data().find(started_logging_successfully_string_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver")
                                << "Received valid start logging response!" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvLoggingStarted());
    }
    else if (raw.raw_data().find(sleep_response_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver") << "Received sleep response"
                                << std::endl;
        state_machine().process_event(sensor_command::statechart::EvSensorSleeping());
    }
    else if (raw.raw_data().find(stopped_logging_successfully_string_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver")
                                << "Received valid stop logging response!" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvLoggingStopped());
    }
    else if (raw.raw_data().find(invalid_command_response_string_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver") << "Received invalid command response"
                                << std::endl;
    }
    else if ((raw.raw_data().find(start_config_data_response_string_) != std::string::npos) ||
             (raw.raw_data().find(output_salinity_string_) != std::string::npos) ||
             (raw.raw_data().find(output_sv_string_) != std::string::npos) ||
             (raw.raw_data().find(output_sigma_string_) != std::string::npos) ||
             (raw.raw_data().find(measurements_string_) != std::string::npos) ||
             (raw.raw_data().find(pump_string_) != std::string::npos) ||
             (raw.raw_data().find(samp_interval_string_) != std::string::npos) ||
             (raw.raw_data().find(output_format_string_) != std::string::npos) ||
             (raw.raw_data().find(transmit_string_) != std::string::npos) ||
             (raw.raw_data().find(optode_string_) != std::string::npos) ||
             (raw.raw_data().find(output_executed_string_) != std::string::npos) ||
             (raw.raw_data().find(echo_string_) != std::string::npos) ||
             (raw.raw_data().find(end_config_data_response_string_) != std::string::npos))
    {
        // add line to buffer
        configuration_data_result += raw.raw_data();

        // check to see if we have hit the end of the configuration data string
        if (raw.raw_data().find(end_config_data_response_string_) != std::string::npos)
        {
            // read/parse current configuration of CTD, CTD returns yes, no, etc.
            // the return values need to be translated into the protobuf's format
            protobuf::CTD_SBE_SensorConfig reported_cfg;
            reported_cfg.set_output_salinity(
                extract_value(output_salinity_string_) == "yes" ? true : false);
            reported_cfg.set_output_sound_velocity(
                extract_value("OutputSoundVelocity") == "yes" ? true : false);
            reported_cfg.set_output_sigmat_v_i(extract_value("OutputSigmaT_V_I") == "yes" ? true
                                                                                          : false);
            reported_cfg.set_measurements_per_sample(
                std::stoi(extract_value("MeasurementsPerSample")));

            // convert pump string to PumpMode
            if (extract_value("Pump") == "run pump for 0.5 sec")
            {
                reported_cfg.set_pump_mode(protobuf::CTD_SBE_SensorConfig::RUN_PUMP_BEFORE_SAMPLE);
            }
            else if (extract_value("Pump") == "no pump")
            {
                reported_cfg.set_pump_mode(protobuf::CTD_SBE_SensorConfig::NO_PUMP);
            }
            else if (extract_value("Pump") == "run pump during sample")
            {
                reported_cfg.set_pump_mode(protobuf::CTD_SBE_SensorConfig::RUN_PUMP_DURING_SAMPLE);
            }

            reported_cfg.set_sample_interval(std::stoi(extract_value("SampleInterval")));

            // convert output format string to OutputFormat
            if (extract_value("OutputFormat") == "raw HEX")
            {
                reported_cfg.set_output_format(
                    protobuf::CTD_SBE_SensorConfig::OUTPUT_RAW_HEXADECIMAL);
            }
            else if (extract_value("OutputFormat") == "converted HEX")
            {
                reported_cfg.set_output_format(
                    protobuf::CTD_SBE_SensorConfig::OUTPUT_CONVERTED_DATA_HEXADECIMAL);
            }
            else if (extract_value("OutputFormat") == "raw decimal")
            {
                reported_cfg.set_output_format(protobuf::CTD_SBE_SensorConfig::OUTPUT_RAW_DECIMAL);
            }
            else if (extract_value("OutputFormat") == "converted decimal")
            {
                reported_cfg.set_output_format(
                    protobuf::CTD_SBE_SensorConfig::OUTPUT_CONVERTED_DATA_DECIMAL);
            }
            else if (extract_value("OutputFormat") == "converted XML UVIC")
            {
                reported_cfg.set_output_format(
                    protobuf::CTD_SBE_SensorConfig::OUTPUT_CONVERTED_DATA_DECIMAL_XML);
            }

            reported_cfg.set_transmit_real_time(extract_value("TransmitRealTime") == "yes" ? true
                                                                                           : false);
            reported_cfg.set_optode(extract_value("OPTODE") == "yes" ? true : false);

            glog.is(DEBUG3) && glog << group("ctd_sbe::driver")
                                    << "Reported configuration [Configure Sampling Phase]: "
                                    << reported_cfg.ShortDebugString() << std::endl;
            glog.is(DEBUG3) && glog << group("ctd_sbe::driver")
                                    << "Proposed configuration [Configure Sampling Phase]: "
                                    << cfg().sensor().ShortDebugString() << std::endl;

            // if the two configurations are the same
            std::string reported_config_string;
            std::string proposed_config_string;
            reported_cfg.SerializeToString(&reported_config_string);
            cfg().sensor().SerializeToString(&proposed_config_string);

            if (reported_config_string == proposed_config_string)
            {
                glog.is(DEBUG2) &&
                    glog << group("ctd_sbe::driver")
                         << "Reported and proposed instrument configuration are the same"
                         << std::endl;
                state_machine().process_event(sensor_command::statechart::EvConfigured());
            }
            else
            {
                // not configured, post this, with desired configuration
                glog.is(DEBUG2) && glog << group("ctd_sbe::driver")
                                        << "Reported and proposed instrument configuration are the "
                                           "NOT the same, time to configure"
                                        << std::endl;
                state_machine().process_event(
                    sensor_command::statechart::EvNotConfigured<protobuf::CTD_SBE_SensorConfig>(
                        cfg().sensor()));
            }
            configuration_data_result.clear();
        }
    }
    else if (raw.raw_data().find(already_logging_response_string_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("ctd_sbe::driver") << "Received "
                                << already_logging_response_string_ << std::endl;
        state_machine().process_event(sensor_command::statechart::EvLoggingStarted());
    }
    else if (raw.raw_data().find(already_notlogging_response_string_) != std::string::npos)
    {
        state_machine().process_event(sensor_command::statechart::EvLoggingStopped());
    }
}

std::string cgsn::CTD_SBE_Driver::extract_value(std::string tag)
{
    std::string xml_start_tag = "<" + tag + ">";
    std::string xml_stop_tag = "</" + tag + ">";
    unsigned first = configuration_data_result.find(xml_start_tag) + xml_start_tag.length();
    unsigned last = configuration_data_result.find(xml_stop_tag);

    std::string found_value = configuration_data_result.substr(first, last - first);

    glog.is(DEBUG3) && glog << group("ctd_sbe::driver") << "extracted value for <" << tag
                            << ">: " << found_value << std::endl;

    return found_value;
}
