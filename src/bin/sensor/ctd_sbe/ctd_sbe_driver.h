// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef CTD_SBE_DRIVER_H
#define CTD_SBE_DRIVER_H

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/ctd_sbe/ctd_sbe_sensor_command_statechart.h"

namespace cgsn
{
class CTD_SBE_Configurator : public DriverConfigurator<protobuf::CTD_SBE_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::CTD_SBE_DriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().sample_interval_with_units());
    }
};

class CTD_SBE_Driver;

using CTD_SBE_DriverBase =
    DriverBase<cgsn::protobuf::CTD_SBE_DriverConfig,
               cgsn::io::AsciiLineSerialThread<groups::ctd_sbe::raw_in, groups::ctd_sbe::raw_out>,
               validator::CTD_SBE_ValidatorThread, cgsn::groups::ctd_sbe::raw_in,
               cgsn::protobuf::sensor::CTD_SBE>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                              | Layer           | Type                         | Value
/// -----------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::ctd_sbe::raw_out     | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                              | Layer          | Type                        | Value
/// -----------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::ctd_sbe::raw_in      | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class CTD_SBE_Driver : public CTD_SBE_DriverBase
{
  public:
    CTD_SBE_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw) override;
    std::string extract_value(std::string xml_tag);

    std::string configuration_data_result;

    const std::string ctd_prompt_{"S>"};
    const std::string executed_prompt_{"<Executed/>"};
    const std::string started_logging_successfully_string_{"start logging at"};
    const std::string stopped_logging_successfully_string_{"logging stopped"};
    // response when invalid command is received
    const std::string invalid_command_response_string_{"INVALID COMMAND"};
    const std::string already_logging_response_string_{"Command not allowed while logging"};
    const std::string already_notlogging_response_string_{"Command only allowed while logging"};
    const std::string sleep_response_{"QS"};

    // responses from GetCD
    const std::string start_config_data_response_string_{"<ConfigurationData>"};
    const std::string end_config_data_response_string_{"</ConfigurationData>\r\n"};
    const std::string output_salinity_string_{"OutputSalinity"};
    const std::string output_sv_string_{"OutputSoundVelocity"};
    const std::string output_sigma_string_{"OutputSigmaT_V_I"};
    const std::string measurements_string_{"MeasurementsPerSample"};
    const std::string pump_string_{"Pump"};
    const std::string samp_interval_string_{"SampleInterval"};
    const std::string output_format_string_{"OutputFormat"};
    const std::string transmit_string_{"TransmitRealTime"};
    const std::string optode_string_{"OPTODE"};
    const std::string output_executed_string_{"OutputExecutedTag"};
    const std::string echo_string_{"EchoCharacters"};

    const std::string repeat_command_{"repeat the command to verify"};
};
} // namespace cgsn

#endif
