// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef VELPT_NORTEK_H
#define VELPT_NORTEK_H

#include "cgsn-mooring/sensor/velpt_nortek/velpt_nortek_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/velpt_nortek/velpt_nortek_sensor_command_statechart.h"

namespace cgsn
{
class VELPT_NORTEK_Configurator : public DriverConfigurator<protobuf::VELPT_NORTEK_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::VELPT_NORTEK_DriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().sampling_interval_with_units());
    }

    void finalize_serial_cfg(protobuf::SerialConfig& serial_cfg,
                             const protobuf::VELPT_NORTEK_DriverConfig& cfg) override
    {
        serial_cfg.set_end_of_line(response_ackack_ + "|" + response_nacknack_);
    }
    const std::string response_ackack_{{0x06, 0x06}};
    const std::string response_nacknack_{{0x15, 0x15}};
};

class VELPT_NORTEK_Driver;

using VELPT_NORTEK_DriverBase =
    DriverBase<cgsn::protobuf::VELPT_NORTEK_DriverConfig,
               cgsn::io::NortekDataBinarySerialThread<groups::velpt_nortek::raw_in,
                                                      groups::velpt_nortek::raw_out>,
               validator::VELPT_NORTEK_ValidatorThread, cgsn::groups::velpt_nortek::raw_in,
               cgsn::protobuf::sensor::VELPT_NORTEK>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                                 | Layer           | Type                         | Value
/// --------------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::velpt_nortek::raw_out | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                                | Layer          | Type                        | Value
/// -------------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::velpt_nortek::raw_in | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class VELPT_NORTEK_Driver : public VELPT_NORTEK_DriverBase
{
  public:
    VELPT_NORTEK_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;

    // Command mode
    const std::string response_command_mode_{"Command mode"};
    const std::string response_ackack_{{0x06, 0x06}};
    const std::string response_nacknack_{{0x15, 0x15}};
};
} // namespace cgsn

#endif
