// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/sensor/velpt_nortek/velpt_nortek_validator.h"

#include "velpt_nortek.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(VELPT_NORTEK_)

cgsn::VELPT_NORTEK_Driver::VELPT_NORTEK_Driver()
{
    glog.add_group("velpt_nortek::driver", goby::common::Colors::green);
}

void cgsn::VELPT_NORTEK_Driver::driver_loop() {}

void cgsn::VELPT_NORTEK_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    if (raw_data.raw_data().find(response_command_mode_) != std::string::npos)
    {
        glog.is(DEBUG2) && glog << group("velpt_nortek::driver") << "Response is COMMAND MODE"
                                << std::endl;
        state_machine().process_event(sensor_command::statechart::EvNortekBreakReceived());
    }
    else if (raw_data.raw_data().find(response_ackack_) != std::string::npos)
    {
        glog.is(DEBUG2) && glog << group("velpt_nortek::driver")
                                << "Response is VELPT NORTEK ACK ACK" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvNortekAckAck());
    }
    else if (raw_data.raw_data() == response_nacknack_)
    {
        glog.is(DEBUG2) && glog << group("velpt_nortek::driver")
                                << "Response is VELPT NORTEK NACK NACK" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvNortekNackNack());
    }
}
