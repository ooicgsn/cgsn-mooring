// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_validator.h"

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_parse.h"
#include "cgsn-mooring/sensor/adcp_rdi/exception.h"

#include "adcp_rdi.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(ADCP_RDI_)

goby::time::MicroTime
cgsn::ADCP_RDI_Configurator::sampling_interval(const protobuf::ADCP_RDI_DriverConfig& cfg) const
{
    return goby::time::MicroTime(
        cgsn::sensor_internal::time_per_ensemble(cfg.sensor().time_per_ensemble()));
}

cgsn::ADCP_RDI_Driver::ADCP_RDI_Driver()
{
    glog.add_group("adcp_rdi::driver", goby::common::Colors::green);
}

void cgsn::ADCP_RDI_Driver::driver_loop() {}

void cgsn::ADCP_RDI_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    // look for ack to configuration command. E.g. ack to "TEXXXX\r\n" is ">TEXXXX\r\n"
    if (state_machine().state() == protobuf::sensor::INITIALIZE__CONFIGURE ||
        state_machine().state() == protobuf::sensor::ON__CONFIGURE)
    {
        const auto* time_access =
            state_machine().state_cast<const sensor_command::statechart::TimeAccess*>();
        if (time_access)
        {
            const auto& last_command = time_access->last_command();
            if (!last_command.empty() &&
                raw_data.raw_data().find(response_settings_echo_ + last_command) !=
                    std::string::npos)
                state_machine().process_event(sensor_command::statechart::EvConfigureValueAck());
        }
    }

    if (raw_data.raw_data() == response_prompt_)
        state_machine().process_event(sensor_command::statechart::EvPromptReceived());

    else if (raw_data.raw_data().substr(0, response_break_wakeup_.size()) == response_break_wakeup_)
        state_machine().process_event(sensor_command::statechart::EvBreakWakeup());

    else if (raw_data.raw_data() == response_powering_down_)
        state_machine().process_event(sensor_command::statechart::EvSensorSleeping());

    else if (raw_data.raw_data().find(response_bandwidth_control_) != std::string::npos)
        // add bandwidth control value to current config string
        current_configuration_string += raw_data.raw_data();

    else if ((raw_data.raw_data() == response_deploy_ ||
              raw_data.raw_data().find(response_transducer_depth_) != std::string::npos ||
              raw_data.raw_data().find(response_salinity_) != std::string::npos ||
              raw_data.raw_data().find(response_time_per_ensemble_) != std::string::npos ||
              raw_data.raw_data().find(response_time_between_pings_) != std::string::npos ||
              raw_data.raw_data().find(response_bandwidth_control_) != std::string::npos ||
              raw_data.raw_data().find(response_num_depth_cells_) != std::string::npos ||
              raw_data.raw_data().find(response_pings_per_ensemble_) != std::string::npos ||
              raw_data.raw_data().find(response_depth_cell_size_) != std::string::npos ||
              raw_data.raw_data().find(response_deploy_end_) != std::string::npos ||
              raw_data.raw_data().find(response_transmit_power_) != std::string::npos ||
              raw_data.raw_data().find(response_heading_alignment_) != std::string::npos ||
              raw_data.raw_data().find(response_heading_bias_) != std::string::npos ||
              raw_data.raw_data().find(response_false_target_threshold_maximum_) !=
                  std::string::npos ||
              raw_data.raw_data().find(response_blank_after_transmit_) != std::string::npos ||
              raw_data.raw_data().find(response_ambiguity_velocity_) != std::string::npos) &&
             // don't add the echo of settings as current configuration string
             raw_data.raw_data().find(response_settings_echo_) == std::string::npos)
    {
        try
        {
            glog.is(DEBUG2) && glog << group("adcp_rdi::driver") << "Received configuration string!"
                                    << std::endl;

            current_configuration_string += raw_data.raw_data();

            // check to see if we have received all configuration settings needed
            if (raw_data.raw_data().find(response_deploy_end_) != std::string::npos)
            {
                glog.is(DEBUG2) && glog << group("adcp_rdi::driver")
                                        << "Parsing from: " << current_configuration_string
                                        << std::endl;

                // read/parse current configuration of ADCP
                protobuf::ADCP_RDI_SensorConfig reported_cfg;
                reported_cfg.set_transmit_power(
                    std::stoi(extract_cfg_value(response_transmit_power_, true)));
                reported_cfg.set_heading_alignment(
                    std::stoi(extract_cfg_value(response_heading_alignment_, true)));
                reported_cfg.set_heading_bias(
                    std::stoi(extract_cfg_value(response_heading_bias_, true)));
                reported_cfg.set_transducer_depth(
                    std::stoi(extract_cfg_value(response_transducer_depth_, true)));
                reported_cfg.set_salinity(std::stoi(extract_cfg_value(response_salinity_, true)));
                reported_cfg.set_time_per_ensemble(
                    extract_cfg_value(response_time_per_ensemble_, false));
                reported_cfg.set_time_between_pings(
                    extract_cfg_value(response_time_between_pings_, false));

                // e.g. "050,001"
                std::string threshold_max =
                    extract_cfg_value(response_false_target_threshold_maximum_, false);
                reported_cfg.set_false_target_threshold_maximum(
                    std::stoi(threshold_max.substr(0, threshold_max.find(","))));

                int bandwidth_ctl =
                    std::stoi(extract_cfg_value(response_bandwidth_control_, false));
                if (bandwidth_ctl == 0)
                    reported_cfg.set_model_bandwidth_control(protobuf::ADCP_RDI_SensorConfig::WIDE);
                else if (bandwidth_ctl == 1)
                    reported_cfg.set_model_bandwidth_control(
                        protobuf::ADCP_RDI_SensorConfig::NARROW);
                else
                    glog.is(WARN) && glog << group("adcp_rdi::driver")
                                          << "Could not parse response from WB? command, not "
                                             "setting model_bandwidth_control in protobuf"
                                          << std::endl;

                reported_cfg.set_blank_after_transmit(
                    std::stoi(extract_cfg_value(response_blank_after_transmit_, false)));
                reported_cfg.set_num_depth_cells(
                    std::stoi(extract_cfg_value(response_num_depth_cells_, false)));
                reported_cfg.set_pings_per_ensemble(
                    std::stoi(extract_cfg_value(response_pings_per_ensemble_, false)));
                reported_cfg.set_depth_cell_size(
                    std::stoi(extract_cfg_value(response_depth_cell_size_, false)));
                reported_cfg.set_ambiguity_velocity(
                    std::stoi(extract_cfg_value(response_ambiguity_velocity_, false)));

                glog.is(DEBUG2) && glog << group("adcp_rdi::driver")
                                        << "Reported ADCP configuration: "
                                        << reported_cfg.ShortDebugString() << std::endl;
                glog.is(DEBUG2) && glog << group("adcp_rdi::driver")
                                        << "Proposed ADCP configuration: "
                                        << cfg().sensor().ShortDebugString() << std::endl;

                // check if the configurations are the same
                std::string reported_config_string;
                std::string proposed_config_string;
                reported_cfg.SerializeToString(&reported_config_string);
                cfg().sensor().SerializeToString(&proposed_config_string);

                if (reported_config_string == proposed_config_string)
                {
                    glog.is(DEBUG2) &&
                        glog << group("adcp_rdi::driver")
                             << "Reported and proposed instrument configurations are the same"
                             << std::endl;
                    state_machine().process_event(sensor_command::statechart::EvConfigured());
                }
                else
                {
                    // not configured, post this, with desired configuration
                    glog.is(DEBUG2) && glog << group("adcp_rdi::driver")
                                            << "Reported and proposed instrument configurations "
                                               "are the NOT the same, time to configure"
                                            << std::endl;
                    state_machine().process_event(sensor_command::statechart::EvNotConfigured<
                                                  protobuf::ADCP_RDI_SensorConfig>(cfg().sensor()));
                }
                current_configuration_string.clear();
            }
        }
        catch (cgsn::adcp_rdi::Exception& e)
        {
            glog.is(WARN) && glog << "Failed to parse command response: " << e.what() << std::endl;
        }
    }

    else if (raw_data.raw_data() == response_self_test_)
        state_machine().process_event(sensor_command::statechart::EvSelfTestCompleted());
}

// This function extracts the configuration value from the X? responses
std::string cgsn::ADCP_RDI_Driver::extract_cfg_value(std::string cmd, bool has_equal_sign)
{
    boost::trim(cmd);
    std::string start_string = cmd + " ";
    if (has_equal_sign)
        start_string += "= ";
    std::string stop_string = " -------";
    if (current_configuration_string.find(start_string) != std::string::npos)
    {
        unsigned int start_index =
            current_configuration_string.find(start_string) + start_string.length();
        unsigned int stop_index = current_configuration_string.find(stop_string, start_index + 1);

        std::string found_value =
            current_configuration_string.substr(start_index, stop_index - start_index);
        return found_value;
    }
    else
    {
        throw(adcp_rdi::Exception("Failed to parse response for \"" + cmd + "\""));
    }
}
