// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef ADCP_RDI_H
#define ADCP_RDI_H

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_sensor_command_statechart.h"

namespace cgsn
{
/// \brief Sets the validator timeouts based on the sampling interval
class ADCP_RDI_Configurator : public DriverConfigurator<protobuf::ADCP_RDI_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::ADCP_RDI_DriverConfig& cfg) const override;
};

class ADCP_RDI_Driver;

using ADCP_RDI_DriverBase =
    DriverBase<cgsn::protobuf::ADCP_RDI_DriverConfig,
               cgsn::io::AsciiLineSerialThread<cgsn::groups::adcp_rdi::raw_in,
                                               cgsn::groups::adcp_rdi::raw_out>,
               validator::ADCP_RDI_ValidatorThread, cgsn::groups::adcp_rdi::raw_in,
               cgsn::protobuf::sensor::ADCP_RDI>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                              | Layer           | Type                         | Value
/// -----------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::adcp_rdi::raw_out    | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                              | Layer          | Type                        | Value
/// -----------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::adcp_rdi::raw_in     | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class ADCP_RDI_Driver : public ADCP_RDI_DriverBase
{
  public:
    ADCP_RDI_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;

    // extract "255" from "CQ = 255 ----------------- Xmt Power (0=Low, 255=High)" for cmd = "CQ", has_equal_sign = true
    // extract "030" from "WN 030 ----------------- Number of depth cells (1-255)" for cmd = "WN", has_equal_sign = false
    std::string extract_cfg_value(std::string cmd, bool has_equal_sign);

  private:
    const std::string response_break_wakeup_{"[BREAK Wakeup"};
    const std::string response_prompt_{">\r\n"};
    const std::string response_powering_down_{"Powering Down\r\n"};
    const std::string response_user_default_config_{"[Parameters set to USER defaults]\r\n"};
    const std::string response_factory_default_config_{"[Parameters set to FACTORY defaults]\r\n"};
    const std::string response_self_test_{"PRE_DEPLOYMENT TESTS"};
    const std::string response_deploy_{"Deployment Commands:\r\n"};
    const std::string response_heading_alignment_{"EA"};
    const std::string response_heading_bias_{"EB"};
    const std::string response_transducer_depth_{"ED"};
    const std::string response_salinity_{"ES"};
    const std::string response_time_per_ensemble_{"TE"};
    const std::string response_time_between_pings_{"TP"};
    const std::string response_false_target_threshold_maximum_{"WA"};
    const std::string response_blank_after_transmit_{"WF"};
    const std::string response_num_depth_cells_{"WN"};
    const std::string response_pings_per_ensemble_{"WP"};
    const std::string response_depth_cell_size_{"WS"};
    const std::string response_ambiguity_velocity_{"WV"};
    // intentional spaces at end of string to differentiate between echo of cmd and setting response (e.g. "WB?"  versus "WB = "
    const std::string response_transmit_power_{"CQ "};
    const std::string response_bandwidth_control_{"WB "};

    // the last response we parse before checking all configuration
    const std::string response_deploy_end_{"W? "};

    const std::string response_settings_echo_{">"};

    std::string current_configuration_string{};
};
} // namespace cgsn

#endif
