// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DRIVER_PATTERN_H
#define DRIVER_PATTERN_H

#include "cgsn-mooring/sensor/driver_pattern/driver_pattern_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/driver_pattern/driver_pattern_sensor_command_statechart.h"

namespace cgsn
{
/// \brief Performs any runtime configuration modifications specific to this driver (in addition or in-place of those in the YAML->PB configuration
class DriverPatternConfigurator : public DriverConfigurator<protobuf::DriverPatternDriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::DriverPatternDriverConfig& cfg) const override
    {
        // REQUIRES CUSTOMIZATION
        // replace "sampling_interval" with the appropriate
        // value from the sensor's configuration
        return goby::time::MicroTime(cfg.sensor().sampling_interval_with_units());
    }
};

class DriverPatternDriver;

using DriverPatternDriverBase =
    DriverBase<cgsn::protobuf::DriverPatternDriverConfig,
               cgsn::io::AsciiLineSerialThread<groups::driver_pattern::raw_in,
                                               groups::driver_pattern::raw_out>,
               validator::DriverPatternValidatorThread, cgsn::groups::driver_pattern::raw_in,
               cgsn::protobuf::sensor::DRIVER_PATTERN>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                                 | Layer           | Type                         | Value
/// --------------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::driver_pattern::raw_out | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                                | Layer          | Type                        | Value
/// -------------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::driver_pattern::raw_in | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class DriverPatternDriver : public DriverPatternDriverBase
{
  public:
    DriverPatternDriver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;
};
} // namespace cgsn

#endif
