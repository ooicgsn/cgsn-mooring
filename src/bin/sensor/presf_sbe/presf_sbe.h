// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PRESF_SBE_H
#define PRESF_SBE_H

#include "cgsn-mooring/sensor/presf_sbe/presf_sbe_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/presf_sbe/presf_sbe_sensor_command_statechart.h"

namespace cgsn
{
class PRESF_SBE_Configurator : public DriverConfigurator<protobuf::PRESF_SBE_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::PRESF_SBE_DriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().tide_interval_with_units());
    }

    void finalize_serial_cfg(protobuf::SerialConfig& serial_cfg,
                             const protobuf::PRESF_SBE_DriverConfig& cfg) override
    {
        serial_cfg.set_end_of_line("\r\n|S>|new value =");
    }
};

class PRESF_SBE_Driver;

using PRESF_SBE_DriverBase = DriverBase<
    cgsn::protobuf::PRESF_SBE_DriverConfig,
    cgsn::io::AsciiLineSerialThread<groups::presf_sbe::raw_in, groups::presf_sbe::raw_out>,
    validator::PRESF_SBE_ValidatorThread, cgsn::groups::presf_sbe::raw_in,
    cgsn::protobuf::sensor::PRESF_SBE>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                              | Layer           | Type                         | Value
/// -----------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::presf_sbe::raw_out   | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                              | Layer          | Type                        | Value
/// -----------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::presf_sbe::raw_in    | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class PRESF_SBE_Driver : public PRESF_SBE_DriverBase
{
  public:
    PRESF_SBE_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;
    std::string extract_and_set_config_values(std::string start_string, std::string end_string);
    std::string extract_and_set_config_values(std::string start_string, std::string end_string,
                                              std::string start_searching_string);

    const std::string presf_prompt_{"S>"};
    const std::string current_time_menu_response_{"set current time:\r\n"};
    const std::string stop_logging_response_{"Stop\r\n"};
    const std::string start_logging_response_{"logging will start"};
    const std::string sleep_response_{"QS"};
    // query as part of the "SetSampling" batch
    const std::string set_sampling_response_{"new value ="};
    // configuration setting responses from DS cmd
    const std::string start_display_settings_user_info_response_{"user info="};
    const std::string temp_sensor_response_{"temperature sensor"};
    const std::string conductivity_response_{"conductivity"};
    const std::string tide_measurement_response_{"tide measurement"};
    const std::string wave_burst_sample_sched_response_{"measure waves every"};
    const std::string wave_sample_response_{"wave samples/burst"};
    const std::string logging_start_time_response_{"logging start time"};
    const std::string logging_stop_time_response_{"logging stop time"};
    const std::string wave_stats_response_{"real-time wave statistics"};
    const std::string wave_burst_response_{"real-time wave burst data"};
    const std::string tide_data_response_{"real-time tide data"};
    const std::string end_display_settings_response_{"logging ="};

    const std::string crlf_{"\r\n"};

    std::string configuration_data_result;
};
} // namespace cgsn

#endif
