// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/presf_sbe/presf_sbe_validator.h"

#include "presf_sbe.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(PRESF_SBE_)

cgsn::PRESF_SBE_Driver::PRESF_SBE_Driver()
{
    glog.add_group("presf_sbe::driver", goby::common::Colors::magenta);
}

void cgsn::PRESF_SBE_Driver::driver_loop() {}

void cgsn::PRESF_SBE_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    if (raw_data.raw_data().find(presf_prompt_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("presf_sbe::driver") << "Received PRESF prompt (S>)"
                                << std::endl;
        state_machine().process_event(sensor_command::statechart::EvPromptReceived());

        if (state_machine().state() == protobuf::sensor::INITIALIZE__CONFIGURE ||
            state_machine().state() == protobuf::sensor::ON__CONFIGURE)
        {
            const auto* time_access =
                state_machine().state_cast<const sensor_command::statechart::TimeAccess*>();
            if (time_access)
            {
                if (raw_data.header().mooring_time_with_units() > time_access->last_time())
                    state_machine().process_event(
                        sensor_command::statechart::EvConfigureValueAck());
            }
        }
    }
    else if (raw_data.raw_data() == stop_logging_response_)
    {
        glog.is(DEBUG3) && glog << group("presf_sbe::driver") << "Received stop logging response"
                                << std::endl;
        state_machine().process_event(
            sensor_command::statechart::EvLoggingStoppedCommandReceived());
    }
    else if (raw_data.raw_data().find(start_logging_response_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("presf_sbe::driver") << "Received start logging response"
                                << std::endl;
        state_machine().process_event(sensor_command::statechart::EvLoggingStarted());
    }
    else if (raw_data.raw_data().find(sleep_response_) != std::string::npos)
    {
        glog.is(DEBUG3) && glog << group("presf_sbe::driver") << "Received sleep response"
                                << std::endl;
        state_machine().process_event(sensor_command::statechart::EvSensorSleeping());
    }
    else if (raw_data.raw_data().find(set_sampling_response_) != std::string::npos)
    {
        state_machine().process_event(sensor_command::statechart::EvConfigureValueAck());
    }
    else if (raw_data.raw_data().find(start_display_settings_user_info_response_) !=
                 std::string::npos ||
             raw_data.raw_data().find(temp_sensor_response_) != std::string::npos ||
             raw_data.raw_data().find(conductivity_response_) != std::string::npos ||
             raw_data.raw_data().find(tide_measurement_response_) != std::string::npos ||
             raw_data.raw_data().find(wave_burst_sample_sched_response_) != std::string::npos ||
             raw_data.raw_data().find(wave_sample_response_) != std::string::npos ||
             raw_data.raw_data().find(logging_start_time_response_) != std::string::npos ||
             raw_data.raw_data().find(logging_stop_time_response_) != std::string::npos ||
             raw_data.raw_data().find(wave_stats_response_) != std::string::npos ||
             raw_data.raw_data().find(wave_burst_response_) != std::string::npos ||
             raw_data.raw_data().find(tide_data_response_) != std::string::npos ||
             raw_data.raw_data().find(end_display_settings_response_) != std::string::npos)
    {
        // add line to buffer
        configuration_data_result += raw_data.raw_data();

        //check if we have hit end of the display configuration response
        if (raw_data.raw_data().find(end_display_settings_response_) != std::string::npos)
        {
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Received end of dispay settings buffer" << std::endl;

            // read/parse current configuration of PRESF
            protobuf::PRESF_SBE_SensorConfig reported_cfg;

            // get and set tide_interval
            std::string found_value = extract_and_set_config_values("interval = ", "minutes,",
                                                                    tide_measurement_response_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Tide Interval = " << found_value << std::endl;
            reported_cfg.set_tide_interval(std::stoi(found_value));

            // get and set tide_measurement_duration
            found_value = extract_and_set_config_values("duration = ", " seconds\r\n",
                                                        tide_measurement_response_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Tide Measurement Duration = " << found_value << std::endl;
            reported_cfg.set_tide_measurement_duration(std::stoi(found_value));

            // get and set wave_burst_sample_sched
            found_value =
                extract_and_set_config_values("measure waves every ", " tide samples\r\n");
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Wave Burst Sample Sched = " << found_value << std::endl;
            reported_cfg.set_wave_burst_sample_sched(std::stoi(found_value));

            // get and set wave_samples_per_burst
            found_value = extract_and_set_config_values("samples\r\n", " wave samples/burst");
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Wave Samples per Burst = " << found_value << std::endl;
            reported_cfg.set_wave_samples_per_burst(std::stoi(found_value));

            // get and set wave_sample_duration
            found_value = extract_and_set_config_values("/burst at ", " scans/sec");
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Wave Sample Duration = " << found_value << std::endl;
            reported_cfg.set_wave_sample_duration(std::stoi(found_value));

            // get and set real_time_tide_data
            found_value = extract_and_set_config_values("real-time tide data = ", crlf_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Tx Real-time Tide Data = " << found_value << std::endl;
            reported_cfg.set_real_time_tide_data(found_value == "YES" ? true : false);

            // get and set real_time_wave_burst_data
            found_value = extract_and_set_config_values("real-time wave burst data = ", crlf_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Tx Real-time Wave Burst Data = " << found_value
                                    << std::endl;
            reported_cfg.set_real_time_wave_burst_data(found_value == "YES" ? true : false);

            // get and set external_temp
            reported_cfg.set_external_temp(
                (configuration_data_result.find("external temperature sensor") != std::string::npos)
                    ? true
                    : false);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "External Temp = " << reported_cfg.external_temp()
                                    << std::endl;

            // get and set conductivity
            found_value = extract_and_set_config_values("conductivity = ", crlf_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Conductivity = " << found_value << std::endl;
            reported_cfg.set_conductivity(found_value == "YES" ? true : false);

            // get and set user_info
            found_value = extract_and_set_config_values("user info=", crlf_);
            glog.is(DEBUG3) && glog << group("presf_sbe::driver") << "User info = " << found_value
                                    << std::endl;
            reported_cfg.set_user_info(found_value);

            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Reported configuration [Configure Sampling Phase]: "
                                    << reported_cfg.ShortDebugString() << std::endl;
            glog.is(DEBUG3) && glog << group("presf_sbe::driver")
                                    << "Proposed configuration [Configure Sampling Phase]: "
                                    << cfg().sensor().ShortDebugString() << std::endl;

            std::string reported_config_string;
            std::string proposed_config_string;
            reported_cfg.SerializeToString(&reported_config_string);
            cfg().sensor().SerializeToString(&proposed_config_string);

            if (reported_config_string == proposed_config_string)
            {
                glog.is(DEBUG2) &&
                    glog << group("presf_sbe::driver")
                         << "Reported and proposed instrument configuration are the same"
                         << std::endl;
                state_machine().process_event(sensor_command::statechart::EvConfigured());
            }
            else
            {
                glog.is(DEBUG2) && glog << group("presf_sbe::driver")
                                        << "Reported and proposed instrument configuration are the "
                                           "NOT the same, time to configure"
                                        << std::endl;

                state_machine().process_event(
                    sensor_command::statechart::EvNotConfigured<protobuf::PRESF_SBE_SensorConfig>(
                        cfg().sensor()));
            }
            configuration_data_result.clear();
        }
    }
}

std::string cgsn::PRESF_SBE_Driver::extract_and_set_config_values(std::string start_string,
                                                                  std::string end_string)
{
    unsigned first = configuration_data_result.find(start_string) + start_string.length();
    unsigned last = configuration_data_result.find(end_string, first);
    return configuration_data_result.substr(first, last - first);
}

std::string cgsn::PRESF_SBE_Driver::extract_and_set_config_values(
    std::string start_string, std::string end_string, std::string start_searching_string)
{
    unsigned first = configuration_data_result.find(
                         start_string, configuration_data_result.find(start_searching_string)) +
                     start_string.length();
    unsigned last = configuration_data_result.find(end_string, first);
    return configuration_data_result.substr(first, last - first);
}
