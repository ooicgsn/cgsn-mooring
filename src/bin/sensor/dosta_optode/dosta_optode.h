// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DOSTA_OPTODE_H
#define DOSTA_OPTODE_H

#include "cgsn-mooring/sensor/dosta_optode/dosta_optode_common.h"

#include "cgsn-mooring/sensor/driver_base.h"

#include "cgsn-mooring/sensor/dosta_optode/dosta_optode_sensor_command_statechart.h"

namespace cgsn
{
class DOSTA_OPTODE_Configurator : public DriverConfigurator<protobuf::DOSTA_OPTODE_DriverConfig>
{
  private:
    goby::time::MicroTime
    sampling_interval(const protobuf::DOSTA_OPTODE_DriverConfig& cfg) const override
    {
        return goby::time::MicroTime(cfg.sensor().interval_with_units());
    }

    void finalize_serial_cfg(protobuf::SerialConfig& serial_cfg,
                             const protobuf::DOSTA_OPTODE_DriverConfig& cfg) override
    {
        serial_cfg.set_end_of_line("\r\n|%|!|#|\\*");
    }

    void finalize_command_cfg(protobuf::SensorCommandConfig& command_cfg,
                              const protobuf::DOSTA_OPTODE_DriverConfig& cfg) override
    {
        // "Save" can take a while
        command_cfg.set_config_value_timeout_with_units(8 * boost::units::si::seconds);
    }
};

class DOSTA_OPTODE_Driver;

using DOSTA_OPTODE_DriverBase = DriverBase<
    cgsn::protobuf::DOSTA_OPTODE_DriverConfig,
    cgsn::io::AsciiLineSerialThread<groups::dosta_optode::raw_in, groups::dosta_optode::raw_out>,
    validator::DOSTA_OPTODE_ValidatorThread, cgsn::groups::dosta_optode::raw_in,
    cgsn::protobuf::sensor::DOSTA_OPTODE>;

/// \brief Main application for the driver. Launches/joins threads
/// \details
/// Publishes (see also publish for DriverBase):
/// Group                               | Layer           | Type                         | Value
/// ------------------------------------|-----------------|------------------------------|-------------------------
/// cgsn::groups::dosta_optode::raw_out | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands via serial
/// ----------
/// Subscribes (see also subscribe for DriverBase):
/// Group                              | Layer          | Type                        | Value
/// -----------------------------------|----------------|-----------------------------|-------------------------
/// cgsn::groups::dosta_optode::raw_in | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the serial line
class DOSTA_OPTODE_Driver : public DOSTA_OPTODE_DriverBase
{
    int product_number, serial_number;

  public:
    DOSTA_OPTODE_Driver();

  private:
    void driver_loop() override;
    void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data) override;
    std::string extract_value_from_response_string(std::string cmd);
    std::string extract_product_serial_number(std::string cmd, std::string raw_data_string);
    void set_product_number(int x);
    void set_serial_number(int y);

  private:
    const std::string response_product_number_{"Product Number"};
    const std::string response_serial_number_{"Serial Number"};

    const std::string response_interval_value_{"Interval[s]"};
    const std::string response_enable_sleep_value_{"Enable Sleep"};
    const std::string response_enable_polled_mode_value_{"Enable Polled Mode"};
    const std::string response_enable_text_value_{"Enable Text"};
    const std::string response_enable_decimalformat_value_{"Enable Decimalformat"};
    const std::string response_enable_airsaturation_value_{"Enable AirSaturation"};
    const std::string response_enable_rawdata_value_{"Enable Rawdata"};
    const std::string response_enable_temp_value_{"Enable Temperature"};
    const std::string response_salinity_value_{"Salinity[PSU]"};

    std::string all_configuration_settings;
};
} // namespace cgsn

#endif
