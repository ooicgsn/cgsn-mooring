// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

#include "cgsn-mooring/sensor/dosta_optode/dosta_optode_validator.h"

#include "dosta_optode.h"

using namespace goby::common::logger;
using goby::glog;

CGSN_SENSOR_DRIVER_MAIN(DOSTA_OPTODE_)

cgsn::DOSTA_OPTODE_Driver::DOSTA_OPTODE_Driver()
{
    goby::glog.add_group("dosta_optode::driver", goby::common::Colors::green);
}

void cgsn::DOSTA_OPTODE_Driver::driver_loop() {}

void cgsn::DOSTA_OPTODE_Driver::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    if (raw_data.raw_data() == "*")
    {
        glog.is(DEBUG2) && glog << group("dosta_optode::driver") << "Received invalid command!"
                                << std::endl;
    }
    else if (raw_data.raw_data() == "#")
    {
        state_machine().process_event(sensor_command::statechart::EvValidCommandResponse());
        state_machine().process_event(sensor_command::statechart::EvPromptReceived());

        if (state_machine().state() == protobuf::sensor::INITIALIZE__CONFIGURE ||
            state_machine().state() == protobuf::sensor::ON__CONFIGURE)
        {
            const auto* time_access =
                state_machine().state_cast<const sensor_command::statechart::TimeAccess*>();
            if (time_access)
            {
                if (raw_data.header().mooring_time_with_units() > time_access->last_time())
                    state_machine().process_event(
                        sensor_command::statechart::EvConfigureValueAck());
            }
        }
    }
    else if (raw_data.raw_data() == "!")
    {
        glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                << "Received communicator ready indicator" << std::endl;
        state_machine().process_event(sensor_command::statechart::EvPromptReceived());
    }
    else if (raw_data.raw_data() == "%")
    {
        glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                << "Received: Instrument is entering sleep mode." << std::endl;
        state_machine().process_event(sensor_command::statechart::EvSensorSleeping());
    }
    else if (raw_data.raw_data() == "TEST\t" + std::to_string(product_number) + "\t" +
                                        std::to_string(serial_number) + "\r\n")
    {
        // takes ~15 seconds after sending "Do Test" to receive response
        glog.is(DEBUG2) && glog << group("dosta_optode::driver")
                                << "Received: Instrument performed self test." << std::endl;
        state_machine().process_event(sensor_command::statechart::EvSelfTestCompleted());
    }
    else if (raw_data.raw_data().find(response_product_number_) != std::string::npos)
    {
        product_number =
            std::stoi(extract_product_serial_number(response_product_number_, raw_data.raw_data()));
        glog.is(DEBUG2) && glog << group("dosta_optode::driver")
                                << "Product number: " << product_number << std::endl;
        set_product_number(product_number);
    }
    else if (raw_data.raw_data().find(response_serial_number_) != std::string::npos)
    {
        serial_number =
            std::stoi(extract_product_serial_number(response_serial_number_, raw_data.raw_data()));
        glog.is(DEBUG2) && glog << group("dosta_optode::driver")
                                << "Serial number: " << serial_number << std::endl;
        set_serial_number(serial_number);
    }
    else if (raw_data.raw_data().find(response_interval_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_sleep_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_polled_mode_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_text_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_decimalformat_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_airsaturation_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_rawdata_value_) != std::string::npos ||
             raw_data.raw_data().find(response_enable_temp_value_) != std::string::npos ||
             raw_data.raw_data().find(response_salinity_value_) != std::string::npos ||
             raw_data.raw_data().find("Reference\t" + std::to_string(product_number) + "\t" +
                                      std::to_string(serial_number) + "\t\r\n") !=
                 std::string::npos)
    {
        all_configuration_settings += raw_data.raw_data();

        // this is the last line of the "Get All" response
        if (raw_data.raw_data().find("Reference\t" + std::to_string(product_number) + "\t" +
                                     std::to_string(serial_number) + "\t\r\n") != std::string::npos)
        {
            glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                    << "Received end of config settings response" << std::endl;

            // parse configuration into sensor config
            protobuf::DOSTA_OPTODE_SensorConfig reported_cfg;

            int interval_val =
                std::stoi(extract_value_from_response_string(response_interval_value_));
            glog.is(DEBUG3) && glog << group("dosta_optode::driver") << "Interval: " << interval_val
                                    << std::endl;
            reported_cfg.set_interval(interval_val);

            std::string enable_decimal_format_string =
                extract_value_from_response_string(response_enable_decimalformat_value_);
            glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                    << "Enable Decimal Format: " << enable_decimal_format_string
                                    << std::endl;
            reported_cfg.set_enable_decimal_format(enable_decimal_format_string == "Yes" ? true
                                                                                         : false);

            std::string enable_air_saturation_string =
                extract_value_from_response_string(response_enable_airsaturation_value_);
            glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                    << "Enable Air Saturation: " << enable_air_saturation_string
                                    << std::endl;
            reported_cfg.set_enable_air_saturation(enable_air_saturation_string == "Yes" ? true
                                                                                         : false);

            std::string enable_raw_data_string =
                extract_value_from_response_string(response_enable_rawdata_value_);
            glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                    << "Enable Raw Data: " << enable_raw_data_string << std::endl;
            reported_cfg.set_enable_raw_data(enable_raw_data_string == "Yes" ? true : false);

            std::string enable_temp_string =
                extract_value_from_response_string(response_enable_temp_value_);
            glog.is(DEBUG3) && glog << group("dosta_optode::driver")
                                    << "Enable Temperature: " << enable_temp_string << std::endl;
            reported_cfg.set_enable_temperature(enable_temp_string == "Yes" ? true : false);

            int salinity_val =
                std::stoi(extract_value_from_response_string(response_salinity_value_));
            glog.is(DEBUG3) && glog << group("dosta_optode::driver") << "Salinity: " << salinity_val
                                    << std::endl;
            reported_cfg.set_salinity(salinity_val);

            glog.is(DEBUG2) && glog << group("dosta_optode::driver")
                                    << "Reported configuration: " << reported_cfg.ShortDebugString()
                                    << std::endl;
            glog.is(DEBUG2) && glog << group("dosta_optode::driver") << "Proposed configuration: "
                                    << cfg().sensor().ShortDebugString() << std::endl;

            // if the two configurations are the same
            std::string reported_config_string;
            std::string proposed_config_string;
            reported_cfg.SerializeToString(&reported_config_string);
            cfg().sensor().SerializeToString(&proposed_config_string);

            all_configuration_settings.clear();

            if (reported_config_string == proposed_config_string)
            {
                glog.is(DEBUG2) &&
                    glog << group("dosta_optode::driver")
                         << "Reported and proposed instrument configuration are the same"
                         << std::endl;
                state_machine().process_event(sensor_command::statechart::EvConfigured());
            }
            else
            {
                reported_cfg.Clear();
                // not configured, post this, with desired configuration
                glog.is(DEBUG2) && glog << group("dosta_optode::driver")
                                        << "Reported and proposed instrument configuration are the "
                                           "NOT the same, time to configure"
                                        << std::endl;
                state_machine().process_event(sensor_command::statechart::EvNotConfigured<
                                              protobuf::DOSTA_OPTODE_SensorConfig>(cfg().sensor()));
            }
        }
    }
}

std::string cgsn::DOSTA_OPTODE_Driver::extract_value_from_response_string(const std::string cmd)
{
    std::string start_string =
        cmd + "\t" + std::to_string(product_number) + "\t" + std::to_string(serial_number) + "\t";
    std::string stop_string = "\r\n";

    if (all_configuration_settings.find(start_string) != std::string::npos)
    {
        unsigned int start_index =
            all_configuration_settings.find(start_string) + start_string.length();
        unsigned int stop_index = all_configuration_settings.find(stop_string, start_index + 1);
        std::string found_value =
            all_configuration_settings.substr(start_index, stop_index - start_index);

        glog.is(DEBUG2) && glog << group("dosta_optode::driver") << "extracted value for " << cmd
                                << ": " << found_value << std::endl;

        return found_value;
    }
    else
    {
        glog.is(WARN) && glog << group("dosta_optode::driver") << cmd
                              << " was not found in configuration settings response. Returning "
                                 "default string: Yes "
                              << std::endl;
        return "";
    }
}

std::string cgsn::DOSTA_OPTODE_Driver::extract_product_serial_number(const std::string cmd,
                                                                     std::string raw_data_string)
{
    std::string start_string = "\t";
    std::string stop_string = "\r\n";
    unsigned int start_index = raw_data_string.find_last_of(start_string) + start_string.length();
    unsigned int stop_index = raw_data_string.find(stop_string, start_index + 1);
    std::string found_value = raw_data_string.substr(start_index, stop_index - start_index);

    glog.is(DEBUG2) && glog << group("dosta_optode::driver") << "extracted value for " << cmd
                            << ": " << found_value << std::endl;

    return found_value;
}

void cgsn::DOSTA_OPTODE_Driver::set_product_number(int x) { product_number = x; }

void cgsn::DOSTA_OPTODE_Driver::set_serial_number(int y) { serial_number = y; }
