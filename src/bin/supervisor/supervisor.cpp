// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "supervisor.h"

#include "cgsn-mooring/supervisor/parse.h"

#include "cgsn-mooring/serial/ascii_line_based_serial.h"

using goby::glog;
using namespace goby::common::logger;

using SerialThread = cgsn::io::AsciiLineSerialThread<cgsn::groups::supervisor::mpic_raw_in,
                                                     cgsn::groups::supervisor::mpic_raw_out>;

cgsn::Supervisor::Supervisor()
    : goby::MultiThreadApplication<protobuf::SupervisorConfig>(10 * boost::units::si::hertz)
{
    // initialize to avoid required fields not begin set
    io_status_.set_state(protobuf::IO__STATE_UNKNOWN);

    interthread().subscribe<groups::supervisor::mpic_raw_in, protobuf::SensorRaw>(
        [this](const protobuf::SensorRaw& raw) { this->incoming_raw(raw); });

    interprocess().subscribe<groups::supervisor::mpic_command, protobuf::MPICCommand>(
        [this](const protobuf::MPICCommand& cmd) { this->handle_mpic_command(cmd); });

    // only publish an overall status if there's an IO error, otherwise wait on the MPICStatus message
    interthread().subscribe<cgsn::groups::io::status, protobuf::IOStatus>(
        [this](const protobuf::IOStatus& status) {
            io_status_ = status;
            if (status.has_error())
                publish_status();
        });

    this->template launch_thread<SerialThread>(this->cfg().mpic_serial());
}

cgsn::Supervisor::~Supervisor() { this->template join_thread<SerialThread>(); }

void cgsn::Supervisor::incoming_raw(const cgsn::protobuf::SensorRaw& raw_data)
{
    const auto& msg = raw_data.raw_data();

    if (!cgsn::supervisor::checksum_valid(msg))
    {
        glog.is(WARN) && glog << "Received message with invalid checksum. Ignoring" << std::endl;
    }
    else
    {
        std::string identifier = msg.substr(0, msg.find(":"));

        if (identifier == "dcl")
            handle_dcl_message(raw_data);
        else if (identifier == "poff" || identifier == "pon")
            handle_pon_poff_message(raw_data);
        else if (identifier == "get_pcfg")
            handle_pcfg_message(raw_data);
    }
}
void cgsn::Supervisor::publish_status()
{
    auto status = cgsn::make_with_header<protobuf::SupervisorStatus>();
    if (mpic_status_.IsInitialized())
        *status.mutable_mpic() = mpic_status_;
    *status.mutable_io() = io_status_;

    interprocess().publish<cgsn::groups::supervisor::status>(status);

    // require a new mpic status
    mpic_status_.Clear();
}

void cgsn::Supervisor::handle_dcl_message(const cgsn::protobuf::SensorRaw& raw_data)
{
    try
    {
        mpic_status_ = cgsn::supervisor::parse_mpic_dcl_status(raw_data.raw_data());
        glog.is(DEBUG2) && glog << "Received DCL status: " << mpic_status_.ShortDebugString()
                                << std::endl;

        // trigger status publication off the MPIC report
        publish_status();
    }
    catch (cgsn::supervisor::Exception& e)
    {
        glog.is(WARN) && glog << "Failed to parse DCL status: " << e.what() << std::endl;
    }
}

void cgsn::Supervisor::handle_pon_poff_message(const cgsn::protobuf::SensorRaw& raw_data)
{
    auto parsed_pon_poff =
        cgsn::supervisor::parse_mpic_pon_poff(raw_data.raw_data(), cgsn::protobuf::Port::FROM_MPIC);

    if (!parsed_pon_poff.has_error())
    {
        glog.is(VERBOSE) && glog << "Success excecuting "
                                 << cgsn::protobuf::Port::PowerState_Name(parsed_pon_poff.state())
                                 << " for port id: " << parsed_pon_poff.port_id() << std::endl;
    }
    else
    {
        glog.is(WARN) && glog << "MPIC reports failure ("
                              << cgsn::protobuf::PortOnOff::Error_Name(parsed_pon_poff.error())
                              << ") in executing "
                              << cgsn::protobuf::Port::PowerState_Name(parsed_pon_poff.state())
                              << " for port id: " << parsed_pon_poff.port_id() << " ["
                              << raw_data.raw_data() << "]" << std::endl;
    }
}

void cgsn::Supervisor::handle_mpic_command(const cgsn::protobuf::MPICCommand& cmd)
{
    cmd_queue_.push_back({cmd});
}

void cgsn::Supervisor::handle_pcfg_message(const cgsn::protobuf::SensorRaw& raw_data)
{
    try
    {
        auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(
            raw_data.raw_data(), cgsn::protobuf::Port::FROM_MPIC, cgsn::protobuf::Port::GET);

        if (cmd_queue_.empty() || parsed_pcfg.port_id() != cmd_queue_.front().cmd.port_id())
        {
            glog.is(WARN) &&
                glog << "Received get_pcfg for different port than front of command queue."
                     << std::endl;
        }
        else
        {
            auto& cmd_meta = cmd_queue_.front();
            if (pcfg_equivalent(parsed_pcfg, cmd_meta.cmd.port_cfg()))
            {
                glog.is(DEBUG2) && glog << "Actual port power configuration matches desired."
                                        << std::endl;
                process_cmd_action(cmd_meta.cmd);
                cmd_queue_.pop_front();
            }
            else
            {
                glog.is(DEBUG2) && glog << "Port power configuration mismatch. Desired: "
                                        << cmd_meta.cmd.port_cfg().ShortDebugString()
                                        << ", Actual: " << parsed_pcfg.ShortDebugString()
                                        << std::endl;
                publish_mpic_raw_out(
                    "set_pcfg " + std::to_string(cmd_meta.cmd.port_cfg().port_id()) + " " +
                    std::to_string(static_cast<int>(cmd_meta.cmd.port_cfg().voltage())) + " " +
                    std::to_string(cmd_meta.cmd.port_cfg().current_limit()) + " " +
                    std::to_string(static_cast<int>(cmd_meta.cmd.port_cfg().protocol())) + " " +
                    std::to_string(static_cast<int>(cmd_meta.cmd.port_cfg().soft_start())) + " " +
                    std::to_string(static_cast<int>(cmd_meta.cmd.port_cfg().power())));
            }
        }
    }
    catch (cgsn::supervisor::Exception& e)
    {
        glog.is(WARN) && glog << "Failed to parse get_pcfg response: " << e.what() << std::endl;
    }
}

void cgsn::Supervisor::publish_mpic_raw_out(std::string msg)
{
    auto raw_out = make_with_header<cgsn::protobuf::SensorRaw>();
    auto cs = supervisor::calc_checksum(msg.begin(), msg.end());
    std::stringstream msg_ss;
    msg_ss << msg << " " << std::hex << cs << "\r\n";
    raw_out.set_raw_data(msg_ss.str());
    interprocess().publish<groups::supervisor::mpic_raw_out>(raw_out);
}

void cgsn::Supervisor::loop()
{
    while (!cmd_queue_.empty())
    {
        auto& cmd_meta = cmd_queue_.front();
        if (cmd_meta.cmd.has_port_cfg())
        {
            auto now = goby::time::now();
            if (now > cmd_meta.pcfg_query_time + pcfg_timeout_)
            {
                publish_mpic_raw_out("get_pcfg " + std::to_string(cmd_meta.cmd.port_id()));
                cmd_meta.pcfg_query_time = now;
            }
            break;
        }
        else
        {
            process_cmd_action(cmd_meta.cmd);
            cmd_queue_.pop_front();
        }
    }
}

void cgsn::Supervisor::process_cmd_action(const cgsn::protobuf::MPICCommand& cmd)
{
    switch (cmd.command())
    {
        case cgsn::protobuf::Port::ON:
            publish_mpic_raw_out("pon " + std::to_string(cmd.port_id()));
            break;
        case cgsn::protobuf::Port::OFF:
            publish_mpic_raw_out("poff " + std::to_string(cmd.port_id()));
            break;
    }
}

int main(int argc, char* argv[]) { return goby::run<cgsn::Supervisor>(argc, argv); }
