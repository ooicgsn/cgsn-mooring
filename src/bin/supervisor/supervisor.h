// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SUPERVISOR_20180619_H
#define SUPERVISOR_20180619_H

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/config/supervisor_config.pb.h"

#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"


namespace cgsn
{
    /// \brief Communicates with the Master PIC, oversees the health of the sensor drivers
    /// \details
    /// Publishes:
    /// Group                                 | Layer           | Type                         | Value
    /// --------------------------------------|-----------------|------------------------------|-------------------------
    /// cgsn::groups::supervisor::mpic_raw_in | interprocess    | cgsn::protobuf::SensorRaw    | Outgoing commands to the Master PIC
    /// cgsn::groups::supervisor::mpic_status | interprocess    | cgsn::protobuf::MPICStatus   | Parsed status from Master PIC
    /// ----------
    /// Subscribes:
    /// Group                                  | Layer          | Type                        | Value
    /// ---------------------------------------|----------------|-----------------------------|-------------------------
    /// cgsn::groups::supervisor::mpic_raw_in  | interthread    | cgsn::protobuf::SensorRaw   | Raw data from the Master PIC
    /// cgsn::groups::supervisor::mpic_command | interprocess   | cgsn::protobuf::MPICCommand | Commands for the MPIC (e.g. port on/off)
    class Supervisor : public goby::MultiThreadApplication<protobuf::SupervisorConfig>
    {
    public:
        Supervisor();
        ~Supervisor();

    private:
        void incoming_raw(const cgsn::protobuf::SensorRaw& raw_data);
        void handle_mpic_command(const cgsn::protobuf::MPICCommand& cmd);
        void process_cmd_action(const cgsn::protobuf::MPICCommand& cmd);

        void handle_dcl_message(const cgsn::protobuf::SensorRaw& raw_data);

        void handle_pon_poff_message(const cgsn::protobuf::SensorRaw& raw_data);
        void handle_pcfg_message(const cgsn::protobuf::SensorRaw& raw_data);

        void publish_mpic_raw_out(std::string msg);

        // get_pcfg does not return the same current offset we set (usually a few mA off).
        // thus, we consider the returned pcfg equivalent if the current offset is within
        // a configurable epsilon (see supervisor_config.proto)
        bool pcfg_equivalent(const protobuf::PowerConfig& p1, const protobuf::PowerConfig& p2)
        {
            return (p1.port_id() == p2.port_id()) &&
                (p1.voltage() == p2.voltage()) &&
                (boost::units::abs(p1.current_limit_with_units() - p2.current_limit_with_units()) < cfg().allowed_current_limit_offset_with_units()) &&
                (p1.protocol() == p2.protocol()) &&
                (p1.soft_start() == p2.soft_start()) &&
                (p1.power() == p2.power()) &&
                (p1.error() == p2.error());
        }

        void loop() override;
        void publish_status();

      private:

        const goby::time::MicroTime pcfg_timeout_ { 3*boost::units::si::seconds };

        struct CommandMeta
        {
            cgsn::protobuf::MPICCommand cmd;
            goby::time::MicroTime pcfg_query_time { 0*goby::time::MicroTimeUnit() }; // when did we send get_pcfg?
        };

        std::deque<CommandMeta> cmd_queue_;

        // copy of the IO thread status message
        protobuf::IOStatus io_status_;
        protobuf::MPICStatus mpic_status_;
    };
}



#endif
