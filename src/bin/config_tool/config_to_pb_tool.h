// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef CONFIG_TO_PB_TOOL_20181115_H
#define CONFIG_TO_PB_TOOL_20181115_H

#include <string>

#include <google/protobuf/text_format.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"
#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/config/serial_config.pb.h"


namespace cgsn
{
    namespace config
    {
        class ConfigToProtobufTool
        {
        public:
            ConfigToProtobufTool(const cgsn::protobuf::ConfigToolConfig& app_cfg) : _app_cfg(app_cfg) {}
            void run();

        private:
            const cgsn::protobuf::ConfigToolConfig& app_cfg() const { return _app_cfg; }

            const google::protobuf::FieldDescriptor* find_submessage_desc(const std::string& field_name, const google::protobuf::Descriptor* desc);
            const google::protobuf::FieldDescriptor* find_sensor_desc(const google::protobuf::Descriptor* desc) { return find_submessage_desc("sensor", desc); }

            void write_pb_cfg(const google::protobuf::Message& pb_cfg);

            template<typename Config>
            Config* mutable_submessage(google::protobuf::Message& pb_cfg, const std::string& field_name);

            std::string replace_special(const std::string& str);

        private:
            const cgsn::protobuf::ConfigToolConfig& _app_cfg;
        };
    }
}

#endif
