// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/common/application_base3.h>
#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/supervisor_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/sensor/autoconfig.h"

#include "common/exception.h"
#include "common/find_protobuf_name.h"
#include "common/yaml_raii.h"

#include "example_config_tool.h"

using goby::glog;


//
// WRITE_EXAMPLE_YAML_FILES
//

void cgsn::config::ExampleConfigTool::run()
{
    if(app_cfg().priority() == cgsn::FieldOptions::YAMLConfig::NEVER)
        throw(Exception("Cannot use priority == NEVER as a configuration parameter"));


    YAML::Emitter yaml_master_out;
    YAML::Emitter yaml_mooring_out;
    YAML::Emitter yaml_assets_out;
    YMap master_base_map(yaml_master_out);
    master_base_map.add("platform", "cp01cnsm");
    YMap mooring_base_map(yaml_mooring_out);
    YMap assets_base_map(yaml_assets_out);

    auto& MEDIUM = cgsn::FieldOptions::YAMLConfig::MEDIUM;
    auto& LOW = cgsn::FieldOptions::YAMLConfig::LOW;
    auto& NEVER = cgsn::FieldOptions::YAMLConfig::NEVER;
    {

        mooring_base_map.add_key("interprocess");
        YMap interprocess_children(yaml_mooring_out, app_cfg().compact());
        write_yaml_from_pb(goby::protobuf::InterProcessPortalConfig::descriptor(), yaml_mooring_out,
                           interprocess_children,
                           {{"platform", NEVER}, // set by top-level platform key
                            {"send_queue_size", LOW},
                            {"receive_queue_size", LOW},
                            {"zeromq_number_io_threads", LOW},
                            {"manager_timeout_seconds", LOW},
                            {"socket_name", MEDIUM},
                            {"ipv4_address", MEDIUM},
                            {"tcp_port", LOW}});
    }

    {
        mooring_base_map.add_key("glog_config");
        YMap glog_cfg(yaml_mooring_out, app_cfg().compact());
        const auto* glog_desc = goby::common::protobuf::GLogConfig::descriptor();
        write_yaml_from_pb(glog_desc, yaml_mooring_out, glog_cfg,
                           {{"show_gui", MEDIUM}, {"tty_verbosity", MEDIUM}});
    }

    {
        mooring_base_map.add_key("logger");
        YMap logger_children(yaml_mooring_out, app_cfg().compact());
        write_yaml_from_pb(goby::protobuf::LoggerConfig::descriptor(), yaml_mooring_out,
                           logger_children,
                           {{"app", NEVER}, // set by top-level app.glog_config, etc.
                            {"interprocess", NEVER},
                            {"type_regex", LOW},
                            {"group_regex", MEDIUM}}); // set by top-level interprocess
    }

    {
        mooring_base_map.add_key("supervisor");
        YMap supervisor_children(yaml_mooring_out, app_cfg().compact());
        write_yaml_from_pb(cgsn::protobuf::SupervisorConfig::descriptor(), yaml_mooring_out,
                           supervisor_children, {{"app", NEVER}, {"interprocess", NEVER}});
    }

    {
        mooring_base_map.add_key("instruments");
        assets_base_map.add_key("instruments");
        YSeq mooring_instrument_seq(yaml_mooring_out);
        YSeq assets_instrument_seq(yaml_assets_out);
        {
            master_base_map.add_key("subassemblies");
            YSeq master_subassemblies_seq(yaml_master_out);
            {
                YMap buoy_map(yaml_master_out);
                buoy_map.add("name", "buoy");
                write_instruments("buoy", yaml_master_out, yaml_mooring_out, yaml_assets_out,
                                  buoy_map, mooring_base_map, assets_base_map);
            }
            {
                YMap nsif_map(yaml_master_out);
                nsif_map.add("name", "nsif");
                write_instruments("nsif", yaml_master_out, yaml_mooring_out, yaml_assets_out,
                                  nsif_map, mooring_base_map, assets_base_map);
            }
            {
                YMap mfn_map(yaml_master_out);
                mfn_map.add("name", "mfn");
                write_instruments("mfn", yaml_master_out, yaml_mooring_out, yaml_assets_out,
                                  mfn_map, mooring_base_map, assets_base_map);
            }
        }
    }
        
    std::ofstream master_fout(app_cfg().example_master_yaml().c_str());
    master_fout << yaml_master_out.c_str() << std::endl;
    std::cout << "Wrote " << app_cfg().example_master_yaml() << std::endl;

    std::ofstream mooring_fout(app_cfg().example_mooring_yaml().c_str());
    mooring_fout << yaml_mooring_out.c_str() << std::endl;
    std::cout << "Wrote " << app_cfg().example_mooring_yaml() << std::endl;

    std::ofstream assets_fout(app_cfg().example_assets_yaml().c_str());
    assets_fout << yaml_assets_out.c_str() << std::endl;
    std::cout << "Wrote " << app_cfg().example_assets_yaml() << std::endl;
}

void cgsn::config::ExampleConfigTool::write_instruments(const std::string& subassembly, YAML::Emitter& yaml_master_out, YAML::Emitter& yaml_mooring_out, YAML::Emitter& yaml_assets_out, YMap& master_parent_map, YMap& mooring_base_map, YMap& assets_base_map)
{
    static int dcl_count = 0;
    ++dcl_count;
    
    master_parent_map.add_key("instruments");
    YSeq master_instrument_seq(yaml_master_out);

    const auto* sensor_type_desc = cgsn::protobuf::sensor::Type_descriptor();

    int port = 1;
    for(int i = 0, n = sensor_type_desc->value_count(); i < n; ++i)
    {
        const auto* sensor_type_val_desc = sensor_type_desc->value(i);


        const auto& ev_options = sensor_type_val_desc->options().GetExtension(cgsn::ev).type_data();

        auto protobuf_name = find_protobuf_name(
            cgsn::protobuf::app::SENSOR_DRIVER,
            static_cast<cgsn::protobuf::sensor::Type>(sensor_type_val_desc->number()));

        const auto* desc = dccl::DynamicProtobufManager::find_descriptor(protobuf_name);
        if(!desc)
            throw(Exception(std::string("no protobuf with name: " + protobuf_name + " found")));

        for(const auto& supports : ev_options.supports())
        {
            const auto& instrument_type = supports.type();
            YMap master_instruments(yaml_master_out);
            YMap mooring_instruments(yaml_mooring_out);
            YMap assets_instruments(yaml_assets_out);
            const auto instrument_key = subassembly + "-" + instrument_type + "-N";

            // defaults file
            YAML::Emitter yaml_sensor_default_out;
            {
                write_instrument_sensor_config(desc, yaml_sensor_default_out);
            }
            boost::format default_fmt(app_cfg().example_instrument_yaml());
            default_fmt.exceptions( boost::io::all_error_bits ^ ( boost::io::too_many_args_bit | boost::io::too_few_args_bit));

            std::string manufacturer = supports.manufacturer();
            boost::replace_all(manufacturer, " ", "_");
            boost::to_lower(manufacturer);

            std::string model = supports.model();
            boost::replace_all(model, " ", "_");
            boost::to_lower(model);

            default_fmt % instrument_type % manufacturer % model;
            std::ofstream defaults_fout(default_fmt.str().c_str());
            defaults_fout << yaml_sensor_default_out.c_str() << std::endl;
            std::cout << "Wrote " << default_fmt.str() << std::endl;

            master_instruments.add("instrument_key", instrument_key);
            mooring_instruments.add("instrument_key", instrument_key);
            assets_instruments.add("instrument_key", instrument_key);

            master_instruments.add("port", port++);
            master_instruments.add("type", instrument_type);
            master_instruments.add("cpu", "dcl" + std::to_string(dcl_count) + "5");

            master_instruments.add("legacy_name", instrument_type + "N");

            mooring_instruments.add("setting_defaults", default_fmt.str());

            if(app_cfg().priority() >= cgsn::FieldOptions::YAMLConfig::MEDIUM)
            {
                mooring_instruments.add_key("setting_overrides");
                {
                    YMap overrides(yaml_mooring_out);
                }
            }

            mooring_instruments.add_key("schedule");
            {
                YMap schedule(yaml_mooring_out, app_cfg().compact());
                const auto* sched_desc = schedule_desc();
                write_yaml_from_pb(sched_desc, yaml_mooring_out, schedule);
            }

            mooring_instruments.add_key("serial");
            {
                YMap serial(yaml_mooring_out, app_cfg().compact());
                const auto* serial_desc = find_serial_desc(desc)->message_type();
                write_yaml_from_pb(serial_desc, yaml_mooring_out, serial);
            }
            mooring_instruments.add_key("power");
            {
                YMap power(yaml_mooring_out, app_cfg().compact());
                const auto* power_desc = find_power_desc(desc)->message_type();
                write_yaml_from_pb(power_desc, yaml_mooring_out, power);
            }

            mooring_instruments.add_key("command");
            {
                YMap command(yaml_mooring_out, app_cfg().compact());
                const auto* command_desc = find_command_desc(desc)->message_type();
                write_yaml_from_pb(command_desc, yaml_mooring_out, command);
            }

            assets_instruments.add("manufacturer", supports.manufacturer());
            assets_instruments.add("model", supports.model());

        }

    }

}



void cgsn::config::ExampleConfigTool::write_instrument_sensor_config(const google::protobuf::Descriptor* desc, YAML::Emitter& yaml_out)
{
    const auto* sensor_desc = find_sensor_desc(desc)->message_type();
    const auto& sensor_cfg = sensor_desc->options().GetExtension(cgsn::msg).sensor_cfg();
    const auto& fixed_params = sensor_command::autoconfig::fixed_params(sensor_desc);

    {
        std::stringstream fixed_params_comment;
        fixed_params_comment << "Fixed parameters (sent before configurable values):\n";
        bool has_fixed_params_before = fixed_params.size() > 0;
        for(const auto& fixed_param_pair : fixed_params)
            fixed_params_comment << "\t\"" << replace_special(fixed_param_pair.first) << "\" -- " << fixed_param_pair.second.purpose() << "\n";

        if(!has_fixed_params_before)
            fixed_params_comment << "\t(none)\n";

        if(sensor_cfg.has_store_cfg_command())
        {
            fixed_params_comment << "\nStore parameter command (sent after any configuration commands):\n"
                                 << "\t\"" << replace_special(sensor_cfg.store_cfg_command()) << "\"\n";
        }

        fixed_params_comment << "\nRuntime configurable parameters/values:";
        yaml_out << YAML::Comment(fixed_params_comment.str());
    }

    {
        YMap settings_map(yaml_out);
        write_yaml_from_pb(sensor_desc, yaml_out, settings_map);
    }
}

const google::protobuf::FieldDescriptor* cgsn::config::ExampleConfigTool::find_submessage_desc(const std::string& field_name, const google::protobuf::Descriptor* desc)
{
    const auto* field_desc = desc->FindFieldByName(field_name);
    if(!field_desc || field_desc->cpp_type() != google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
    {
        throw(Exception(std::string("No embedded message field called '" + field_name + "' found in ") + desc->full_name()));
    }
    return field_desc;
}

void cgsn::config::ExampleConfigTool::write_yaml_from_pb(const google::protobuf::Descriptor* desc,
                                                         YAML::Emitter& yaml_out,
                                                         YMap& parent_map,
                                                         std::map<std::string, cgsn::FieldOptions::YAMLConfig::Priority> static_field_priorities)
{
    std::map<std::string, std::vector<boost::format>> batch_formats;
    std::map<std::string, std::vector<boost::format>::iterator> batch_iterators;
    for(const auto& batch : desc->options().GetExtension(cgsn::msg).sensor_cfg().batch())
    {
        for (const auto& fmt : batch.format())
        {
            boost::format formatter(replace_special(fmt));
            formatter.exceptions(boost::io::all_error_bits ^
                                 (boost::io::too_many_args_bit | boost::io::too_few_args_bit));
            formatter % batch.key();
            batch_formats[batch.key()].push_back(formatter);
        }
    }

    for (auto& p : batch_formats) batch_iterators.insert(std::make_pair(p.first, p.second.begin()));

    for(int i = 0, n = desc->field_count(); i < n; ++i)
    {
        bool has_sensor_cfg = desc->options().GetExtension(cgsn::msg).has_sensor_cfg();
        boost::format single_fmt(replace_special(desc->options().GetExtension(cgsn::msg).sensor_cfg().format()));
        const auto* field_desc = desc->field(i);
        const auto& field_cfg = field_desc->options().GetExtension(cgsn::field);
        const auto& field_sensor_cfg = field_cfg.sensor_cfg();

        auto priority = field_cfg.yaml_cfg().priority();
        if(static_field_priorities.count(field_desc->name()))
            priority = static_field_priorities[field_desc->name()];

        if(priority > app_cfg().priority())
            continue;

        single_fmt % field_sensor_cfg.key();
        bool is_in_batch = batch_formats.count(field_sensor_cfg.key());
        if (is_in_batch)
        {
            if (batch_iterators[field_sensor_cfg.key()] ==
                batch_formats[field_sensor_cfg.key()].end())
                throw(InvalidSensorConfiguration("Too few format strings given for batch: " +
                                                 field_sensor_cfg.key()));
        }

        auto& fmt = is_in_batch ? *(batch_iterators[field_sensor_cfg.key()]++) : single_fmt;

        std::string enum_opts;

        auto write_comment = [&]() {
            const auto& purpose = field_sensor_cfg.purpose();
            std::string comment;

            if(has_sensor_cfg)
            {
                comment += purpose;
                if(field_sensor_cfg.has_min() && field_sensor_cfg.has_max())
                {
                    std::stringstream range;
                    range << "\nRange: [" << field_sensor_cfg.min() << ", " << field_sensor_cfg.max() <<  "]";
                    comment += range.str();
                }

                if(!field_desc->is_repeated())
                {
                    if(!is_in_batch)
                    {
                        comment += "\n(e.g. \"" + fmt.str() + "\")";
                    }
                    else
                    {
                        comment += "\n(e.g. (part of " + field_sensor_cfg.key() +
                            " command batch) \"" + fmt.str() + "\")";
                    }
                }
                else
                {
                    std::cout << "Warning: repeated field not supported for sensor configuration generation: " << field_desc->DebugString() << std::endl;
                }


            }
            if(!enum_opts.empty())
            {
                if(!comment.empty())
                    comment += "\n";
                comment += enum_opts;
            }

            if(field_desc->options().GetExtension(goby::field).has_description())
            {
                if(!comment.empty())
                    comment += "\n";
                comment += field_desc->options().GetExtension(goby::field).description();
            }

            if(field_desc->options().GetExtension(dccl::field).has_units())
            {
                auto units_cfg = field_desc->options().GetExtension(dccl::field).units();
                if(!comment.empty())
                    comment += "\n";

                if(!units_cfg.has_system())
                    units_cfg.set_system(units_cfg.system());

                comment += "units { " + units_cfg.ShortDebugString() + " }";
            }


            if(!comment.empty() && !app_cfg().compact())
                yaml_out << YAML::Comment(comment);
        };



        switch(field_desc->cpp_type())
        {
            case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_int32(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_int64(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_uint32(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_uint64(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_double(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
                write_yaml_from_pb_single_val(field_desc,
                                              yaml_out,
                                              field_desc->default_value_float(),
                                              fmt,
                                              parent_map);
                break;
            case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
            {
                auto default_val = field_desc->default_value_bool();
                
                fmt % sensor_command::autoconfig::bool_to_string(default_val, desc);
                if(field_desc->is_repeated())
                {
                    parent_map.add_key(field_desc->name());
                    YSeq field_seq(yaml_out, true);
                    for(int a = 0; a < 5; ++a)
                        field_seq.add(default_val);
                }
                else
                {
                    parent_map.add(field_desc->name(), default_val);
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
            {
                auto default_val_int = field_desc->default_value_enum()->number();
                fmt % default_val_int;

                auto default_val_string = field_desc->default_value_enum()->name();
                if(field_desc->is_repeated())
                {
                    parent_map.add_key(field_desc->name());
                    YSeq field_seq(yaml_out, true);
                    for(int a = 0; a < 5; ++a)
                        field_seq.add(default_val_string);
                }
                else
                {
                    parent_map.add(field_desc->name(), default_val_string);
                }

                for(int i = 0, n = field_desc->enum_type()->value_count(); i < n; ++i)
                {
                    const auto& name = field_desc->enum_type()->value(i)->name();
                    const auto& value = field_desc->enum_type()->value(i)->number();

                    const std::string enum_str = "enum: ";
                    if(!i)
                        enum_opts += enum_str;
                    else
                        enum_opts += ", ";
                    enum_opts += name + "=" + std::to_string(value);
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
            {
                auto default_val = field_desc->default_value_string();
                fmt % default_val;
                parent_map.add(field_desc->name(), default_val);
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:

                const auto* child_desc = field_desc->message_type();
                parent_map.add_key(field_desc->name());

                write_comment();

                if(field_desc->is_repeated())
                {
                    YSeq field_seq(yaml_out);
                    YMap field_map(yaml_out);
                    write_yaml_from_pb(child_desc,
                                       yaml_out,
                                       field_map);
                }
                else
                {
                    YMap field_map(yaml_out);
                    write_yaml_from_pb(child_desc,
                                       yaml_out,
                                       field_map);
                }

                break;
        }

        if(field_desc->cpp_type() != google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
            write_comment();

    }
}

template<typename T>
void cgsn::config::ExampleConfigTool::write_yaml_from_pb_single_val(const google::protobuf::FieldDescriptor* field_desc,
                                                                    YAML::Emitter& yaml_out,
                                                                    T default_val,
                                                                    boost::format& fmt,
                                                                    YMap& parent_map)
{
    fmt % default_val;
    if(field_desc->is_repeated())
    {
        parent_map.add_key(field_desc->name());
        YSeq field_seq(yaml_out, true);
        for(int a = 0; a < 5; ++a)
            field_seq.add(default_val);
    }
    else
    {
        parent_map.add(field_desc->name(), default_val);
    }
}

std::string cgsn::config::ExampleConfigTool::replace_special(const std::string& str)
{
    auto s = str;
    boost::replace_all(s, "\n", "\\n");
    boost::replace_all(s, "\r", "\\r");
    return s;
}

