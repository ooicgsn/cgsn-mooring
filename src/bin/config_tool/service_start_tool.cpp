// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/common/application_base3.h>

#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"
#include "cgsn-mooring/config/config_tool_config.pb.h"

#include "common/exception.h"
#include "common/find_sensor_type.h"
#include "common/yaml_reader.h"

#include "service_start_tool.h"

using goby::glog;


/// \brief Writes the script used by systemd to start the correct service (e.g. driver) on a given port
void cgsn::config::ServiceStartCommandTool::run()
{   
    if(!app_cfg().has_cpu())
        throw(Exception("Must define \"cpu\" when using action == " + cgsn::protobuf::ConfigToolConfig::Action_Name(app_cfg().action())));
    if(!app_cfg().has_port_id())
        throw(Exception("Must define \"port_id\" when using action == " + cgsn::protobuf::ConfigToolConfig::Action_Name(app_cfg().action())));

    const auto& app_type_str = cgsn::protobuf::app::Type_Name(app_cfg().app_type());
    switch(app_cfg().app_type())
    {
        case cgsn::protobuf::app::LEGACY_LOGGER:
        case cgsn::protobuf::app::SENSOR_DRIVER: break;
        default:
            throw(Exception("Cannot write start command for app type " + app_type_str));
    } 
    
    YAML::Node master_yaml, mooring_yaml, assets_yaml;
    std::tie(master_yaml, mooring_yaml, assets_yaml) = yaml::load_yamls(app_cfg());

    const auto& subassemblies = master_yaml["subassemblies"];
    if (!subassemblies)
        throw(Exception("No \"subassemblies:\" key found in " + app_cfg().master_yaml() + " YAML file"));
    if (!subassemblies.IsSequence())
        throw(Exception("\"subassemblies:\" must be a sequence"));


    bool port_is_configured = false;
    for (YAML::Node subassembly : subassemblies)
    {
        const auto& master_instruments = subassembly["instruments"];

        if(!master_instruments)
            throw(Exception("No \"instruments:\" key found in " + app_cfg().master_yaml() + " YAML file"));
        if(!master_instruments.IsSequence())
            throw(Exception("\"instruments:\" must be a sequence"));

        // search for the instrument matching the given CPU and port ID (if any)
        for(YAML::Node master_yaml_instrument : master_instruments)
        {
            const auto& instrument_key = master_yaml_instrument["instrument_key"].as<std::string>();
                    
            // Skip this instrument if it isn't on the CPU and port we care about
            const auto& cpu_node = yaml::find_required_node(master_yaml_instrument, "cpu", app_cfg().master_yaml(), instrument_key);
            const auto& port_node = yaml::find_required_node(master_yaml_instrument, "port", app_cfg().master_yaml(), instrument_key);
            if(cpu_node.as<std::string>() != app_cfg().cpu())
                continue;
            if(port_node.as<int>() != app_cfg().port_id())
                continue;
            
            // Signal that we found an instrument configured to this port
            port_is_configured = true;
            
            // Figure out which binary we want to execute
            std::string binary;
            if (app_cfg().app_type() == cgsn::protobuf::app::LEGACY_LOGGER)
            {
                binary = "cgsn_legacy_logger";
            }
            else if (app_cfg().app_type() == cgsn::protobuf::app::SENSOR_DRIVER)
            {
                // Look up the driver configured for this instrument
                cgsn::protobuf::sensor::Type sensor_type = find_sensor_type(
                    master_yaml, app_cfg().master_yaml(),
                    assets_yaml, app_cfg().assets_yaml(),
                    instrument_key);
    
                const auto* sensor_type_desc = cgsn::protobuf::sensor::Type_descriptor();
                const auto* sensor_type_val_desc = sensor_type_desc->FindValueByNumber(sensor_type);
                const auto& ev_options = sensor_type_val_desc->options().GetExtension(cgsn::ev).type_data();
                binary = ev_options.binary();
            }

            const auto binary_path = app_cfg().binary_dir() + "/" + binary;
            
            const auto config_tool_path = app_cfg().binary_dir() + "/cgsn_config_tool";
            const std::string cfg_file = "<( " + config_tool_path + " --action CONVERT_YAML_TO_PROTOBUF --app_type " + app_type_str + " --instrument_key " + instrument_key + " )";
            const std::string app_name = binary + "-" + instrument_key;
            std::stringstream cmd;
            cmd << binary_path << " " << cfg_file << " --app_name " << app_name;
            
            const std::string exec_cmd = "exec " + cmd.str();
            if(app_cfg().exec())
            {
                if(execl("/bin/bash", "/bin/bash", "-c", exec_cmd.c_str(), static_cast<const char*>(nullptr)) == -1)
                    throw(Exception(std::string("Failed to execl: ") + std::strerror(errno)));
            }
            else
            {
                std::cout << cmd.str();
            }
            
            // No need to consider any more instruments
            break;
        }   
    }
    
    if(!port_is_configured)
    {
        std::cout << "Port id " << app_cfg().port_id()
                  << " is not configured in " << app_cfg().master_yaml()
                  << std::endl;
    }
}
