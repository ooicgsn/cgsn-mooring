// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/common/application_base3.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/common.h"

#include "config_to_pb_tool.h"
#include "example_config_tool.h"
#include "service_start_tool.h"
#include "sim_launchfile_tool.h"


namespace cgsn
{
    namespace config
    {
        class ConfigMetaTool : public goby::common::ApplicationBase3<cgsn::protobuf::ConfigToolConfig>
        {
        public:
            ConfigMetaTool();
            ~ConfigMetaTool() { }

        private:
            void run() override { }
        };
    }
}

cgsn::config::ConfigMetaTool::ConfigMetaTool()
{
    try
    {
        switch(app_cfg().action())
        {
            case cgsn::protobuf::ConfigToolConfig::WRITE_EXAMPLE_YAML_FILES:
                cgsn::config::ExampleConfigTool(app_cfg()).run();
                break;
            case cgsn::protobuf::ConfigToolConfig::CONVERT_YAML_TO_PROTOBUF:
                cgsn::config::ConfigToProtobufTool(app_cfg()).run();
                break;
            case cgsn::protobuf::ConfigToolConfig::WRITE_SERVICE_START_COMMAND:
                cgsn::config::ServiceStartCommandTool(app_cfg()).run();
                break;
            case cgsn::protobuf::ConfigToolConfig::WRITE_SIMULATION_LAUNCHFILE:
                cgsn::config::SimulatorLaunchfileTool(app_cfg()).run();
                break;
            case cgsn::protobuf::ConfigToolConfig::WRITE_CPU_NAME:
                std::cout << cgsn::system::cpu_name() << std::endl;
                break;
            case cgsn::protobuf::ConfigToolConfig::WRITE_MOORING_NAME:
                std::cout << cgsn::system::mooring_name() << std::endl;
                break;
        }
        quit();
    }
    catch(const std::exception& e)
    {
        std::cerr << "cgsn_config_tool: Error: " << e.what() << std::endl;
        exit(1);
    }
}

int main(int argc, char* argv[])
{
    goby::run<cgsn::config::ConfigMetaTool>(argc, argv);
    dccl::DynamicProtobufManager::protobuf_shutdown();
    return 0;
}

