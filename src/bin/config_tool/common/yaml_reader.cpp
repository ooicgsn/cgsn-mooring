// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <tuple>

#include <yaml-cpp/yaml.h>

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/common/application_base3.h>
#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/config/serial_config.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"

#include "exception.h"
#include "find_sensor_type.h"
#include "yaml_reader.h"

using cgsn::config::Exception;
using goby::glog;

static std::string to_string(const YAML::Node& node)
{
    YAML::Emitter em;
    em << node;
    return em.c_str();
}

/// \brief Accepts ranges in the format "0-10" or "2-7" and returns the numeric vector (e.g. {0,1,2,3,4,5,6,7,8,9,10} or {2,3,4,5,6,7}
template <typename Unsigned>
static std::vector<Unsigned> convert_with_ranges(const std::vector<std::string> svec)
{
    std::vector<Unsigned> uvec;
    for (const auto& sval : svec)
    {
        auto hyphen_pos = sval.find('-');

        if (hyphen_pos == std::string::npos)
        {
            // just a number
            try
            {
                uvec.push_back(boost::lexical_cast<Unsigned>(sval));
            }
            catch (boost::bad_lexical_cast& e)
            {
                throw(Exception("Value: \"" + sval + "\" is not an unsigned value."));
            }
        }
        else
        {
            // range of uvec
            try
            {
                int begin_val = boost::lexical_cast<Unsigned>(sval.substr(0, hyphen_pos));
                int end_val = boost::lexical_cast<Unsigned>(sval.substr(hyphen_pos + 1));

                if (begin_val > end_val)
                    throw(Exception("Invalid range: 'A-B' where A > B: " + sval));

                for (int h = begin_val; h <= end_val; ++h) uvec.push_back(h);
            }
            catch (boost::bad_lexical_cast& e)
            {
                throw(
                    Exception("Value: \"" + sval + "\" is not a valid range of unsigned values."));
            }
        }
    }
    return uvec;
}

YAML::Node cgsn::config::yaml::find_master_instrument(const YAML::Node& master_yaml,
                                                      const std::string& file_name,
                                                      const std::string& instrument_key)
{
    const auto& subassemblies = master_yaml["subassemblies"];
    if (!subassemblies)
        throw(Exception("No \"subassemblies:\" key found in " + file_name + " YAML file"));

    if (!subassemblies.IsSequence())
        throw(Exception("\"subassemblies:\" must be a sequence"));

    for (YAML::Node subassembly : subassemblies)
    {
        try
        {
            return find_instrument(subassembly, file_name, instrument_key);
        }
        catch (Exception& e)
        {
            // continue looking through other subassemblies
        }
    }

    throw(Exception("No instrument_key found any of the subassemblies in " + file_name +
                    " matching " + instrument_key));

    return YAML::Node();
}

YAML::Node cgsn::config::yaml::find_instrument(const YAML::Node& yaml, const std::string& file_name,
                                               const std::string& instrument_key)
{
    const auto& instruments = yaml["instruments"];

    if (!instruments)
        throw(Exception("No \"instruments:\" key found in " + file_name + " YAML file"));

    if (!instruments.IsSequence())
        throw(Exception("\"instruments:\" must be a sequence"));

    bool instrument_found = false;
    for (YAML::Node instrument : instruments)
    {
        if (instrument["instrument_key"].as<std::string>() == instrument_key)
        {
            instrument_found = true;
            return instrument;
        }
    }
    if (!instrument_found)
        throw(Exception("No instrument_key found in " + file_name + " matching " + instrument_key));

    return YAML::Node();
}

YAML::Node cgsn::config::yaml::find_required_sequence(const YAML::Node& yaml,
                                                      const std::string& key)
{
    const auto& seq = yaml[key];
    if (!seq)
        throw(Exception("No \"" + key + "\" key found in YAML file"));
    if (!seq.IsSequence())
        throw(Exception("\"" + key + ":\" must be a sequence"));
    return seq;
}

YAML::Node cgsn::config::yaml::find_required_node(const YAML::Node& instrument,
                                                  const std::string& key,
                                                  const std::string& yaml_file,
                                                  const std::string& instrument_key)
{
    const auto& node = instrument[key];
    if (!node)
        throw(Exception("No \"" + key + ":\" key found for instrument_key \"" + instrument_key +
                        "\" in " + yaml_file));
    return node;
}

void cgsn::config::yaml::read_single_cfg(const YAML::Node& key, const YAML::Node& value,
                                         google::protobuf::Message& cfg)
{
    const auto* desc = cfg.GetDescriptor();
    const auto* refl = cfg.GetReflection();

    const auto* field_desc = desc->FindFieldByName(key.as<std::string>());

    if (!field_desc)
        throw(Exception("No such settings key \"" + key.as<std::string>() + "\" defined in " +
                        desc->full_name()));

    if (field_desc->is_repeated() && !value.IsSequence())
        throw(Exception(field_desc->full_name() + " is repeated but YAML value \"" +
                        to_string(value) + "\" is not a sequence."));
    else if (!field_desc->is_repeated())
    {
        if (field_desc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE &&
            !value.IsMap())
            throw(
                Exception(field_desc->full_name() +
                          " is a singular (required/optional) embedded message but YAML value \"" +
                          to_string(value) + "\" is not a map."));
        else if (field_desc->cpp_type() != google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE &&
                 !value.IsScalar())
            throw(
                Exception(field_desc->full_name() +
                          " is a singular (required/optional) embedded message but YAML value \"" +
                          to_string(value) + "\" is not a scalar."));
    }

    try
    {
        switch (field_desc->cpp_type())
        {
            case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
            {
                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<std::int32_t>>())
                        refl->AddInt32(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetInt32(&cfg, field_desc, value.as<std::int32_t>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
            {
                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<std::int64_t>>())
                        refl->AddInt64(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetInt64(&cfg, field_desc, value.as<std::int64_t>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
            {
                if (field_desc->is_repeated())
                {
                    auto uvec =
                        convert_with_ranges<std::uint32_t>(value.as<std::vector<std::string>>());
                    for (auto v : uvec) refl->AddUInt32(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetUInt32(&cfg, field_desc, value.as<std::uint32_t>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
            {
                if (field_desc->is_repeated())
                {
                    auto uvec =
                        convert_with_ranges<std::uint64_t>(value.as<std::vector<std::string>>());
                    for (auto v : uvec) refl->AddUInt64(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetUInt64(&cfg, field_desc, value.as<std::uint64_t>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
            {
                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<double>>())
                        refl->AddDouble(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetDouble(&cfg, field_desc, value.as<double>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
            {
                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<float>>())
                        refl->AddFloat(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetFloat(&cfg, field_desc, value.as<float>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
            {
                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<bool>>()) refl->AddBool(&cfg, field_desc, v);
                }
                else
                {
                    refl->SetBool(&cfg, field_desc, value.as<bool>());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
            {
                auto find_enum_val =
                    [field_desc](std::string v) -> const google::protobuf::EnumValueDescriptor* {
                    const auto* enum_desc = field_desc->enum_type();
                    const auto* enum_value_desc = enum_desc->FindValueByName(v);
                    if (!enum_value_desc)
                        throw(Exception("No such enum value: " + v +
                                        " for field: " + field_desc->full_name()));
                    return enum_value_desc;
                };

                if (field_desc->is_repeated())
                {
                    for (auto v : value.as<std::vector<std::string>>())
                        refl->AddEnum(&cfg, field_desc, find_enum_val(v));
                }
                else
                {
                    refl->SetEnum(&cfg, field_desc, find_enum_val(value.as<std::string>()));
                }

                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
            {
                refl->SetString(&cfg, field_desc, value.as<std::string>());
                break;
            }
            case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
            {
                if (field_desc->is_repeated())
                {
                    for (auto seq_v : value)
                    {
                        auto* child_msg = refl->AddMessage(&cfg, field_desc);
                        for (auto setting_pair : seq_v)
                            read_single_cfg(setting_pair.first, setting_pair.second, *child_msg);
                    }
                }
                else
                {
                    auto* child_msg = refl->MutableMessage(&cfg, field_desc);
                    for (auto setting_pair : value)
                        read_single_cfg(setting_pair.first, setting_pair.second, *child_msg);
                }

                break;
            }
        }
    }
    catch (const YAML::Exception& e)
    {
        throw(Exception("Failed to parse: " + field_desc->full_name() + " using \"" +
                        to_string(value) + "\""));
    }
}

void cgsn::config::yaml::read_instrument_cfg(const YAML::Node& instrument,
                                             google::protobuf::Message& sensor_cfg)
{
    if (!instrument["setting_defaults"])
        throw(Exception("No \"setting_defaults:\" key found in \"instruments:\" sequence"));

    auto setting_defaults_yaml_file = instrument["setting_defaults"].as<std::string>();
    try
    {
        YAML::Node defaults_yaml = YAML::LoadFile(setting_defaults_yaml_file);

        for (auto setting_pair : defaults_yaml)
            read_single_cfg(setting_pair.first, setting_pair.second, sensor_cfg);

        if (auto overrides = instrument["setting_overrides"])
        {
            for (auto overrides_pair : overrides)
                read_single_cfg(overrides_pair.first, overrides_pair.second, sensor_cfg);
        }
    }
    catch (const std::exception& e)
    {
        glog.is_die() && glog << "Failed to parse defaults (" << setting_defaults_yaml_file
                              << ") YAML file: " << e.what() << std::endl;
    }
}

void cgsn::config::yaml::read_serial_cfg(const YAML::Node& instrument,
                                         cgsn::protobuf::SerialConfig& serial_cfg)
{
    const auto& serial_node = instrument["serial"];
    if (!serial_node)
        throw(Exception("No \"serial:\" key found for instrument"));

    for (auto setting_pair : serial_node)
        read_single_cfg(setting_pair.first, setting_pair.second, serial_cfg);
}

void cgsn::config::yaml::read_command_cfg(const YAML::Node& instrument,
                                          cgsn::protobuf::SensorCommandConfig& command_cfg)
{
    const auto& command_node = instrument["command"];
    if (command_node)
    {
        for (auto setting_pair : command_node)
            read_single_cfg(setting_pair.first, setting_pair.second, command_cfg);
    }
}

void cgsn::config::yaml::read_power_cfg(const YAML::Node& master_instrument,
                                        const YAML::Node& mooring_instrument,
                                        cgsn::protobuf::PowerConfig& power_cfg)
{
    const auto& power_node = mooring_instrument["power"];
    if (!power_node)
        throw(Exception("No \"power:\" key found for instrument in mooring YAML"));

    for (auto setting_pair : power_node)
        read_single_cfg(setting_pair.first, setting_pair.second, power_cfg);

    const auto& port_node = master_instrument["port"];
    if (!port_node)
        throw(Exception("No \"port:\" key found for instrument in master YAML"));

    power_cfg.set_port_id(port_node.as<int>());
}

void cgsn::config::yaml::read_interprocess_cfg(
    const YAML::Node& mooring_yaml, goby::protobuf::InterProcessPortalConfig& interprocess_cfg)
{
    const auto& interprocess_node = mooring_yaml["interprocess"];
    if (!interprocess_node)
        return; // fully optional to define interprocess in the YAML file

    for (auto setting_pair : interprocess_node)
        read_single_cfg(setting_pair.first, setting_pair.second, interprocess_cfg);
}

void cgsn::config::yaml::read_glog_cfg(const YAML::Node& mooring_yaml,
                                       goby::common::protobuf::GLogConfig& glog_cfg)
{
    const auto& glog_config_node = mooring_yaml["glog_config"];
    if (!glog_config_node)
        return;

    for (auto setting_pair : glog_config_node)
        read_single_cfg(setting_pair.first, setting_pair.second, glog_cfg);
}

std::tuple<YAML::Node, YAML::Node, YAML::Node>
cgsn::config::yaml::load_yamls(const cgsn::protobuf::ConfigToolConfig& cfg)
{
    YAML::Node master_yaml, mooring_yaml, assets_yaml;

    try
    {
        master_yaml = YAML::LoadFile(cfg.master_yaml());
    }
    catch (const std::exception& e)
    {
        throw(
            Exception("Failed to parse master (" + cfg.master_yaml() + ") YAML file: " + e.what()));
    }

    try
    {
        mooring_yaml = YAML::LoadFile(cfg.mooring_yaml());
    }
    catch (const std::exception& e)
    {
        throw(Exception("Failed to parse mooring (" + cfg.mooring_yaml() +
                        ") YAML file: " + e.what()));
    }

    try
    {
        assets_yaml = YAML::LoadFile(cfg.assets_yaml());
    }
    catch (const std::exception& e)
    {
        throw(
            Exception("Failed to parse assets (" + cfg.assets_yaml() + ") YAML file: " + e.what()));
    }

    return std::make_tuple(master_yaml, mooring_yaml, assets_yaml);
}

void cgsn::config::yaml::for_each_instrument_on_this_cpu(
    const cgsn::protobuf::ConfigToolConfig& cfg,
    std::function<void(const YAML::Node& mooring_yaml_instrument, std::string instrument_key,
                       int port_id, cgsn::protobuf::sensor::Type sensor_type)>
        func)
{
    YAML::Node master_yaml, mooring_yaml, assets_yaml;
    std::tie(master_yaml, mooring_yaml, assets_yaml) = yaml::load_yamls(cfg);

    if (!cfg.has_cpu())
        throw(Exception("Must define --cpu"));
    const auto& cpu = cfg.cpu();

    auto subassemblies = yaml::find_required_sequence(master_yaml, "subassemblies");
    bool cpu_found = false;
    for (const auto& subassembly : subassemblies)
    {
        auto master_instruments = yaml::find_required_sequence(subassembly, "instruments");
        for (const auto& master_yaml_instrument : master_instruments)
        {
            const auto& instrument_key = master_yaml_instrument["instrument_key"].as<std::string>();
            const auto& cpu_node = master_yaml_instrument["cpu"];
            if (!cpu_node)
                throw(Exception("No \"cpu:\" key found for instrument_key \"" + instrument_key +
                                "\" in master YAML"));

            if (cpu_node.as<std::string>() != cpu)
                continue;

            cpu_found = true;
            try
            {
                const YAML::Node& mooring_yaml_instrument =
                    yaml::find_instrument(mooring_yaml, cfg.mooring_yaml(), instrument_key);
                const auto& port_node = yaml::find_required_node(master_yaml_instrument, "port",
                                                                 cfg.master_yaml(), instrument_key);

                const auto& type_node = yaml::find_required_node(master_yaml_instrument, "type",
                                                                 cfg.master_yaml(), instrument_key);

                const YAML::Node& assets_yaml_instrument =
                    yaml::find_instrument(assets_yaml, cfg.assets_yaml(), instrument_key);
                const auto& manufacturer_node = yaml::find_required_node(
                    assets_yaml_instrument, "manufacturer", cfg.assets_yaml(), instrument_key);
                const auto& model_node = yaml::find_required_node(
                    assets_yaml_instrument, "model", cfg.assets_yaml(), instrument_key);

                cgsn::protobuf::sensor::Type sensor_type = find_sensor_type(
                    type_node.as<std::string>(), manufacturer_node.as<std::string>(),
                    model_node.as<std::string>());

                func(mooring_yaml_instrument, instrument_key, port_node.as<int>(), sensor_type);
            }
            catch (const YAML::Exception& e)
            {
                throw(Exception("Parser failure for instrument_key \"" + instrument_key +
                                "\": " + e.what()));
            }
        }
    }

    if (!cpu_found)
        throw(Exception("No instruments found for cpu: \"" + cpu + "\""));
}
