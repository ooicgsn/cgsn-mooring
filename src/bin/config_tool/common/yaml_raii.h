// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef YAMLRAII_20180815_H
#define YAMLRAII_20180815_H

#include "yaml-cpp/yaml.h"


namespace cgsn
{
    namespace config
    {
        class YSeq
        {
        public:
        YSeq(YAML::Emitter& out, bool flow = false): out_(out)
            {
                //                std::cerr << "YAML::BeginSeq" << std::endl;
                if(flow)
                    out << YAML::Flow;
                else
                    out << YAML::Block;

                out << YAML::BeginSeq;
            }
            ~YSeq()
            {
                //                std::cerr << "YAML::EndSeq" << std::endl;
                out_ << YAML::EndSeq;
            }

            template <typename A>
                void add(A& a)
            {
                //                std::cerr << a << std::endl;
                out_ << a;
            }

        private:
            YAML::Emitter& out_;
        };


        class YMap
        {
        public:
        YMap(YAML::Emitter& out, bool flow = false): out_(out)
            {
                //                std::cerr << "YAML::BeginMap" << std::endl;
                if(flow)
                    out << YAML::Flow;
                else
                    out << YAML::Block;

                out << YAML::BeginMap;
            }

            template <typename A, typename B>
                YMap(YAML::Emitter& out, A key, B value): YMap(out)
            { add(key, value); }

            template <typename A>
                YMap(YAML::Emitter& out, A key): YMap(out)
            { add_key(key); }


            ~YMap() {
                //                std::cerr << "YAML::EndMap" << std::endl;
                out_ << YAML::EndMap;
            }

            template <typename A, typename B>
                void add(A key, B value)
            {
                //                std::cerr << "YAML::Key " << key << " YAML::Value " << value << std::endl;
                out_ << YAML::Key << key << YAML::Value << value;
            }

            template<typename A>
                void add_key(A key)
            {
                //                std::cerr << "YAML::Key " << key << " YAML::Value" << std::endl;
                out_ << YAML::Key << key << YAML::Value ;
            }

            //template<typename B>
            //    void add_value(B value)
            //        { out_ << value; }



        private:
            YAML::Emitter& out_;
        };
    }
}


#endif
