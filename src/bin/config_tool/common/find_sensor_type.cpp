// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "yaml-cpp/yaml.h"

#include "exception.h"
#include "find_sensor_type.h"
#include "yaml_reader.h"



cgsn::protobuf::sensor::Type cgsn::config::find_sensor_type(const std::string& type,
                                                            const std::string& manufacturer,
                                                            const std::string& model)
{
    const auto* sensor_type_desc = cgsn::protobuf::sensor::Type_descriptor();
    for(int i = 0, n = sensor_type_desc->value_count(); i < n; ++i)
    {
        const auto* sensor_type_val_desc = sensor_type_desc->value(i);
        const auto& ev_options = sensor_type_val_desc->options().GetExtension(cgsn::ev);

        for(const auto& supports : ev_options.type_data().supports())
        {
            if(supports.type() == type && supports.manufacturer() == manufacturer && supports.model() == model)
                return static_cast<cgsn::protobuf::sensor::Type>(sensor_type_val_desc->number());
        }
    }

    throw(Exception("No driver supports sensor: [type: " + type + ", manufacturer: " + manufacturer + ", model: " + model + "]"));
}

cgsn::protobuf::sensor::Type cgsn::config::find_sensor_type(YAML::Node& master_yaml,
                                                            const std::string& master_filename,
                                                            YAML::Node& assets_yaml,
                                                            const std::string& assets_filename,
                                                            const std::string& instrument_key)
{
    const YAML::Node& master_yaml_instrument = yaml::find_master_instrument(master_yaml, master_filename, instrument_key);
    const auto& type_node = yaml::find_required_node(master_yaml_instrument, "type", master_filename, instrument_key);
    
    const YAML::Node& assets_yaml_instrument = yaml::find_instrument(assets_yaml, assets_filename, instrument_key);
    const auto& manufacturer_node = yaml::find_required_node(assets_yaml_instrument, "manufacturer",assets_filename, instrument_key);
    const auto& model_node = yaml::find_required_node(assets_yaml_instrument, "model", assets_filename, instrument_key);
    
    return find_sensor_type(
        type_node.as<std::string>(),
        manufacturer_node.as<std::string>(),
        model_node.as<std::string>());
}
