// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include "find_protobuf_name.h"

std::string cgsn::config::find_protobuf_name(cgsn::protobuf::app::Type app_type,
                                             cgsn::protobuf::sensor::Type sensor_type)
{
    switch (app_type)
    {
        case cgsn::protobuf::app::SENSOR_DRIVER:
        {
            // read from sensor_types.proto
            const auto* enum_val_desc =
                cgsn::protobuf::sensor::Type_descriptor()->FindValueByNumber(sensor_type);
            const auto& ev_options = enum_val_desc->options().GetExtension(cgsn::ev);
            return ev_options.type_data().cfg_name();
        }
        default:
        {
            // read from application_types.proto
            const auto* enum_val_desc =
                cgsn::protobuf::app::Type_descriptor()->FindValueByNumber(app_type);
            const auto& ev_options = enum_val_desc->options().GetExtension(cgsn::ev);
            return ev_options.type_data().cfg_name();
        }
    }
}
