// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef FIND_PROTOBUF_NAME_20181115_H
#define FIND_PROTOBUF_NAME_20181115_H

#include <string>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/messages/application_types.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"

namespace cgsn
{
namespace config
{
std::string find_protobuf_name(
    cgsn::protobuf::app::Type app_type,
    cgsn::protobuf::sensor::Type sensor_type = cgsn::protobuf::sensor::DRIVER_PATTERN);
}
} // namespace cgsn

#endif
