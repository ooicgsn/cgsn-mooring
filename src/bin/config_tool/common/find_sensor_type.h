// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef FIND_SENSOR_TYPE_20181115_H
#define FIND_SENSOR_TYPE_20181115_H

#include <string>

#include "yaml-cpp/yaml.h"

#include "cgsn-mooring/messages/sensor_types.pb.h"


namespace cgsn {
    namespace config {
        /// \brief Maps an instrument <type, manufacturer, model> string tuple
        /// to the corresponding sensor type.
        cgsn::protobuf::sensor::Type find_sensor_type(const std::string& type,
                                                      const std::string& manufacturer,
                                                      const std::string& model);

        /// \brief Convenience function for finding the sensor type by
        /// instrument key
        cgsn::protobuf::sensor::Type find_sensor_type(YAML::Node& master_yaml,
                                                      const std::string& master_filename,
                                                      YAML::Node& assets_yaml,
                                                      const std::string& assets_filename,
                                                      const std::string& instrument_key);
    }
}

#endif
