// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef YAML_READER_20181112_H
#define YAML_READER_20181112_H

#include <string>
#include <tuple>

#include <yaml-cpp/yaml.h>

#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/config/sensors_config.pb.h"
#include "cgsn-mooring/config/serial_config.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"

namespace cgsn
{
namespace config
{
namespace yaml
{
YAML::Node find_master_instrument(const YAML::Node& master_yaml, const std::string& file_name,
                                  const std::string& instrument_key);
YAML::Node find_instrument(const YAML::Node& yaml, const std::string& file_name,
                           const std::string& instrument_key);
YAML::Node find_required_node(const YAML::Node& instrument, const std::string& key,
                              const std::string& yaml_file, const std::string& instrument_key);

YAML::Node find_required_sequence(const YAML::Node& yaml, const std::string& key);

void read_single_cfg(const YAML::Node& key, const YAML::Node& value,
                     google::protobuf::Message& cfg);
void read_instrument_cfg(const YAML::Node& instrument, google::protobuf::Message& sensor_cfg);
void read_serial_cfg(const YAML::Node& instrument, cgsn::protobuf::SerialConfig& serial_cfg);
void read_power_cfg(const YAML::Node& master_instrument, const YAML::Node& mooring_instrument,
                    cgsn::protobuf::PowerConfig& power_cfg);
void read_command_cfg(const YAML::Node& instrument,
                      cgsn::protobuf::SensorCommandConfig& command_cfg);

void read_interprocess_cfg(const YAML::Node& mooring_yaml,
                           goby::protobuf::InterProcessPortalConfig& interprocess_cfg);
void read_glog_cfg(const YAML::Node& mooring_yaml, goby::common::protobuf::GLogConfig& glog_cfg);

/// \brief Run this function for each instrument for a given CPU
void for_each_instrument_on_this_cpu(
    const cgsn::protobuf::ConfigToolConfig& cfg,
    std::function<void(const YAML::Node& mooring_yaml_instrument, std::string instrument_key,
                       int port_id, cgsn::protobuf::sensor::Type sensor_type)>
        func);

std::tuple<YAML::Node, YAML::Node, YAML::Node>
load_yamls(const cgsn::protobuf::ConfigToolConfig& cfg);

} // namespace yaml
} // namespace config
} // namespace cgsn

#endif
