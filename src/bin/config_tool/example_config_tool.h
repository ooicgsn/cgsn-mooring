// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef EXAMPLE_CONFIG_TOOL_20181115_H
#define EXAMPLE_CONFIG_TOOL_20181115_H

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/common/application_base3.h>
#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/supervisor_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"

#include "common/yaml_raii.h"


namespace cgsn
{
    namespace config
    {
        class ExampleConfigTool
        {
        public:
            ExampleConfigTool(const cgsn::protobuf::ConfigToolConfig& app_cfg) : _app_cfg(app_cfg) {}
            void run();

        private:
            const cgsn::protobuf::ConfigToolConfig& app_cfg() { return _app_cfg; }

            void write_instruments(const std::string& subassembly, YAML::Emitter& yaml_master_out, YAML::Emitter& yaml_mooring_out, YAML::Emitter& yaml_assets_out, YMap& master_parent_map, YMap& mooring_base_map, YMap& assets_base_map);
            
            
            void write_instrument_sensor_config(const google::protobuf::Descriptor* desc,
                                                YAML::Emitter& yaml_out);
            void write_yaml_from_pb(const google::protobuf::Descriptor* desc,
                                    YAML::Emitter& yaml_out,
                                    YMap& parent_map,
                                    std::map<std::string, cgsn::FieldOptions::YAMLConfig::Priority> static_field_priorities = decltype(static_field_priorities)());
           template<typename T>
            void write_yaml_from_pb_single_val(const google::protobuf::FieldDescriptor* field_desc,
                                               YAML::Emitter& yaml_out,
                                               T default_val,
                                               boost::format& fmt,
                                               YMap& parent_map);

            const google::protobuf::FieldDescriptor* find_submessage_desc(const std::string& field_name, const google::protobuf::Descriptor* desc);
            const google::protobuf::FieldDescriptor* find_sensor_desc(const google::protobuf::Descriptor* desc) { return find_submessage_desc("sensor", desc); }
            const google::protobuf::FieldDescriptor* find_serial_desc(const google::protobuf::Descriptor* desc) { return find_submessage_desc("serial", desc); }
            const google::protobuf::FieldDescriptor* find_power_desc(const google::protobuf::Descriptor* desc) { return find_submessage_desc("power", desc); }
            const google::protobuf::FieldDescriptor*
            find_command_desc(const google::protobuf::Descriptor* desc)
            {
                return find_submessage_desc("command", desc);
            }

            const google::protobuf::Descriptor* schedule_desc()
                { return cgsn::protobuf::PortSchedule::descriptor(); }

            std::string replace_special(const std::string& str);
        
        private:
            const cgsn::protobuf::ConfigToolConfig& _app_cfg;
        };
    }
}

#endif
