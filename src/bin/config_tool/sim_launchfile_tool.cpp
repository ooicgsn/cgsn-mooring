// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <yaml-cpp/yaml.h>

#include "dccl/dynamic_protobuf_manager.h"
#include <goby/common/application_base3.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"

#include "common/exception.h"
#include "common/find_sensor_type.h"
#include "common/yaml_reader.h"

#include "sim_launchfile_tool.h"

using goby::glog;

/// \brief Writes a script that starts the simulated cgsn-mooring system for local testing
void cgsn::config::SimulatorLaunchfileTool::run()
{
    if (!app_cfg().has_cpu())
        throw(Exception("Must define \"cpu\" when using action == " +
                        cgsn::protobuf::ConfigToolConfig::Action_Name(app_cfg().action())));

    YAML::Node master_yaml, mooring_yaml, assets_yaml;
    std::tie(master_yaml, mooring_yaml, assets_yaml) = yaml::load_yamls(app_cfg());

    auto add_socat = [](std::string sim_serial_port, std::string driver_serial_port) {
        std::cout << "[kill=SIGTERM] socat pty,link=" << sim_serial_port
                  << ",raw,echo=0 pty,link=" << driver_serial_port << ",raw,echo=0\n";
    };

    auto write_app = [&, this](cgsn::protobuf::app::Type app_type,
                               std::string extra_params = std::string(),
                               std::string extra_apps = std::string(),
                               std::string sim_serial_port = std::string(),
                               std::string driver_serial_port = std::string()) {
        const auto* app_type_desc = cgsn::protobuf::app::Type_descriptor();
        const auto* app_type_val_desc = app_type_desc->FindValueByNumber(app_type);
        const auto& ev_options = app_type_val_desc->options().GetExtension(cgsn::ev).type_data();
        const auto& binary = ev_options.binary();

        std::cout << "# " << ev_options.description() << "\n";
        if (!sim_serial_port.empty() && !driver_serial_port.empty())
            add_socat(sim_serial_port, driver_serial_port);

        if (!extra_apps.empty())
            std::cout << extra_apps << "\n";

        std::cout << binary << " <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type "
                  << cgsn::protobuf::app::Type_Name(app_type) << " --cpu " << app_cfg().cpu()
                  << ") " << extra_params << "\n\n";
    };

    write_app(cgsn::protobuf::app::GOBYD);
    write_app(cgsn::protobuf::app::GOBY_LOGGER);

    std::stringstream supervisor_params, mpic_sim;
    const std::string mpic_sim_serial_port = "/tmp/dcl-mpic-sim";
    const std::string mpic_supervisor_serial_port = "/tmp/dcl-mpic-supervisor";
    supervisor_params << "--mpic_serial 'port: \"" << mpic_supervisor_serial_port
                      << "\" baud: 38400'";
    mpic_sim << "[kill=SIGTERM] cgsn_simulator_mpic --supervisor_serial 'port: \""
             << mpic_sim_serial_port << "\" baud: 38400' --type DCL";
    write_app(cgsn::protobuf::app::SUPERVISOR, supervisor_params.str(), mpic_sim.str(),
              mpic_sim_serial_port, mpic_supervisor_serial_port);

    write_app(cgsn::protobuf::app::SCHEDULER);
    write_app(cgsn::protobuf::app::SYSTEM_HEALTH);

    // writes the instrument sensor driver to the std::cout
    // if it exists in sensor_types.proto also writes the instrument simulator and socat pseudoterminal
    // unless hwil_server is defined, in which case it writes socat instructions to connect to the serial port on the hardware server (e.g. barnaclese.whoi.edu)
    yaml::for_each_instrument_on_this_cpu(app_cfg(), [&](const YAML::Node& mooring_yaml_instrument,
                                                         std::string instrument_key, int port_id,
                                                         cgsn::protobuf::sensor::Type sensor_type) {
        const auto* sensor_type_desc = cgsn::protobuf::sensor::Type_descriptor();
        const auto* sensor_type_val_desc = sensor_type_desc->FindValueByNumber(sensor_type);
        const auto& ev_options = sensor_type_val_desc->options().GetExtension(cgsn::ev).type_data();
        const auto& binary = ev_options.binary();

        const std::string app_name = app_cfg().cpu() + "-" + binary + "-" + instrument_key;
        const std::string sim_serial_port = "/tmp/dcl-port" + std::to_string(port_id) + "-sensor";
        const std::string driver_serial_port =
            "/tmp/dcl-port" + std::to_string(port_id) + "-driver";

        cgsn::protobuf::SerialConfig serial_cfg;
        yaml::read_serial_cfg(mooring_yaml_instrument, serial_cfg);

        std::cout << "# " << instrument_key << "\n";

        if (app_cfg().has_hwil_server())
        {
            std::cout << "# run on " << app_cfg().hwil_server()
                      << " (first ensure no other processes are using the serial port): \n"
                      << "# socat file:/dev/dcl-port" << port_id << ",echo=0,b" << serial_cfg.baud()
                      << " tcp-l:" << (50000 + port_id) << ",reuseaddr,fork\n"
                      << "[kill=SIGTERM] socat tcp:" << app_cfg().hwil_server() << ":"
                      << (50000 + port_id) << " pty,link=" << driver_serial_port << ",raw,echo=0\n";
        }
        else
        {
            if (ev_options.has_simulator())
            {
                // socat
                add_socat(sim_serial_port, driver_serial_port);
                // simulator
                const auto& simulator = ev_options.simulator();
                std::cout << simulator << " --serial 'port: \"" << sim_serial_port
                          << "\" baud: " << serial_cfg.baud() << "'\n";
            }
        }

        // logger
        std::cout << "cgsn_legacy_logger"
                  << " <("
                  << "cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type "
                     "LEGACY_LOGGER --instrument_key "
                  << instrument_key << ")\n";

        // driver
        std::cout << binary << " <("
                  << "cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type "
                     "SENSOR_DRIVER --instrument_key "
                  << instrument_key << ") --app_name " << app_name << " --serial 'port: \""
                  << driver_serial_port << "\" baud: " << serial_cfg.baud() << "'\n\n";
    });
}
