// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <iostream>
#include <tuple>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <google/protobuf/text_format.h>
#include <yaml-cpp/yaml.h>

#include <dccl/dynamic_protobuf_manager.h>
#include <goby/common/application_base3.h>
#include <goby/middleware/protobuf/logger_config.pb.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/config/logger_config.pb.h"
#include "cgsn-mooring/config/scheduler_config.pb.h"
#include "cgsn-mooring/config/serial_config.pb.h"
#include "cgsn-mooring/config/system_health_config.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/messages/option_extensions.pb.h"
#include "cgsn-mooring/messages/sensor_types.pb.h"

#include "common/exception.h"
#include "common/find_protobuf_name.h"
#include "common/find_sensor_type.h"
#include "common/yaml_reader.h"

#include "config_to_pb_tool.h"

using goby::glog;

//
// CONVERT_YAML_TO_PROTOBUF
//

void cgsn::config::ConfigToProtobufTool::run()
{
    if (!app_cfg().has_app_type())
        throw(Exception("Must define \"app_type\" when using action == CONVERT_YAML_TO_PROTOBUF"));

    YAML::Node master_yaml, mooring_yaml, assets_yaml;
    std::tie(master_yaml, mooring_yaml, assets_yaml) = yaml::load_yamls(app_cfg());
    try
    {
        std::string protobuf_name;
        if (app_cfg().app_type() == cgsn::protobuf::app::SENSOR_DRIVER)
        {
            cgsn::protobuf::sensor::Type sensor_type =
                find_sensor_type(master_yaml, app_cfg().master_yaml(), assets_yaml,
                                 app_cfg().assets_yaml(), app_cfg().instrument_key());
            protobuf_name = find_protobuf_name(app_cfg().app_type(), sensor_type);
        }
        else
        {
            protobuf_name = find_protobuf_name(app_cfg().app_type());
        }

        auto pb_cfg = dccl::DynamicProtobufManager::new_protobuf_message<
            std::unique_ptr<google::protobuf::Message>>(protobuf_name);
        if (!pb_cfg)
            throw(Exception(std::string("no protobuf with name: " + protobuf_name + " found")));

        const auto& platform_node = master_yaml["platform"];
        if (!platform_node)
            throw(Exception(std::string("no \"platform:\" defined in master YAML")));

        std::string this_cpu;
        if (app_cfg().has_cpu())
        {
            this_cpu = app_cfg().cpu();
        }
        else if (app_cfg().has_instrument_key())
        {
            const YAML::Node& master_yaml_instrument = yaml::find_master_instrument(
                master_yaml, app_cfg().master_yaml(), app_cfg().instrument_key());
            const auto& cpu_node = yaml::find_required_node(
                master_yaml_instrument, "cpu", app_cfg().master_yaml(), app_cfg().instrument_key());
            this_cpu = cpu_node.as<std::string>();
        }
        else
        {
            throw(Exception("Must define --cpu or --instrument_key"));
        }

        cgsn::protobuf::Module cpu_module;
        if (!cgsn::protobuf::Module_Parse("MODULE__" + boost::to_upper_copy(this_cpu), &cpu_module))
            throw(Exception("Unknown CPU [" + this_cpu +
                            "]. Make sure it is defined in module_health.proto"));

        if (app_cfg().app_type() == cgsn::protobuf::app::SENSOR_DRIVER)
        {
            const YAML::Node& master_yaml_instrument = yaml::find_master_instrument(
                master_yaml, app_cfg().master_yaml(), app_cfg().instrument_key());
            const YAML::Node& mooring_yaml_instrument = yaml::find_instrument(
                mooring_yaml, app_cfg().mooring_yaml(), app_cfg().instrument_key());

            const auto* refl = pb_cfg->GetReflection();
            const auto* desc = pb_cfg->GetDescriptor();

            const auto* instrument_key_field_desc = desc->FindFieldByName("instrument_key");
            if (!instrument_key_field_desc)
                throw(Exception("No protobuf field named \"instrument_key\" in " +
                                desc->full_name()));
            refl->SetString(pb_cfg.get(), instrument_key_field_desc, app_cfg().instrument_key());

            const auto* sensor_field_desc = find_sensor_desc(pb_cfg->GetDescriptor());
            auto* sensor_cfg = refl->MutableMessage(pb_cfg.get(), sensor_field_desc);
            yaml::read_instrument_cfg(mooring_yaml_instrument, *sensor_cfg);

            auto* serial_cfg = mutable_submessage<cgsn::protobuf::SerialConfig>(*pb_cfg, "serial");
            yaml::read_serial_cfg(mooring_yaml_instrument, *serial_cfg);

            if (!app_cfg().skip_power_cfg())
            {
                auto* power_cfg = mutable_submessage<cgsn::protobuf::PowerConfig>(*pb_cfg, "power");
                yaml::read_power_cfg(master_yaml_instrument, mooring_yaml_instrument, *power_cfg);
            }

            auto* command_cfg =
                mutable_submessage<cgsn::protobuf::SensorCommandConfig>(*pb_cfg, "command");
            yaml::read_command_cfg(mooring_yaml_instrument, *command_cfg);
        }
        else if (app_cfg().app_type() == cgsn::protobuf::app::GOBY_LOGGER)
        {
            const auto& logger_node = mooring_yaml["logger"];
            if (!logger_node)
                throw(Exception("No \"logger:\" key found for mooring YAML"));

            for (auto setting_pair : logger_node)
                yaml::read_single_cfg(setting_pair.first, setting_pair.second, *pb_cfg);
        }
        else if (app_cfg().app_type() == cgsn::protobuf::app::SUPERVISOR)
        {
            const auto& supervisor_node = mooring_yaml["supervisor"];
            if (!supervisor_node)
                throw(Exception("No \"supervisor:\" key found for mooring YAML"));

            for (auto setting_pair : supervisor_node)
                yaml::read_single_cfg(setting_pair.first, setting_pair.second, *pb_cfg);
        }
        else if (app_cfg().app_type() == cgsn::protobuf::app::SCHEDULER)
        {
            auto& scheduler_cfg = dynamic_cast<cgsn::protobuf::SchedulerConfig&>(*pb_cfg);

            yaml::for_each_instrument_on_this_cpu(
                app_cfg(),
                [&](const YAML::Node& mooring_yaml_instrument, std::string instrument_key,
                    int port_id, cgsn::protobuf::sensor::Type sensor_type) {
                    const auto& schedule_node =
                        yaml::find_required_node(mooring_yaml_instrument, "schedule",
                                                 app_cfg().mooring_yaml(), instrument_key);
                    auto& sched = *scheduler_cfg.add_schedule();
                    for (auto setting_pair : schedule_node)
                        yaml::read_single_cfg(setting_pair.first, setting_pair.second, sched);

                    sched.set_port_id(port_id);
                    sched.set_sensor_type(sensor_type);
                });
        }
        else if (app_cfg().app_type() == cgsn::protobuf::app::SYSTEM_HEALTH)
        {
            auto& system_health_cfg = dynamic_cast<cgsn::protobuf::SystemHealthConfig&>(*pb_cfg);

            yaml::for_each_instrument_on_this_cpu(
                app_cfg(),
                [&](const YAML::Node& mooring_yaml_instrument, std::string instrument_key,
                    int port_id, cgsn::protobuf::sensor::Type sensor_type) {
                    system_health_cfg.mutable_sensor_driver()->add_configured_port(port_id);
                });

            system_health_cfg.set_this_cpu(cpu_module);
        }
        else if (app_cfg().app_type() == cgsn::protobuf::app::LEGACY_LOGGER)
        {
            // Find this instrument in master.yml
            const YAML::Node& master_yaml_instrument = yaml::find_master_instrument(
                master_yaml, app_cfg().master_yaml(), app_cfg().instrument_key());

            // Create a config protobuf message
            auto& logger_cfg = dynamic_cast<cgsn::protobuf::LegacyLogServiceConfig&>(*pb_cfg);
            logger_cfg.set_instrument_key(app_cfg().instrument_key());

            // If a legacy name is provided, copy it to the protobuf message
            const auto& legacy_name_node = master_yaml_instrument["legacy_name"];
            if (legacy_name_node)
                logger_cfg.set_legacy_name(legacy_name_node.as<std::string>());

            // Get the legacy log settings for this sensor type
            cgsn::protobuf::sensor::Type sensor_type =
                find_sensor_type(master_yaml, app_cfg().master_yaml(), assets_yaml,
                                 app_cfg().assets_yaml(), app_cfg().instrument_key());

            const auto* sensor_type_desc = cgsn::protobuf::sensor::Type_descriptor();
            const auto* sensor_type_val_desc = sensor_type_desc->FindValueByNumber(sensor_type);
            const auto& ev_options =
                sensor_type_val_desc->options().GetExtension(cgsn::ev).type_data();

            if (!ev_options.has_legacy_log())
                throw(Exception("No legacy log settings can be inferred for instrument " +
                                app_cfg().instrument_key()));
            logger_cfg.mutable_settings()->CopyFrom(ev_options.legacy_log());
        }

        auto* interprocess_cfg =
            mutable_submessage<goby::protobuf::InterProcessPortalConfig>(*pb_cfg, "interprocess");
        interprocess_cfg->set_platform(platform_node.as<std::string>());
        yaml::read_interprocess_cfg(mooring_yaml, *interprocess_cfg);

        auto* app3_cfg = mutable_submessage<goby::protobuf::App3Config>(*pb_cfg, "app");
        auto* glog_cfg =
            mutable_submessage<goby::common::protobuf::GLogConfig>(*app3_cfg, "glog_config");
        yaml::read_glog_cfg(mooring_yaml, *glog_cfg);

        if (!pb_cfg->IsInitialized())
        {
            std::vector<std::string> errors;
            pb_cfg->FindInitializationErrors(&errors);
            std::stringstream ss;
            for (const auto& e : errors) ss << e << ",";
            throw(Exception(protobuf_name + " fields are not initialized: " + ss.str()));
        }

        write_pb_cfg(*pb_cfg);
    }
    catch (const std::exception& e)
    {
        throw(Exception("Failed to parse master (" + app_cfg().master_yaml() + "), mooring (" +
                        app_cfg().mooring_yaml() + "), or assets (" + app_cfg().assets_yaml() +
                        ") YAML file: " + e.what()));
    }
}

void cgsn::config::ConfigToProtobufTool::write_pb_cfg(const google::protobuf::Message& pb_cfg)
{
    google::protobuf::TextFormat::Printer printer;
    printer.SetUseShortRepeatedPrimitives(true);
    std::string out;
    printer.PrintToString(pb_cfg, &out);
    std::cout << out << std::flush;
}

const google::protobuf::FieldDescriptor*
cgsn::config::ConfigToProtobufTool::find_submessage_desc(const std::string& field_name,
                                                         const google::protobuf::Descriptor* desc)
{
    const auto* field_desc = desc->FindFieldByName(field_name);
    if (!field_desc || field_desc->cpp_type() != google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
    {
        throw(Exception(
            std::string("No embedded message field called '" + field_name + "' found in ") +
            desc->full_name()));
    }
    return field_desc;
}

template <typename Config>
Config* cgsn::config::ConfigToProtobufTool::mutable_submessage(google::protobuf::Message& pb_cfg,
                                                               const std::string& field_name)

{
    const auto* field_desc = find_submessage_desc(field_name, pb_cfg.GetDescriptor());
    const auto* refl = pb_cfg.GetReflection();
    auto* cfg = dynamic_cast<Config*>(refl->MutableMessage(&pb_cfg, field_desc));
    if (!cfg)
        throw(Exception("Field \"" + field_name + "\" is not a " +
                        Config::descriptor()->full_name()));
    return cfg;
}

std::string cgsn::config::ConfigToProtobufTool::replace_special(const std::string& str)
{
    auto s = str;
    boost::replace_all(s, "\n", "\\n");
    boost::replace_all(s, "\r", "\\r");
    return s;
}
