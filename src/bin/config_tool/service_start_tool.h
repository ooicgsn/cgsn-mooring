// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef SERVICE_START_TOOL_20181115_H
#define SERVICE_START_TOOL_20181115_H

#include <goby/common/application_base3.h>

#include "cgsn-mooring/config/config_tool_config.pb.h"


namespace cgsn
{
    namespace config
    {
        class ServiceStartCommandTool
        {
        public:
            ServiceStartCommandTool(const cgsn::protobuf::ConfigToolConfig& app_cfg) : _app_cfg(app_cfg) {}
            void run();

        private:
            const cgsn::protobuf::ConfigToolConfig& app_cfg() const { return _app_cfg; }

        private:
            const cgsn::protobuf::ConfigToolConfig& _app_cfg;
        };
    }
}

#endif
