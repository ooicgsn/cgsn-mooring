## Configuration

The configuration for the CGSN system is based on YAML. This document discusses only the configuration values used by the `cgsn-mooring` software running on the mooring. Other values may exist for other uses (OMS++, asset tracking, etc.).

The YAML configuration is read by the `cgsn_config_tool` to generate the required Protobuf configuration used by all the Goby3 applications in cgsn-mooring.

To avoid confusion, keys in the YAML files will be written in `monotype` and keys in the Protobuf configuration will be written in **bold**.

### cgsn_config_tool

The `cgsn_config_tool` is the parser for the YAML files for the `cgsn_mooring` software. It is essentially a translation layer between the Protobuf configuration used by the `cgsn_mooring` applications, and the YAML configuration that is used by the entire system. As much as is feasible, the structure and keys of the configuration are symmetric between the YAML and Protobuf configuration.

`cgsn_config_tool` is also a Goby3 application itself, which means it takes Protobuf configuration to define what actions to take on the YAML files.

#### action = CONVERT_YAML_TO_PROTOBUF

This is the primary purpose of the `cgsn_config_tool`, which is to read the `master.yml` ("Master YAML"), `mooring.yml` ("Mooring YAML"), and `assets.yml` ("Assets YAML") to create the runtime configuration for the application defined by the **app_type** passed to `cgsn_config_tool`, which can be one of:

 - GOBYD: for `gobyd`
 - SENSOR_DRIVER: for any of the `cgsn_mooring` drivers. The specific driver is defined by the `type:` in the Master YAML plus the `manufacturer:` and `model:` defined in the Assets YAML for the **instrument_key** passed to `cgsn_config_tool`. The drivers and the sensors they support are defined in `src/lib/messages/sensor_types.proto`.
 - GOBY_LOGGER: for the `goby_logger`.
 - SCHEDULER: for the `cgsn_scheduler`. Gathers schedules for all the instruments defined in the `master.yml` for a given **subassembly** passed to `cgsn_config_tool`.
 - SUPERVISOR: for the `cgsn_supervisor`.
 - SYSTEM_HEALTH: for the `cgsn_system_health` monitoring tool.

For example, to run the `cgsn_scheduler` using the parsed output of the YAML logs from the `cgsn_config_tool`, you would run:

```
cgsn_scheduler <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SCHEDULER --cpu dcl36)
```

Similarly, to run a sensor driver, you can run

```
cgsn_ctd_sbe_driver <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-ctdbp1)
```

#### action = WRITE_EXAMPLE_YAML_FILES

This action writes a set of example files to the working directory, by default it writes `mooring-example.yml` and `{type}-{manufacturer}-{model}-defaults-example.yml` for each sensor type (e.g. `adcp`, `ctdbp`) for which a driver exists, where `{manufacturer}` and `{model}` are converted to lower case and have spaces replaced with underscores. These example files can provide a starting point for creating the actual deployment YAML or for seeing the valid key/values accepted by the `cgsn-mooring` code. 

```
cgsn_config_tool --action WRITE_EXAMPLE_YAML_FILES
```

currently writes

```
master-example.yml
mooring-example.yml
assets-example.yml

gps-garmin-18x-defaults-example.yml
ctdbp-seabird-sbe-16_plus-defaults-example.yml
adcp-teledyne_rdi-rdi_workhorse-defaults-example.yml
velpt-nortek-aquadopp-defaults-example.yml
dosta-aanderra-oxygen_optode_3975-defaults-example.yml
presf-seabird-sbe-26-defaults-example.yml
```

By default, only the most necessary configuration values are written to the example files and numerous values that can often by defaulted are omitted. You can choose to show more configuration values by setting (the default is `--priority HIGH`):

```
cgsn_config_tool --action WRITE_EXAMPLE_YAML_FILES --priority MEDIUM
```

or even more by showing

```
cgsn_config_tool --action WRITE_EXAMPLE_YAML_FILES --priority LOW
```

In addition, you can write a compact version of the YAML files (without comments, and using the JSON flow syntax for most maps):

```
cgsn_config_tool --action WRITE_EXAMPLE_YAML_FILES --compact true
```


#### action == WRITE_SERVICE_START_COMMAND

This action writes the command to standard output that launches a service for a given CPU, based on the `port` defined in the Master YAML. This action is used by the `systemd` services to ensure the configuration is correct for a given port.

For instance, sensor drivers can be launched by passing `--app_type SENSOR_DRIVER`. This will use the `manufacturer` plus `model` defined in the Assets YAML to determine the correct driver to spawn.

For more details, see [Systemd Init](doc_03-systemd.md).

For example, to spawn the driver for port 2, the command

```
cgsn_config_tool --action WRITE_SERVICE_START_COMMAND --app_type SENSOR_DRIVER --cpu dcl36 --port_id 2
```

writes:

```
## port2_driver_start_command ##
/usr/bin/cgsn_ctd_sbe_driver <(/usr/bin/cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-ctdbp1) --app_name cgsn_ctd_sbe_driver-mfn-ctdbp1
```

Or to spawn the legacy logger for the same port, the command

```
cgsn_config_tool --action WRITE_SERVICE_START_COMMAND --app_type LEGACY_LOGGER --cpu dcl36 --port_id 2
```

writes:

```
## port2_logger_start_command ##
/usr/bin/cgsn_legacy_logger <(/usr/bin/cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER --instrument_key mfn-ctdbp1) --app_name cgsn_legacy_logger-mfn-ctdbp1
```

No newline is output at the end of the command, so if you are executing this in an interactive shell, appending `; echo` may be helpful.

You can also directly execute the command rather than display it with `--exec`.

```
cgsn_config_tool --action WRITE_SERVICE_START_COMMAND --app_type SENSOR_DRIVER --cpu dcl36 --port_id 2 --exec
```


#### action == WRITE_SIMULATION_LAUNCHFILE

This action creates a launch file to standard output that can be used by `goby_launch` to locally simulate one or more CPUs on the mooring.

For example, given the examples below,

```
cgsn_config_tool --action WRITE_SIMULATION_LAUNCHFILE --cpu dcl36 > cgsn-sim.launch
```

writes `cgsn-sim.launch`:

```
## cgsn-sim.launch ##
# Goby Daemon
gobyd <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type GOBYD --cpu dcl36) 

# Goby Binary Logger
goby_logger <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type GOBY_LOGGER --cpu dcl36) 

# Supervisor (MPIC interface)
[kill=SIGTERM] socat pty,link=/tmp/dcl-mpic-sim,raw,echo=0 pty,link=/tmp/dcl-mpic-supervisor,raw,echo=0
[kill=SIGTERM] cgsn_simulator_mpic --supervisor_serial 'port: "/tmp/dcl-mpic-sim" baud: 38400' --type DCL
cgsn_supervisor <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SUPERVISOR --cpu dcl36) --mpic_serial 'port: "/tmp/dcl-mpic-supervisor" baud: 38400'

# Scheduler
cgsn_scheduler <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SCHEDULER --cpu dcl36) 

# System Health Aggregator
cgsn_system_health <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SYSTEM_HEALTH --cpu dcl36) 

# mfn-ctdbp1
[kill=SIGTERM] socat pty,link=/tmp/dcl-port2-sensor,raw,echo=0 pty,link=/tmp/dcl-port2-driver,raw,echo=0
ctd_sbe_simulator --serial 'port: "/tmp/dcl-port2-sensor" baud: 9600'
cgsn_legacy_logger <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER --instrument_key mfn-ctdbp1)
cgsn_ctd_sbe_driver <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-ctdbp1) --app_name dcl36-cgsn_ctd_sbe_driver-mfn-ctdbp1 --serial 'port: "/tmp/dcl-port2-driver" baud: 9600'

# mfn-adcp1
cgsn_legacy_logger <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER --instrument_key mfn-adcp1)
cgsn_adcp_rdi_driver <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-adcp1) --app_name dcl36-cgsn_adcp_rdi_driver-mfn-adcp1 --serial 'port: "/tmp/dcl-port3-driver" baud: 9600'

# mfn-presf1
cgsn_legacy_logger <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER --instrument_key mfn-presf1)
cgsn_presf_sbe_driver <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-presf1) --app_name dcl36-cgsn_presf_sbe_driver-mfn-presf1 --serial 'port: "/tmp/dcl-port4-driver" baud: 9600'

```

As the simulators are implemented, they will be included in here via the `sensor_types.proto` "simulator" tag, along with a corresponding `socat` instantiation to create a virtual serial port between the simulator and driver.

To launch (in GNU screen, `-s`), run

```
goby_launch -s <(cgsn_config_tool --action WRITE_SIMULATION_LAUNCHFILE --cpu dcl36)
```

For hardware-in-the-loop testing using the instruments connected to the testbed (rather than the pure software simulators), the `--hwil-server=barnaclese.whoi.edu` flag provides the appropriate launch file:

```
cgsn_config_tool --action WRITE_SIMULATION_LAUNCHFILE --cpu dcl36 --hwil_server=barnaclese.whoi.edu
```

The `cgsn-sim.launch` file is simply a list of commands to be run by `goby_launch`, which provides several ways to launching the applications: GNU screen, gnome-terminal, xterm, and nohup. See `goby_launch -h` for more information.


#### action == WRITE_MOORING_NAME

This outputs the current mooring name, as defined by the part of the hostname (as given in `/etc/hostname`) preceeding the first hyphen (`-`):

```
cgsn_config_tool --action=WRITE_MOORING_NAME
```

This will output "`cp01cnsm`" for a machine with hostname "`cp01cnsm-dcl37`". If the name cannot be parsed, `{unknown}` is given.

#### action == WRITE_CPU_NAME

This outputs the current CPU name, as defined by the part of the hostname (as given in `/etc/hostname`) following the first hyphen (`-`):

```
cgsn_config_tool --action=WRITE_CPU_NAME
```

This will output "`dcl37`" for a machine with hostname "`cp01cnsm-dcl37`". If the name cannot be parsed, `{unknown}` is given.



### master.yml

The Master YAML is used by all the CGSN subsystems and outlines the high-level configuration of the mooring and instruments for a given deployment. For example:

```
## master.yml ##
platform: barnaclese
subassemblies:
  - name: mfn
    instruments:
      - instrument_key: mfn-ctdbp1
        port: 2
        type: ctdbp
        cpu: dcl36
        legacy_name: ctdbp1
      - instrument_key: mfn-adcp1
        port: 3
        type: adcp
        cpu: dcl36
      - instrument_key: mfn-presf1
        port: 4
        type: presf
        cpu: dcl36
```

### assets.yml (sensor manufacturer, model, serial number)

The Assets YAML defines the specific sensor that is currently used for a given instrument key. This is used to run the correct sensor driver to support this particular manufacturer and model instrument.

```
## assets.yml ##
instruments:
  - instrument_key: mfn-ctdbp1
    manufacturer: Seabird
    model: SBE-16 Plus
  - instrument_key: mfn-adcp1
    manufacturer: Teledyne RDI
    model: RDI Workhorse
  - instrument_key: mfn-presf1
    manufacturer: Seabird
    model: SBE-26
```

### mooring.yml (serial port mapping, schedule, power info, etc.)

The Mooring YAML defines the details for the `cgsn-mooring` system, as required to actually run the mooring. For example:

```
## mooring.yml ##
interprocess:
  transport: TCP  # enum: IPC=2, TCP=3
logger:
  log_dir: "/media/data/logs"
  type_regex: ".*"
  group_regex: ".*"
glog_config:
  tty_verbosity: QUIET  # enum: QUIET=1, WARN=2, VERBOSE=3, DEBUG1=5, DEBUG2=6, DEBUG3=7
  show_gui: false
  file_log:
    - file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2  # enum: QUIET=1, WARN=2, VERBOSE=3, DEBUG1=5, DEBUG2=6, DEBUG3=7
supervisor:
  mpic_serial:
    port: "/dev/dcl-mpic"
    baud: 38400
    flow_control: NONE
instruments:
  - instrument_key: mfn-ctdbp1
    setting_defaults: ctdbp-seabird-sbe-16_plus-defaults.yml
    setting_overrides:
      sample_interval: 20
    schedule:
      schedule_type: INTERVAL
      interval: 15
      offset: 1
      duration: 5
    command:
      op_mode: NORMAL
    serial:
      port: "/dev/dcl-port2"
      baud: 9600
      flow_control: NONE  # enum: NONE=0, SOFTWARE=1, HARDWARE=2
    power:
      voltage: VOLTAGE_12V  # enum: VOLTAGE_UNKNOWN=-1, VOLTAGE_12V=1, VOLTAGE_24V=2
      current_limit: 2500  # units { derived_dimensions: "current" system: "si" prefix: "milli" }
      protocol: RS232  # enum: PROTOCOL_UNKNOWN=-1, RS232=0, RS232_WITH_FLOW_CTRL=1, RS485=2, RS422=3, LOGIC_LEVEL=4
      soft_start: false
      power: LOW  # enum: POWER_SELECTION_UNKNOWN=-1, LOW=1, HIGH=2
  - instrument_key: mfn-adcp1
    setting_defaults: adcp-teledyne_rdi-rdi_workhorse-defaults.yml
    schedule:
      schedule_type: HOURLY
      hour: [ 0-23 ]
      offset: 0
      duration: 10
    serial:
      port: "/dev/dcl-port3"
      baud: 9600
      flow_control: NONE  # enum: NONE=0, SOFTWARE=1, HARDWARE=2
    power:
      voltage: VOLTAGE_24V  # enum: VOLTAGE_UNKNOWN=-1, VOLTAGE_12V=1, VOLTAGE_24V=2
      current_limit: 2500  # units { derived_dimensions: "current" system: "si" prefix: "milli" }
      protocol: RS232  # enum: PROTOCOL_UNKNOWN=-1, RS232=0, RS232_WITH_FLOW_CTRL=1, RS485=2, RS422=3, LOGIC_LEVEL=4
      soft_start: false
      power: HIGH  # enum: POWER_SELECTION_UNKNOWN=-1, LOW=1, HIGH=2
  - instrument_key: mfn-presf1
    setting_defaults: presf-seabird-sbe-26-defaults.yml
    schedule:
      schedule_type: HOURLY
      hour: [ 0-10, 11-23 ]
      offset: 0
      duration: 10
    serial:
      port: "/dev/dcl-port4"
      baud: 9600
      flow_control: NONE  # enum: NONE=0, SOFTWARE=1, HARDWARE=2
    power:
      voltage: VOLTAGE_12V  # enum: VOLTAGE_UNKNOWN=-1, VOLTAGE_12V=1, VOLTAGE_24V=2
      current_limit: 1500  # units { derived_dimensions: "current" system: "si" prefix: "milli" }
      protocol: RS232  # enum: PROTOCOL_UNKNOWN=-1, RS232=0, RS232_WITH_FLOW_CTRL=1, RS485=2, RS422=3, LOGIC_LEVEL=4
      soft_start: false
      power: LOW  # enum: POWER_SELECTION_UNKNOWN=-1, LOW=1, HIGH=2

```

### ctdbp-seabird-sbe-16_plus-defaults.yml

Each of the sensors can have a defaults file, from which the configuration is read. Any changes can be put in the mooring.yml under `setting_overrides`, which will overwrite the setting in the defaults file. For quick reference, the sensor configuration that is automatically sent (fixed parameters) is also listed as a comment. These values are typically required for correct functioning of the sensor driver, which is why they are fixed and not runtime configurable.

These defaults are autogenerated from the applicable **sensor** configuration field in the sensor driver's Protobuf configuration. In this case, cgsn::protobuf::CTD_SBE_SensorConfig from `src/lib/config/ctd_sbe_config.proto` is used to populate this YAML file:

```
## ctdbp-seabird-sbe-16_plus-defaults.yml ##
# Fixed parameters (sent BEFORE configurable values):
# 	"OUTPUTEXECUTEDTAG=N\r\n" -- Do not wrote </Executed> tag after each command
# 	"ECHO=Y\r\n" -- Echo serial commands back
# 
# Fixed parameters (sent AFTER configurable values):
# 	(none)
# 
# Runtime configurable parameters/values:
output_salinity: true  # Sets output of salinity data
                       # (e.g. "OUTPUTSAL=Y\r\n")
output_sound_velocity: true  # Sets output of sound velocity data
                             # (e.g. "OUTPUTSV=Y\r\n")
output_sigmat_v_i: true  # Sets output of sigma-t, voltage, and current data
                         # (e.g. "OUTPUTUCSD=Y\r\n")
measurements_per_sample: 4  # Sets the number of samples per measurement
                            # Range: [1, 100]
                            # (e.g. "NCYCLES=4\r\n")
pump_mode: RUN_PUMP_BEFORE_SAMPLE  # Sets pump operational mode.
                                   # (e.g. "PUMPMODE=1\r\n")
                                   # enum: NO_PUMP=0, RUN_PUMP_BEFORE_SAMPLE=1, RUN_PUMP_DURING_SAMPLE=2
sample_interval: 10  # Sets the time between samples in seconds
                     # Range: [10, 14400]
                     # (e.g. "SAMPLEINTERVAL=10\r\n")
                     # units { base_dimensions: "T" system: "si" }
output_format: OUTPUT_CONVERTED_DATA_DECIMAL  # Sets the format of the outputted data
                                              # (e.g. "OUTPUTFORMAT=3\r\n")
                                              # enum: OUTPUT_RAW_HEXADECIMAL=0, OUTPUT_CONVERTED_DATA_HEXADECIMAL=1, OUTPUT_RAW_DECIMAL=2, OUTPUT_CONVERTED_DATA_DECIMAL=3, INVALID_OUTPUT_FORMAT=4, OUTPUT_CONVERTED_DATA_DECIMAL_XML=5
transmit_real_time: true  # If true, data are transmitted in real time, otherwise they are only stored on the flash memory.
                          # (e.g. "TXREALTIME=Y\r\n")
optode: false  # Set true to enable interface with an Aanderaa Optode
               # (e.g. "OPTODE=N\r\n")
```

### YAML <-> Protobuf mapping

The valid Protobuf configuration on any Goby application can be read by running 

```
application_name -e
```

for example, running

```
cgsn_supervisor -e
```

gives (somewhat reduced here for space)

```
app {  #  (optional)
  name: "myapp_g"  # default is compiled name - change this to 
                   # run multiple instances (optional)
  glog_config {  # configure the Goby Logger (TTY terminal and 
                 # file debugging logger) (optional)
    tty_verbosity: QUIET  # Terminal verbosity (QUIET, WARN, 
                          # VERBOSE, DEBUG1, DEBUG2, DEBUG3) 
                          # (optional) (default=QUIET)
    show_gui: false  # Set to true to display windowed NCurses 
                     # GUI for terminal output. (optional) 
                     # (default=false)
    file_log {  # Open one or more files for (debug) logging, the 
                # symbol '%1%' will be replaced by the current UTC 
                # date and time. (repeated)
      file_name: ""  #  (required)
      verbosity: VERBOSE  # Verbosity for this file log (QUIET, 
                          # WARN, VERBOSE, DEBUG1, DEBUG2, DEBUG3) 
                          # (optional) (default=VERBOSE)
    }
  }
}
interprocess {  #  (optional)
  platform: "default_goby_platform"  #  (optional) 
                                     # (default="default_goby_platfo
                                     # rm")
  transport: IPC  #  (IPC, TCP) (optional) (default=IPC)
  socket_name: ""  #  (optional)
  ipv4_address: "127.0.0.1"  #  (optional) (default="127.0.0.1")
  tcp_port: 11144  #  (optional) (default=11144)
}
mpic_serial {  #  (required)
  port: ""  #  (required)
  baud:   #  (required)
  end_of_line: "\n"  #  (optional) (default="\n")
  flow_control: NONE  #  (NONE, SOFTWARE, HARDWARE) (optional)
                      # (default=NONE)
  out_mail_max_interval_ms: 100  #  (optional) (default=100)
}
allowed_current_limit_offset: 10  #  (optional) (default=10)
```



All applications share the same `interprocess` settings defined in the `mooring.yml`. The keys and values are the same as the Protobuf configuration's **interprocess** section. Similarly, all applications share the `glog_config` settings using the same keys and values as the Protobuf **app.glog_config** section.

The rest of the configuration varies slightly based on the **app_type** setting given to `cgsn_config_tool`:

 - GOBYD: no further configuration is used from the the YAMLs.
 - SENSOR_DRIVER: settings are merged from the appropriate `instrument_key` map in the mooring.yml, given by the **instrument_key** passed to `cgsn_config_tool`. The type of the sensor is read from the `master.yml`. The Protobuf **sensor** sub-message is populated from the `instrument_defaults` file and the `setting_overrides` (if present). The **serial** and **power** Protobuf sub-messages are read directly from the `serial` and `power` keys of the appropriate `instrument_key`, except for the **power.port_id**, which is read from the `master.yml`'s `port` field. 
 - GOBY_LOGGER: configuration is read in a symmetric manner as the Protobuf configuration from the `logger` key of the `mooring.yml`.
 - SCHEDULER: A **schedule** submessage is created for each `instrument_key` in the `mooring.yml` for the given **cpu**, depending on whether `schedule` or `interval_schedule` is set. Like for the sensor drivers, the **port_id** is read from the master.yml `port` field. The **sensor_type** is inferred from the `manufacturer` and `model` in the Assets YAML.
 - SUPERVISOR: configuration is read in a symmetric manner as the Protobuf configuration from the `supervisor` key of the `mooring.yml`.
 - SYSTEM_HEALTH: reads all the currently configured port IDs (as defined in master.yml) into the **sensor_driver.configured_port** field.
 - LEGACY_LOGGER: a `legacy_name` can be provided in `master.yml` to set the output file name

### Example outputs

For the above example YAML files, the following gives the example outputs in Protobuf TextFormat.

#### Gobyd

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type GOBYD  --cpu dcl36
```

gives

```
## gobyd.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}

```

#### Supervisor

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SUPERVISOR  --cpu dcl36
```

gives

```
## supervisor.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}
mpic_serial {
  port: "/dev/dcl-mpic"
  baud: 38400
  flow_control: NONE
}
```

#### Scheduler

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SCHEDULER --cpu dcl36
```

gives

```
## scheduler.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}
schedule {
  port_id: 2
  sensor_type: CTD_SBE
  schedule_type: INTERVAL
  duration: 5
  offset: 1
  interval: 15
}
schedule {
  port_id: 3
  sensor_type: ADCP_RDI
  schedule_type: HOURLY
  duration: 10
  offset: 0
  hour: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
}
schedule {
  port_id: 4
  sensor_type: PRESF_SBE
  schedule_type: HOURLY
  duration: 10
  offset: 0
  hour: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
}
```

#### CTD Driver

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER  --instrument_key mfn-ctdbp1
```

gives

```
## ctd.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}
instrument_key: "mfn-ctdbp1"
serial {
  port: "/dev/dcl-port2"
  baud: 9600
  flow_control: NONE
}
power {
  port_id: 2
  voltage: VOLTAGE_12V
  current_limit: 2500
  protocol: RS232
  soft_start: false
  power: LOW
}
sensor {
  output_salinity: true
  output_sound_velocity: true
  output_sigmat_v_i: true
  measurements_per_sample: 4
  pump_mode: RUN_PUMP_BEFORE_SAMPLE
  sample_interval: 20
  output_format: OUTPUT_CONVERTED_DATA_DECIMAL
  transmit_real_time: true
  optode: false
}
command {
 op_mode: NORMAL
}
```

Note that the value of `sample_interval` is overwritten from the default value of 10 by the `setting_overrides` field.

#### System Health

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SYSTEM_HEALTH --cpu dcl36
```

gives

```
## system_health.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}
this_cpu: MODULE__DCL36
sensor_driver {
  configured_port: [2, 3, 4]
}
```

#### Legacy Logger

```
cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER  --instrument_key mfn-ctdbp1
```

gives

```
## legacy_logger.pb.cfg ##
app {
  glog_config {
    tty_verbosity: QUIET
    show_gui: false
    file_log {
      file_name: "/media/data/logs/debug/%2%_%1%.txt"
      verbosity: DEBUG2
    }
  }
}
interprocess {
  platform: "barnaclese"
  transport: TCP
}
instrument_key: "mfn-ctdbp1"
settings {
  format: LOG_FORMAT_RAW_TIMESTAMPED
  timestamp: YYYYMMDD
}
legacy_name: "ctdbp1"
```

