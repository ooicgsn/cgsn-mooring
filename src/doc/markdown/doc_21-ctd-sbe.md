## SBE 16plus CTD Driver  

This driver is for the SBE 16plus V2 SeaCAT.  

* Datasheet: [SBE_16plusV2_Tech_Specs.pdf](https://drive.google.com/file/d/1zBKmGS-H5xDsnhq_BM1ZF4MYdhgBvXwL/view?usp=sharing).  
* Firmware version(s) tested: Output of `GetHD` command:

```
<HardwareData DeviceType = 'SBE16plus' SerialNumber = '01650111'>            
   <Manufacturer>Sea-Bird Electronics, Inc.</Manufacturer>                   
   <FirmwareVersion>2.5.2</FirmwareVersion>                                  
   <FirmwareDate>12 Mar 2013 11:50</FirmwareDate>           
   <CommandSetVersion>2.3</CommandSetVersion>
...
</HardwareData>
```

* Original author(s): Charlie Carnevale <ccarnevale@seacorp.com>, Toby Schneider <toby@gobysoft.org>

### Configuration  
Configuration Message: cgsn::protobuf::CTD_SBE_SensorConfig (defined in cgsn-mooring/config/ctd_sbe_config.proto).  

The CTD-SBE has two configuration phases.  The first phase is called the `PromptPhase` which sets the following settings one time when the data logger is started:  

`OutputExecutedTag=N`  

`Echo=Y`  


The second phase is called the `SamplingPhase` which sets the following settings one time when the data logger is started:  


| CTD_SBE_SensorConfig field | value | CTD-SBE Command    |
|----------------------------|-------|--------------------|
|output_salinity             | false | OUTPUTSAL=N        |
|output_salinity             | true  | OUTPUTSAL=Y        |
|output_sound_velocity       | false | OUTPUTSV=N         |
|output_sound_velocity       | true  | OUTPUTSV=Y         | 
|output_sigmat_v_i           | false | OUTPUTUCSD=N       |
|output_sigmat_v_i           | true  | OUTPUTUCSD=Y       | 
|measurements_per_sample     | false | NCYCLES=N          |
|measurements_per_sample     | true  | NCYCLES=Y          | 
|pump_mode                   | NO_PUMP                | OUTPUT=0   |
|pump_mode                   | RUN_PUMP_BEFORE_SAMPLE | PUMPMODE=1 | 
|pump_mode                   | RUN_PUMP_DURING_SAMPLE | PUMPMODE=2 |
|output_format               | OUTPUT_RAW_HEXADECIMAL            | OUTPUTFORMAT=0 | 
|output_format               | OUTPUT_CONVERTED_DATA_HEXADECIMAL | OUTPUTFORMAT=1 | 
|output_format               | OUTPUT_RAW_DECIMAL                | OUTPUTFORMAT=2 |
|output_format               | OUTPUT_CONVERTED_DATA_DECIMAL     | OUTPUTFORMAT=3 | 
|output_format               | INVALID_OUTPUT_FORMAT             | OUTPUTFORMAT=4 | 
|output_format               | OUTPUT_CONVERTED_DATA_DECIMAL_XML | OUTPUTFORMAT=5 | 
|transmit_real_time          | false | TXREALTIME=N       | 
|transmit_real_time          | true  | TXREALTIME=Y       | 
|optode                      | false | OPTODE=N           | 
|optode                      | true  | OPTODE=Y           | 


### Operation modes  

The CTD-SBE is operated in two modes: one where the sensor is preconfigured to record, even when the CE is off (`SENSOR_INTERNAL_SCHEDULE`). In the other (`NORMAL`) mode, the sensor driver controls start/stop, etc. of the data coming from the CTD sensor.  

#### NORMAL  

States:

   - `WaitForPrompt`: Verify configuration via `GetCD` command and ensure `OutputExecutedTag=N`. If `OutputExecutedTag=Y` then set it correctly. 
		- We may need to add some events for handling the `S>` or `<Executed/>` which will get handled by a custom reaction in `WaitForPrompt`.
   - `Configure`: NCycles and SampleInterval are runtime configurable. The remaining config is hardcoded (e.g. OutputSal, OutputSV, etc.) Refer to Configuration_Example XLSX document for more details. Send it each time the data logger starts.
   - `CommandStartLogging`: Send `STARTNOW` command. If already logging, go to Logging state.
   - `CommandStopLogging`: Send `STOP` plus `QS` commands  

#### SENSOR_INTERNAL_SCHEDULE  

States:

   - `WaitForPrompt`: Do nothing
   - `Configure`: Do nothing
   - `CommandStartLogging`: Do nothing
   - `CommandStopLogging`: Do nothing

### Sensor Command Statechart
The CTD-SBE requires interrogating for a prompt prior to accepting commands.  

**Initialize child states**  

![](../figures/ctd_sbe_sensor_command_initialize.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/ctd_sbe_sensor_command_initialize.png}
\endlatexonly


**On child states**  

![](../figures/ctd_sbe_sensor_command_on.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/ctd_sbe_sensor_command_on.png}
\endlatexonly



**Off child states**  


![](../figures/ctd_sbe_sensor_command_off.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/ctd_sbe_sensor_command_off.png}
\endlatexonly


### Quirks  
* Sometimes must send `\r\n` multiple times to get a prompt
* Cannot send commands while logging
* If status=logging, need to send many `\r\n` to get prompt before sending STOP
* Must send commands one at a time; no echo or response from instrument indicating valid command sent, only if invalid command sent
