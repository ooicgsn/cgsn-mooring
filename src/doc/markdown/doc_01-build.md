## Building cgsn-mooring

This document covers building the software manually, but not packaging it. Normally, the build is performed in pre-configured Docker containers. See [CircleCI](doc_10-circleci.md) and [Packaging](doc_11-packaging.md) for details.

The supported build system is Ubuntu 18.04 LTS "Bionic".

Add the CGSN apt repository:

```
sudo apt install software-properties-common
sudo add-apt-repository ppa:tes/cgsn
```

Install dependencies:

```
sudo apt update && sudo apt install clang git ssh cmake dpkg-dev doxygen graphviz \
    build-essential libprotobuf-dev protobuf-compiler libdccl3-dev dccl3-compiler \
    libgoby3-dev libyaml-cpp-dev \
    texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended
```

Install runtime tools:

```
sudo apt install socat goby3-apps
```

Clone cgsn-mooring:

```
git clone git@bitbucket.org:ooicgsn/cgsn-mooring.git
```

Run the build script:

```
./build.sh
```

Build artifacts will be placed in the `build/` directory.

Under the hood, the build script invokes CMake followed by the build tool itself (e.g., `make`). See below for environment variables that can tune the build settings.

To run the unit tests, simply run:

```
./build.sh test
```


### Build configuration

When running `build.sh`, any parameters you want to pass to CMake can be specified in the environmental variable `CGSN_MOORING_CMAKE_FLAGS` and any parameters for `make` can be specified in `CGSN_MOORING_MAKE_FLAGS`:

```
CGSN_MOORING_CMAKE_FLAGS="-Dbuild_doc=ON" ./build.sh
```

Useful CMake flags include:

* `-Dbuild_doc=ON` -- Build the Doxygen documentation
* `-Dbuild_sandbox=ON` -- Build sandbox (prototype) code
* `-DGOBY_DIR=$(pwd)/goby3/build` -- Compile against a Goby3 build directory
* `-DSANITIZE_*=ON` -- Compile using one of the runtime sanitizers (see below)

You can also edit CMake options in the `build/CMakeCache.txt` file or by using the `ccmake` command from the `cmake-curses-gui` package.


### Cross-compiling

To cross-compile for arm64, use the pre-configured Docker container. See the [Circle CI](doc_10-circleci.md) page for information on the Docker container.

```
cd cgsn-mooring
rm -Rf build
scripts/dockerize.py --job arm64-bionic-build -- /bin/sh -c 'apt update && apt upgrade -y; CGSN_MOORING_TARGET=aarch64-linux-gnu ./build.sh -j`nproc`'
```


### Sanitizers

This project can be built with [AddressSanitizer][asan], [ThreadSanitizer][tsan], and [UndefinedBehaviorSanitizer][ubsan]. These tools instrument the compiled binaries to perform extensive runtime checks.

[asan]: https://clang.llvm.org/docs/AddressSanitizer.html
[tsan]: https://clang.llvm.org/docs/ThreadSanitizer.html
[ubsan]: https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html

AddressSanitizer checks for memory corruption bugs like buffer overflows. It can be enabled with the `-DSANITIZE_ADDRESS=ON` CMake flag.

ThreadSanitizer checks for race conditions between threads. It can be enabled with the `-DSANITIZE_THREAD=ON` CMake flag.

UndefinedBehaviorSanitizer checks for code that relies on undefined behavior. This sanitizer can be enabled with either AddressSanitizer or ThreadSanitizer, or on its own, using the `-DSANITIZE_UNDEFINED=ON` CMake flag.
