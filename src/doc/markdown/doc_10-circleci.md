## CircleCI Continuous Integration

[CircleCI][cci] is a continuous integration service that builds each push to the Git repository (Bitbucket). In our case we build these into Debian packages (see [Packaging](doc_11-packaging.md)), and run the unit tests as part of the Debian package build.

[cci]: https://circleci.com/

### Quick Reference

 * CircleCI page: https://circleci.com/bb/ooicgsn
     * Each build posts to Slack (`circleci`)
     * Each build compiles the project for `arm64` and `amd64`
 * Downloading *.deb from a build
     * (one-time) Create a [Circle CI API token](https://circleci.com/bb/ooicgsn/cgsn-mooring/edit#api) selecting a scope of "Build-Artifacts" and save it to a secure location (e.g. `deb-artifacts-token`).
     * Download the debs to the current directory. `BUILD_NUM` is the CircleCI build number.

            CIRCLE_TOKEN=`cat deb-artifacts-token`
            BUILD_NUM=155
            cgsn-mooring/scripts/download_circleci_debs.sh $BUILD_NUM $CIRCLE_TOKEN .

 * Installing the *.debs

            sudo dpkg -i cgsn-mooring_3.0.0~alpha4+15+g93978bb-0~ubuntu18.04.1_amd64.deb
            (optionally, install the documentation to /usr/share/doc/cgsn-mooring-doc/html/index.html)
            sudo dpkg -i cgsn-mooring-doc_3.0.0~alpha4+15+g93978bb-0~ubuntu18.04.1_all.deb

### CircleCI config.yml

CircleCI uses a YAML-based configuration file. See `cgsn-mooring/.circleci/config.yml` for the latest version of this file.

This gets committed to the repository, and CircleCI reads it automatically from this location. At this point we are ready to set-up the project on CircleCI.

#### Commit builds

The current setup builds Debian packages on each commit. The source for the Debian packages is [cgsn-mooring-debian][cgsn-mooring-debian]. If a branch in `cgsn-mooring-debian` exists with the same name as the `cgsn-mooring` branch that is being used, this branch of `cgsn-mooring-debian` is used. Otherwise, the `master` branch of `cgsn-mooring-debian` is used.

The unit tests are built as part of the Debian package build for `amd64`.

[cgsn-mooring-debian]: https://bitbucket.org/ooicgsn/cgsn-mooring-debian/src/master/

#### Nightly builds

The code is built and unit tests are run for `amd64` using the Address Sanitizer, Thread Sanitizer and Static Analysis (SCAN) tools built into the Clang compiler. These jobs are `amd64+asan-bionic-build`, `amd64+tsan-bionic-build`, and `amd64+scan-bionic-build`, respectively.

These builds are run at 07:00 UTC each day.

### CircleCI project setup

After the Docker image and the config.yml file are generated, all of CircleCI is configured from the web interface: <https://circleci.com/dashboard>

You select your Bitbucket login initially, then configure the project under add project: https://circleci.com/add-projects/bb/ooicgsn

Once the project is added, you can further configure the settings from https://circleci.com/bb/ooicgsn/cgsn-mooring/edit, however the majority of the settings are in `config.yml`.

The additional settings from the web UI are:

* The Slack integration was enabled using https://circleci.com/bb/ooicgsn/cgsn-mooring/edit#hooks. Currently CircleCI is configured to send build results to the `circleci` Slack channel.

### Building using the CircleCI Environment

You can simulate the CircleCI environment to perform local builds. You will need to install Docker ([Ubuntu instructions][docker-ubuntu]) and configure [non-root management][docker-manage]. You will also need the [CircleCI CLI][localcli].

[localci]: https://circleci.com/docs/2.0/local-cli/
[docker-ubuntu]: https://docs.docker.com/install/linux/docker-ce/ubuntu/
[docker-manage]: https://docs.docker.com/install/linux/linux-postinstall/

Note that the local environment may differ from real CircleCI servers, such as having a different kernel configuration.

The CircleCI CLI can perform a completely automated build. From the top level of the `cgsn-mooring` repo, to run a local build of the `amd64-bionic-build` job:

```
circleci local execute --job amd64-bionic-build
```

There is currently no way to get build artifacts out of the CircleCI local build environment.

For an interactive environment based on the same Docker image that CircleCI uses:

```
scripts/dockerize.py --job amd64-bionic-build
```

You can list available jobs with `scripts/dockerize.py --show-jobs`.

Note that the Docker images are not re-built automatically. If you update the Dockerfiles, you need to rebuild them; see below.

### Docker Setup

CircleCI 2.0 uses Docker containers for all builds. We use the following containers for this project:

 - a base container starting with an Ubuntu amd64 image with the necessary packages that are common to both builds. This container is used for building `amd64`.
     - `cgsn-mooring/.circleci/base-docker-bionic/Dockerfile` is used for Ubuntu 18.04 "bionic".
 - a CircleCI build container for `arm64` builds (64-bit cross-compile). This container replaces some of the non-multiarch compatible libraries (currently libcrypto++, and libyaml-cpp) with the arm64 version.
     - `cgsn-mooring/.circleci/arm64-docker-bionic/Dockerfile` is used for the Ubuntu 18.04 `arm64` builds.

We store the images on the [Docker Hub][hub]. After pushing images to Docker Hub, they are ready to be referenced in the CircleCI config.yml.

[hub]: https://hub.docker.com/u/ooicgsn/dashboard/

When building new Docker container images, be sure to keep the version numbers synchronized between the CircleCI config.yml and other dependent Dockerfiles.

The version of the Docker containers must match those used in the CircleCI config.yml. They are otherwise independent of other versions (e.g. of `cgsn-mooring`). The `2.x.y` version series is based on Ubuntu 18.04 "Bionic".

To automatically rebuild all of the Docker images and increment the minor numbers only, run

```
scripts/update_docker_images.py
```

This script can also be executed with `--dry-run` to show the commands that it runs and the files that it updates.
