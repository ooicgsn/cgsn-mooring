## PRESF SBE Driver

This driver is for the SBE 26plus Seagauge Wave and Tide Recorder (PRESF).

  - Datasheet: [SBE 26plus Manual.pdf](https://drive.google.com/open?id=1OdFxgKhwR0sEq9iuHGdPGVubh7_f4JyA)
  - Firmware version(s) tested: SBE 26plus-quartz V 7.2
  - Original author(s): Charlie Carnevale <ccarnevale@seacorp.com>

### Configuration

Configuration Message: cgsn::protobuf::PRESF_SBE_SensorConfig (defined in cgsn-mooring/config/presf_sbe_config.proto)

The SBE26 has two configuration phases. 

The first phase is called TimePhase which sets the sensors current time once when the data logger is started by sending the following command:

`DateTime=mmddyyyyhhmmss`

The second phase is called the `SamplingPhase` where the current configuration of the sensor is compared against the proposed configuration and if different, the proposed configuration is sent to the sensor.  

First, the driver sends `SetSampling` command to enter the sampling configuration menu. 

Next, the prompts are answered and then an additional set of configuration settings are sent:


PRESF_SBE_SensorConfig field    |  value              |  PRESF Command Equivalent
----------|---------------------|----------------------------------
 tide_interval | X (numeric value)  | `X`
 tide_measurement_duration | X  | `X`
 wave_burst_sample_sched | X  | `X`
 wave_samples_per_burst | X  | `X`
 wave_sample_duration | X  | `X`
 use_start_time | true  | `Y`
 use_start_time | false  | `N`
 use_stop_time | true  | ``Y``
 use_stop_time | false  | `N`
real_time_wave_stats | true | `Y`
real_time_wave_stats | false | `N`
real_time_tide_data | true | `TxTide=Y`
real_time_tide_data | false | `TxTide=N`
real_time_wave_burst_data | true | `TxWave=Y`
real_time_wave_burst_data | false | `TxWave=N`
external_temp | true | `ExternalTemperature=Y`
external_temp | false | `ExternalTemperature=N`
conductivity | true | `Conductivity=Y`
conductivity | false | `Conductivity=N`
user_info | String | `UserInfo="<String>"`


### Sensor Command Statechart

#### Initialize child states

![](../figures/presf_sensor_command_initialize.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/presf_sensor_command_initialize.png}
\endlatexonly

#### On child states

![](../figures/presf_sensor_command_on.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/presf_sensor_command_on.png}
\endlatexonly

#### Off child states

![](../figures/presf_sensor_command_off.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/presf_sensor_command_off.png}
\endlatexonly

### Quirks

* Tide Interval must be great than 18
* User Info setting cannot contain carriage returns
