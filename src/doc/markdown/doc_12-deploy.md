## Deploying Updated Mooring Software

Note: this is a work in progress that describes the current test system (`barnaclese.whoi.edu`) adn will likely change substantially before the first deployment on the real system.

### Install configs

  - Install testing config files

        cd ~
        git clone git@bitbucket.org:ooicgsn/cgsn-mooring_dev-test.git --branch barnaclese-test
        cd cgsn-mooring_dev-test/test/yaml/barnaclese
        # symlink to /opt/cgsn
        sudo ./deploy.sh

### Install cgsn-mooring code

  - Make logs directory (TODO, wrap this into `cgsn-mooring_rootfs-build`)

        sudo mkdir -p /media/data/logs/debug
        sudo chown -R cgsnuser:cgsnuser /media/data/logs

  - Get the CircleCI download script

         wget https://bitbucket.org/ooicgsn/cgsn-mooring/raw/master/scripts/download_circleci_debs.sh
         chmod u+x download_circleci_debs.sh

  - Get a CircleCI API token (https://circleci.com/bb/ooicgsn/cgsn-mooring/edit#api) selecting a scope of "Build-Artifacts"  and save it to the disk (e.g. `deb-artifacts-token`).
  - Download the debs to the current directory. `BUILD_NUM` is the CircleCI build number, make sure you pick the correct architecture build (`amd64` or `arm64`) for the correct distribution (`bionic`, etc.)

         CIRCLE_TOKEN=`cat deb-artifacts-token`
         BUILD_NUM=1281
         ./download_circleci_debs.sh $BUILD_NUM $CIRCLE_TOKEN .

  - Install the debs (when prompted, choose Mooring Name: "benchtest", CE Name: "dcl36", Autoset IP addresses: "No"). 

         sudo dpkg -i cgsn-mooring_3.0.0~alpha5+73+gf786f0d-0~18.04.1_arm64.deb

  - Add ssh keys (to the underlying read-only rootfs in case the mSATA fails, we can still login) and change the password to a secure one.

         sudo overlayroot-chroot
         # edit /home/cgsnuser/.ssh/authorized_keys with any public keys we need
         # change password from "cgsnuser@02543^" and store in password manager, on paper, etc.
         sudo -u cgsnuser passwd
         exit
