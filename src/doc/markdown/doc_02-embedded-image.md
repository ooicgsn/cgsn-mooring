## The Embedded ARM filesystem image

The basic outline of creating an image is obtaining and/or building (and writing to the SD card):

  - one or more bootloaders
  - the Linux kernel and initramfs
  - Debian or Ubuntu root filesystem

In our case, we will also layer a writeable overlay on top of a read-only root filesystem, for storing changes to the fileystem and deployment (sensor) data (both of these on an mSATA drive):

  - root filesystem writable overlay
  - data directory

### Embedded Image design notes

The root filesystem is installed on an SD card. The SD card filesystem is designed to be immutable (read-only) under normal usage (i.e. when the mSATA drive is attached and working). This greatly reduces the opportunity for filesystem corruption from hard power-off events (i.e. loss of power without a clean `halt`). If the mSATA drive is unavailable after a certain time, the system will boot from the SD card will boot into read/write mode to allow diagnostics.

Certain parts of the filesystem are required to be read/write. This is primarily `/var` but also `/etc` as some programs will modify their own configuration. Also, we obviously need a place to store data from the sensors.

For all these writeable needs, we have an mSATA solid state disk. This isolates the potential corruption to a separate drive, reducing the chance we will have an unbootable system in the case of corruption.

The mSATA disk has two partitions (one for the overlay and one for the data) which are both formatted `btrfs` which is a modern filesystem that provides fault tolerance, and repair.

We use the `overlayroot` Ubuntu package which provides easy configuration of the overlay filesystem in the initramfs. The configuration is in `/etc/overlayroot.conf`.

The overlay partition on the mSATA drive (partition 1) is used for all changes to the root filesystem that occur while the embedded system is powered on. This allows us to store system logs, update packages, etc., and have a clear picture of what has changed, relative to the baseline read-only rootfs created earlier using live-build.

The data partition on the mSATA drive (partition 2) is used for all deployment data (logs from sensors, etc.).

Graphically, the drives and partitions are created as such:

![](../figures/embedded-filesystem-design.png)

\latexonly
\includegraphics[width=0.4\textwidth]{../figures/embedded-filesystem-design.png}
\endlatexonly


#### Building `rootfs`

The `rootfs` generation uses the `live-build` project (`lb`) and is inspired by the Gumstix [Live Build](https://github.com/gumstix/live-build) project. We will use it build a common rootfs for various ARM based sytems (Raspberry Pi, BeagleBone, Gumstix, etc.). Also, reference the live-build project documentation: [https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html](https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html).

The following is a brief overview of the portion of the live-build configuration we are using here:

 - `auto` - scripts that define the `build`, `clean`, and `config` steps. Edit the `config` script to change the distro release (e.g. "bionic") or architecture (e.g. "arm64").
 - `customization` - files that get copied to live-build's `config` directory after `lb config` is run
     - `archives` - Additional APT repositories to pull from (here we have an Ubuntu PPA)
     - `hooks` - Scripts that are run during the build to set the username, etc.
     - `includes.chroot`- Files to include in the image to overwrite the Ubuntu defaults
         - `etc`
            - `fstab`: Defines the partition mounting baseline (`overlayroot` modifies this at boot).
            - `hostname`: Computer name
            - `hosts`: Local DNS entries
            - `overlayroot.conf`: Configuration for overlayroot (defines the overlay as the first partition of the mSATA card (`/dev/sda1`))
            - `resolv.conf`: DNS servers
            - `systemd/journald.conf`: Configures the systemd journal (logger) to write persistent logs (instead of logs that are erased each boot).
            - `network/interfaces`: Network interface settings (e.g. IP addresses).
            - `init.d/regenerate_ssh_host_keys`: Generates new SSH keys once on first boot.
     - `package-lists` - APT (Debian) packages to install in the image.


#### Extras

If you want to make modifications on a running target system directly to the read-only root partition 2 of the SD card, you can log-in (read-write) using

  - (target)

          sudo overlayroot-chroot

This is helpful, for example, for updating the cgsn-mooring .deb package or adding critical configuration changes. Use this sparingly, as we will to mostly capture all the changes to the `rootfs` on the mSATA overlay partition for integration back to into `cgsn-mooring_rootfs-build` for inclusion in future builds.

Note that the first SD card partition (`/dev/mmcblk0p1`) is mounted to `/boot/firmware` read-only and if edits are required must be remounted rewrite beforehand:

         sudo mount -o remount,rw /boot/firmware

#### Data assurance summary

 - SD card is mounted read-only, to minimize chance of corruption of the root filesystem.
 - mSATA drive is formatted with the `btrfs` filesystem, which should provide better fault tolerance and recovery features than older filesystems.
 - Where feasible, processes are run as a non-privileged user to avoid accidental deletion, modification, etc.
 - Log files are marked read-only after they are complete.


### Provisioning a new system

#### SD Card Setup

Scripts in the `cgsn-mooring_rootfs-build` project can be used to create a new SD card:

* `scripts/master_raspi_base_image.sh` creates a bootable "base image" file with `boot` and `rootfs` partitions. Due to bugs in older QEMU versions, this script must be run on Ubuntu 18.04 or newer (see `cgsn-mooring/.circleci/master-raspi-docker/Dockerfile` for dependencies), or from the `cgsn-mooring-master-raspi` Docker image:

           sudo docker run \
              --privileged \
              --volume "$(pwd)":/root/cgsn-mooring \
              --workdir /root/cgsn-mooring \
              ooicgsn/cgsn-mooring-master-raspi:1.0.0 \
                cgsn-mooring_rootfs-build/scripts/master_raspi_base_image.sh

* `scripts/install_into_image.sh` installs Debian packages into the `rootfs` partition of a given base image. (It cannot be used to install a new `linux-image` package, i.e., a new Linux kernel.)

Please see the implementation details of these files for specifics on how the images are created. Currently, the implementation is specific to the Raspberry Pi; if other hardware is available in the future, construction of the `boot` partition may differ.

Images are automatically mastered on CircleCI for `master` and branches beginning in `image`. The `raspi-image-master` will then run and create an image that can be downloaded from the `Artifacts` tab. Images from CircleCI will be bzip2 compressed. The file name produced by CircleCI includes both the version of `cgsn-mooring_rootfs-build` (as `img-{VERSION}`) and the version of `cgsn-mooring` (as `code-{VERSION}`).

Once an image is produced (either via CircleCI or locally using the `master_raspi_base_image.sh` script), it can be copied onto an SD card:

```
# Be careful to specify the correct device for the SD card to avoid overwriting your hard drives
export SDCARD=/dev/mmcblk0
bzcat cgsn-mooring_master_img-1.0.1_code-3.0.1.img.bz2 | sudo dd status=progress of=$SDCARD 
```

(If the image is not compressed, substitute `cat` for `bzcat` in the above command).

At this point, insert the image into the Raspberry Pi and boot it once (without the mSATA card inserted).

The SD card images are 2 GB. If you need to expand the `rootfs` partition to take advantage of more space available on the card:

```
sudo growpart /dev/mmcblk0 2
sudo resize2fs /dev/mmcblk0p2
```


#### mSATA Setup (or second SD card)

After booting the SD card once, shut down the Raspberry Pi.

Insert the mSATA drive (or second SD card for the Gumstix All-in-one board) into the host. The following instructions assume assume that the mSATA is on `/dev/sdd` on the host. Adjust as needed.

 - (host) Create two primary partitions (one 32 GB and the other the remainder of the disk, both Linux). If prompted for a partition label type, choose "dos".

         export SATAHDD=/dev/sdd
         sudo cfdisk $SATAHDD

 - (host) Format BTRFS

         sudo mkfs.btrfs -L overlay ${SATAHDD}1
         sudo mkfs.btrfs -L data ${SATAHDD}2

Install the mSATA into the Raspberry Pi and boot.


### (DEPRECATED) Building the kernel

These instructions are not used as of Ubuntu 18.04 bionic, as we use the linux-image-* packages provided by Ubuntu. They are left here for reference if a custom kernel is required in the future.

The output of this step is a `linux-image` and `linux-libc-dev` Debian package (*.deb). If you have those already from a previous build, and you don't need to update them, you can skip to "Building the `rootfs`"

#### Common steps

 - (host) Set the environmental variables for the correct cross-compiler toolchain for `armhf` (32-bit) or for `arm64` (64-bit). These must be set for the remainder of the kernel building steps:

         # For armhf
         export CROSS_COMPILE=arm-linux-gnueabihf-
         export ARCH=arm
         export KBUILD_DEBARCH=armhf
         # Or, for arm64
         export CROSS_COMPILE=aarch64-linux-gnu-
         export ARCH=arm64
         export KBUILD_DEBARCH=arm64

#### Raspberry Pi Specific steps

 - (host) Clone the Raspberry Pi Linux fork.

         git clone https://github.com/raspberrypi/linux.git --branch raspberrypi-kernel_1.20180924-1
         cd linux

 - (host) Set the defconfig file to use (based on architecture):

         # For armhf
         export DEFCONFIG=bcm2709_defconfig
         # For arm64
         export DEFCONFIG=bcmrpi3_defconfig

 - (host) In the same terminal, set up the default kernel configuration.

         make $DEFCONFIG

 - (host, optional) Configure the kernel: https://www.raspberrypi.org/documentation/linux/kernel/configuring.md

         make menuconfig

#### Common steps

 - (host) Build a .deb for the kernel

         make deb-pkg LOCALVERSION="ooicgsn" -j`nproc`

 - If creating a new image, skip to "Building the rootfs".

 - (target) If updating an existing rootfs, copy the .debs onto the target (either on the SD card in the host computer or via Ethernet). Install the kernel (on the running target). 

         export VERSION=4.14.71-v8ooicgsn
         dpkg -i linux-image-$VERSION*.deb linux-libc-dev_$VERSION*.deb

#### Raspberry Pi Specific steps

 - (target) Update the bootloader configuration (add to or update `/boot/config.txt`):

         initramfs initrd.img-4.14.71-v8ooicgsn followkernel
         kernel=vmlinuz-4.14.71-v8ooicgsn

 - (target) Reboot the Pi into the new kernel. Check that you are now running the correct kernel version that you just built:

         uname -r
