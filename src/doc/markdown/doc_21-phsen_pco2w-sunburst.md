## PHSEN_PCO2W SUNBURST Driver

This driver supports the Sunburst CO2 and pH sensors (PCO2W and PHSEN, respectively).

  - Datasheets:
    - Both: [Low level operation of the SAMI/AFT][low-level]
    - PHSEN: [PHSEN Command List][command-list], [Operating Manual: SAMI2-pH][phsen-manual], [PHSEN with External Logger][phsen-external]
    - PCO2W: [Operating Manual: SAMI2-CO2][pco2w-manual], [PCO2W with External Logger][pco2w-external]
  - Firmware version(s) tested: Unknown
  - Original author(s): Toby Schneider <toby@gobysoft.org>, Charlie Carnevale <ccarnevale@seacorp.com>

[low-level]: https://drive.google.com/file/d/1_wuT9baopdm0jhl6cJBSMKidKnQn1plz/view?usp=sharing
[command-list]: https://drive.google.com/file/d/1pBrDCkPKqMWNBut5Za34DFKnyl_pIR0t/view?usp=sharing
[phsen-manual]: https://drive.google.com/file/d/17hhIWPTTWXzNHCTNUrU_-KjMZiXPxwRi/view?usp=sharing
[phsen-external]: https://drive.google.com/file/d/1CgicJFWFHMFpGUDLu-xuuVP0yhywOkIz/view?usp=sharing
[pco2w-manual]: https://drive.google.com/file/d/15I7JN91Kdtaf5NqRkq16zeyKd2wbV32K/view?usp=sharing
[pco2w-external]: https://drive.google.com/file/d/1kcH7lN04ofRlCEToLqAzQWK5V5lPmLJD/view?usp=sharing

### Configuration

Configuration Message: cgsn::protobuf::PHSEN_PCO2W_SUNBURST_SensorConfig (defined in cgsn-mooring/config/phsen_pco2w_sunburst_config.proto)

Currently, only `op_mode: SENSOR_INTERNAL_SCHEDULE` is implemented so the configuration is not handled by the driver.

### Sensor Command Statechart

Currently, only `op_mode: SENSOR_INTERNAL_SCHEDULE` is implemented so all the states are passthroughs (no-ops) except for WaitForPrompt and SensorSleep, where we set the RTS line  and disable/enable the SAMI status message.

### WaitForPrompt

Here we set the RTS line high and disable the 1Hz status message.

![](../figures/sami-wait-for-prompt.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/sami-wait-for-prompt.png}
\endlatexonly

### SensorSleep

Here we re-enable the 1Hz status message and set the RTS line low.

![](../figures/sami-sensor-sleep.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/sami-sensor-sleep.png}
\endlatexonly





### Quirks

 - Sunburst uses only carriage-return (`\r`) as the end-of-line
 - Binary data protocol, but sent as ASCII-encoded hexadecimal.
 - RTS line is held high used to enable serial comms with the instruments. Note this is *not* the usual RTS/CTS flow control usage. Rather this non-conventional usage operates much like the DTR line would be used normally.