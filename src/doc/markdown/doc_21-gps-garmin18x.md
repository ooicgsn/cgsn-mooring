## GPS Garmin18x Driver

This driver is for the Garmin 18X GPS. It makes use of Garmin proprietary sentences so it may be suitable for other Garmin GPS sensors, but not for standard NMEA-0183 GPS units in general.

  - Datasheet: [GPS_18x_Tech_Specs.pdf](https://drive.google.com/file/d/1ngWbcTAykC_jevyWND0-eS5w0I9rkQ_N/view?usp=sharing).
  - Firmware version(s) tested: Unknown
  - Original author(s): Toby Schneider <toby@gobysoft.org>

### Configuration

Configuration Message: cgsn::protobuf::GPSSensorConfig (defined in cgsn-mooring/config/gps_garmin18x_config.proto).

All the Garmin18X configuration handled by this driver is set in the Garmin `$PGRMC` ("Configuration Phase 1") and `$PGRMC1` ("Configuration Phase 2") messages:

 GPSSensorConfig field    |  value              |  `$PGRMC[1]` Equivalent
----------|---------------------|----------------------------------
 fix_mode | FIX_MODE_AUTOMATIC  | `$PGRMC,A,,,,,,,,,,,,,*hh<CR><LF>`
 fix_mode | FIX_MODE_3D         | `$PGRMC,3,,,,,,,,,,,,,*hh<CR><LF>`
measurement_pulse_enabled | true  | `$PGRMC,,,,,,,,,,,,2,,*hh<CR><LF>`
measurement_pulse_enabled | false | `$PGRMC,,,,,,,,,,,,1,,*hh<CR><LF>`
output_period_seconds     | N     | `$PGRMC1,N,,,,,,,,,,,,,,*64<CR><LF>`

### Sensor Command Statechart

The Garmin18X doesn't require interrogating for a prompt, but we send `$PGRMIE` anyway to make sure it is responding to our commands.



#### Initialize child states

![](../figures/gps_garmin18x_sensor_command_initialize.png)

\latexonly
\includegraphics[width=0.4\textwidth]{../figures/gps_garmin18x_sensor_command_initialize.png}
\endlatexonly

#### On child states

![](../figures/gps_garmin18x_sensor_command_on.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/gps_garmin18x_sensor_command_on.png}
\endlatexonly

#### Off child states

![](../figures/gps_garmin18x_sensor_command_off.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/gps_garmin18x_sensor_command_off.png}
\endlatexonly

### Quirks

None noted. The Garmin18X is a well established and reliable sensor.
