## DOSTA OPTODE Driver


The driver is for the Aanderaa Oxygen Optode 4831 (DOSTA).

  - Datasheet: [Oxygen_Optode_Operating_Manual](https://drive.google.com/open?id=13oCI9-RNY7kulvxzIT1MxtZVyaw1hUqg)
  - Firmware version(s) tested: v491
  - Original author(s): Charlie Carnevale <ccarnevale@seacorp.com>

### Configuration

Configuration Message: cgsn::protobuf::DOSTA_OPTODE_SensorConfig (defined in cgsn-mooring/config/dosta_optode_config.proto)

The DOSTA OPTODE has two configuration phases.  

The first phase is called `ConfigurePasskey` which sets the DOSTA's passkey for write protection on factory settings and must be `1000` :  

`Set Passkey(1000)`  

The second phase is called the `ConfigureDeployProperties` phase which sets the following settings:

 DOSTA_OPTODE_SensorConfig field        |  value           |  Command Equivalent
------------------	|-------------------|----------------------------------
interval 			| N  				| `Set Interval(N)<CR><LF>`
comm_timeout 		| ALWAYS_ON  		| `Set Comm Timeout(Always On)<CR><LF>`
comm_timeout 		| TEN_SEC		  	| `Set Comm Timeout(10 s)<CR><LF>`
comm_timeout 		| TWENTY_SEC  			| `Set Comm Timeout(20 s)<CR><LF>`
comm_timeout 		| THIRTY_SEC 	 		| `Set Comm Timeout(30 s)<CR><LF>`
comm_timeout 		| ONE_MIN  			| `Set Comm Timeout(1 min)<CR><LF>`
comm_timeout 		| TWO_MIN  			| `Set Comm Timeout(2 min)<CR><LF>`
comm_timeout 		| FIVE_MIN  			| `Set Comm Timeout(5 min)<CR><LF>`
comm_timeout 		| TEN_MIN  			| `Set Comm Timeout(10 min)<CR><LF>`
enable_comm 		| true 				| `Set Enable Comm Indicator(Yes)<CR><LF>`
enable_comm 		| false				| `Set Enable Comm Indicator(No)<CR><LF>`
enable_sleep     	| false    			| `Set Enable Sleep(No)<CR><LF>`
enable_sleep     	| true    			| `Set Enable Sleep(Yes)<CR><LF>`
enable_polled_mode  | false	   			| `Set Enable Polled Mode(No)<CR><LF>`
enable_polled_mode  | true	   			| `Set Enable Polled Mode(Yes)<CR><LF>`
enable_text    		| false   			| `Set Enable Text(No)<CR><LF>`
enable_text    		| true   			| `Set Enable Text(Yes)<CR><LF>`
enable_decimal_format | true     		| `Set Enable Decimalformat(Yes)<CR><LF>`
enable_decimal_format | false     		| `Set Enable Decimalformat(No)<CR><LF>`
enable_air_saturation | true     		| `Set Enable AirSaturation(Yes)<CR><LF>`
enable_air_saturation | false     		| `Set Enable AirSaturation(No)<CR><LF>`
enable_raw_data   	| true     			| `Set Enable Rawdata(Yes)<CR><LF>`
enable_raw_data   	| false    			| `Set Enable Rawdata(No)<CR><LF>`
enable_temperature	| true     			| `Set Enable Temperature(Yes)<CR><LF>`
enable_temperature	| false    			| `Set Enable Temperature(No)<CR><LF>`
salinity			| N     			| `Set Salinity (N)<CR><LF>`

**NOTE: Per conversation with SME Hilary Palevsky on 6/15/18, the salinity value is usually set to either 0 or 35 (for fresh water and salt water, respectively)**


### Sensor Command Statechart



#### Initialize child states

![](../figures/dosta_sensor_command_initialize.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/dosta_sensor_command_initialize.png}
\endlatexonly

#### On child states

![](../figures/dosta_sensor_command_on.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/dosta_sensor_command_on.png}
\endlatexonly

#### Off child states

![](../figures/dosta_sensor_command_off.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/dosta_sensor_command_off.png}
\endlatexonly

### Quirks

* Both ";" and "//" are accepted comment strings, following characters are ignored  

* Carriage Return & Line Feeds need to be used  
	* In minicom use Ctrl+J"  

* Set Passkey(<value>) needs to be entered to be able to write/alter/read most commands  

* Reset will resume sampling automatically, even with sleep mode enabled 

* A valid command string is acknowledged with the character ‘#’ while character ‘*’ indicates an error.  

