## Systemd

Systemd is the `init` manager for most new Linux distributions. Thus, we are using it to launch and manage the `cgsn-mooring` software in integration with the rest of the system processes (daemons).

Systemd uses `service` files to define the process the run, dependencies, conditions, and any pre/post actions. The `cgsn-mooring` service files are in `src/config/systemd` and are installed to `/lib/systemd/system` as is standard for Debian/Ubuntu.

This page does not intend to restate `systemd` documentation. For that, see `man systemd.unit` and `systemd.service` or [https://www.freedesktop.org/wiki/Software/systemd/](https://www.freedesktop.org/wiki/Software/systemd/)

Of particularly note, it is worth learning the basics of the `systemctl` and `journalctl` command line tools.

### Boot dependencies

The order of init for a DCL is:

![](../graphviz/systemd-dcl.png)

\latexonly
\includegraphics[width=0.6\textwidth]{../graphviz/systemd-dcl.png}
\endlatexonly

### cgsn.service

This is the root of the CGSN mooring init. It generates the script required for starting the correct sensor drivers:

```
cgsn_config_tool --action WRITE_DRIVER_START_SCRIPT --ce dcl12
```

See [Configuration](doc_23-config.md) for more details on `cgsn_config_tool`.

You can use this service to start/stop/restart the entire mooring:

```
sudo systemctl start cgsn
sudo systemctl stop cgsn
sudo systemctl restart cgsn
```

### Status

You can list any number of services to get their current status

```
systemctl status cgsn cgsn-gobyd cgsn-logger cgsn-supervisor cgsn-scheduler cgsn-driver-port@{1..8}
```

### Log

If a service isn't running (e.g. `cgsn-supervisor`), check

```
journalctl -u cgsn-supervisor
```

Also, check the logs in `/media/data/logs/debug` for more information.

### Clean shutdown

All the services use the `goby_terminate` tool to cleanly command a shutdown using the systemd `ExecStop` directive:

```
ExecStop=/usr/bin/goby_terminate <(/usr/bin/cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type GOBY_TERMINATE) --target_pid $MAINPID --response_timeout 10
```

See [Goby3](doc_06-goby3.md) for more details on `goby_terminate`.

### Real time clock

The DS1307 real time clock is enabled using a combination of `/etc/modules`, `udev` and `systemd`. This is all configured to work automatically when using the `cgsn-mooring_rootfs-build` project (see [the Embedded ARM filesystem image](doc_02-embedded-image.md) for more details), but this section explains how it works.

The `rtc-ds1307` line in the `/etc/modules` file loads this kernel driver (which is used for a variety of the Maxim RTCs including the DS1307 and DS3231 chips).

The udev rule `09-cgsn-rtc-start.rules` creates a systemd target (`sys-module-rtc_ds1307.device`) when the `rtc-ds1307` module is loaded.

This `.device` target is then used as the dependency (`Requires=`) for the `cgsn-rtc-i2c.service` job which then enables the chip in the `rtc-ds1307` module (currently address 0x68 on i2c bus 0) which then creates the RTC devices (`/dev/rtc0`). After this is sucessful (`ExecStartPost=`), the system clock is set from the hardware clock.


