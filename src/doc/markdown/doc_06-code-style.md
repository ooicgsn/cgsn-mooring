# Coding Style

This project uses a coding style based on LLVM using Allman brace breaking.

Pointers should by left aligned; prefer `int* x` to `int *x`. Do not declare multiple pointer variables on the same line, e.g., `int* x, * y`.

File names should be written in snake_case. Use the `.h` extension for header files.

Header files should have an include guard that includes the date:

        #pragma once
        #ifndef HEADER_20181030_H
        #define HEADER_20181030_H
        ...
        #endif

Tabs should render as 4 spaces.

## Automatic code formatting

This project includes a `.clang-format` file that specifies the desired formatting.

You can use [`clang-format`][clang-format] to automatically apply consistent code styling to a source file. 

[clang-format]: https://clang.llvm.org/docs/ClangFormat.html

        apt-get install clang-format
        clang-format -i  src/bin/sensors/ctd_sbe/ctd_sbe_driver.cpp

The command above saves changes to the file (the `-i` option). To view a diff of what will be changed:

        diff -u src/bin/sensors/ctd_sbe/ctd_sbe_driver.cpp <(clang-format src/bin/sensors/ctd_sbe/ctd_sbe_driver.cpp)

To view any uncommitted new lines that need to be reformatted:

        git diff -U0 --no-color | clang-format-diff.py -i -p1

You can integrate clang-format with various commonly used editors such as EMACS and VI; see this [document][clang-format-editor-integration] for details.

[clang-format-editor-integration]: https://clang.llvm.org/docs/ClangFormat.html

## Clang-format integration with Git

Rather than have to remember to manually run clang-format, it is recommended to automatically apply the `clang-format` settings to the code just before commit using the [`clang-format-hooks`][clang-format-hooks] project, which has been included in `cgsn-mooring/scripts/clang-format-hooks`.

[clang-format-hooks]: https://github.com/barisione/clang-format-hooks

You can register the hook by running

        cd cgsn-mooring
        ./scripts/clang-format-hooks/git-pre-commit-format install

After this, you will be prompted to apply automatic format patches before each commit, only to code that is modified in this commit.