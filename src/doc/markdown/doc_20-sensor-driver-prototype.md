## Sensor Driver Prototype

We have two drivers as a starting point for creating new drivers:

  - The Garmin 18x GPS driver (`gps_garmin18x`) was written as a demonstration for writing new sensor drivers.
  - The `driver_pattern` driver is a minimal skeleton pattern for creating new drivers. It doesn't do anything useful on its own. To create a new driver from `driver_pattern`, run

        cd cgsn-mooring/scripts
        ./generate_driver_from_pattern.sh ctd_sbe

    where "ctd_sbe" is the name of the new driver.

The rest of this document describes the driver design using the `gps_garmin18x` as an example.

### Software components

![](../figures/gpsdriver-components.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/gpsdriver-components.png}
\endlatexonly

The sensor driver is compiled into at least two binaries, one is the actual driver, another is the parser, and the others are unit tests. To do this, the shared components are put into a library (in this case `libcgsn_gps_garmin18x.so.` Note that Linux prefixes shared libraries with `lib` and adds a suffix of `.so`).

Serial I/O is put in its own library (`cgsn_serial`) since multiple sensor drivers will use it. Similarly, the configuration Protobuf messages are in `cgsn_config` and the runtime (publish/subscribe) messages are in `cgsn_messages`. These could technically be in the same library, but since they serve somewhat different purposes, they are separated for clarity.


### Runtime components (threads)

![](../figures/gpsdriver-threads.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/gpsdriver-threads.png}
\endlatexonly


At runtime, the GPSDriver main thread launches two additional threads which (along with the main thread) perform the three functionalities of a CGSN sensor driver:

  - Command / Control (main thread)
  - Input/Output (I/O): in this case, serial
  - Validation (error checking, data integrity validation)

The GPSParser main thread launches a single additional thread which performs the parsing task:

  - Parsing (conversion of sensor data formats to a usable format, in this case Protobuf)

These threads communicate via the Goby3 middleware using the InterThread layer. 

The InterThread _groups_ are:

  - `cgsn::groups::ready`: Published by each thread to signal that the thread is ready to receive published data.
  - `cgsn::groups::error`: Published by the serial I/O thread for errors in reading the serial port. Could be extended for handling errors from other threads as needed.
  - `cgsn::groups::sensor_command_state`: Published by the GPSDriver main thread (Sensor Command) to indicate the current sensor command state machine.
  - `cgsn::groups::command::logging`: Published by the GPSDriver main thread (Sensor Command) to enable/disable the Validator (based on whether we are logging data or not).

In addition, data are published in the InterProcess layer for other processes to use.

The InterProcess publication _groups_ are:

  - `cgsn::groups::gps::raw_in`: Published by the serial I/O thread for all incoming (relative to the sensor driver) serial messages. This contains the raw data as a cgsn::protobuf::SensorRaw message.
  - `cgsn::groups::gps::raw_out`: Subscribed to by the serial I/O thread for all outgoing serial messages (that is, messages from the sensor driver to the sensor). This is used for commanding the sensor over the serial interface (which this GPS driver does not currently do)
  - `cgsn::groups::gps::raw_in_validated`: Published by the validation thread for all incoming serial messages which pass the first-level validation (e.g. correct talker for NMEA, correct checksum, etc.)
  - `cgsn::groups::gps::parsed`: The parsed GPS data. We will want a different group for each parsed sensor data type.
  - `cgsn::groups::sensor::driver_status`: The overall health/state of this driver (combination of Validator and Sensor Command state machines, plus any errors, if applicable)
  - `cgsn::groups::scheduler::command`: A command from the scheduler to enable/disable logging.

The following figure shows the publish/subscribe connections between the various components in the driver and parser (green = interthread, blue = interprocess):

![](../graphviz/gps-example-pubsub.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../graphviz/gps-example-pubsub.png}
\endlatexonly

For further details, see the individual classes within the Doxygen documentation. 

### State machines

Each sensor driver is governed by two hierarchical state machines (HSMs, also known as statecharts): 

  1. the sensor command state machine (which governs the state of commanding the port and instrument to start and stop logging); and
  2. the validator state machine (which is based on the data and quality of the data that we are receiving).
  
These statecharts are implemented in C++ using Boost.Statechart (http://www.boost.org/doc/libs/1_58_0/libs/statechart/doc/index.html)

#### Sensor Command Statechart

The sensor command statechart is partially reimplemented for each sensor driver. Some of the states (shown in blue in the statechart below) must be partially reimplemented for each sensor driver. The common implementation of the state machine then takes these implementations and re-uses them as needed (e.g. `WaitForPrompt`) to compose the completed state machine.

States:

  - `On`: Port is or should be powered on
     - `PortPoweringOn`: Port is in the process of powering on.
        - `MPICPortOn`: Port is being powered on via the Master PIC (MPIC).
        - `LaunchingIOThread`: The I/O thread (e.g. serial) is being launched.
     - `Critical Failure`: Port fails to power on after several tries, or other driver specific failure.
     - `WaitForPrompt`: Driver is waiting or interrogating for the command prompt from the sensor.
     - `StartLogging`: Driver is commanding the sensor to commence logging.
     - `Logging`: Driver is logging.
  - `Off`
     - `WaitForPrompt`: Same idea as `On::WaitForPrompt`.
     - `StopLogging`: Driver is commanding the sensor to stop logging. Must be capable of posting EvLoggingStopped correctly even if the driver was not logging.
     - `SensorSleep`: If applicable, driver is commanding the sensor to sleep, or go into a shutdown or power-off sequence.
     - `PortPoweringOff`: Port is in the process of powering off (via the Master PIC).
        - `JoiningIOThread`: The I/O thread is being joined (stopped).
        - `MPICPortOff`: Port is being powered off via the Master PIC.
     - `Standby`: Sensor command is idle and waiting to be re-enabled.
     - `Critical Failure`: Port fails to power off after several tries, or other driver specific failure.
  - `Initialize`: all child states are the same as their `On` or `Off` equivalents except `SelfTest`.
     - `StopLogging`: Used here in case the sensor driver was turned on while the sensor was still logging.
     - `SelfTest`: If supported, the driver is running the sensor's self testing capability or any other relevant automated quality control checks.

![](../figures/sensor_command_statechart.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/sensor_command_statechart.png}
\endlatexonly

#### Validator Statechart

The validator statechart should not need to be reimplemented for each sensor driver. Data are of three types: Bytes (anything from the sensor), Valid Raw (raw data that passes basic validity checks based on the sensor type. For example, the NMEA message is well-formed and the checksum is correct), Valid Parsed (data are parsed and of reasonable values. For example, we have a GPS fix and the latitude and longitude are real values). Parsed Valid is not intended to indicate that the data are of scientific quality (this must be done shore-side).

States:

   - `Logging`: Sensor should be logging.
      - `Initializing`: Logging has just been enabled and is waiting up to a timeout value for data.
      - `Critical Failure`: No bytes are received within a configurable timeout.
      - `Degraded`: The validator is receiving bytes, but they are not valid within a configurable timeout.
      - `Functioning`: The validator is receiving valid data.
         - `RawDataValid`: Raw data are valid but the parser is not returned valid values or the parser is not running.
         - `ParsedDataValid`: Parsed data are valid. This implies that the raw data are valid as well.
   - `NotLogging` : Sensor should not be logging.
      - `Standby`: Validator is idle.

![](../figures/validator_statechart.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/validator_statechart.png}
\endlatexonly


### Sensor driver minimal pure-software playback simulation


![](../graphviz/interprocess-gps-driver-sim.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../graphviz/interprocess-gps-driver-sim.png}
\endlatexonly

This figure represents the processes running for a standalone purely software simulation of the Garmin 18x GPS driver. This model should be similar for other instruments, and can be extended to run multiple sensors in simulation simultaneously. To do so, we simply run multiple `*_playback` tools, socat instances, and drivers.

#### Running the simulation

 - Start the Goby Daemon

          gobyd

 - Start the Goby Logger (this command logs to the current directory, change `--log_dir` as desired). This is optional, depending on what you're trying to debug.

          goby_logger --log_dir .

 - Start socat to create two virtual serial ports (pseudoterminals, aka `pty`). This is the pure software version of a null modem cable.

          socat pty,link=/tmp/ttygps_sim,raw,echo=0 \
                pty,link=/tmp/ttygps,raw,echo=0 

 - Start the GPS driver and connect it to one of the `pty`s. You can increase the verbosity of the debug output to STDOUT the `tty_verbosity` value (from QUIET, WARN, VERBOSE, DEBUG1, DEBUG2, DEBUG3 in order from least to most verbose).

          cgsn_gps_garmin18x_driver --serial 'port: "/tmp/ttygps" baud: 4800' \
           --app 'glog_config { tty_verbosity: DEBUG2 show_gui: true }'

 - (optionally) Start the GPS parser. Without the parser, the driver will function and log raw data, but the validator will only reached the FUNCTIONING level. Eventually, we will call the `cgsn-parsers` Python parsers from this code. `-vv` corresponds to `tty_verbosity: DEBUG1`.

          cgsn_gps_garmin18x_parser -vv

 - Start playback of an existing log to simulate the output of the sensor.

          gps_simulator --serial 'port: "/tmp/ttygps_sim" baud: 4800' \
          --log ~/Desktop/ooi-test-logs/default_goby_platform_20180306T190445.goby  -vvv

 - The driver starts up in STANDBY (not logging). Use the `cgsn_scheduler` to turn on and off the serial I/O (via the `cgsn::groups::scheduler::command` group). To enable the port use:

          cgsn_scheduler --init 'port_command { type: GPS_GARMIN18X port: 0 enable: true }'

    Then to disable, run

          cgsn_scheduler --init 'port_command { type: GPS_GARMIN18X port: 0 enable: false }'

#### Things to test

 - You can simulate failure of the serial ports by killing (CTRL-C) on `socat` to remove the pseudoterminals. This would be similar to the FTDI USB chip disconnecting and the serial devices being removed by the Linux kernel.
 - You can simulate complete failure of the sensor by killing (CTRL-C) `gps_playback`.


### Sensor driver minimal hardware-in-the-loop

![](../graphviz/interprocess-gps-driver.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../graphviz/interprocess-gps-driver.png}
\endlatexonly

This scenario is identical to the pure-software simulation except we replace the `gps_playback` and `socat` `pty`s with a real sensor connected to a real serial port (e.g. `/dev/ttyUSB`.

In this case, we need to run:

          gobyd
          goby_logger --log_dir .
          cgsn_gps_garmin18x_driver --serial 'port: "/dev/ttyUSB0" baud: 4800' \
            --app 'glog_config { tty_verbosity: DEBUG2 show_gui: true }'
          cgsn_gps_garmin18x_parser -vv
          cgsn_scheduler --init 'port_command { type: GPS_GARMIN18X port: 0 enable: true }'

