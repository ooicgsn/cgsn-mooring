## VELPT NORTEK Driver

This driver is for the Nortek Aquadopp Deep Water Current Meter (VELPT: "VELocity PoinT")

  - Datasheet: [Nortek System Integrator Manual][nortek-manual]
  - OOI: [Single Point Velocity Meter][ooi-velpt]
  - I/O Thread: cgsn::io::NortekDataBinarySerialThread
  - Firmware version(s) tested: Unknown
  - Original author(s): Toby Schneider <toby@gobysoft.org>, Charlie Carnevale <ccarnevale@seacorp.com>


[nortek-manual]: https://drive.google.com/file/d/1j3t63Lwey2ESpBUsn77cfX3tdM-3yUB1/view?usp=sharing
[ooi-velpt]: https://oceanobservatories.org/instrument-class/velpt/

### Configuration

Configuration Message: cgsn::protobuf::VELPT_NORTEK_SensorConfig (defined in cgsn-mooring/config/velpt_nortek_config.proto)

Only `op_mode: SENSOR_INTERNAL_SCHEDULE` is implemented so far, so no configuration is handled by this driver.

### Sensor Command Statechart

Only `op_mode: SENSOR_INTERNAL_SCHEDULE` is implemented so far, so the sensor command states are passthroughs (no-ops).


### Quirks

 - Little-endian data format (somewhat unconventional as big-endian is generally used for data protocols and networking).
