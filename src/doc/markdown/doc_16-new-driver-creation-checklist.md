## New Driver Creation Checklist
This checklist is intended to be completed while developing a new sensor driver to ensure all critical pieces are implemented.  

[ ] Create initial driver files from `driver_pattern` 
* See [Sensor Driver Prototype Markdown document](doc_20-sensor-driver-prototype.md) for more information  

[ ] Implement WaitForPrompt  

[ ] Implement Configure  

[ ] Implement StartLogging  

[ ] Implement StopLogging  

[ ] Implement SensorSleep  

[ ] Implement OperationMode (if applicable)  

[ ] Implement SelfTest  

[ ] Implement validate_raw()

[ ] Complete validator unit test

[ ] Complete driver markdown document  

[ ] Open Pull Request in bitbucket