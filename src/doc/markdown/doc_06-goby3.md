## Goby3

The CGSN Mooring project is built on version 3 of the [Goby Underwater Autonomy
Project][goby3], also known as Goby3.

[goby3]: https://github.com/GobySoft/goby3

This document briefly covers the concepts and components of Goby3 that are
relevant to this project. 

Additional resources currently available:

 - [Doxygen API documentation][goby3-doc] provides descriptive class and
   function definitions.
 - [Goby3 examples][goby3-examples] provide basic working code samples.
 - For more context, the design of Goby3 is explained in the [2016 IEEE AUV
   paper][goby3-paper] and [2017 MOOS-DAWG presentation][goby3-talk].

[goby3-doc]: http://gobysoft.org/doc/3.0
[goby3-examples]: https://github.com/GobySoft/goby3-examples
[goby3-paper]: https://gobysoft.org/dl/schneider-auv-2016-goby3.pdf
[goby3-talk]: https://gobysoft.org/dl/schneider-moos-dawg-goby3-talk.pdf

### Applications

Executables that use the Goby3 framework are called *applications*.

Applications define a class (e.g., `class Tool`) that subclasses
[`goby::common::ApplicationBase3<ToolConfig>`][app3-doc], where `ToolConfig` is
a Protobuf message definition that describes the application's configuration
options. Goby3 provides support for reading the configuration from a file, the
output of another process, and/or command-line arguments.

[app3-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1common_1_1ApplicationBase3.html

The `main()` routine of the application just passes control to
`goby::run<ToolConfig>`, which constructs a new `Tool` instance. The
constructor of the `Tool` has access to `app_cfg()`, the `ToolConfig` instance
that contains the application's configuration values.

### Publish / Subscribe

Goby3 is designed around a nested publisher / subscriber pattern. Some
applications have access to *transporters* which relay messages, for instance
between threads within the same process (*interthread*), between separate
processes on the same system (*interprocess*), or even between multiple systems
(*intervehicle*). The intervehicle layer is not yet complete, and thus is not
used in `cgsn-mooring` at this time.

Goby3 can pass multiple types of messages, depending on the layer. The
interthread layer can pass any C++ object (as it simply passes a
`std::shared_ptr`), the interprocess layer can pass any serializable object
which has a defined "marshalling scheme" (or just *scheme* for short). In
`cgsn-mooring` we currently exclusively use the Google Protocol Buffers
(*Protobuf*) scheme.

By inheriting from [`SingleThreadApplication`][singlethreadapp-doc] (instead of
`ApplicationBase3`), an application can access the `interprocess()`
transporter, which connects all processes affiliated with a single `gobyd`
instance, as well as the `intervehicle()` transporter. As the name implies, the
`interthread()` transporter is not available in this class.

[`MultiThreadApplication`][multithreadapp-doc] applications can additionally
publish messages to other threads in the same process, using the
`interthread()` transporter, which is nested in the `interprocess()`
transporter, so all messages published on the interprocess layer are also
published on the interthread layer. New threads inherit from
[`goby::SimpleThread`][simplethread-doc], which provides a similar API to the
main thread of `MultiThreadApplication`.

[singlethreadapp-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1SingleThreadApplication.html
[multithreadapp-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1MultiThreadApplication.html
[simplethread-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1SimpleThread.html

All published messages are tagged with *group* identifiers (of type
[`goby::Group`][group-doc]). For "interthread" and "interprocess," a group is
typically initialized with a string (`const char*` to allow constexpr usage). A
subscriber registers to receive all messages of a given type sent to one or
more groups.

[group-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1Group.html

Here is an example of subscribing to a specific message:

    interprocess().subscribe<groups::supervisor::mpic_command, protobuf::MPICCommand>(
        [this] (const protobuf::MPICCommand& cmd) { ... });

The template parameters to `subscribe()` specify the `goby::Group` to which we
are subscribing and the expected Protobuf message type. The `subscribe()`
function takes a callback which receives the message, in this case a lambda
expression that captures `this` (our application instance).

Publishing a message is similar:

    interprocess().publish<cgsn::groups::supervisor::mpic_status>(status);

Here, we need only specify the group to publish to, and the message type is
inferred from the parameter.

The main interface used on all the publish/subscribe layers is
[goby::StaticTransporterInterface][static-interface-doc]. In addition, the
[goby::InterProcessTransporterBase][interprocess-doc] provides additional
methods for the `interprocess()` layer (such as a regex-based subscription).

[static-interface-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1StaticTransporterInterface.html
[interprocess-doc]: http://gobysoft.org/doc/3.0/classgoby_1_1InterProcessTransporterBase.html

#### gobyd

`gobyd` is the daemon responsible for interprocess and intervehicle message
dispatch.


### Logging

Goby3 provides a shared logging output stream called `goby::glog`, which is
intended for human-readable debug logging. The output stream is a custom
subclass of `std::ostream` which also provides a convenient way to check the
configured logging level:

    glog.is(DEBUG2) && glog << "a debug log" << std::endl;

Additionally, the `goby_logger` Goby3 logging daemon subscribes to all groups
(or a subset using a regex filter) and records them to a custom binary log
format (*.goby). The tools that can be used to parse this file are described in
a [separate document](doc_30-logger.md).


### Line-based Communications

Goby3 provides utilities for line-based communication protocols, such as TCP
sockets and serial devices, and can handle hexadecimal and NMEA0183 encoding.


### Time

In the `goby::time` namespace there are [some utilities][time-doc] for
converting between time representation formats (boost::units and
boost::posix_time). The standard time unit used in Goby3 and `cgsn-mooring` is
microseconds since UNIX (1970-01-01 00:00:00 Z), aka `goby::time::MicroTime`.

[time-doc]: http://gobysoft.org/doc/3.0/namespacegoby_1_1time.html


### Simulation

TBD.

### Process Init (Launch)

The `goby_launch` tool allows for launch and shutdown of multiple binaries simultaneously. We use this for `cgsn-mooring` for development and simulation, and use [`systemd`](doc_03-systemd.md) for this purpose on the deployed CPUs. 

For example, to launch a simulation for DCL36 running in GNU `screen` (`-s), use

```
goby_launch -s <(cgsn_config_tool --action WRITE_SIMULATION_LAUNCHFILE --cpu dcl36)
```

See the [Configuration](doc_23-config) document for more on the `cgsn_config_tool`. Run `goby_launch -h` for all its available commands.

`goby_launch` performs cleanup by first running `goby_terminate`. If that fails, SIGTERM is sent; and if that fails, SIGKILL is sent. This is similar to how systemd behaves. The configuration parameters `not_goby` can be set in the launchfile to omit the `goby_terminate` step. This is useful when using `goby_launch` to run non-Goby binaries, such as `socat`:

```
# launch a gobyd
gobyd
# launch a non-goby app, e.g. socat
socat pty,link=/tmp/dcl-mpic-sim,raw,echo=0 pty,link=/tmp/dcl-mpic-supervisor,raw,echo=0

# tell goby_launch not to run goby_terminate for socat, but rather immediately send SIGTERM on exit
# [goby_launch]: not_goby=socat
```

### Process Termination (Clean quit without signals)

The `goby_terminate` tool can be used to request that one or more processes quit cleanly (that is, return from `int main()`). It requires the `interprocess {}` configuration for a running `gobyd`, and then one or more targets to request to quit. These targets can be specified either by their process ID (PID, e.g. 16063) using `--target_pid 16063` or their configured name (That is, `foo` if `app { name: "foo" } }`) using `--target_name foo`. Both `--target_name` and `--target_pid` can be multiply specified if desired and mixed as needed to terminate multiple processes.

`goby_terminate` uses a request/response model to command Goby applications to cleanly shutdown (that is, call the `ApplicationBase3::quit()` method):

 - `goby_terminate` connects to the configured `gobyd`, sends a publication to the `goby::terminate::request` group to request that a process with the given `target_pid` (or `target_name`) cleanly quit.
 - All the Goby applications (including `gobyd`) subscribe to this group. If a process is running that matches the `target_pid`, it will reply back to `goby_terminate` to `goby::terminate::response` with its name and PID. After sending this response, it immediately calls `quit()`, which could take some time to complete, depending on how complicated the Goby application's cleanup actions are (defined in its destructor).
 - Upon receiving the response from the target application, `goby_terminate` waits up to `response_timeout` seconds for the process to fully quit (as evidenced by `kill -0` on the given PID).
 - Once all target applications have responded and fully quit, `goby_terminate` exits sucessfully itself.
 - If the timeout is reached before all targets have quit cleanly, `goby_terminate` quits with `EXIT_FAILURE`. At this point, the targets would need to be externally cleaned up, if desired using SIGTERM and/or SIGKILL.

