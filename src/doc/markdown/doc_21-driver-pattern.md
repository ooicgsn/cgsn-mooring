## Driver Pattern Driver

_Note: this documentation is used as a starting point for all future drivers._

_Brief description and list of the sensor(s) that this driver is written for._

  - Datasheet: _link_
  - Firmware version(s) tested: _versions_
  - Original author(s): _author(s)_

### Configuration

Configuration Message: cgsn::protobuf::DriverPatternSensorConfig (defined in cgsn-mooring/config/driver_pattern_config.proto)

_Description/table of the configuration message, and how these fields map onto the sensor's configuration sentences._

### Sensor Command Statechart

_Text and Statechart drawings for the implementation of the Base states in the Sensor Command Statechart for this driver._

### Quirks

_Notes of any quirks or other issues discovered while writing this driver._

