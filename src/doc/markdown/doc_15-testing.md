## Testing

The testing philosophy for this project is based on a pyramid, where the base of the pyramid represents unit testing, where most of the bugs should be caught. This is also where catching bugs is the cheapest (in terms of developer, hardware, and support resources). At the tip of the pyramid is full system testing, where only the last few bugs that depend on complex interaction between all the system components should be found.

![Testing Pyramid](../figures/test-pyramid.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/test-pyramid.png}
\endlatexonly


A few guidelines:

  - Write unit tests while developing new code, especially if this code does not directly touch hardware. It is much easier and more fun to write tests while writing the code, rather than writing tests after the fact. 
  - Do not spend excessive time writing tests that do not test realistic execution paths or for code that would be much better tested using full simulation or hardware.

The types of testing used in CGSN are:

  - CTest tests
     - `unit`: These are typically written using [Boost Test][boost-test]. These generally test individual functions or methods, and represent the simplest tests to write.
     - `multithreaded`: Unit tests that use goby::MultiThreadTest to run the actual Goby3 threads (but still in a single test binary that can automatically be run). These are more challenging to write due to the asynchronous nature of the publish/subscribe infrastructure. Thus, these may not exist for each sensor driver depending on the perceived cost/benefit tradeoff.
  - Pure software simulation tests
    - `simulators`: Currently these simulate sensors based on playing back historically collected data. They do not simulate the data based on simulated input (using a model, for example). This may be added in the future depending on the level of autonomy needed.
  - Hardware in the loop tests: These are running manually by connecting hardware to a laptop or test rig and collecting data.
  - Full system tests: Run on the full mooring prior to deployment (_burn-in_).

[boost-test]: https://www.boost.org/doc/libs/1_58_0/libs/test/doc/html/index.html

## CTest

The tests run by `CTest` can be built and run locally using (this parallelizes both building and testing by the number of CPU cores on your computer)

    cd cgsn-mooring/build
    cmake -Denable_testing=ON .. && cmake --build . -- -j`nproc`
    ctest -j`nproc`

To run a single test or several tests matching a regex, you can use `-R`, for example

    # run cgsn_test_log_formatters
    ctest -R cgsn_test_log_formatters
    # run all validator tests
    ctest -R '.*validator$'

The `--output-on-failure` flag is especially helpful to for debugging failing tests, as it outputs the test's standard output and error if the test fails

    ctest --output-on-failure

The tests are automatically run by [Circle CI](doc_10-circleci.md) as part of the [Debian package](doc_11-packaging.md) build. This is enabled in the `rules` file of the Debian package source via the variable `DEB_MAKE_CHECK_TARGET = test`, as CTest is automatically run when `make test` is invoked (when using the Makefile generator for CMake, which we are).

### Unit Tests

The unit tests are run using [CTest][ctest], which is enabled in the `CMakeLists.txt` with the `add_test` command. These tests should run quickly (less than a minute, but usually a few seconds at most) and not require the use of any external resources (sensors, network, etc.)

Unit tests must return the success code (0) if sucessful, or any other value if failing. In addition, a failing test can thrown an uncaught exception, assertion fail (`assert(false)`), etc.

[Boost Test][boost-test] is a useful framework for writing multiple tests within a single binary, and provides macros and test fixtures that allow for concise test code. While Boost Test isn't required for unit tests in `cgsn-mooring`, its use is recommended when feasible. Generally, multiple tests that are related to a single functional unit (e.g. multiple tests that test the same library or group of related functions) should be aggregated into a single test binary.

[ctest]: https://cmake.org/cmake/help/v3.5/manual/ctest.1.html

### Multithreaded tests

These tests are slightly more complicated than the unit tests as they spawn multiple `goby::Thread`s  and do some amount of publish/subscribe testing. However, they are still standalone binaries and are run using `CTest` (enabled via `add_test()`).

## Simulators

The CGSN simulators are intended to replicate a sufficient amount of functionality in software to allow testing of the `cgsn-mooring` code without external hardware (Master PICs, instruments, etc.). The are not intended to be a 100% replication of functionality, just enough to allow full system simulation and reduce the number of integration bugs in the `cgsn-mooring` code before running on the actual hardware.

Where possible, the CGSN simulators should

 - replicate quirks or firmware bugs in the hardware they are simulating (e.g. mispelled messages, out-of-order messages, occasional ignored commands, etc.). When a new quirk is discovered, it should first be added to the corresponding simulator. Then the breaking behavior should be exhibited with the simulator and the relevant `cgsn-mooring` driver, and then fixed in the `cgsn-mooring` driver. 
 - replicate the physical interface sufficiently so that the `cgsn-mooring` code is not using a different configuration when running against the simulator versus the real hardware. This means, in most cases, using a virtual serial interface (`pty`) set up by `socat` or other means.

### MPIC Simulator

The MPIC Simulator replicates the functionality of the Master PIC as used by the [cgsn_supervisor](doc_25-supervisor.md).

To run:

- Start a pair of virtual serial ports (pseudoterminals)

        sudo socat pty,link=/dev/dcl-mpic,raw,echo=0,user=$USER pty,link=/tmp/dcl-mpic-out,raw,echo=0,user=$USER

- Launch the simulator

        cgsn_simulator_mpic --supervisor_serial 'port: "/tmp/dcl-mpic-out" baud: 38400' --type DCL

- Launch the supervisor

        cgsn_supervisor <(cgsn_config_tool --action CONVERT_YAML_TO_PROTOBUF --app_type SUPERVISOR --protobuf_format TEXT_FORMAT)

