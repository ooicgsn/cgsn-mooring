## CGSN Mooring Software Overview

This documentation is for the software running on the CGSN Moorings (started at version 3.0.0 and newer).

### Getting Started

 * [Building](doc_01-build.md): Instructions for installing dependencies and compiling `cgsn-mooring`.
 * [Filesystem Hierarchy](doc_05-filesystem-hierarchy.md): Overview of what lives where in this repository.

### Continuous Integration / Testing

 * [Testing](doc_15-testing.md): Testing methodology and framework.
 * [Packaging](doc_11-packaging.md): Deployment-ready Debian packages.
 * [CircleCI](doc_10-circleci.md): Continuous Integration setup that builds, packages, and tests every push.

### Deployment

 * [Embedded ARM Filesystem Image](doc_02-embedded-image.md): Creating and deploying the software components to boot the embedded system (e.g. Raspberry Pi).
 * [Systemd Init](doc_03-systemd.md): Automatic launching of processes at boot and flexible start/top management using the `systemd` init manager.

### Writing a new driver

 * [Sensor Driver Prototype](doc_20-sensor-driver-prototype.md): Example driver (for the Garmin18X GPS) and skeleton code (`driver_pattern`) that can be automatically used (via script) for creating a driver for a new sensor.

### Implemented drivers

 * [driver-pattern](doc_21-driver-pattern.md): Pattern or "template" driver: the minimal driver code that will compile, used as a starting point for new drivers.
 * [phsen_pco2w-sunburst](doc_21-phsen_pco2w-sunburst.md): Driver for the phsen_pco2w-sunburst.
 * [velpt-nortek](doc_21-velpt-nortek.md): Driver for the velpt-nortek.
 * [adcp-rdi](doc_21-adcp-rdi.md): Driver for the adcp-rdi.
 * [ctd-sbe](doc_21-ctd-sbe.md): Driver for the SBE 16plus V2 SeaCAT Conductivity, Temperature and Pressure (Depth) instrument.
 * [gps-garmin18x](doc_21-gps-garmin18x.md): Driver for the Garmin 18x GPS. For reference when writing new drivers. Not intended to be used in deployments.
 * [velpt-nortek](doc_21-velpt-nortek.md): Driver for the Nortek Aquadopp (VELPT).
 * [dosta-optode](doc_21-dosta-optode.md): Driver for the DOSTA Optode.
 * [presf-sbe](doc_21-presf-sbe.md): Driver for the presf-sbe.

### System Control

 * [Supervisor](doc_25-supervisor.md): Interfaces to the power microcontroller subsystem (Master PIC, and indirectly Channel PICs)
 * [Scheduler](doc_26-scheduler.md): Starts and stops logging of the sensor drivers based on several schedule formats.

### Health and Status

 * [System Health](doc_27-system-health.md): Aggregates the health of all the system modules

### Configuration

 * [Configuration](doc_23-config.md): Explains the YAML configuration and how it is converted to the Protobuf configuration used by the `cgsn-mooring` software.

### Logging

  * [Logging](doc_30-logger.md): Explains the logging tools available in `cgsn-mooring`.

### Goby Underwater Autonomy Project Version 3

  * [Goby3](doc_06-goby3.md): Primer on concepts used by the Goby3 framework on which this project is built
