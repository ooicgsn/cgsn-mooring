## ADCP RDI Driver

This driver is for Teledyne RD Instrument's WorkHorse Broadband Acoustic Doppler Current Profiler (ADCP).  

  - Datasheet: [WorkHorse Commands and Output Data Format](https://drive.google.com/open?id=1F5jWTnliYuwUcvp114klobKxKMfwAAzo)
  - Firmware version(s) tested: _Version 50.40_
  - Original author(s):  Charlie Carnevale <ccarnevale@seacorp.com>, Toby Schneider <toby@gobysoft.org>

### Configuration

Configuration Message: cgsn::protobuf::ADCP_RDI_SensorConfig (defined in cgsn-mooring/config/adcp_rdi_config.proto)

The ADCP-RDI has two configuration phases. The first phase is called `TimePhase` which sets the following setting one time when the data logger is started:  

`TS<DCL current time>`

The second phase is called the `DeployPhase` which sets the following settings one time when the data logger is started:

 ADCP_RDI_SensorConfig field  |  value (example)             |  ADCP Command Format Equivalent (example)
-----------------------------|-------------------------|----------------------------
transducer_depth			 | N (01320)			   | `EDN<CR><LF>` (`ED01320<CR><LF>`)
salinity 		  			 | N (36)                  | `ESN<CR><LF>` (`ESN36<CR><LF>`)
time_per_ensemble            | string (00:01:00.00)    | `TE<string><CR><LF>` (`TE00:01:00.00<CR><LF>`)
time_between_pings           | string (00:03.00)       | `TP<string><CR><LF>` (`TP00:03.00<CR><LF>`)
model_bandwith_control       | N (0)                   | `WBN<CR><LF>` (`WB0<CR><LF>`)
num_depth_cells              | N (036)                 | `WNN<CR><LF>` (`WN036<CR><LF>`)
pings_per_ensemble           | N (00005)               | `WPN<CR><LF>` (`WP00005<CR><LF>`) 
depth_cell_size              | N (0402)                | `WSN<CR><LF>` (`WS0402<CR><LF>`)

### Sensor Command Statechart


#### Initialize child states

![](../figures/adcp_sensor_command_initialize.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/adcp_sensor_command_initialize.png}
\endlatexonly

#### On child states

![](../figures/adcp_sensor_command_on.png)

\latexonly
\includegraphics[width=0.8\textwidth]{../figures/adcp_sensor_command_on.png}
\endlatexonly

#### Off child states

![](../figures/adcp_sensor_command_off.png)

\latexonly
\includegraphics[width=0.5\textwidth]{../figures/adcp_sensor_command_off.png}
\endlatexonly


### Quirks

* If using Minicom to send break to wake up instrument use Ctrl-A Shift-F (Not END key)
	* Python PySerial send_break() works for wake up as well  

* `AC` to display active calibration  

* `CZ` to power down  

* `>` is command prompt  

* A break will cause the ADCP to stop pinging and the deployment will be interrupted  

* `deploy` prints the current configuration, although it is missing the following settings that we set: CQ, PD, WA, WB, TG  

* Any command followed by `?` will return the value of that setting, e.g., `CQ?` will return CQ setting  

* Responds with `Device #0 does not have any free directory entries!\r\n` when the sensor runs out of recorder space and will no longer function properly; may want to send `RI0` to force recorder to append to  existing deployment file rather than creating a new one every time  

* `RS` displays Recorder Space used/free (mb)  