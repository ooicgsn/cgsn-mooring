## Debian Packaging 


Debian packages provide a uniform way to install, upgrade, and uninstall `cgsn-mooring` on Ubuntu and Debian systems using the standard `apt` and `dpkg` tools.
See [CircleCI](doc_10-circleci.md) for information regarding *.deb packages built by CircleCI.

### Versioning scheme

The upstream versioning scheme used by the `cgsn-mooring` on CircleCI is

     {last-tag}+{revs-since-tag-if-nonzero}+g{git-hash}

where `{last-tag}` has all `_` converted to `~` to allow Debian pre-versions even though `~` is a disallowed tag character in Git. This is derived from the output of `git describe --tags HEAD` with all `-` converted to `+`.

For example, if the current commit (HEAD) is tagged, the version is simply that tag (with `_` converted to `~`):

     tag (HEAD) -> 3.0.0_beta1
     version -> 3.0.0~beta1

If the current commit is two revisions since the tag, it might look more like this:

     tag (HEAD^^) -> 3.0.0_beta1
     version -> 3.0.0~beta1+2+ge393bc

Thus, to get clean versions without the git hash (e.g. `3.0.0~beta1`, `3.0.1`, etc), simply tag the relevant releases. For more detail on Debian versioning, see [https://www.debian.org/doc/debian-policy/#version](https://www.debian.org/doc/debian-policy/#version)

### Build environment

The build environment for the Debian package build is set in the [rules](https://bitbucket.org/ooicgsn/cgsn-mooring-debian/src/master/rules) file using the [CDBS CMake class](http://cdbs-doc.duckcorp.org/en/cdbs-doc.xhtml#id468422).

`DEB_CMAKE_EXTRA_FLAGS` are passed to `CMake`.

### Building with a local Docker container

To build and package closest to how things work in CircleCI, see [the relevant docs](doc_10-circleci.md).

## _Everything that follows is for reference only, and is unlikely to be needed in daily usage. It may be helpful if one needs to recreate the CircleCI setup (or similar) from scratch_.

### Debian Packaging Setup

#### Standard sbuild Setup (Debian official workflow, not our CircleCI workflow)

While CircleCI creates automatic releases, we can also manually create a new release and build it using the Debian official `sbuild` workflow.

Environmental variables DEBEMAIL and DEBFULLNAME can be set in ~/.bashrc to default to your correct name and email for `dch`. For example:

    export DEBFULLNAME="Toby Schneider"
    export DEBEMAIL="tes.aubergine@gmail.com"

If you wish to build packages locally (presumably on your amd64 architecture machine), you will need to install `sbuild`:

* If you have never run `sbuild` before on this machine

        sudo apt install sbuild
        mk-sbuild bionic

     Then, log out and back in again to set the `sbuild` group. Edit `~/.sbuildrc` if desired, for example to change the output directory for the Debian packages (`*.deb`) which defaults to `$HOME/ubuntu/build` on Ubuntu 18.04. You can also add the following to `~/.sbuildrc` to run multiple cores during a build:

        $build_environment = {
          'DEB_BUILD_OPTIONS' => 'parallel=4'
        };
        
* Create the `sbuild` chroots (for native and armhf-crossbuild, respectively):

        mk-sbuild bionic
        mk-sbuild --target=armhf bionic

* Get the key from the Launchpad PPA for DCCL/Goby dependencies and add the Clang C/C++ compiler:

        sudo schroot -c source:bionic-amd64 -u root
        gpg --keyserver keyserver.ubuntu.com --recv FE54CBF4
        gpg --export --armour 'FE54CBF4' | apt-key add
        apt install clang

        sudo schroot -c source:bionic-amd64-armhf -u root
        gpg --keyserver keyserver.ubuntu.com --recv FE54CBF4
        gpg --export --armour 'FE54CBF4' | apt-key add
        apt install clang

* (optional) to speed up builds, mount the sbuild build dir as tmpfs by adding to `/etc/fstab`

        none    /var/lib/schroot/union/overlay  tmpfs   size=3000M,uid=root,gid=root,mode=0750 0 0
        
* (optional) to speed up apt-get in builds, add 

        sudo apt install apt-cacher-ng
        echo 'Acquire::http::Proxy "http://127.0.0.1:3142";' | sudo tee /etc/apt/apt.conf.d/01acng


#### Manual Process (with sbuild)

* Merge all relevant pull requests into version branch (e.g. 3.x)
* Check that the CircleCI build completes successfully.
* Update version, release date (and soversion, if relevant) in `cmake_modules/CGSNVersion.cmake`
* Commit and push updated version, etc.
* Tag the release

        TAG=3.0.0_alpha3
        git tag $TAG
        git push --tags

* Grab the source tarball for the release using `git archive`:

        RELEASE=$(echo $TAG | sed 's/_/~/')
        git archive --prefix=cgsn-mooring-$RELEASE/ -o cgsn-mooring-$RELEASE.tar.gz $TAG

* Build the Debian source package

        mv cgsn-mooring-$RELEASE.tar.gz ~/deb/src/cgsn-mooring_$RELEASE.orig.tar.gz
        cd ~/deb/src
        tar xfz cgsn-mooring_$RELEASE.orig.tar.gz
        cd cgsn-mooring-$RELEASE

* (First time only): see "Creating the Debian Packaging source", below
* Clone the Debian packaging source

        git clone git@bitbucket.org:ooicgsn/cgsn-mooring-debian.git -b master debian

* Add a new entry to `changelog`

        cd debian
        dch --newversion=$RELEASE-0~ubuntu18.04.1 -D bionic "Brief description of change"

* Create the Debian source package using `debuild -S`. `-sa` indicates include the full original tarball when uploading with `dput` (e.g. to a PPA). This is only required for the first upload of a new release version.

        debuild -S -sa

* (optional) Edit changelog for other Ubuntu or Debian releases. Here we use `-sd` to avoid uploading the original tarball again when we run `dput` (later), for example Ubuntu 18.04 (bionic).
 
        emacs changelog
        # change release version (e.g. 18.04) and release distro codename (e.g., bionic)
        debuild -S -sd

* (optional) Build the package locally using `sbuild` (the official Debian builder), which is written to `$HOME/ubuntu/build` by default (change in `~/.sbuildrc`)

        cd ../..
        sbuild -A -d bionic-amd64 --extra-repository='deb http://ppa.launchpad.net/tes/cgsn/ubuntu bionic main' --build-dep-resolver=aptitude cgsn-mooring_$RELEASE*.dsc
        sbuild -A -d bionic-amd64-armhf --host armhf --extra-repository='deb http://ppa.launchpad.net/tes/cgsn/ubuntu bionic main' --build-dep-resolver=aptitude cgsn-mooring_$RELEASE*.dsc

* Upload to PPA (as needed)

        dput ppa:tes/cgsn cgsn-mooring_$RELEASE*ubuntu18.04*.changes


#### Creating the Debian packaging source

This only needs to happen once. These instructions are here for future reference, as needed.

 - The first step is to ensure the `make install` target is functional. This is done in CMake with various `install()` directives.
 - Create the initial source packaging directory:

        cd cgsn-mooring-3.0.0~alpha2
        dh_make

 - Edit `debian` files as needed. See [https://www.debian.org/doc/manuals/maint-guide/](https://www.debian.org/doc/manuals/maint-guide/).
 - Initialize and push the Git repo
 
        git init .
        git add .
        git commit -m "Initial commit"
        git remote add origin git@bitbucket.org:ooicgsn/cgsn-mooring-debian.git
        git push -u origin master
        
