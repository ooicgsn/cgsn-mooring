## Logging

`cgsn-mooring` contains a number of tools and services for storing, transmitting, and processing data logs and interprocess messages.


### Goby Logs

All interprocess messages are captured by the `goby_logger` daemon and written to out to disk in a binary log format.

The log directory is configured in `mooring.yml` under the `logger` key. Files are named `<hostname>_<timestamp>.goby`, based on the time that `goby_logger` started. The log file is not rotated.

When using the Protobuf marshalling `scheme` in Goby3, this format uses the
standard encoding for Google Protocol Buffers and wraps each message with
metadata for scheme (numeric ID for the scheme), group (e.g.,
`cgsn::groups::sensor::driver_status`) and type (i.e., `Message full_name()` in Protobuf).

In addition, each message is protected by a CRC16 and a 4-byte magic word
(`GBY3` in ASCII) is prepended to allow seeking past corruption.

This `cgsn_log_tool` can converts a `*.goby` file to a text file:

    export CGSN_LOG=default_goby_platform_20180319T141038.goby
    cgsn_log_tool --input_file=${CGSN_LOG} --output_file=${CGSN_LOG}.txt --format LOG_FORMAT_DEBUG_TEXT

The `--format` option specifies the desired output format. The default, `LOG_FORMAT_DEBUG_TEXT`, is designed to aid in development and
debugging. It can be easily fed through `grep` to filter on specific messages.
Each output line follows the format

```
group | type | Protobuf Message DebugString() [aka TextFormat]
```

An example of this file is

```
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468694000089 sensor_command_state: OFF__STANDBY validator_state: NOTLOGGING__STANDBY
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468695000117 sensor_command_state: OFF__STANDBY validator_state: NOTLOGGING__STANDBY
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468696000114 sensor_command_state: OFF__STANDBY validator_state: NOTLOGGING__STANDBY
cgsn::gps::control | cgsn.protobuf.GPSCommand | read_gps: true
cgsn::gps::raw::out | cgsn.protobuf.SensorRaw | logger_time: 1521468696238440 raw_data: "$PGRMO,GPRMC,1*3D\r\n"
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468696670435 raw_data: "$GPRMC,141136,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*79\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468696670435 raw_data: "$GPRMC,141136,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*79\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468696670435 sensor_time: 1521468696000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468696710155 raw_data: "$PGRMO,GPRMC,1*3D\r\n"
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468697000105 sensor_command_state: ON__LOGGING validator_state: LOGGING__INITIALIZING
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468697664180 raw_data: "$GPRMC,141137,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*78\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468697664180 raw_data: "$GPRMC,141137,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*78\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468697664180 sensor_time: 1521468697000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468698000110 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468698674553 raw_data: "$GPRMC,141138,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*77\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468698674553 raw_data: "$GPRMC,141138,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*77\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468698674553 sensor_time: 1521468698000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468699000076 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468699664505 raw_data: "$GPRMC,141139,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*76\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468699664505 raw_data: "$GPRMC,141139,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*76\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468699664505 sensor_time: 1521468699000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468700000117 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468700670547 raw_data: "$GPRMC,141140,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*78\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468700670547 raw_data: "$GPRMC,141140,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*78\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468700670547 sensor_time: 1521468700000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468701000114 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468701667604 raw_data: "$GPRMC,141141,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*79\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468701667604 raw_data: "$GPRMC,141141,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*79\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468701667604 sensor_time: 1521468701000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468702000103 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468702657520 raw_data: "$GPRMC,141142,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*7A\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468702657520 raw_data: "$GPRMC,141142,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*7A\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468702657520 sensor_time: 1521468702000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
cgsn::health | cgsn.protobuf.DriverStatus | type: GPS_GARMIN18X logger_time: 1521468703000077 sensor_command_state: ON__LOGGING validator_state: LOGGING__FUNCTIONING__PARSED_DATA_VALID
cgsn::gps::raw::in | cgsn.protobuf.SensorRaw | logger_time: 1521468703664492 raw_data: "$GPRMC,141143,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*7B\r\n"
cgsn::gps::raw::in_validated | cgsn.protobuf.SensorRaw | logger_time: 1521468703664492 raw_data: "$GPRMC,141143,A,4131.4831,N,07040.2585,W,000.0,000.0,190318,014.8,W*7B\r\n"
cgsn::gps::parsed | cgsn.protobuf.GPSPosition | logger_time: 1521468703664492 sensor_time: 1521468703000000 fix_valid: true latitude: 41.52471833333334 longitude: -70.670975
```


### Legacy Logs

Existing OOI infrastructure works with textual log files:

```
2018/10/28 15:58:06.722 [ctdbp2:DLOGP3]:Logger started: Idle state, without initialize
2018/10/28 15:58:14.725 [ctdbp2:DLOGP3]:Battery on - port power on
2018/10/28 16:00:04.342 [ctdbp2:DLOGP3]:time out
2018/10/28 16:00:08.496 # 14.4499,  0.00010,   -0.095, 28 Oct 2018 16:00:06<CR>
2018/10/28 16:03:00.610 [ctdbp2:DLOGP3]:Battery on - port power off
2018/10/28 16:14:01.097 [ctdbp2:DLOGP3]:Battery on - stop
```

On the mooring, these files are stored in `/media/data/legacy/cg_data/<instrument>/<datestamp>.<instrument>.log`. Note that on `platcon_sw` moorings, the directory structure is `/data/cg_data/...`.
  
In `cgsn-mooring`, a new instrument naming convention is used, so we refer to the instrument identifier used for logging purposes as the "legacy name". In `master.yml`, the key for this is `legacy_name`.

The `cgsn_legacy_logger` service listens to raw messages published to any `:&zwj;:raw:&zwj;:in_validated` <!-- Sorry, it's a Doxygen thing --> group by a specific instrument (filtered by `instrument_key`) and writes these out to textual logs. The log files are rolled over at midnight. There is one logger service listening for each configured instrument.

A Goby3 log file can also be batch-converted to the legacy format using the `cgsn_log_tool`:

```
cgsn_log_tool --input_file=${CGSN_LOG} --output_file=${CGSN_LOG}.log --format LOG_FORMAT_LEGACY_RAW
```


### Hierarchical Data Format 5 (HDF5)

Given a `*.goby` log file, we can convert this to HDF5
(.h5) using the `cgsn-mooring` plugin paired with the binary tool from Goby3
(`goby_hdf5`).

    export GOBY_HDF5_PLUGIN="$(pwd)"/build/lib/libcgsn_hdf5_plugin.so
    export CGSN_LOG=default_goby_platform_20180319T141038.goby
    goby_hdf5 --input_file=${CGSN_LOG} --output_file=${CGSN_LOG}.h5

Now you can open the `${CGSN_LOG}.h5` file using MATLAB, Octave, Python, C++,
etc. For example, in Octave, you can simply run

    load default_goby_platform_20180319T141038.goby.h5

and the entire file is loaded as structs (based on group name) with the pattern
`group.type.field,` for example
`cgsn__gps__parsed.cgsn_protobuf_GPSPosition.latitude` provides a vector of
latitudes with respect to the time given in
`cgsn__gps__parsed.cgsn_protobuf_GPSPosition._utime_`.
