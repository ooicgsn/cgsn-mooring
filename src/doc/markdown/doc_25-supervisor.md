## Supervisor

The supervisor (cgsn::Supervisor) is the interface between the sensor drivers and the power control system (Master PIC, "MPIC" and Channel PICs "CPIC"s).

All the protobuf types used by the supervisor are defined in mpic.proto.

The currently implemented commands to the MPIC are all triggered from a single command (cgsn::protobuf::MPICCommand) from each sensor driver:

| MPIC Command | Description | Protobuf Type | Subscription |
|--------------|-------------|---------------|--------------|
| `get_pcfg`   | Get Port power configuration (used to query configuration between sending `set_pcfg` if necessary) |cgsn::protobuf::MPICCommand | cgsn::groups::supervisor::mpic_command |
| `set_pcfg`   | Set Port power configuration (only if desired configuration differs from actual) |cgsn::protobuf::MPICCommand | cgsn::groups::supervisor::mpic_command |
| `pon`        | Turn Port on                 |cgsn::protobuf::MPICCommand | cgsn::groups::supervisor::mpic_command |
| `poff`       | Turn Port off                |cgsn::protobuf::MPICCommand | cgsn::groups::supervisor::mpic_command |

The responses parsed from the MPIC are:

| MPIC Response | Description | Protobuf Type | Publication |
|---------------|-------------|---------------|-------------|
| `dcl:`      | DCL status message (includes Port power state) | cgsn::protobuf::SupervisorStatus | cgsn::groups::supervisor::status |
| `get_pcfg:` | Response to `get_pcfg` command | cgsn::protobuf::PowerConfig | N/A (used to determine if `set_pcfg` is necessary from the values requested in cgsn::protobuf::MPICCommand |
| `pon: `     | Response to `pon` command | N/A | N/A (Sensor drivers read `dcl:` message to determine port state, and retry as needed) |
| `poff: `    | Response to `pon` command | N/A | N/A (Sensor drivers read `dcl:` message to determine port state) |

The following shows how the supervisor fits in the CGSN software system in relation to one of the sensor drivers (all the drivers interact the same since the `cgsn_supervisor` interfaces are handled by cgsn::DriverBase). `gobyd` has been omitted for clarity.

![](../graphviz/supervisor.png)

\latexonly
\includegraphics[width=0.6\textwidth]{../graphviz/supervisor.png}
\endlatexonly

