## Scheduler

The scheduler (cgsn::Scheduler) is responsible for maintaining the sampling schedule of the sensors. In addition, it is a first-level watchdog on the sensor drivers in the event a sensor driver fails to turn a port off after being commanded to do so.

The protobuf types used for publishing by the scheduler are defined in scheduler.proto. The schedules are defined using the config in scheduler_config.proto.

The scheduler supports three types of schedule syntaxes: 

 1. the new Protobuf based syntax;
 2. conversion from the legacy interval ("isched") string format.
 3. conversion from the legacy hourly ("sched") string format.

### Protobuf Schedule format

The following types of schedules are supported:

 * ALWAYS_ON: The sensor is always on.
 * ALWAYS_OFF: The sensor is always off (disabled).
 * HOURLY: The sensor is turned on selected hours of the day for a certain duration.
 * INTERVAL: The sensor is turned on and off at a regular interval starting from a reference time, potentially decoupled from the hour and day (depending on the interval and the reference). If the reference is midnight UTC, and the interval is 60 minutes, this is the same as an HOURLY schedule that has all the hours defined (0-23).

#### ALWAYS_ON

```
schedule { 
  port_id: 1             # 1-8
  sensor_type: CTD_SBE   # see sensor_types.proto 
  schedule_type: ALWAYS_ON
}
```

#### ALWAYS_OFF

```
schedule {
  port_id: 1             # 1-8
  sensor_type: CTD_SBE   # see sensor_types.proto
  schedule_type: ALWAYS_OFF
}
```

#### HOURLY

```
schedule {
  port_id: 1             # 1-8
  sensor_type: CTD_SBE   # see sensor_types.proto
  schedule_type: HOURLY
  offset: 10             # minutes after the hour to start the schedule (optional)
  duration: 30           # duration (in minutes) between start and stop sampling
  force_stop_delay: 30   # seconds following "stop sampling" commanding off to force the port off if it is still on
  slot_buffer_time: 60   # required seconds between one slot stop and the next slot start
  hour: [4,10,11,13]     # list of hours of the day (UTC) to enable the sensor
}
```
#### INTERVAL

```
schedule {
  port_id: 1             # 1-8
  sensor_type: CTD_SBE   # see sensor_types.proto
  schedule_type: INTERVAL
  offset: 5              # minutes after the schedule start time to start the schedule (optional)
  force_stop_delay: 30   # seconds following "stop sampling" commanding off to force the port off if it is still on
  slot_buffer_time: 60   # required seconds between one slot stop and the next slot start
  interval: 30           # minutes between intervals
  duration: 10           # duration (in minutes) between start and stop sampling
  reference_time: 1262476800000000  # the time (in microseconds since UNIX) of the first slot start. This can be used to align intervals with another reference source. (optional). The default is 1262476800000000 = Sunday 1/3/2010 at 00:00:00 UTC (start of day & start of week).
}
```

### Legacy formats

All legacy formats with the first field disabled (set to 0) are converted to `ALWAYS_OFF` schedules. Otherwise, they are converted to an HOURLY or INTERVAL schedule as specified below.

#### sched

The legacy hour format ("sched") matches the regex:


    ^[01]:(([0-9]+|[0-9]+-[0-9]+),)*([0-9]+|[0-9]+-[0-9]+):[0-9]+:[0-9]+$


The four fields are colon-delimited with the following contents:

  1. [enable/disable (bool: 0,1)]
  2. [enabled hours since day start (int, single or hyphen-delimited range, comma-delimited)]
  3. [offset (int, minutes)]
  4. [duration (int, minutes)]

For example, "1:2,5-10:0:30" would be enabled for hours 2,5,6,7,8,9,10 with an offset of 0 minutes for 30 minutes.

This example converts to the Protobuf format:

```
schedule {
   schedule_type: HOURLY
   duration: 30
   offset: 0
   hour: [2,5,6,7,8,9,10]
}
```

#### isched

The legacy interval schedule format ("isched" matchs the regex:


    ^[01]:[0-9]+:[0-9]+:[0-9]+$


The four fields are colon-delimited with the following contents:

  1. [enable/disable (bool: 0,1)]
  2. [interval (int, minutes)]
  3. [offset (int, minutes)]
  4. [duration (int, minutes)]

For example, "1:15:1:5" would enable the port every 15 minutes with an offset of 1 minute for 5 minutes.

In Protobuf format, this example would read:

```
schedule {
  schedule_type: INTERVAL
  duration: 5
  offset: 1
  interval: 15
}
```


### Schedule Validation

At launch time, the scheduler checks all schedules for most syntactical and logical errors. The legacy schedules are checked using a regex and then converted into a Protobuf formatted schedule. Then each Protobuf schedule is checked, and if an error is found, it is noted in the log the scheduler quits. The possible errors are enumerated as cgsn::protobuf::ScheduleError and are defined in scheduler_config.proto.

If all schedules pass validation, the scheduler begins operation.

### Schedule Processing

For each port, the scheduler follows this basic progression (handled as a set of timers for each port):


![](../graphviz/scheduler-timers.png)

\latexonly
\includegraphics[width=0.6\textwidth]{../graphviz/scheduler-timers.png}
\endlatexonly

### Scheduler Publish/Subscribe

The following shows how the scheduler fits in the CGSN software system with several example sensor drivers. The interaction with the supervisor is only in the event that one of the sensor drivers fails to stop within the configured `force_stop_delay`. gobyd has been omitted for clarity.

![](../graphviz/scheduler.png)

\latexonly
\includegraphics[width=1\textwidth]{../graphviz/scheduler.png}
\endlatexonly
