// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE presf_sbe-validator-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/presf_sbe_config.pb.h"
#include "cgsn-mooring/sensor/presf_sbe/presf_sbe_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::PRESF_SBE_ValidatorThread,
                                                    cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE (test_raw_valid, TestFixture)
{
    check_raw("tide: start time = 28 May 2018 18:11:58, p = 13.9815, pt = 22.053, t = 21.9058\r\n", true);
    check_raw("tide: start time = 6 May 2018 18:11:58, p = 13.9815, pt = 22.053, t = 21.9058\r\n", true);
    check_raw("tide: start time = 31 Dec 2018 04:00:00, p = -113.4, pt = 15.0, t = -2.000\r\n", true);

}

BOOST_FIXTURE_TEST_CASE (test_raw_invalid, TestFixture)
{
    // invalid non-ascii data
    for(int i = 0; i < 31; ++i)
    {
      auto invalid_test_str =  std::string("tide: start time = 6 May 2018 18:11:58, p = 13.9816, pt = 22.053, t = 21.9055") + std::string(1, i);
      // do the test
      check_raw(invalid_test_str, false);
    }

    // invalid field ("d")
    check_raw("tide: start time = 6 May 2018 18:11:58, d = 13.9815, pt = 22.053, t = 21.9058\r\n", false);


    check_raw("S>\r\n", false);
    check_raw("logging will start", false);
    check_raw("tide measurement", false);
}

BOOST_FIXTURE_TEST_CASE (test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::PRESF_SBE_Data pos;
//      check_parsed(pos, true);
    }
}

BOOST_FIXTURE_TEST_CASE (test_parsed_invalid, TestFixture)
{
    {
        cgsn::protobuf::PRESF_SBE_Data pos;
//        check_parsed(pos, false);
    }
}
