// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE nortek_message_test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/serial/nortek_data_message.h"

struct TestFixture
{
    TestFixture()
        // from 20170911.velpt2.log
        : bytes_({0xa5, 0x01, 0x15, 0x00, 0x00, 0x00, 0x11, 0x00, 0x17, 0x09, 0x00,
                  0x00, 0x00, 0x00, 0x84, 0x00, 0x83, 0x3b, 0x48, 0x06, 0xf3, 0xff,
                  0xff, 0xff, 0x00, 0x31, 0x65, 0x05, 0x47, 0x08, 0xa7, 0xff, 0x61,
                  0xff, 0x3e, 0x00, 0xa7, 0x96, 0xa5, 0x00, 0xed, 0xd7}),
          msg_(bytes_)
    {
    }

    ~TestFixture() {}

    std::vector<std::uint8_t> bytes_;
    cgsn::io::NortekDataMessage msg_;
};

BOOST_FIXTURE_TEST_CASE(check_components, TestFixture)
{
    const auto sync_word = cgsn::io::NortekDataMessage::nortek_sync;
    BOOST_CHECK_EQUAL(msg_.sync(), sync_word);
    BOOST_CHECK_EQUAL(msg_.id(), 0x01);
    BOOST_CHECK_EQUAL(msg_.size_words(),
                      bytes_.size() / cgsn::io::NortekDataMessage::bytes_in_word);
    BOOST_CHECK_EQUAL(msg_.size_bytes(), bytes_.size());
    BOOST_CHECK_EQUAL(msg_.reported_checksum(), 0xd7ed);

    auto body = msg_.body();
    BOOST_CHECK_EQUAL(body.size(), bytes_.size() - cgsn::io::NortekDataMessage::sync_field_size -
                                       cgsn::io::NortekDataMessage::id_field_size -
                                       cgsn::io::NortekDataMessage::size_field_size -
                                       cgsn::io::NortekDataMessage::checksum_field_size);
    BOOST_CHECK_EQUAL(body[2], 0x11);
    BOOST_CHECK_EQUAL(body[4], 0x17);
}

BOOST_FIXTURE_TEST_CASE(check_checksum, TestFixture)
{
    BOOST_CHECK_EQUAL(msg_.reported_checksum(), msg_.calculated_checksum());
    BOOST_CHECK(msg_.is_valid());
}

BOOST_AUTO_TEST_CASE(check_bounds)
{
    std::size_t too_small_size = 6 - cgsn::io::NortekDataMessage::bytes_in_word;
    BOOST_CHECK_THROW(cgsn::io::NortekDataMessage(std::string(too_small_size, 0x00)),
                      std::out_of_range);

    // 16-bit size field in words
    std::size_t too_large_size = (0xFFFF * cgsn::io::NortekDataMessage::bytes_in_word) +
                                 cgsn::io::NortekDataMessage::bytes_in_word;
    BOOST_CHECK_THROW(cgsn::io::NortekDataMessage(std::string(too_large_size, 0x00)),
                      std::out_of_range);

    std::size_t odd_size = 11;
    BOOST_CHECK_THROW(cgsn::io::NortekDataMessage(std::string(odd_size, 0x00)), std::out_of_range);
}
