// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE velpt_nortek - validator - test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/velpt_nortek_config.pb.h"
#include "cgsn-mooring/sensor/validator_test_fixture.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"
#include "cgsn-mooring/sensor/velpt_nortek/velpt_nortek_validator.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::VELPT_NORTEK_ValidatorThread,
                                                     cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE(test_raw_valid, TestFixture)
{
    // from 20170911.velpt2.log
    std::vector<unsigned char> bytes(
        {0xa5, 0x01, 0x15, 0x00, 0x00, 0x00, 0x11, 0x00, 0x17, 0x09, 0x00, 0x00, 0x00, 0x00,
         0x84, 0x00, 0x83, 0x3b, 0x48, 0x06, 0xf3, 0xff, 0xff, 0xff, 0x00, 0x31, 0x65, 0x05,
         0x47, 0x08, 0xa7, 0xff, 0x61, 0xff, 0x3e, 0x00, 0xa7, 0x96, 0xa5, 0x00, 0xed, 0xd7});

    check_raw(std::string(bytes.begin(), bytes.end()), true);
}

BOOST_FIXTURE_TEST_CASE(test_raw_invalid, TestFixture)
{
    std::vector<unsigned char> bytes(
        {0xa5, 0x01, 0x15, 0x00, 0x00, 0x00, 0x11, 0x00, 0x17, 0x09, 0x00, 0x00, 0x00, 0x00,
         0x84, 0x00, 0x83, 0x3b, 0x48, 0x06, 0xf3, 0xff, 0xff, 0xff, 0x00, 0x31, 0x65, 0x05,
         0x47, 0x08, 0xa7, 0xff, 0x61, 0xff, 0x3e, 0x00, 0xa7, 0x96, 0xa5, 0x00, 0xed, 0x00});

    // wrong checksum
    check_raw(std::string(bytes.begin(), bytes.end()), false);
}

BOOST_FIXTURE_TEST_CASE(test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::VELPT_NORTEK_Data pos;
        //      check_parsed(pos, true);
    }
}

BOOST_FIXTURE_TEST_CASE(test_parsed_invalid, TestFixture)
{
    {
        cgsn::protobuf::VELPT_NORTEK_Data pos;
        //        check_parsed(pos, false);
    }
}
