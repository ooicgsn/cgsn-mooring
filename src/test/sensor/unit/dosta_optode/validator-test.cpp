// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE dosta_optode-validator-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/dosta_optode_config.pb.h"
#include "cgsn-mooring/sensor/dosta_optode/dosta_optode_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::DOSTA_OPTODE_ValidatorThread,
                                                    cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE (test_raw_valid, TestFixture)
{
    // normal sample
    check_raw("4831\t463\t263.671\t99.920\t23.776\t29.841\t29.841\t38.200\t8.359\t982.5\t880.8\t119.0\r\n", true);
    // normal sample with negative values
    check_raw("4831\t463\t263.671\t99.920\t23.776\t29.841\t29.841\t38.200\t-8.359\t982.5\t-880.8\t119.0\r\n", true);
}

BOOST_FIXTURE_TEST_CASE (test_raw_invalid, TestFixture)
{

    // junk at front
    check_raw("#4831\t463\t263.671\t99.920\t23.776\t29.841\t29.841\t38.200\t-8.359\t982.5\t-880.8\t119.0\r\n", false);

    // missing value
    check_raw("4831\t463\t\t99.920\t23.776\t29.841\t29.841\t38.200\t-8.359\t982.5\t-880.8\t119.0\r\n", false);

    check_raw("\r\n", false);
    check_raw("Product number: ", false);
    check_raw("!", false);
    check_raw("%", false);
    check_raw("Enable Decimalformat\t4831\t463\tYes\r\n", false);

}

BOOST_FIXTURE_TEST_CASE (test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::DOSTA_OPTODE_Data pos;
//      check_parsed(pos, true);
    }
}

BOOST_FIXTURE_TEST_CASE (test_parsed_invalid, TestFixture)
{
    {
        cgsn::protobuf::DOSTA_OPTODE_Data pos;
//        check_parsed(pos, false);
    }
}
