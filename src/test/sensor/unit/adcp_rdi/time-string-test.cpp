// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE adcp-rdi-time-string-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_parse.h"

BOOST_AUTO_TEST_CASE(valid_time_per_ensemble)
{
    {
        auto t = cgsn::sensor_internal::time_per_ensemble("10:00:00.00");
        BOOST_CHECK_EQUAL(t/boost::units::si::seconds, 10*3600);
    }

    {
        auto t = cgsn::sensor_internal::time_per_ensemble("00:15:00.00");
        BOOST_CHECK_EQUAL(t/boost::units::si::seconds, 15*60);
    }

    {
        auto t = cgsn::sensor_internal::time_per_ensemble("00:06:12.45");
        BOOST_CHECK_EQUAL(t/boost::units::si::seconds, 6*60+12.45);
    }


}



BOOST_AUTO_TEST_CASE(invalid_time_per_ensemble)
{
    BOOST_CHECK_THROW(cgsn::sensor_internal::time_per_ensemble("0:06:12.45"),
                      cgsn::InvalidSensorConfiguration);
    BOOST_CHECK_THROW(cgsn::sensor_internal::time_per_ensemble("00:06:12"),
                      cgsn::InvalidSensorConfiguration);

    BOOST_CHECK_THROW(cgsn::sensor_internal::time_per_ensemble("40:65:12.00"),
                      cgsn::InvalidSensorConfiguration);
}



BOOST_AUTO_TEST_CASE(valid_time_between_pings)
{
    {
        auto t = cgsn::sensor_internal::time_between_pings("15:00.00");
        BOOST_CHECK_EQUAL(t/boost::units::si::seconds, 15*60);
    }

    {
        auto t = cgsn::sensor_internal::time_between_pings("06:12.45");
        BOOST_CHECK_EQUAL(t/boost::units::si::seconds, 6*60+12.45);
    }
}



BOOST_AUTO_TEST_CASE(invalid_time_between_pings)
{
    BOOST_CHECK_THROW(cgsn::sensor_internal::time_between_pings("0:06:12.45"),
                      cgsn::InvalidSensorConfiguration);

    BOOST_CHECK_THROW(cgsn::sensor_internal::time_between_pings("65:12.00"),
                      cgsn::InvalidSensorConfiguration);
}
