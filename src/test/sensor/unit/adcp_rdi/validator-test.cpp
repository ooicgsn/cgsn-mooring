// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE adcp_rdi-validator-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/adcp_rdi_config.pb.h"
#include "cgsn-mooring/sensor/adcp_rdi/adcp_rdi_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::ADCP_RDI_ValidatorThread,
                                                    cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE (test_raw_valid, TestFixture)
{
    // valid header
    check_raw("2018/02/15 00:00:00.00 00735\r\n", true);
    // valid ensemble line
    check_raw("1     --     --      16    -219     152  -32768     69     61\r\n", true);
}

BOOST_FIXTURE_TEST_CASE (test_raw_invalid, TestFixture)
{
    // invalid non-ascii data
    for(int i = 0; i < 31; ++i)
    {
      auto invalid_test_str =  std::string("1     --     --      16    -219     152  -32768     69     61") + std::string(1, i) + "\r\n";
      // do the test
      check_raw(invalid_test_str, false);
    }

    check_raw("[BREAK Wakeup", false);
    check_raw(">\r\n", false);
    check_raw("PRE_DEPLOYMENT TESTS", false);
}

BOOST_FIXTURE_TEST_CASE (test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::ADCP_RDI_Data pos;
//      check_parsed(pos, true);
    }
}

BOOST_FIXTURE_TEST_CASE (test_parsed_invalid, TestFixture)
{
    {
        cgsn::protobuf::ADCP_RDI_Data pos;
//        check_parsed(pos, false);
    }
}
