// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE phsen_pco2w_sunburst__validator__test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/phsen_pco2w_sunburst_config.pb.h"
#include "cgsn-mooring/sensor/phsen_pco2w_sunburst/phsen_pco2w_sunburst_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture =
    cgsn::test::ValidatorTestFixture<cgsn::validator::PHSEN_PCO2W_SUNBURST_ValidatorThread,
                                     cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE(test_raw_valid, TestFixture)
{
    check_raw("*961285D5E0ABFB00430002EF000000017C0FB2", true);
    check_raw("^0A", true);
}

BOOST_FIXTURE_TEST_CASE(test_raw_invalid, TestFixture)
{
    check_raw("05693A", false);
    check_raw(":D8372A49004300017E0000000023766C", false);
}

BOOST_FIXTURE_TEST_CASE(test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::PHSEN_PCO2W_SUNBURST_Data pos;
        //      check_parsed(pos, true);
    }
}

BOOST_FIXTURE_TEST_CASE(test_parsed_invalid, TestFixture)
{
    {
        cgsn::protobuf::PHSEN_PCO2W_SUNBURST_Data pos;
        //        check_parsed(pos, false);
    }
}
