// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE ctd_sbe-validator-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/ctd_sbe_config.pb.h"
#include "cgsn-mooring/sensor/ctd_sbe/ctd_sbe_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::CTD_SBE_ValidatorThread,
                                                    cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE (test_raw_valid, TestFixture)
{
    // valid sentence with \r\n at the end
    check_raw("# 22.6862,  0.00012,    0.041, 23 Apr 2018 20:36:29\r\n", true);
    // extra blank line is "valid" as the CTD outputs this after each data line
    check_raw("\r\n", true);
}

BOOST_FIXTURE_TEST_CASE (test_raw_invalid, TestFixture)
{
    check_raw("FOOBAR", false);

    // incorrect number of samples is invalid
    check_raw("# 22.6863,    0.054, 23 Apr 2018 20:36:29\r\n", false);

    // missing # but has correct number of samples is invalid
    check_raw ("22.6863,  0.00000,    0.054, 23 Apr 2018 20:36:29\r\n", false);

    // extra whitespace at the front is invalid
    check_raw("  # 22.6988,  0.00000,    0.054, 23 Apr 2018 20:36:49\r\n", false);

    // extra space at the front is invalid
    check_raw(" \r\n", false);
    
}

