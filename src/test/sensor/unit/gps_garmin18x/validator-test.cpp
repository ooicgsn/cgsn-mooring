// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE gps-validator-test
#include <boost/test/included/unit_test.hpp>

#include "cgsn-mooring/config/gps_garmin18x_config.pb.h"
#include "cgsn-mooring/sensor/gps_garmin18x/gps_validator.h"
#include "cgsn-mooring/sensor/validator_test_fixture.h"
#include "cgsn-mooring/sensor/validator_test_fixture_impl.h"

using TestFixture = cgsn::test::ValidatorTestFixture<cgsn::validator::GPSValidatorThread,
                                                    cgsn::protobuf::ValidatorConfig>;

BOOST_FIXTURE_TEST_CASE (test_raw_valid, TestFixture)
{
    // valid sentence with \r\n at the end
    check_raw("$GPRMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*7C\r\n", true);
    // no \r\n is still valid
    check_raw("$GPRMC,144321,A,4131.4821,N,07040.2552,W,000.0,000.0,060418,014.8,W*7A", true);
    // extra whitespace at the front is valid
    check_raw("  $GPRMC,144324,A,4131.4821,N,07040.2552,W,000.0,000.0,060418,014.8,W*7F", true);

}

BOOST_FIXTURE_TEST_CASE (test_raw_invalid, TestFixture)
{
    check_raw("FOOBAR", false);

    // wrong checksum
    check_raw("$GPRMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*8B\r\n", false);

    // wrong talker
    check_raw("$GPXMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*7C\r\n", false);

}

BOOST_FIXTURE_TEST_CASE (test_parsed_valid, TestFixture)
{
    {
        cgsn::protobuf::GPSPosition pos;
        pos.mutable_parsed_header()->set_sensor_time(1511888947000000ll);
        pos.set_fix_valid(true);
        pos.set_latitude(41.5247);
        pos.set_longitude(-70.6709466666667);

        check_parsed(pos, true);
    }


    {
        cgsn::protobuf::GPSPosition pos;
        pos.mutable_parsed_header()->set_sensor_time(1511888947000000ll);
        pos.set_fix_valid(true);
        pos.set_latitude(-24.25745);
        pos.set_longitude(15.0);
        check_parsed(pos, true);
    }

}

BOOST_FIXTURE_TEST_CASE (test_parsed_invalid, TestFixture)
{
    // parser reports invalid fix
    {
        cgsn::protobuf::GPSPosition pos;
        pos.mutable_parsed_header()->set_sensor_time(1511888947000000ll);
        pos.set_fix_valid(false);
        pos.set_latitude(41.5247);
        pos.set_longitude(-70.6709466666667);

        check_parsed(pos, false);
    }


    // NaN lat or lon
    {
        cgsn::protobuf::GPSPosition pos;
        pos.mutable_parsed_header()->set_sensor_time(1511888947000000ll);
        pos.set_fix_valid(true);
        pos.set_latitude(std::numeric_limits<double>::quiet_NaN());
        pos.set_longitude(std::numeric_limits<double>::quiet_NaN());

        check_parsed(pos, false);
    }

}
