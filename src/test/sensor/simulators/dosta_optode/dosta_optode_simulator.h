// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef DOSTA_OPTODE_PLAYBACK_H
#define DOSTA_OPTODE_PLAYBACK_H

#include "cgsn-mooring/sensor/simulator_base.h"
#include "cgsn-mooring/messages/dosta_optode.pb.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/config/dosta_optode_config.pb.h"

namespace cgsn
{
    class DOSTA_OPTODE_Simulator : public SimulatorBase<cgsn::protobuf::DOSTA_OPTODE_SimulatorConfig, groups::dosta_optode::raw_in, groups::dosta_optode::raw_in_validated, groups::dosta_optode::raw_out>
    {
    public:
        DOSTA_OPTODE_Simulator();
    private:
        void simulator_loop() override;
    private:

    };
}

#endif
