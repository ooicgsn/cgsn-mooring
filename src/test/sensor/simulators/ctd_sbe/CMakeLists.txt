add_executable(ctd_sbe_simulator ctd_sbe_simulator.cpp)
target_link_libraries(ctd_sbe_simulator
  goby_middleware
  cgsn_ctd_sbe)
