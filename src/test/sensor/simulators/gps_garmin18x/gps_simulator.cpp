// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/middleware/log.h>

#include "cgsn-mooring/serial/ascii_line_based_serial.h"
#include "cgsn-mooring/config/gps_garmin18x_config.pb.h"

#include "gps_simulator.h"

using goby::glog;
using namespace goby::common::logger;

int main(int argc, char* argv[])
{ return goby::run<cgsn::GPSSimulator>(argc, argv); }

cgsn::GPSSimulator::GPSSimulator()
{
    //    set_is_logging(true);
}

void cgsn::GPSSimulator::simulator_loop()
{
}
