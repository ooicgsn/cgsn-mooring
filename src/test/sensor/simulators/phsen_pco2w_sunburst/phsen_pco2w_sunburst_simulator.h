// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef PHSEN_PCO2W_SUNBURST_PLAYBACK_H
#define PHSEN_PCO2W_SUNBURST_PLAYBACK_H

#include "cgsn-mooring/config/phsen_pco2w_sunburst_config.pb.h"
#include "cgsn-mooring/messages/groups.h"
#include "cgsn-mooring/messages/phsen_pco2w_sunburst.pb.h"
#include "cgsn-mooring/sensor/simulator_base.h"

namespace cgsn
{
class PHSEN_PCO2W_SUNBURST_Simulator
    : public SimulatorBase<cgsn::protobuf::PHSEN_PCO2W_SUNBURST_SimulatorConfig,
                           groups::phsen_pco2w_sunburst::raw_in_validated>
{
  public:
    PHSEN_PCO2W_SUNBURST_Simulator();

  private:
    void simulator_loop() override;

  private:
};
} // namespace cgsn

#endif
