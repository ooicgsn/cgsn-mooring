// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/config/gps_garmin18x_config.pb.h"

#include "cgsn-mooring/sensor/gps_garmin18x/gps_parser.h"
#include "cgsn-mooring/sensor/gps_garmin18x/gps_common.h"

using cgsn::protobuf::GPSPosition;
using cgsn::protobuf::GPSTestConfig;

using namespace goby::common::logger;
using goby::glog;

namespace cgsn
{
    class GPSTestParser : public goby::MultiThreadTest<cgsn::protobuf::GPSTestConfig>
    {
    public:
        GPSTestParser();
        ~GPSTestParser();
    private:
	void parser_ready();
	void parsed_data(const GPSPosition& pos);
    };
}

int main(int argc, char* argv[])
{ return goby::run<cgsn::GPSTestParser>(argc, argv); }

cgsn::GPSTestParser::GPSTestParser()
{
    interthread().subscribe<cgsn::groups::ready, cgsn::DriverModule>(
        [this](const cgsn::DriverModule& module)
        {
            if(module == cgsn::DriverModule::PARSER)
		parser_ready();
	}
        );


    interthread().subscribe<cgsn::groups::gps::parsed, GPSPosition>([this](const GPSPosition& pos) { parsed_data(pos); });

    launch_thread<cgsn::parser::GPSParserThread>(cfg().parser());

}

void cgsn::GPSTestParser::parser_ready()
{
    glog.is(DEBUG1) && glog << "Publishing $GPRMC" << std::endl;
    cgsn::protobuf::SensorRaw raw;
    raw.mutable_header()->set_mooring_time(1511888947123456ll);
    raw.set_raw_data("$GPRMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*7C\r\n");
    interthread().publish<cgsn::groups::gps::raw_in_validated>(raw);
}


void cgsn::GPSTestParser::parsed_data(const GPSPosition& pos)
{
    double eps = 1e-6;
    assert(pos.parsed_header().raw_data_mooring_time() == 1511888947123456ll);
    assert(pos.parsed_header().sensor_time() == 1511888947000000ll);
    assert(std::abs(pos.latitude() - 41.5247) <= eps);
    assert(std::abs(pos.longitude() - -70.6709466666667) <= eps);

    // tests complete, so request clean quit
    quit();
}

cgsn::GPSTestParser::~GPSTestParser()
{
    std::cout << "All tests complete" << std::endl;
}
