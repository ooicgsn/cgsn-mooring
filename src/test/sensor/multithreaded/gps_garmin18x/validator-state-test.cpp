// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/config/gps_garmin18x_config.pb.h"

#include "cgsn-mooring/sensor/gps_garmin18x/gps_validator.h"

using cgsn::protobuf::GPSPosition;
using cgsn::protobuf::GPSTestConfig;

using namespace goby::common::logger;
using goby::glog;

namespace cgsn
{
class GPSTestValidator : public goby::MultiThreadTest<cgsn::protobuf::GPSTestConfig>
{
  public:
    GPSTestValidator();
    ~GPSTestValidator();

  private:
    void loop() override;
    void ready();
    void check_health(const cgsn::protobuf::sensor::ValidatorStatus& health);

    void timeout_wait() { usleep(timeout_sec_ * 1e6); }
    void brief_wait() { usleep(timeout_sec_ / 4 * 1e6); }

  private:
    bool validator_ready_{false};
    int pretest_{0};
    int test_{0};
    int subtest_{0};
    float timeout_sec_{0.2};
    int health_discard_{0};
};
} // namespace cgsn

int main(int argc, char* argv[]) { return goby::run<cgsn::GPSTestValidator>(argc, argv); }

cgsn::GPSTestValidator::GPSTestValidator()
    : goby::MultiThreadTest<cgsn::protobuf::GPSTestConfig>(10 * boost::units::si::hertz)
{
    interthread().subscribe<cgsn::groups::ready, cgsn::DriverModule>(
        [this](const cgsn::DriverModule& module) {
            if (module == cgsn::DriverModule::VALIDATOR)
                ready();
        });

    auto validator_cfg = cfg().validator();
    validator_cfg.set_bytes_timeout_with_units(timeout_sec_ * boost::units::si::seconds);
    validator_cfg.set_valid_raw_data_timeout_with_units(timeout_sec_ * boost::units::si::seconds);
    validator_cfg.set_valid_parsed_data_timeout_with_units(timeout_sec_ *
                                                           boost::units::si::seconds);

    interthread()
        .subscribe<cgsn::groups::sensor::validator_status, protobuf::sensor::ValidatorStatus>(
            [this](const protobuf::sensor::ValidatorStatus& health) { check_health(health); });

    launch_thread<cgsn::validator::GPSValidatorThread>(validator_cfg);
}

void cgsn::GPSTestValidator::loop()
{
    if (!validator_ready_)
        return;

    // prereqs for each test
    if (pretest_ <= test_)
    {
        switch (test_)
        {
            case 0: break;
            case 1:
                // enable logging
                interthread().publish<cgsn::groups::command::logging>(true);
                // wait for this to get received
                brief_wait();
                break;
            case 2:
                // wait for timeout
                timeout_wait();
                break;
            case 3:
            {
                cgsn::protobuf::SensorRaw raw;
                populate_header(*raw.mutable_header());
                raw.set_raw_data(
                    "$GPRMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*7C\r\n");
                interthread().publish<cgsn::groups::gps::raw_in>(raw);
                // wait for this to get received
                brief_wait();
            }
            break;

            case 4:
            {
                timeout_wait();
                health_discard_ = 4;
                cgsn::protobuf::SensorRaw raw;
                populate_header(*raw.mutable_header());
                raw.set_raw_data("FOOBAR");
                interthread().publish<cgsn::groups::gps::raw_in>(raw);
                brief_wait();
            }

            break;

            case 5:
            {
                cgsn::protobuf::SensorRaw raw;
                populate_header(*raw.mutable_header());
                raw.set_raw_data(
                    "$GPRMC,170907,A,4131.4820,N,07040.2568,W,000.0,000.0,281117,014.8,W*7C\r\n");
                interthread().publish<cgsn::groups::gps::raw_in>(raw);
                brief_wait();

                interthread().publish<cgsn::groups::gps::raw_in>(raw);
                cgsn::protobuf::GPSPosition pos;
                populate_header(*pos.mutable_header());
                pos.mutable_parsed_header()->set_sensor_time(1511888947000000ll);
                pos.set_fix_valid(true);
                pos.set_latitude(41.5247);
                pos.set_longitude(-70.6709466666667);

                interthread().publish<cgsn::groups::gps::parsed>(pos);
                brief_wait();
            }
            break;

            default: quit(); break;
        }

        pretest_ = test_ + 1;
    }
}

void cgsn::GPSTestValidator::ready()
{
    glog.is(DEBUG1) && glog << "validator ready" << std::endl;
    validator_ready_ = true;
}

void cgsn::GPSTestValidator::check_health(const cgsn::protobuf::sensor::ValidatorStatus& health)
{
    auto current_test = test_;
    auto current_subtest = subtest_;
    glog.is(DEBUG1) && glog << "============= Test " << current_test << " (Part " << current_subtest
                            << ") =================" << std::endl;

    glog.is(DEBUG1) && glog << "Received health: " << health.ShortDebugString() << std::endl;

    switch (test_)
    {
        // check starting up in standby
        case 0:
            assert(health.state() == cgsn::protobuf::sensor::NOTLOGGING__STANDBY);
            ++test_;
            break;

        // should be initializing
        case 1:
            assert(health.state() == cgsn::protobuf::sensor::LOGGING__INITIALIZING);
            ++test_;
            break;

        // CRITICAL_FAILURE after timeouts
        case 2:
            switch (subtest_)
            {
                case 0:
                    // enter critical failure
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__CRITICAL_FAILURE);
                    ++subtest_;
                    break;

                case 1:
                    // set_error
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__CRITICAL_FAILURE);
                    assert(health.error().code() == cgsn::protobuf::Error::IO__SERIAL_DATA_TIMEOUT);
                    ++test_;
                    subtest_ = 0;
                    break;
            }
            break;

        // raw data validated
        case 3:
            switch (subtest_)
            {
                case 0:
                    // clear error (on leaving critical failure)
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__CRITICAL_FAILURE);
                    assert(!health.error().has_code());
                    ++subtest_;
                    break;

                case 1:
                    // pass through degraded from critical failure back to functioning
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__DEGRADED);
                    ++subtest_;
                    break;

                case 2:
                    assert(health.state() ==
                           cgsn::protobuf::sensor::LOGGING__FUNCTIONING__RAW_DATA_VALID);
                    ++test_;
                    subtest_ = 0;
                    break;
            }

            break;

        case 4:
            switch (subtest_)
            {
                // three critical failure messages: one entering, one adding error, one clearing error
                case 0:
                case 1:
                case 2:
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__CRITICAL_FAILURE);
                    ++subtest_;
                    break;
                case 3:
                    assert(health.state() == cgsn::protobuf::sensor::LOGGING__DEGRADED);
                    ++test_;
                    subtest_ = 0;
                    break;
            }
            break;

        case 5:
            switch (subtest_)
            {
                // pass through raw data valid -> parsed data valid
                case 0:
                    assert(health.state() ==
                           cgsn::protobuf::sensor::LOGGING__FUNCTIONING__RAW_DATA_VALID);
                    ++subtest_;
                    break;
                case 1:
                    ++test_;
                    subtest_ = 0;
                    assert(health.state() ==
                           cgsn::protobuf::sensor::LOGGING__FUNCTIONING__PARSED_DATA_VALID);
                    break;
            }
            break;
    }
    glog.is(DEBUG1) && glog << "============= End Test " << current_test << " (Part "
                            << current_subtest << ") =================" << std::endl;
}

cgsn::GPSTestValidator::~GPSTestValidator() { std::cout << "All tests complete" << std::endl; }
