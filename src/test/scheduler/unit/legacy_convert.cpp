// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE scheduler-legacy-convert-test
#include <boost/test/included/unit_test.hpp>

#include <google/protobuf/text_format.h>

#include "cgsn-mooring/scheduler/legacy_convert.h"
#include "cgsn-mooring/scheduler/validate.h"

template<typename LegacySchedule>
struct TestFixture
{
    TestFixture()
        {
            legacy_.set_port_id(1);
            legacy_.set_sensor_type(cgsn::protobuf::sensor::GPS_GARMIN18X);

            always_off_schedule_.set_port_id(1);
            always_off_schedule_.set_sensor_type(cgsn::protobuf::sensor::GPS_GARMIN18X);
            always_off_schedule_.set_schedule_type(cgsn::protobuf::PortSchedule::ALWAYS_OFF);
        }


    ~TestFixture()
        {
            cgsn::scheduler::validate(schedule_);
        }


    LegacySchedule legacy_;
    cgsn::protobuf::PortSchedule schedule_, expected_schedule_;
    cgsn::protobuf::PortSchedule always_off_schedule_;

};

BOOST_FIXTURE_TEST_CASE(hour_sched1, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyHourlySchedule>)
{
    legacy_.set_sched("1:0-23:0:30");
    schedule_ = cgsn::scheduler::convert_legacy_hour_sched(legacy_);


    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: HOURLY "
        "duration: 30 "
        "offset: 0 "
        "hour: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());

}

BOOST_FIXTURE_TEST_CASE(hour_sched2, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyHourlySchedule>)
{
    legacy_.set_sched("1:1,4,6-8,4:09:45");
    schedule_ = cgsn::scheduler::convert_legacy_hour_sched(legacy_);

    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: HOURLY "
        "duration: 45 "
        "offset: 9 "
        "hour: [1,4,6,7,8]";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());

}


BOOST_FIXTURE_TEST_CASE(hour_sched3, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyHourlySchedule>)
{
    legacy_.set_sched("1:0-23:58:10");

    schedule_ = cgsn::scheduler::convert_legacy_hour_sched(legacy_);

    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: HOURLY "
        "duration: 10 "
        "offset: 58 "
        "hour: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());

}



BOOST_FIXTURE_TEST_CASE(hour_sched_invalid_range, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyHourlySchedule>)
{
    legacy_.set_sched("1:1,4,8-5:0:30");
    BOOST_CHECK_THROW(cgsn::scheduler::convert_legacy_hour_sched(legacy_), cgsn::scheduler::LegacyScheduleException);

    // set to valid
    schedule_ = always_off_schedule_;
}


BOOST_FIXTURE_TEST_CASE(interval_sched_no_offset, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyIntervalSchedule>)
{
    legacy_.set_isched("1:15:00:04");
    schedule_ = cgsn::scheduler::convert_legacy_interval_sched(legacy_);

    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: INTERVAL "
        "duration: 4 "
        "offset: 0 "
        "interval: 15";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());

}

BOOST_FIXTURE_TEST_CASE(interval_sched_offset, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyIntervalSchedule>)
{
    legacy_.set_isched("1:15:2:05");
    schedule_ = cgsn::scheduler::convert_legacy_interval_sched(legacy_);

    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: INTERVAL "
        "duration: 5 "
        "offset: 2 "
        "interval: 15";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());

}


BOOST_FIXTURE_TEST_CASE(interval_sched_off, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyIntervalSchedule>)
{
    legacy_.set_isched("0:15:00:04");
    schedule_ = cgsn::scheduler::convert_legacy_interval_sched(legacy_);

    std::string expected_string = "port_id: 1 "
        "sensor_type: GPS_GARMIN18X "
        "schedule_type: ALWAYS_OFF";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_schedule_);

    BOOST_CHECK_MESSAGE(expected_schedule_.SerializeAsString() == schedule_.SerializeAsString(),
                        "Expected: " << expected_schedule_.DebugString() << "Received: " << schedule_.DebugString());
}

BOOST_FIXTURE_TEST_CASE(interval_sched_missing_parts, TestFixture<cgsn::protobuf::SchedulerConfig::LegacyIntervalSchedule>)
{
    legacy_.set_isched("0:15:04");
    BOOST_CHECK_THROW(cgsn::scheduler::convert_legacy_interval_sched(legacy_), cgsn::scheduler::LegacyScheduleException);

    // set to valid
    schedule_ = always_off_schedule_;
}
