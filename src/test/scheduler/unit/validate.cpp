// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE scheduler - validate - test
#include <boost/test/included/unit_test.hpp>

#include <google/protobuf/text_format.h>

#include "cgsn-mooring/scheduler/validate.h"

template <cgsn::protobuf::PortSchedule::ScheduleType schedule_type>
struct SchedulerValidateTestFixture
{
    SchedulerValidateTestFixture()
    {
        schedule_.set_port_id(1);
        schedule_.set_sensor_type(cgsn::protobuf::sensor::GPS_GARMIN18X);
        schedule_.set_schedule_type(schedule_type);
    }

    ~SchedulerValidateTestFixture() {}

    cgsn::protobuf::PortSchedule schedule_;
};

BOOST_FIXTURE_TEST_CASE(valid_always_on_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::ALWAYS_ON>)
{
    BOOST_CHECK_NO_THROW(cgsn::scheduler::validate(schedule_));
}

BOOST_FIXTURE_TEST_CASE(invalid_always_on_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::ALWAYS_ON>)
{
    schedule_.set_duration_with_units(120 * boost::units::si::seconds);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_ALWAYS_ON_GIVEN_PARAMETERS;
        });
}

BOOST_FIXTURE_TEST_CASE(valid_always_off_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::ALWAYS_OFF>)
{
    BOOST_CHECK_NO_THROW(cgsn::scheduler::validate(schedule_));
}

BOOST_FIXTURE_TEST_CASE(invalid_always_off_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::ALWAYS_OFF>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_interval_with_units(3 * minutes);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_ALWAYS_OFF_GIVEN_PARAMETERS;
        });
}

BOOST_FIXTURE_TEST_CASE(valid_hourly_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_duration_with_units(120 * boost::units::si::seconds);
    schedule_.set_offset_with_units(3 * minutes);
    schedule_.add_hour(0);
    schedule_.add_hour(1);
    schedule_.add_hour(10);

    BOOST_CHECK_NO_THROW(cgsn::scheduler::validate(schedule_));
}

BOOST_FIXTURE_TEST_CASE(invalid_hourly_schedule_1,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    schedule_.add_hour(0);
    schedule_.set_interval(10);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_HOURLY_GIVEN_INTERVAL;
        });
}

BOOST_FIXTURE_TEST_CASE(invalid_hourly_schedule_2,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    schedule_.add_hour(0);
    schedule_.add_hour(24);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_HOURLY_INVALID_HOUR;
        });
}

BOOST_FIXTURE_TEST_CASE(invalid_hourly_schedule_3,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    schedule_.set_offset_with_units(4200 * boost::units::si::seconds);
    schedule_.add_hour(0);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_HOURLY_INVALID_OFFSET;
        });
}

BOOST_FIXTURE_TEST_CASE(invalid_hourly_schedule_5,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    schedule_.set_offset_with_units(1800 * boost::units::si::seconds);
    schedule_.set_duration_with_units(3600 * boost::units::si::seconds);
    schedule_.set_slot_buffer_time_with_units(60 * boost::units::si::seconds);
    schedule_.add_hour(0);
    schedule_.add_hour(1);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_HOURLY_OFFSET_PLUS_DURATION_OVERFLOW;
        });
}

BOOST_FIXTURE_TEST_CASE(valid_interval_schedule,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::INTERVAL>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_duration_with_units(120 * boost::units::si::seconds);
    schedule_.set_offset_with_units(3 * minutes);
    schedule_.set_interval_with_units(60 * minutes);

    BOOST_CHECK_NO_THROW(cgsn::scheduler::validate(schedule_));
}

BOOST_FIXTURE_TEST_CASE(invalid_interval_schedule_1,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::INTERVAL>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_interval_with_units(60 * minutes);
    schedule_.add_hour(0);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_INTERVAL_GIVEN_HOURS;
        });
}

BOOST_FIXTURE_TEST_CASE(invalid_interval_schedule_2,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::INTERVAL>)
{
    BOOST_CHECK_EXCEPTION(cgsn::scheduler::validate(schedule_),
                          cgsn::scheduler::InvalidScheduleException,
                          [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
                              return e.error() == cgsn::protobuf::SCHEDULE_NO_INTERVAL_GIVEN;
                          });
}

BOOST_FIXTURE_TEST_CASE(invalid_interval_schedule_3,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::INTERVAL>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_interval_with_units(120 * minutes);
    schedule_.set_duration_with_units(120 * minutes);
    schedule_.set_offset_with_units(60 * minutes);
    schedule_.set_slot_buffer_time_with_units(1 * minutes);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() ==
                   cgsn::protobuf::SCHEDULE_ERROR_INTERVAL_OFFSET_PLUS_DURATION_OVERFLOW;
        });
}

BOOST_FIXTURE_TEST_CASE(invalid_interval_schedule_4,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::INTERVAL>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_interval_with_units(120 * minutes);
    schedule_.set_duration_with_units(60 * minutes);
    schedule_.set_offset_with_units(30 * minutes);
    schedule_.set_slot_buffer_time_with_units(1 * minutes);
    schedule_.set_force_stop_delay_with_units(2 * minutes);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() ==
                   cgsn::protobuf::
                       SCHEDULE_ERROR_INTERVAL_FORCE_STOP_DELAY_MUST_BE_LESS_THAN_BUFFER;
        });
}

BOOST_AUTO_TEST_CASE(test_minimum_time_between_slots)
{
    auto hours = boost::units::metric::hour_base_unit::unit_type();

    using QuantitySeconds =
        boost::units::quantity<boost::units::si::second_base_unit::unit_type, int>;

    {
        cgsn::protobuf::PortSchedule schedule;
        schedule.add_hour(0);
        schedule.add_hour(2);
        schedule.add_hour(4);
        schedule.add_hour(10);
        BOOST_CHECK(cgsn::scheduler::minimum_time_between_slots(schedule) ==
                    QuantitySeconds(2 * hours));
    }
    {
        cgsn::protobuf::PortSchedule schedule;
        schedule.add_hour(0);
        schedule.add_hour(5);
        schedule.add_hour(23);
        BOOST_CHECK(cgsn::scheduler::minimum_time_between_slots(schedule) ==
                    QuantitySeconds(1 * hours));
    }
    {
        cgsn::protobuf::PortSchedule schedule;
        schedule.add_hour(10);
        schedule.add_hour(12);
        schedule.add_hour(14);
        schedule.add_hour(15);

        BOOST_CHECK(cgsn::scheduler::minimum_time_between_slots(schedule) ==
                    QuantitySeconds(1 * hours));
    }
    {
        cgsn::protobuf::PortSchedule schedule;
        schedule.add_hour(5);
        schedule.add_hour(10);
        schedule.add_hour(20);

        BOOST_CHECK(cgsn::scheduler::minimum_time_between_slots(schedule) ==
                    QuantitySeconds(5 * hours));
    }
}

BOOST_FIXTURE_TEST_CASE(valid_hourly_schedule_with_duration_over_one_hour,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    // 90 minute duration acceptable since the minimum time between hours is
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_duration_with_units(90 * minutes);
    schedule_.set_offset_with_units(0 * minutes);
    schedule_.add_hour(0);
    schedule_.add_hour(2);
    schedule_.add_hour(4);
    schedule_.add_hour(10);

    BOOST_CHECK_NO_THROW(cgsn::scheduler::validate(schedule_));
}

BOOST_FIXTURE_TEST_CASE(invalid_hourly_schedule_with_duration_over_one_hour,
                        SchedulerValidateTestFixture<cgsn::protobuf::PortSchedule::HOURLY>)
{
    auto minutes{boost::units::metric::minute_base_unit::unit_type()};
    schedule_.set_duration_with_units(90 * minutes);
    schedule_.set_offset_with_units(0 * minutes);
    schedule_.add_hour(0);
    schedule_.add_hour(1);
    schedule_.add_hour(2);
    schedule_.add_hour(10);

    BOOST_CHECK_EXCEPTION(
        cgsn::scheduler::validate(schedule_), cgsn::scheduler::InvalidScheduleException,
        [](const cgsn::scheduler::InvalidScheduleException& e) -> bool {
            return e.error() == cgsn::protobuf::SCHEDULE_ERROR_HOURLY_OFFSET_PLUS_DURATION_OVERFLOW;
        });
}
