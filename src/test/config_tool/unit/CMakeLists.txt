find_program(BASH bash)

add_test(cgsn_test_config_tool ${BASH} ${CMAKE_CURRENT_SOURCE_DIR}/config_tool_test.sh ${project_BIN_DIR}/cgsn_config_tool ${project_BIN_DIR}/cgsn_test_compare_pb ${project_SRC_DIR}/doc/markdown/doc_23-config.md)

# necessary because we want to run cgsn_config_tool which has its RPATH stripped in the debian build environment
set_property(TEST cgsn_test_config_tool PROPERTY ENVIRONMENT "LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}:${project_LIB_DIR}")
