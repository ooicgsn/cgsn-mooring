#!/bin/bash

set -e -u

# usage: config_tool_test.sh /path/to/cgsn_config_tool /path/to/cgsn_test_compare_pb /path/to/doc/markdown/doc_23-config.md

config_tool=$1
test_compare_pb=$2
config_tool_doc=$3

pb_equal() {
    echo yes;
}

# make sure we can parse the example files we write
test_loopback()
{
    mkdir -p /tmp/test_loopback
    pushd /tmp/test_loopback
    (set -x; ${config_tool} --action WRITE_EXAMPLE_YAML_FILES)
    mv master-example.yml master.yml
    mv mooring-example.yml mooring.yml
    mv assets-example.yml assets.yml


    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SCHEDULER --cpu dcl35 > received.pb.cfg)
    cat <<EOF > expected.pb.cfg
app {
  glog_config {
    file_log {
      file_name: ""
      verbosity: VERBOSE
    }
  }
}
interprocess {
  platform: "cp01cnsm"
  transport: IPC
}
schedule {
  port_id: 1
  sensor_type: GPS_GARMIN18X
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 2
  sensor_type: CTD_SBE
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 3
  sensor_type: ADCP_RDI
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 4
  sensor_type: VELPT_NORTEK
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 5
  sensor_type: DOSTA_OPTODE
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 6
  sensor_type: PRESF_SBE
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 7
  sensor_type: PHSEN_PCO2W_SUNBURST
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}
schedule {
  port_id: 8
  sensor_type: PHSEN_PCO2W_SUNBURST
  schedule_type: ALWAYS_ON
  duration: 0
  offset: 0
  hour: [0, 0, 0, 0, 0]
  interval: 0
}

EOF

    
    ${test_compare_pb} received.pb.cfg expected.pb.cfg cgsn.protobuf.SchedulerConfig

    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER --instrument_key mfn-ctdbp-N > received.pb.cfg)
    cat <<EOF > expected.pb.cfg
app {
  glog_config {
    file_log {
      file_name: ""
      verbosity: VERBOSE
    }
  }
}
interprocess {
  platform: "cp01cnsm"
  transport: IPC
}
instrument_key: "mfn-ctdbp-N"
serial {
  port: ""
  baud: 0
}
power {
  port_id: 2
  voltage: VOLTAGE_12V
  current_limit: 0
  protocol: RS232
  soft_start: false
  power: LOW
}
command {
  op_mode: NORMAL
}
sensor {
  output_salinity: true
  output_sound_velocity: true
  output_sigmat_v_i: true
  measurements_per_sample: 4
  pump_mode: RUN_PUMP_BEFORE_SAMPLE
  sample_interval: 10
  output_format: OUTPUT_CONVERTED_DATA_DECIMAL
  transmit_real_time: true
  optode: false
}
EOF
    ${test_compare_pb} received.pb.cfg expected.pb.cfg cgsn.protobuf.CTD_SBE_DriverConfig

    popd
}


# test the examples given in the documentation (markdown/doc_23-config.md)
test_doc_examples()
{
    mkdir -p /tmp/test_doc_examples
    pushd /tmp/test_doc_examples

    extract_file()
    {
        file=$1
        sed -n "/^## ${file} ##$/,/\`\`\`/p;" ${config_tool_doc} | head -n -1 | tail -n +2 > ${file}
    }

    # extract documented files
    extract_file master.yml
    extract_file mooring.yml
    extract_file assets.yml
    extract_file ctdbp-seabird-sbe-16_plus-defaults.yml

    compare()
    {
        name=$1
        pb_name=$2
        echo "Comparing ${name}-result.pb.cfg from cgsn_config_tool to the version in the Markdown doc"
        extract_file ${name}.pb.cfg
        ${test_compare_pb} ${name}-result.pb.cfg ${name}.pb.cfg ${pb_name} || (diff -u ${name}.pb.cfg.out ${name}-result.pb.cfg.out && exit 1)
    }

    # run the config tool and compare to the results given in the doc
    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type GOBYD  --cpu dcl36 > gobyd-result.pb.cfg)
    compare gobyd goby.protobuf.GobyDaemonConfig

    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SUPERVISOR --cpu dcl36 > supervisor-result.pb.cfg)
    compare supervisor cgsn.protobuf.SupervisorConfig

    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SCHEDULER --cpu dcl36  > scheduler-result.pb.cfg)
    compare scheduler cgsn.protobuf.SchedulerConfig


    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SYSTEM_HEALTH --cpu dcl36  > system_health-result.pb.cfg)
    compare system_health cgsn.protobuf.SystemHealthConfig
    
    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type SENSOR_DRIVER  --instrument_key mfn-ctdbp1  > ctd-result.pb.cfg)
    compare ctd cgsn.protobuf.CTD_SBE_DriverConfig

    (set -x; ${config_tool} --action CONVERT_YAML_TO_PROTOBUF --app_type LEGACY_LOGGER  --instrument_key mfn-ctdbp1  > legacy_logger-result.pb.cfg)
    compare legacy_logger cgsn.protobuf.LegacyLogServiceConfig

    (set -x; ${config_tool} --action WRITE_SERVICE_START_COMMAND --app_type SENSOR_DRIVER --cpu dcl36 --port_id 2 > port2_driver_start_command_result)
    extract_file port2_driver_start_command
    diff -u --ignore-all-space port2_driver_start_command port2_driver_start_command_result
    
    (set -x; ${config_tool} --action WRITE_SERVICE_START_COMMAND --app_type LEGACY_LOGGER --cpu dcl36 --port_id 2 > port2_logger_start_command_result)
    extract_file port2_logger_start_command
    diff -u --ignore-all-space port2_logger_start_command port2_logger_start_command_result

    (set -x; ${config_tool} --action WRITE_SIMULATION_LAUNCHFILE --cpu dcl36 > cgsn-sim-result.launch)
    extract_file cgsn-sim.launch
    diff -u --ignore-all-space cgsn-sim.launch cgsn-sim-result.launch

    popd
}


test_loopback
test_doc_examples
