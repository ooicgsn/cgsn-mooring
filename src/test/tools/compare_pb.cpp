// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>

#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include "dccl/dynamic_protobuf_manager.h"
#include "cgsn-mooring/config/config_tool_config.pb.h"
#include "cgsn-mooring/messages/header.pb.h"

// compares two files ("pbfile1"/"pbfile2") in Protobuf TextFormat for the message type "ProtobufMessageName" and returns whether they are equal (0) or not (1).

int main(int argc, char* argv[])
{
    // required to ensure we link libcgsn_config.so
    { cgsn::protobuf::ConfigToolConfig dummy; }
    // required to ensure we link libcgsn_messages.so
    { cgsn::protobuf::Header dummy; }


    if(argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " pbfile1 pbfile2 ProtobufMessageName" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string pbfile1_path(argv[1]);
    std::string pbfile2_path(argv[2]);
    std::string protobuf_name = argv[3];

    std::ifstream pbfile1(pbfile1_path.c_str());
    std::ifstream pbfile2(pbfile2_path.c_str());

    if(!pbfile1.is_open())
    {
        std::cerr << "Failed to open " << pbfile1_path << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!pbfile2.is_open())
    {
        std::cerr << "Failed to open " << pbfile2_path << std::endl;
        exit(EXIT_FAILURE);
    }


    auto pb1_cfg = dccl::DynamicProtobufManager::new_protobuf_message<std::unique_ptr<google::protobuf::Message>>(protobuf_name);
    auto pb2_cfg = dccl::DynamicProtobufManager::new_protobuf_message<std::unique_ptr<google::protobuf::Message>>(protobuf_name);
    if(!pb1_cfg || !pb2_cfg)
    {
        std::cerr << "no protobuf with name: " << protobuf_name << " found" << std::endl;
        dccl::DynamicProtobufManager::protobuf_shutdown();
        exit(EXIT_FAILURE);
    }

    google::protobuf::io::IstreamInputStream iis1(&pbfile1);
    google::protobuf::io::IstreamInputStream iis2(&pbfile2);

    google::protobuf::TextFormat::Parse(&iis1, pb1_cfg.get());
    google::protobuf::TextFormat::Parse(&iis2, pb2_cfg.get());


    if(pb1_cfg->SerializeAsString() == pb2_cfg->SerializeAsString())
    {
        std::cout << "Equal" << std::endl;
        dccl::DynamicProtobufManager::protobuf_shutdown();
        exit(EXIT_SUCCESS);
    }
    else
    {
        std::cout << "Not equal" << std::endl;
        {
            auto pb1out_path = pbfile1_path + ".out";
            std::ofstream pb1_out(pb1out_path.c_str());
            pb1_out << pb1_cfg->DebugString();
            auto pb2out_path = pbfile2_path + ".out";
            std::ofstream pb2_out(pb2out_path.c_str());
            pb2_out << pb2_cfg->DebugString();
        }

        dccl::DynamicProtobufManager::protobuf_shutdown();
        exit(EXIT_FAILURE);
    }
}
