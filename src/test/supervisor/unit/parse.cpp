// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE supervisor-parse-test
#include <boost/test/included/unit_test.hpp>

#include <google/protobuf/text_format.h>

#include "cgsn-mooring/supervisor/parse.h"
#include "cgsn-mooring/supervisor/serialize.h"


BOOST_AUTO_TEST_CASE(mpic_status_1)
{
    std::string dcl_status_line = "dcl: 24.1 24.0 00000000 t -204.1 28.0 0.0 0.0 0.0 h 22.4 p 34.3 gf 0 0.0 0.0 0.0 ld 0 0 0 p1 0 0.0 0.0 0 p2 0 0.0 0.0 0 p3 0 0.0 0.0 0 p4 0 0.0 0.0 0 p5 0 0.0 0.0 0 p6 0 0.0 0.0 0 p7 0 0.0 0.0 0 p8 0 0.0 0.0 0 hb 0 0 0 wake 0 wtc 0 wpc 0 pwr 0 0 0 0.0 0.0 0.0 0.0 0.0 0.0 3596";

    auto last_space_pos = dcl_status_line.find_last_of(" ");
    BOOST_CHECK_EQUAL(cgsn::supervisor::calc_checksum(dcl_status_line.begin(), dcl_status_line.begin()+last_space_pos), 0x3596);

    BOOST_CHECK(cgsn::supervisor::checksum_valid(dcl_status_line));

    // make sure we fail against the wrong checksum
    auto dcl_status_invalid_cs = dcl_status_line;
    dcl_status_invalid_cs[dcl_status_invalid_cs.size() - 2] = '4';
    BOOST_CHECK(!cgsn::supervisor::checksum_valid(dcl_status_invalid_cs));

    auto parsed_status = cgsn::supervisor::parse_mpic_dcl_status(dcl_status_line);

    cgsn::protobuf::MPICStatus expected_status;
    std::string expected_string =
        "main_voltage: 24.1 main_current: 24.0 "
        "temperature { bmp085: -204.1 sht: 28 lp12v: 0 lp24v: 0 hp12v: 0 } "
        "sht_humidity: 22.4 pmb085_pressure: 236490.17 "
        "ground_leak_data_enabled: false ground_leak_data_max: 0 ground_leak_data_max: 0 "
        "ground_leak_data_max: 0 "
        "leak_detect { enabled: false voltage: 0 } leak_detect { enabled: false voltage: 0 } "
        "cpic { port: 1 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 2 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 3 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 4 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 5 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 6 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 7 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 8 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "dpb { pwr_sel: 0 mode: 0 state: 0 vmain: 0 imain: 0 v12: 0 i12: 0 v24: 0 i24: 0 }";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_status);

    BOOST_CHECK_MESSAGE(expected_status.SerializeAsString() == parsed_status.SerializeAsString(),
                        "Expected: " << expected_status.DebugString() << "Received: " << parsed_status.DebugString());

    // serialize back to string
    std::string dcl_status_serialized = cgsn::supervisor::serialize_mpic_dcl_status(parsed_status);
    BOOST_CHECK_EQUAL(dcl_status_line, dcl_status_serialized);
}

BOOST_AUTO_TEST_CASE(mpic_status_2)
{
    std::string dcl_status_line = "dcl: 24.1 24.0 00000250 t -204.1 28.0 12.0 6.2 -1.0 h 22.4 p 34.3 gf 1 1.1 2.2 3.3 ld 2 1 12 p1 0 0.0 0.0 0 p2 1 24.0 20.1 0 p3 0 0.0 0.0 0 p4 1 12.0 100.0 0 p5 0 0.0 0.0 0 p6 0 0.0 0.0 0 p7 0 0.0 0.0 0 p8 0 0.0 0.0 0 hb 0 0 0 wake 0 wtc 0 wpc 0 pwr 1 2 3 4.0 5.0 6.0 7.0 8.0 9.0";
    {
        dcl_status_line += " " + cgsn::supervisor::calc_checksum_str(dcl_status_line.begin(),
                                                                     dcl_status_line.end());
    }


    auto parsed_status = cgsn::supervisor::parse_mpic_dcl_status(dcl_status_line);

    cgsn::protobuf::MPICStatus expected_status;
    std::string expected_string =
        "main_voltage: 24.1 main_current: 24.0 "
        "error: MPIC__DCL_ERR_GFLT_VSRC_2_H "
        "error: MPIC__DCL_ERR_GFLT_VSRC_3_H "
        "error: MPIC__DCL_ERR_GFLT_VSRC_4_GND "
        "temperature { bmp085: -204.1 sht: 28 lp12v: 12.0 lp24v: 6.2 hp12v: -1.0 } "
        "sht_humidity: 22.4 pmb085_pressure: 236490.17 "
        "ground_leak_data_enabled: true ground_leak_data_max: 1.1 ground_leak_data_max: 2.2 "
        "ground_leak_data_max: 3.3 "
        "leak_detect { enabled: false voltage: 1 } leak_detect { enabled: true voltage: 12 } "
        "cpic { port: 1 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 2 state: ON voltage: 24 current: 20.1 ierr: 0 } "
        "cpic { port: 3 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 4 state: ON voltage: 12 current: 100 ierr: 0 } "
        "cpic { port: 5 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 6 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 7 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 8 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "dpb { pwr_sel: 1 mode: 2 state: 3 vmain: 4 imain: 5 v12: 6 i12: 7 v24: 8 i24: 9 }";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_status);

    BOOST_CHECK_MESSAGE(expected_status.SerializeAsString() == parsed_status.SerializeAsString(),
                        "Expected: " << expected_status.DebugString() << "Received: " << parsed_status.DebugString());

    std::string dcl_status_serialized = cgsn::supervisor::serialize_mpic_dcl_status(parsed_status);
    BOOST_CHECK_EQUAL(dcl_status_line, dcl_status_serialized);

}



BOOST_AUTO_TEST_CASE(mpic_pon_from_mpic_1)
{
    std::string dcl_pon_line = "pon: 3 ok";
    dcl_pon_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pon_line.begin(), dcl_pon_line.end());

    auto parsed_pon = cgsn::supervisor::parse_mpic_pon_poff(dcl_pon_line,
                                                            cgsn::protobuf::Port::FROM_MPIC);

    BOOST_CHECK_EQUAL(parsed_pon.state(), cgsn::protobuf::Port::ON);
    BOOST_CHECK_EQUAL(parsed_pon.port_id(), 3);
    BOOST_CHECK(!parsed_pon.has_error());

    std::string dcl_pon_serialized = cgsn::supervisor::serialize_mpic_pon_poff(parsed_pon, cgsn::protobuf::Port::FROM_MPIC);
    BOOST_CHECK_EQUAL(dcl_pon_line, dcl_pon_serialized);
}

BOOST_AUTO_TEST_CASE(mpic_poff_from_mpic_1)
{
    std::string dcl_poff_line = "poff: 5 ok";
    dcl_poff_line += " " + cgsn::supervisor::calc_checksum_str(dcl_poff_line.begin(), dcl_poff_line.end());
    auto parsed_poff = cgsn::supervisor::parse_mpic_pon_poff(dcl_poff_line, cgsn::protobuf::Port::FROM_MPIC);

    BOOST_CHECK_EQUAL(parsed_poff.state(), cgsn::protobuf::Port::OFF);
    BOOST_CHECK_EQUAL(parsed_poff.port_id(), 5);
    BOOST_CHECK(!parsed_poff.has_error());

    std::string dcl_poff_serialized = cgsn::supervisor::serialize_mpic_pon_poff(parsed_poff, cgsn::protobuf::Port::FROM_MPIC);
    BOOST_CHECK_EQUAL(dcl_poff_line, dcl_poff_serialized);
}

BOOST_AUTO_TEST_CASE(mpic_poff_from_mpic_invalid_port_error)
{
    std::string dcl_poff_line = "poff: 0 error (invalid port)";
    dcl_poff_line += " " + cgsn::supervisor::calc_checksum_str(dcl_poff_line.begin(), dcl_poff_line.end());
    auto parsed_poff = cgsn::supervisor::parse_mpic_pon_poff(dcl_poff_line, cgsn::protobuf::Port::FROM_MPIC);

    BOOST_CHECK_EQUAL(parsed_poff.state(), cgsn::protobuf::Port::OFF);
    BOOST_CHECK_EQUAL(parsed_poff.port_id(), 0);
    BOOST_CHECK(parsed_poff.has_error());
    BOOST_CHECK_EQUAL(parsed_poff.error(), cgsn::protobuf::PortOnOff::INVALID_PORT);

    std::string dcl_poff_serialized = cgsn::supervisor::serialize_mpic_pon_poff(parsed_poff, cgsn::protobuf::Port::FROM_MPIC);
    BOOST_CHECK_EQUAL(dcl_poff_line, dcl_poff_serialized);
}

BOOST_AUTO_TEST_CASE(mpic_poff_from_mpic_no_response_error)
{
    std::string dcl_poff_line = "poff: 4 error (no response)";
    dcl_poff_line += " " + cgsn::supervisor::calc_checksum_str(dcl_poff_line.begin(), dcl_poff_line.end());
    auto parsed_poff = cgsn::supervisor::parse_mpic_pon_poff(dcl_poff_line, cgsn::protobuf::Port::FROM_MPIC);

    BOOST_CHECK_EQUAL(parsed_poff.state(), cgsn::protobuf::Port::OFF);
    BOOST_CHECK_EQUAL(parsed_poff.port_id(), 4);
    BOOST_CHECK(parsed_poff.has_error());
    BOOST_CHECK_EQUAL(parsed_poff.error(), cgsn::protobuf::PortOnOff::NO_RESPONSE);

    std::string dcl_poff_serialized = cgsn::supervisor::serialize_mpic_pon_poff(parsed_poff, cgsn::protobuf::Port::FROM_MPIC);
    BOOST_CHECK_EQUAL(dcl_poff_line, dcl_poff_serialized);
}


BOOST_AUTO_TEST_CASE(mpic_poff_to_mpic_1)
{
    std::string dcl_poff_line = "poff 4";
    dcl_poff_line += " " + cgsn::supervisor::calc_checksum_str(dcl_poff_line.begin(), dcl_poff_line.end());
    auto parsed_poff = cgsn::supervisor::parse_mpic_pon_poff(dcl_poff_line, cgsn::protobuf::Port::TO_MPIC);

    BOOST_CHECK_EQUAL(parsed_poff.state(), cgsn::protobuf::Port::OFF);
    BOOST_CHECK_EQUAL(parsed_poff.port_id(), 4);
    BOOST_CHECK(!parsed_poff.has_error());

    std::string dcl_poff_serialized = cgsn::supervisor::serialize_mpic_pon_poff(parsed_poff, cgsn::protobuf::Port::TO_MPIC);
    BOOST_CHECK_EQUAL(dcl_poff_line, dcl_poff_serialized);
}




BOOST_AUTO_TEST_CASE(mpic_from_mpic_pcfg_1)
{
    std::string dcl_pcfg_line = "get_pcfg: 3 0 0 0 0 0 55c";

    auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                         cgsn::protobuf::Port::FROM_MPIC,
                                                         cgsn::protobuf::Port::GET);

    decltype(parsed_pcfg) expected_pcfg;
    std::string expected_string = "port_id: 3 "
        "voltage: VOLTAGE_UNKNOWN "
        "current_limit: 0 "
        "protocol: RS232 "
        "soft_start: false "
        "power: POWER_SELECTION_UNKNOWN";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

    BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                        "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

    std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                            cgsn::protobuf::Port::FROM_MPIC, cgsn::protobuf::Port::GET);
    BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);
}

BOOST_AUTO_TEST_CASE(mpic_from_mpic_pcfg_2)
{
    std::string dcl_pcfg_line = "get_pcfg: 5 2 1000 2 1 2";
    dcl_pcfg_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pcfg_line.begin(), dcl_pcfg_line.end());

    auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                         cgsn::protobuf::Port::FROM_MPIC,
                                                         cgsn::protobuf::Port::GET);

    decltype(parsed_pcfg) expected_pcfg;
    std::string expected_string = "port_id: 5 "
        "voltage: VOLTAGE_24V "
        "current_limit: 1000 "
        "protocol: RS485 "
        "soft_start: true "
        "power: HIGH";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

    BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                        "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

    std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                            cgsn::protobuf::Port::FROM_MPIC, cgsn::protobuf::Port::GET);
    BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);

}

BOOST_AUTO_TEST_CASE(mpic_from_mpic_pcfg_error)
{
    {
        std::string dcl_pcfg_line = "get_pcfg: 3 error no data / invalid response";
        dcl_pcfg_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pcfg_line.begin(), dcl_pcfg_line.end());

        auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                             cgsn::protobuf::Port::FROM_MPIC,
                                                             cgsn::protobuf::Port::GET);

        decltype(parsed_pcfg) expected_pcfg;
        std::string expected_string = "port_id: 3 "
            "error: CPIC_NO_DATA_OR_INVALID_RESPONSE";

        google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

        BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                            "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

        std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                                cgsn::protobuf::Port::FROM_MPIC, cgsn::protobuf::Port::GET);
        BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);

    }

    {
        std::string dcl_pcfg_line = "get_pcfg: 0 error invalid port";
        dcl_pcfg_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pcfg_line.begin(), dcl_pcfg_line.end());

        auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                             cgsn::protobuf::Port::FROM_MPIC,
                                                             cgsn::protobuf::Port::GET);
        decltype(parsed_pcfg) expected_pcfg;
        std::string expected_string = "port_id: 0 "
            "error: INVALID_PORT";

        google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

        BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                            "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

        std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                                cgsn::protobuf::Port::FROM_MPIC, cgsn::protobuf::Port::GET);
        BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);

    }


}


BOOST_AUTO_TEST_CASE(mpic_to_mpic_pcfg_1)
{
    std::string dcl_pcfg_line = "set_pcfg 3 0 0 0 0 0";
    dcl_pcfg_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pcfg_line.begin(), dcl_pcfg_line.end());

    auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                         cgsn::protobuf::Port::TO_MPIC,
                                                         cgsn::protobuf::Port::SET);

    decltype(parsed_pcfg) expected_pcfg;
    std::string expected_string = "port_id: 3 "
        "voltage: VOLTAGE_UNKNOWN "
        "current_limit: 0 "
        "protocol: RS232 "
        "soft_start: false "
        "power: POWER_SELECTION_UNKNOWN";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

    BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                        "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

    std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                            cgsn::protobuf::Port::TO_MPIC, cgsn::protobuf::Port::SET);
    BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);

}

BOOST_AUTO_TEST_CASE(mpic_to_mpic_pcfg_2)
{
    std::string dcl_pcfg_line = "get_pcfg 3";
    dcl_pcfg_line += " " + cgsn::supervisor::calc_checksum_str(dcl_pcfg_line.begin(), dcl_pcfg_line.end());

    auto parsed_pcfg = cgsn::supervisor::parse_mpic_pcfg(dcl_pcfg_line,
                                                         cgsn::protobuf::Port::TO_MPIC,
                                                         cgsn::protobuf::Port::GET);

    decltype(parsed_pcfg) expected_pcfg;
    std::string expected_string = "port_id: 3";

    google::protobuf::TextFormat::ParseFromString(expected_string, &expected_pcfg);

    BOOST_CHECK_MESSAGE(expected_pcfg.SerializeAsString() == parsed_pcfg.SerializeAsString(),
                        "Expected: " << expected_pcfg.DebugString() << "Received: " << parsed_pcfg.DebugString());

    std::string dcl_pcfg_serialized = cgsn::supervisor::serialize_mpic_pcfg(parsed_pcfg,
                                                                            cgsn::protobuf::Port::TO_MPIC, cgsn::protobuf::Port::GET);
    BOOST_CHECK_EQUAL(dcl_pcfg_line, dcl_pcfg_serialized);

}

