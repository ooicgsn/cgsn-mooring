// Copyright (C) 2018-2019 Woods Hole Oceanographic Institution
//
// This file is part of the CGSN Mooring Project ("cgsn-mooring").
//
// cgsn-mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// cgsn-mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cgsn-mooring in the COPYING.md file at the project root.
// If not, see <http://www.gnu.org/licenses/>.


#include <goby/middleware/multi-thread-application.h>

#include "cgsn-mooring/serial/ascii_line_based_serial.h"
#include "cgsn-mooring/messages/sensor_raw.pb.h"
#include "cgsn-mooring/messages/mpic.pb.h"
#include "cgsn-mooring/config/supervisor_config.pb.h"
#include "cgsn-mooring/supervisor/serialize.h"
#include "cgsn-mooring/supervisor/parse.h"


using goby::glog;

namespace cgsn
{
    class MPIC_Simulator : public goby::MultiThreadStandaloneApplication<protobuf::MPICSimulatorConfig>
    {
    public:
        MPIC_Simulator();

    private:

    };

    class MPIC_StatusThread : public goby::SimpleThread<protobuf::MPICSimulatorConfig>
    {
    public:
        MPIC_StatusThread(const protobuf::MPICSimulatorConfig& cfg);

    private:
        void loop() override;
        void handle_pon_poff_message(const cgsn::protobuf::SensorRaw& raw_data);
        void handle_pcfg_message(const cgsn::protobuf::SensorRaw& raw_data, cgsn::protobuf::Port::GetOrSet getorset);

        /// \brief Initialize the status message, e.g. "dcl: "
        cgsn::protobuf::MPICStatus baseline_status();

    private:
        cgsn::protobuf::MPICStatus status_message_;
        // map port ID to pcfg
        std::map<int, cgsn::protobuf::PowerConfig> pcfgs_;
        enum { MIN_PORT = 1, MAX_PORT = 8 };


    };
}

cgsn::MPIC_Simulator::MPIC_Simulator()
{
    launch_thread<cgsn::MPIC_StatusThread>(cfg());
    launch_thread<cgsn::io::AsciiLineSerialThread<cgsn::groups::sim::raw_in,
                                                  cgsn::groups::sim::raw_out>>(cfg().supervisor_serial());
}

cgsn::MPIC_StatusThread::MPIC_StatusThread(const protobuf::MPICSimulatorConfig& cfg)
    : goby::SimpleThread<protobuf::MPICSimulatorConfig>(cfg, cfg.report_freq_with_units()),
    status_message_(baseline_status())
{
    for(int i = MIN_PORT; i <= MAX_PORT; ++i)
    {
        cgsn::protobuf::PowerConfig default_pcfg;
        default_pcfg.set_port_id(i);
        default_pcfg.set_voltage(cgsn::protobuf::PowerConfig::VOLTAGE_UNKNOWN);
        default_pcfg.set_power(cgsn::protobuf::PowerConfig::POWER_SELECTION_UNKNOWN);

        pcfgs_.insert(std::make_pair(i, default_pcfg));
    }


    interthread().subscribe<cgsn::groups::sim::raw_in, cgsn::protobuf::SensorRaw>(
        [this](const cgsn::protobuf::SensorRaw& raw)
        {
            const std::string& msg = raw.raw_data();
            std::string identifier = msg.substr(0, msg.find(" "));
            boost::trim_if(identifier, boost::is_any_of(":"));
            if(identifier == "poff" || identifier == "pon")
                handle_pon_poff_message(raw);
            else if(identifier == "set_pcfg")
                handle_pcfg_message(raw, cgsn::protobuf::Port::SET);
            else if(identifier == "get_pcfg")
                handle_pcfg_message(raw, cgsn::protobuf::Port::GET);
            // ignore any unrecognized message
        }
        );
}


void cgsn::MPIC_StatusThread::loop()
{
    switch(cfg().type())
    {
        case cgsn::protobuf::MPICSimulatorConfig::CPM:
            // TODO: Implement
            break;
        case cgsn::protobuf::MPICSimulatorConfig::DCL:
        {
            auto raw_out = make_with_header<cgsn::protobuf::SensorRaw>();
            raw_out.set_raw_data(cgsn::supervisor::serialize_mpic_dcl_status(status_message_) + "\r\n");
            interthread().publish<cgsn::groups::sim::raw_out>(raw_out);
            break;
        }
        case cgsn::protobuf::MPICSimulatorConfig::STC:
            // TODO: Implement
            break;
    }
}

void cgsn::MPIC_StatusThread::handle_pon_poff_message(const cgsn::protobuf::SensorRaw& raw_data)
{
    auto parsed_pon_poff = cgsn::supervisor::parse_mpic_pon_poff(raw_data.raw_data(),
                                                                 cgsn::protobuf::Port::TO_MPIC);

    auto pon_poff_response = parsed_pon_poff;
    if(parsed_pon_poff.has_error())
    {
        glog.is_warn() && glog << "Invalid port command: " << raw_data.raw_data() << std::endl;
        return;
    }

    if(parsed_pon_poff.port_id() < MIN_PORT || parsed_pon_poff.port_id() > MAX_PORT)
        pon_poff_response.set_error(cgsn::protobuf::PortOnOff::INVALID_PORT);
    else
        status_message_.mutable_cpic(parsed_pon_poff.port_id()-1)->set_state(parsed_pon_poff.state());

    auto raw_out = make_with_header<cgsn::protobuf::SensorRaw>();
    raw_out.set_raw_data(supervisor::serialize_mpic_pon_poff(pon_poff_response, cgsn::protobuf::Port::FROM_MPIC) + "\r\n");
    interthread().publish<cgsn::groups::sim::raw_out>(raw_out);
}

void cgsn::MPIC_StatusThread::handle_pcfg_message(const cgsn::protobuf::SensorRaw& raw_data,
                                                  cgsn::protobuf::Port::GetOrSet getorset)
{
    try
    {

        auto pcfg = cgsn::supervisor::parse_mpic_pcfg(raw_data.raw_data(),
                                                      cgsn::protobuf::Port::TO_MPIC,
                                                      getorset);

        decltype(pcfg) pcfg_response;
        if(getorset == cgsn::protobuf::Port::SET)
            pcfg_response = pcfg;
        else
            pcfg_response = pcfgs_[pcfg.port_id()];


        if(pcfg.port_id() < MIN_PORT || pcfg.port_id() > MAX_PORT)
        {
            pcfg_response.set_error(cgsn::protobuf::PowerConfig::INVALID_PORT);
        }
        else
        {
            switch(getorset)
            {
                case cgsn::protobuf::Port::SET:
                    // XXX mimic quirk where the current limit reported back is slightly off (not sure why?!)
                    pcfg_response.set_current_limit_with_units(pcfg.current_limit_with_units() + cfg().current_limit_quirk_offset_with_units());
                    pcfgs_[pcfg.port_id()] = pcfg_response;
                    break;

                case cgsn::protobuf::Port::GET:
                    break;
            }
        }

        auto raw_out = make_with_header<cgsn::protobuf::SensorRaw>();
        raw_out.set_raw_data(supervisor::serialize_mpic_pcfg(pcfg_response,
                                                             cgsn::protobuf::Port::FROM_MPIC,
                                                             cgsn::protobuf::Port::GET) + "\r\n");
        interthread().publish<cgsn::groups::sim::raw_out>(raw_out);
    }
    catch(cgsn::supervisor::Exception& e)
    {
        glog.is_warn() && glog << "Invalid message: " << raw_data.raw_data() << std::endl;
    }

}


cgsn::protobuf::MPICStatus cgsn::MPIC_StatusThread::baseline_status()
{
    cgsn::protobuf::MPICStatus pb_status;
    std::string status =
        "main_voltage: 24.0 main_current: 20.0 "
        "temperature { bmp085: 0.0 sht: 0 lp12v: 0 lp24v: 0 hp12v: 0 } "
        "sht_humidity: 50.0 pmb085_pressure: 236490 "
        "ground_leak_data_enabled: false ground_leak_data_max: 0 ground_leak_data_max: 0 "
        "ground_leak_data_max: 0 "
        "leak_detect { enabled: false voltage: 0 } leak_detect { enabled: false voltage: 0 } "
        "cpic { port: 1 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 2 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 3 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 4 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 5 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 6 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 7 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "cpic { port: 8 state: OFF voltage: 0 current: 0 ierr: 0 } "
        "dpb { pwr_sel: 0 mode: 0 state: 0 vmain: 0 imain: 0 v12: 0 i12: 0 v24: 0 i24: 0 }";

    google::protobuf::TextFormat::ParseFromString(status, &pb_status);
    return pb_status;
}


int main(int argc, char* argv[])
{
    return goby::run<cgsn::MPIC_Simulator>(argc, argv);
}

