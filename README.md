#CGSN Mooring Software

This codebase `cgsn-mooring` is for the software running on the OOI-CGSN moorings since version 3.0.0.

For full complete documentation, see:

[https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_00-main.md](https://bitbucket.org/ooicgsn/cgsn-mooring/src/master/src/doc/markdown/doc_00-main.md)
