#!/bin/bash

# intentionally allow these to be unset before "set -u"
[ -z "${CGSN_MOORING_CMAKE_FLAGS}" ] && CGSN_MOORING_CMAKE_FLAGS=
[ -z "${CGSN_MOORING_MAKE_FLAGS}" ] && CGSN_MOORING_MAKE_FLAGS=
[ -z "${CGSN_MOORING_TARGET}" ] && CGSN_MOORING_TARGET=$(dpkg-architecture -q DEB_HOST_GNU_TYPE)

set -e -u

########################################
# Out-of-source build
########################################
mkdir -p build
cd build

########################################
# Set compilers
########################################
CC=/usr/bin/clang
CXX=/usr/bin/clang++

########################################
# Set build flags the same way dpkg-buildpackage / CDBS does
########################################

dpkg-buildflags --export=sh > dpkg-buildflags.sh
source dpkg-buildflags.sh

# Set local builds to use -O0 to speed up builds
CGSN_MOORING_CFLAGS_OVERRIDE="-O0"
CGSN_MOORING_CXXFLAGS_OVERRIDE="-O0"

# from CDBS /usr/share/cdbs/1/class/cmake.mk
DEB_CMAKE_CFLAGS="${CFLAGS} ${CPPFLAGS}"
DEB_CMAKE_CXXFLAGS="${CXXFLAGS} ${CPPFLAGS}"
DEB_CMAKE_LDFLAGS="${LDFLAGS}"
DEB_CMAKE_INSTALL_PREFIX=/usr/local
DEB_CMAKE_NORMAL_ARGS=()
DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_INSTALL_PREFIX="${DEB_CMAKE_INSTALL_PREFIX}")
DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_C_COMPILER:FILEPATH="${CC}" -DCMAKE_CXX_COMPILER:FILEPATH="${CXX}")
DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_C_FLAGS="${DEB_CMAKE_CFLAGS} ${CGSN_MOORING_CFLAGS_OVERRIDE}" -DCMAKE_CXX_FLAGS="${DEB_CMAKE_CXXFLAGS} ${CGSN_MOORING_CXXFLAGS_OVERRIDE}")
## No need to skip rpath for local builds - this lets us run from build/bin
#DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_SKIP_RPATH=ON)
## Verbose makefile is unnecessary for normal use
#DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_VERBOSE_MAKEFILE=ON)
DEB_CMAKE_NORMAL_ARGS+=(-DCMAKE_EXE_LINKER_FLAGS="${DEB_CMAKE_LDFLAGS}" -DCMAKE_MODULE_LINKER_FLAGS="${DEB_CMAKE_LDFLAGS}" -DCMAKE_SHARED_LINKER_FLAGS="${DEB_CMAKE_LDFLAGS}")

########################################
# Configure
########################################
echo "Configuring..."
(set -x; cmake .. "${DEB_CMAKE_NORMAL_ARGS[@]}" ${CGSN_MOORING_CMAKE_FLAGS} -DCGSN_MOORING_TARGET="${CGSN_MOORING_TARGET}")

########################################
# Build
########################################
echo "Building..."
(set -x; cmake --build . -- ${CGSN_MOORING_MAKE_FLAGS} "$@")
