#!/bin/bash

set -u -e

if [ "$#" -lt 1 ]; then
    echo "Usage: generate_driver_from_pattern.sh 'sensortype_sensorclass' (e.g. 'ctd_sbe' or 'adcp_rdi')"
    exit 1
fi

pattern=$1

type=$(echo $pattern | cut -s -d "_" -f1)
class=$(echo $pattern | cut -s -d "_" -f2)

if [ -z $type ]; then
    echo "Type string invalid: make sure you use 'sensortype_sensorclass' (e.g. 'ctd_sbe' or 'adcp_rdi')"
    exit 1
fi

if [ -z $class ]; then
    echo "Class string invalid: make sure you use 'sensortype_sensorclass' (e.g. 'ctd_sbe' or 'adcp_rdi')"
    exit 1
fi

scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
src_dir=$(realpath ${scripts_dir}/../src)

# create directories
for dir in `find $src_dir -type d -name "driver_pattern"`;
do
   new_dir=$(echo $dir | sed "s/driver_pattern/${type}_${class}/")
   echo "Generated directory: $new_dir"
   mkdir -p $new_dir

   if [ -e $dir/CMakeLists.txt ]; then
       echo "Generated CMake: $new_dir/CMakeLists.txt"
       cp $dir/CMakeLists.txt $new_dir/CMakeLists.txt
       sed -i "s/driver_pattern/${type}_${class}/g" $new_dir/CMakeLists.txt
   fi

   parent_cmake=$(realpath $dir/../CMakeLists.txt) 
   if [ -e $parent_cmake ]; then      
       echo "Updating CMake: $parent_cmake"
       # don't add if it's already here
       grep -q "${type}_${class}" $parent_cmake && echo "Skipping, entry exists" || echo "add_subdirectory(${type}_${class})" >> $parent_cmake 
   fi       
done

# create code
for file in `find ${src_dir} -type f -regex ".*/.*driver[_-]pattern.*\.\(h\|cpp\|proto\|md\)$"`
do
    new_file=$(echo $file | sed "s/driver\([_-]\)pattern/${type}\1${class}/g")
    echo "Generated file: $new_file"
    cp $file $new_file
    sed -i "s/driver_pattern/${type}_${class}/g" $new_file
    sed -i "s/DRIVER_PATTERN/${type^^}_${class^^}/g" $new_file
    sed -i "s/DriverPattern/${type^^}_${class^^}_/g" $new_file
    sed -i "s/Driver Pattern/${type^^} ${class^^}/g" $new_file
done

# update sensor types proto
sensor_types_proto=$src_dir/lib/messages/sensor_types.proto
previous_type_i=$(egrep '.* = [0-9]' $sensor_types_proto | sed 's/.*= \([0-9\-]*\).*/\1/' | tail -n1)
next_type_i=$(($previous_type_i + 1))

echo "Updating $sensor_types_proto"
grep -q "${type^^}_${class^^}" $sensor_types_proto && echo "Skipping, entry exists" || \
        (
            sed -n "/DRIVER_PATTERN = -1/,/ *; *$/ { s/DRIVER_PATTERN/${type^^}_${class^^}/;s/driver_pattern/${type}_${class}/;s/DriverPattern/${type^^}_${class^^}_/;s/-1/$next_type_i/;p }" $sensor_types_proto > /tmp/generate_sensor_types.txt

            # insert before final }
            sed -i "s/^} *$//"  $sensor_types_proto
            cat /tmp/generate_sensor_types.txt >>  $sensor_types_proto
            echo "}" >>  $sensor_types_proto
        )

# update dependency for Doxygen
doxygen_cmake=$src_dir/doc/doxygen/CMakeLists.txt

echo "Updating $doxygen_cmake"
grep -q "${type}_${class}" $doxygen_cmake && echo "Skipping, entry exists" || sed -i "/cgsn_driver_pattern_driver/a\  cgsn_${type}_${class}\n  cgsn_${type}_${class}_driver" $doxygen_cmake


# update lib/messages/CMakeLists.txt
messages_cmake=$src_dir/lib/messages/CMakeLists.txt
echo "Updating $messages_cmake"
grep -q "${type}_${class}" $messages_cmake && echo "Skipping, entry exists" || sed -i "/driver_pattern.proto/a\  cgsn-mooring/messages/${type}_${class}.proto" $messages_cmake

# update lib/config/CMakeLists.txt
config_cmake=$src_dir/lib/config/CMakeLists.txt
echo "Updating $config_cmake"
grep -q "${type}_${class}" $config_cmake && echo "Skipping, entry exists" || sed -i "/driver_pattern_config.proto/a\  cgsn-mooring/config/${type}_${class}_config.proto" $config_cmake

# update groups.h
groups_h=$src_dir/lib/messages/groups.h
echo "Updating $groups_h"

grep -q "${type}_${class}" $groups_h && echo "Skipping, entry exists" || \
        (
            echo "" > /tmp/generate_driver_groups.txt
            # copy the driver_pattern section
            sed -n "/namespace driver_pattern/,/ *} *$/ { s/driver_pattern/${type}_${class}/;p }" $groups_h >> /tmp/generate_driver_groups.txt   
            # append it after the driver_pattern section
            sed -i "/namespace driver_pattern/,/ *} *$/ {  
   / *} *$/r /tmp/generate_driver_groups.txt
}" $groups_h
        )

# add entry to main doc

main_md=$src_dir/doc/markdown/doc_00-main.md
echo "Updated $main_md"

grep -q "${type}_${class}" $main_md && echo "Skipping, entry exists" || \
        sed -i "/\[driver-pattern\]/a\ * [${type}-${class}](doc_21-${type}-${class}.md): Driver for the ${type}-${class}." $main_md
