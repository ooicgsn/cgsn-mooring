#!/bin/bash

scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
src_dir="${scripts_dir}/../src"

if ! git diff --no-patch --cached --exit-code; then
    echo "There are staged changes, please commit or unstage them first." >&1
    exit 1
fi

git ls-files "$src_dir" | egrep '^.*[.](h|hpp|c|cpp|sh|proto|service)$' | while read -r file; do
    sed -i 's/\s*$//g' "$file"
    git add "$file"
done

git diff --name-only --cached
