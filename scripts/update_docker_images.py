#!/usr/bin/env python
# Copyright (C) 2019 Woods Hole Oceanographic Institution
#
# This file is part of the CGSN Mooring Project ("cgsn-mooring").
#
# cgsn-mooring is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# cgsn-mooring is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cgsn-mooring in the COPYING.md file at the project root.
# If not, see <http://www.gnu.org/licenses/>.
'''
This tool rebuilds all of the Docker images referenced by CircleCI. As part of
the build process, all apt packages are upgraded to their latest versions.
The minor version of each image is incremented and the new version is pushed to
the Docker Hub.
'''

from __future__ import print_function

import argparse
import os
import pipes
import subprocess
import tempfile
import traceback
import sys

try:
  import yaml
except ImportError:
  print('Error: Please install PyYAML', file=sys.stderr)
  print('    pip install pyyaml', file=sys.stderr)
  print('    apt-get install python-yaml', file=sys.stderr)
  raise


# Parse command-line arguments
parser = argparse.ArgumentParser(description=__doc__,
  formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--dry-run', action='store_true',
  help='display commands to be run without executing them')
parser.add_argument('--rebuild', action='store_true',
  help='rebuild images without bumping version numbers')
args = parser.parse_args()


# Path calculations
repo_path = subprocess.check_output(['git', 'rev-parse', '--show-toplevel'],
  cwd=os.path.dirname(__file__)).rstrip()
circleci_path = os.path.join(repo_path, '.circleci')
config_path = os.path.join(circleci_path, 'config.yml')


# Make sure prerequisites are installed
try:
  subprocess.check_call(['circleci', 'version'], stdout=open(os.devnull, 'wb'))
except:
  print('Error: Please install the CircleCI command line tool', file=sys.stderr)
  print('    https://circleci.com/docs/2.0/local-cli', file=sys.stderr)
  sys.exit(1)

try:
  subprocess.check_call(['docker', 'version'], stdout=open(os.devnull, 'wb'))
except:
  print('Error: Please install Docker', file=sys.stderr)
  print('    https://docs.docker.com/install/', file=sys.stderr)
  sys.exit(1)


def format_command(command):
  return ' '.join(pipes.quote(x) for x in command)


def get_images_from_config():
  '''
  Returns the set of container images used by jobs in the CircleCI config
  '''
  # Read the config
  config_yaml = subprocess.check_output(['circleci', 'config', 'process',
    '--skip-update-check', config_path])
  config = yaml.load(config_yaml)
  
  # Get the set of container images referenced
  images = set()
  for job in config['jobs'].values():
    if 'docker' not in job: continue
    image = next(x['image'] for x in job['docker']
      if isinstance(x, dict) and 'image' in x)
    images.add(image)
  return images


def build_image(image, tag):
  '''
  Builds the container image and assigns it the given tag
  '''
  command = [
    'docker', 'build',
    '--tag', image + ':' + tag,
    image_map[image]
  ]
  if args.dry_run:
    print(format_command(command))
  else:
    subprocess.check_call(command)


def push_image(image, tag):
  '''
  Pushes the container image to the Docker hub
  '''
  command = [
    'docker', 'push',
    image + ':' + tag
  ]
  if args.dry_run:
    print(format_command(command))
  else:
    subprocess.check_call(command)


def update_references(path, image, old_tag, new_tag):
  '''
  Replaces all references to image:old_tag in the referenced file
  '''
  with open(path, 'r') as f:
    config = f.read()
  config = config.replace(image + ':' + old_tag, image + ':' + new_tag)
  if args.dry_run:
    with tempfile.NamedTemporaryFile() as f:
      f.write(config)
      f.flush()
      subprocess.call(['diff', '-u', path, f.name])
  else:
    with open(path, 'w') as f:
      f.write(config)


def parse_version(tag):
  '''
  "2.3.1" -> (2, 3, 1)
  '''
  return tuple(int(x) for x in tag.split('.'))


def split_image_tag(image):
  '''
  "ooicgsn/cgsn-mooring-build-base:2.0.1" ->
  ("ooicgsn/cgsn-mooring-build-base", "2.0.1")
  '''
  a, _, b = image.partition(':')
  return (a, b)


def increment_minor(version):
  if len(version) >= 3:
    return version[:2] + (version[2] + 1,)
  elif len(version) == 2:
    return version + (1,)
  elif len(version) == 1:
    return version + (0, 1)
  else:
    raise Exception('Unexpected version number:', version)


# Figure out the latest versions of the images
images = get_images_from_config()
latest_versions = {}
for image in images:
  name, tag = split_image_tag(image)
  if name not in latest_versions:
    latest_versions[name] = tag
  elif parse_version(latest_versions[name]) < parse_version(tag):
    latest_versions[name] = tag


# Map of images identifiers to directories where the Dockerfiles are
image_map = {
  'ooicgsn/cgsn-mooring-build-base':
    os.path.join(circleci_path, 'base-docker-bionic'),
  'ooicgsn/cgsn-mooring-build-armhf':
    os.path.join(circleci_path, 'armhf-docker-bionic'),
  'ooicgsn/cgsn-mooring-build-arm64':
    os.path.join(circleci_path, 'arm64-docker-bionic'),
}


# Update the base image first
base_image = 'ooicgsn/cgsn-mooring-build-base'
old_tag = latest_versions[base_image]
new_version = parse_version(old_tag)
if not args.rebuild:
  new_version = increment_minor(new_version)
new_tag = '.'.join(str(v) for v in new_version)
build_image(base_image, new_tag)
push_image(base_image, new_tag)
update_references(config_path, base_image, tag, new_tag)


# Update all the other Dockerfiles to reference the new base image
for name, dir in image_map.items():
  if name == base_image:
    continue
  df = os.path.join(dir, 'Dockerfile')
  update_references(df, base_image, tag, new_tag)


# Process every other
for image, tag in latest_versions.items():
  if image == base_image:
    continue

  if image not in image_map:
    print('Warning: Dockerfile for image', image, 'is not known, skipping it',
      file=sys.stderr)
    continue

  new_version = parse_version(tag)
  if not args.rebuild:
    new_version = increment_minor(new_version)
  new_tag = '.'.join(str(v) for v in new_version)

  try:
    build_image(image, new_tag)
    push_image(image, new_tag)
    update_references(config_path, image, tag, new_tag)
  except Exception, e:
    print('Warning: Caught an exception trying to update', image,
      file=sys.stderr)
    traceback.print_exc(file=sys.stderr)
    continue
